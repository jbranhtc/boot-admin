package com.htc.ea.mcs.consumer;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

import com.htc.ea.mcs.consumer.util.AppUtil;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.TransactionIdUtil;

public class RestTemplateResponseErrorHandler extends DefaultResponseErrorHandler implements ResponseErrorHandler {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RestTemplateResponseErrorHandler.class);
	
	private final Class<?> clazz = this.getClass();
	private AppUtil appUtil;
	private Map<String, String> parameters;
	
	public RestTemplateResponseErrorHandler(AppUtil appUtil, Map<String, String> parameters) {
		this.appUtil = appUtil;
		this.parameters = parameters;
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		printError(response, "Response error", false);
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return RestUtil.isError(response.getStatusCode());
	}
	
	private void printError(ClientHttpResponse response, String responseType, boolean outbound) throws IOException {
		if (appUtil.logging(this.parameters.get(com.htc.ea.mcs.consumer.Constants.CATEGORY), EventLevel.DEBUG.name())) {
			Map<String, String> headersMap = response.getHeaders().toSingleValueMap()!=null ? response.getHeaders().toSingleValueMap() : new HashMap<>();			
			String serviceCode = "-1", serviceDescrip = "Unknown error"; 
			if (headersMap!=null) {
				serviceCode = headersMap.get(com.htc.ea.util.util.Constants.SERVICE_CODE);
				serviceDescrip = headersMap.get(com.htc.ea.util.util.Constants.SERVICE_DESCRIPTION);
			}
			appUtil.settingUntilLastSequence(headersMap, outbound);
			String headersStr = Arrays.toString(headersMap.entrySet().toArray());
			String detail = String.format("%s - %s => [%s - %s] => headersResponse => %s ", response.getStatusCode(), response.getStatusText(), serviceCode, serviceDescrip, headersStr);
			String changeSequence = TransactionIdUtil.nextSequenceId().toString();
			appUtil.debug(null, clazz, Thread.currentThread().getStackTrace()[2].getMethodName(), responseType, detail, TransactionIdUtil.getMsisdn(), null, this.parameters.get(com.htc.ea.mcs.consumer.Constants.CATEGORY), headersMap.get(Constants.CONSUMER_LOCATION), headersMap, false, changeSequence);
		}
	}
}