package com.htc.ea.mcs.consumer;

import java.util.HashMap;
import java.util.Map;

public class ServiceException extends Exception {

	private static final long serialVersionUID = 9145787592538268510L;
	private static final long DEFAULT_CODE = 1;
	
	private ServiceResponse<?> response;
	private Map<String, String> attributes;
	private long code = DEFAULT_CODE;
	private String description;
	
	public ServiceException(Exception ex) {
		super(ex);
		description = ex.getMessage();
	}
	
	public ServiceException(long code, String description, Map<String, String> attributes, Exception ex) {
		super(ex);
		this.code = code;
		this.description = description;
		getAttributes().putAll(attributes);
	}

	public <T> ServiceException(ServiceResponse<T> response) {
		this.response = response;
		if (response.serviceCode > 0) {
			code = response.getServiceCode();
			description = response.getServiceDescription();
		}
	}	

	@Override
	public String getMessage() {
		return toString();
	}	

	@SuppressWarnings("unchecked")
	public <T> ServiceResponse<T> getResponse() {
		return (ServiceResponse<T>) response;
	}

	public Map<String, String> getAttributes() {
		if (attributes==null) {
			attributes = new HashMap<>();
		}
		return attributes;
	}

	public long getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceException [response=").append(response).append(", attributes=").append(attributes)
				.append(", code=").append(code).append(", description=").append(description).append("]");
		return builder.toString();
	}	
}