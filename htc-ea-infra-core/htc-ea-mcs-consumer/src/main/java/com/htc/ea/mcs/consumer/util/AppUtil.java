package com.htc.ea.mcs.consumer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.ea.mcs.consumer.Constants;
import com.htc.ea.mcs.consumer.LoggerServiceConsumerRest;
import com.htc.ea.mcs.consumer.ServiceRequest;
import com.htc.ea.mcs.consumer.ServiceResponse;
import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.log.EventData;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.log.LoggingServiceFacade;
import com.htc.ea.util.util.TransactionIdUtil;

@Component
public class AppUtil {

	@Autowired
	private LoggingServiceFacade logger;
	@Autowired
	private EventCategoryService eventCategoryService;
	@Autowired
	private Environment env;
	public final static String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private static String[] formatDate = { "yyyy-MM-dd'T'HH:mm:ss", // 0
			"dd/MM/yyyy", // 1
			"dd-MM-yyyy", // 2
			"yyyyMMddHHmmss", // 3
			"yyyy-MM-dd'T'HH:mm:ss.SSS", // 4
			"yyyyMMddHHmmss", // 5
			"MM/dd/yyyy", // 6
			"yyyy-MM-dd'T'HH:mm:ss'Z'", // 7
			"yyyy-MM-dd", // 8
			"dd/MM/yyyy HH:mm:ss", // 9
			"MM/dd/yyyy HH:mm:ss", // 10
			"yyyy-MM-dd'T'HH:mm:ss", // 11
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", // 12
			"yyyy-MM-dd HH:mm:ss", // 13
			"yy/MM/dd", // 14
			"yyyyMMdd HH:mm:ss", // 15
			"yyyy-MM-dd'T'HH:mm:ss.SSSX", // 16
			"yyyyMMdd", // 17
			"yyyy-MM-dd'T'HH:mm:ss.SSS-HH:mm"// 18
	};

	public <T> void exceptionLoggerError(ServiceRequest<?> request, Throwable ex, String sequence,
			Map<String, Object> attributesRest) {
		if (logging(request.getCategory(), EventLevel.ERROR.name())) {
			logger.log(EventData.builder().category(request.getCategory()).level(EventLevel.ERROR.name())
					.endUserLocation(request.getConsumerLocation()).endUser(request.getEndUser())
					.source(LoggerServiceConsumerRest.class).name("execute")
					.message("Exception for " + request.getCategory() + ": " + attributesRest.get(Constants.RESOURCE))
					.detail(null).originReferenceId(request.getOperationRefId()).serverLocation(getIp())
					.addAttributes(attributesRest).automatic(false).successful(false).exception(ex)
					.sequence((sequence != null ? sequence : null)).build());
		}
	}

	public <T> void requestLoggerInfo(ServiceRequest<?> request, String sequence, Map<String, Object> attributesRest) {
		if (logging(request.getCategory(), EventLevel.INFO.name())) {
			logger.log(EventData.builder().category(request.getCategory()).level(EventLevel.INFO.name())
					.endUserLocation(request.getConsumerLocation()).endUser(request.getEndUser())
					.source(LoggerServiceConsumerRest.class).name("request")
					.message("Request for " + request.getCategory() + ": " + attributesRest.get(Constants.RESOURCE)
							+ " => HttpMethod: " + attributesRest.get(Constants.HTTP_METHOD).toString())
					.detail(request.getData() != null ? request.getData().toString() : null)
					.originReferenceId(request.getOperationRefId()).serverLocation(getIp())
					.addAttributes(attributesRest).automatic(false).successful(true)
					.sequence((sequence != null ? sequence : null)).build());
		}
	}

	public <T> void responseLoggerInfo(ServiceRequest<?> request, ServiceResponse<T> response, String sequence,
			Map<String, Object> attributesRest, long duration) {
		if (logging(request.getCategory(), EventLevel.INFO.name())) {
			logger.log(EventData.builder().category(request.getCategory()).level(EventLevel.INFO.name())
					.endUserLocation(request.getConsumerLocation()).endUser(request.getEndUser())
					.source(LoggerServiceConsumerRest.class).name("response")
					.message("Response for " + request.getCategory() + ": " + attributesRest.get(Constants.RESOURCE)
							+ " => HttpCode: " + response.getHttpCode())
					.detail((response.getData() != null ? response.getData().toString() : null))
					.responseCode(response.getServiceCode().toString()).originReferenceId(request.getOperationRefId())
					.serverLocation(getIp()).addAttributes(attributesRest).automatic(false)
					.successful(response.getServiceCode() == 0).sequence((sequence != null ? sequence : null))
					.duration(duration).build());
		}
	}

	public void info(Object data, Class<?> clazz, String serviceName, String msg, String detail, String msisdn,
			Long duration, String category, String endUserLocation) {
		if (logging(category, EventLevel.INFO.name())) {
			endUserLocation = endUserLocation == null ? getIp() : endUserLocation;
			String dataType = data != null ? "json" : null;
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			logger.log(EventData.builder().category(category).level(EventLevel.INFO.name())
					.endUserLocation(endUserLocation).endUser(msisdn).source(clazz).name(serviceName).message(msg)
					.detail(detail).data(detail).originReferenceId(refId).referenceId(refId).duration(duration)
					.automatic(false).successful(true).serverLocation(getIp()).sequence(sequence).data(data)
					.dataType(dataType).build());
		}
	}

	public void debug(Object data, Class<?> clazz, String serviceName, String msg, String detail, String msisdn,
			Long duration, String category, String endUserLocation, Map<String, String> headersMap, boolean outbound,
			String changeSequence) {
		if (logging(category, EventLevel.DEBUG.name())) {
			String sequence = getSequence(headersMap, outbound, changeSequence);
			endUserLocation = endUserLocation == null ? getIp() : endUserLocation;
			String refId = TransactionIdUtil.getId();
			logger.log(EventData.builder().category(category).level(EventLevel.DEBUG.name())
					.endUserLocation(endUserLocation).endUser(msisdn).source(clazz).name(serviceName).message(msg)
					.detail(detail.toString()).data(detail).originReferenceId(refId).referenceId(refId)
					.duration(duration).automatic(false).successful(true).serverLocation(getIp()).sequence(sequence)
					.data(data).build());
		}
	}

	public void error(Object data, Class<?> clazz, String serviceName, String msg, String detail, String msisdn,
			Long duration, String responseCode, Throwable exception, String category, String endUserLocation) {
		if (logging(category, EventLevel.ERROR.name())) {
			endUserLocation = endUserLocation == null ? getIp() : endUserLocation;
			String refId = TransactionIdUtil.getId();
			String sequence = TransactionIdUtil.nextSequenceId();
			logger.log(EventData.builder().category(category).level(EventLevel.ERROR.name())
					.endUserLocation(endUserLocation).endUser(msisdn).source(clazz).name(serviceName).message(msg)
					.detail(detail).data(detail).responseCode(responseCode).originReferenceId(refId).referenceId(refId)
					.duration(duration).automatic(false).successful(true).serverLocation(getIp()).exception(exception)
					.sequence(sequence).build());
		}
	}

	public boolean logging(String category, String level) {
		Boolean logging = Boolean.parseBoolean(env.getProperty(com.htc.ea.util.util.Constants.LOGGING_ENABLED));
		boolean result = false;
		if (logging)
			result = eventCategoryService.verifyEventCategory(category, level);
		return result;
	}

	public String getProperty(String key) {
		return env.getProperty(key);
	}

	public String getIp() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return "UnknownHost";
		}
	}

	public void settingUntilLastSequence(Map<String, String> headersMap, boolean outbound) {
		if (headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE) != null && outbound) {
			Integer seq = Integer
					.parseInt(headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE).toString());
			TransactionIdUtil.setSequence(seq);
			headersMap.put(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE,
					TransactionIdUtil.nextSequenceId().toString());
			headersMap.putAll(headersMap);
		}
	}

	public String getSequence(Map<String, String> headersMap, boolean outbound, String changeSequence) {
		String untilLastSequenceReceived = "";
		String sequence = "";
		if (changeSequence != null)
			sequence = changeSequence;
		else {
			if (headersMap != null && outbound) {
				untilLastSequenceReceived = headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE);
				sequence = untilLastSequenceReceived;
			}
			if (headersMap != null && !outbound && untilLastSequenceReceived.isEmpty()) {
				untilLastSequenceReceived = headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE);
				if (untilLastSequenceReceived != null) {
					Integer seq = Integer.parseInt(untilLastSequenceReceived);
					TransactionIdUtil.setSequence(seq);
					sequence = TransactionIdUtil.nextSequenceId();
				} else {
					sequence = TransactionIdUtil.nextSequenceId().toString();
				}
			}
		}
		return sequence;
	}

	@SuppressWarnings({ "unchecked" })
	public static <T> T castingData(Object object, Class<T> type, Boolean defaultResponse, Integer formatDate) {
		String emptyString = new String();
		Integer emptyInteger = new Integer(0);
		Double emptyDouble = new Double(0d);
		Long emptyLong = new Long(0L);
		Boolean emptyBoolean = new Boolean(false);
		Date emptyDate = new Date();
		BigInteger emptyBigInteger = new BigInteger("0");
		BigDecimal emptyBigDecimal = new BigDecimal(0d);
		Timestamp emptyTimestamp = new Timestamp(new Date().getTime());
		Map<String, Object> emptyMap = new HashMap<String, Object>();
		LinkedHashMap<String, Object> emptyLinkedHashMap = new LinkedHashMap<String, Object>();
		List<Object> emptyListObject = new ArrayList<Object>();
		GenericDto emptyGenericDto = new GenericDto();
		T result = null;
		String typeClass = type.getSimpleName();
		String objectStr = null;
		try {
			if (object != null)
				objectStr = String.valueOf(object.toString());
			else {
				if (defaultResponse == Boolean.TRUE) {
					if (typeClass.equalsIgnoreCase("String")) {
						result = (T) emptyString;
					} else if (typeClass.equalsIgnoreCase("Integer")) {
						result = (T) emptyInteger;
					} else if (typeClass.equalsIgnoreCase("Double")) {
						result = (T) emptyDouble;
					} else if (typeClass.equalsIgnoreCase("Long")) {
						result = (T) emptyLong;
					} else if (typeClass.equalsIgnoreCase("Boolean")) {
						result = (T) emptyBoolean;
					} else if (typeClass.equalsIgnoreCase("Date")) {
						result = (T) emptyDate;
					} else if (typeClass.equalsIgnoreCase("BigInteger")) {
						result = (T) emptyBigInteger;
					} else if (typeClass.equalsIgnoreCase("BigDecimal")) {
						result = (T) emptyBigDecimal;
					} else if (typeClass.equalsIgnoreCase("Timestamp")) {
						result = (T) emptyTimestamp;
					} else if (typeClass.equalsIgnoreCase("HashMap")) {
						result = (T) emptyMap;
					} else if (typeClass.equalsIgnoreCase("LinkedHashMap")) {
						result = (T) emptyLinkedHashMap;
					} else if (typeClass.equalsIgnoreCase("List")) {
						result = (T) emptyListObject;
					} else if (typeClass.equalsIgnoreCase("GenericDto")) {
						result = (T) emptyGenericDto;
					}
				}
				return result;
			}
			if (typeClass.equalsIgnoreCase("String")) {
				String field = objectStr;
				if (formatDate != null) {
					boolean valid = isDateFormatValid(field, formatDate);
					field = valid ? field : null;
				}
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("Integer")) {
				Integer field = Integer.parseInt(objectStr);
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("Double")) {
				Double field = Double.parseDouble(objectStr);
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("Long")) {
				Long field = Long.parseLong(objectStr);
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("Boolean")) {
				Boolean field = Boolean.parseBoolean(objectStr);
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("BigInteger")) {
				BigInteger field = BigInteger.valueOf(new Long(objectStr));
				result = (T) field;
			} else if (typeClass.equalsIgnoreCase("BigDecimal")) { // Tipo de
																	// dato
																	// NUMERIC
																	// en Oracle
				BigDecimal field = BigDecimal.valueOf(new Double(objectStr));
				result = (T) field;
			} else if (object instanceof Timestamp) { // Tipo de dato TIMIESTAMP
														// en Oracle
				Date field = new Date(((Timestamp) object).getTime());
				result = (T) field;
			} else if (typeClass.equals("Date")) {
				String fieldStr = objectStr;
				if (formatDate != null) {
					boolean valid = isDateFormatValid(fieldStr, formatDate);
					fieldStr = valid ? fieldStr : null;
				}
				Date field = null;
				if (fieldStr != null && formatDate != null) {
					field = toDate(fieldStr, formatDate);
				}
				result = (T) field;
			} else if (object instanceof HashMap) {
				Map<String, Object> field = (Map<String, Object>) object;
				result = (T) field;
			} else if (object instanceof LinkedHashMap) {
				LinkedHashMap<String, Object> field = (LinkedHashMap<String, Object>) object;
				result = (T) field;
			} else if (object instanceof List) {
				List<Object> field = (List<Object>) object;
				result = (T) field;
			} else if (object instanceof GenericDto) {
				GenericDto field = (GenericDto) object;
				result = (T) field;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/*****************
	 * Validaciones campos. Valida campos de acuerdo a un flag
	 * (defaultResponse), si esta encendido aunque el campo sea nulo retornara
	 * una instancia vacia para String, numerica cero para Integer,Double,Long,
	 * false para Booleano y Mapa vacio para HashMap; estableciendo previamente
	 * el tipo de clase esperado en type Tambien validara el formato de fecha
	 * recibida en String de acuerdo a un formatDate pasado como Integer
	 * consultado en AppUtil.formatDate[] para todos los formatos soportados. Si
	 * el campo String no es una fecha indicar como formatDate = null para que
	 * omita la validacion respectiva.
	 */
	@SuppressWarnings({ "unchecked" })
	public static <T> T validateField(Object object, Class<T> type, Boolean defaultResponse, Integer formatDate) {
		String emptyString = new String();
		Integer emptyInteger = new Integer(0);
		Double emptyDouble = new Double(0d);
		Long emptyLong = new Long(0L);
		Boolean emptyBoolean = new Boolean(false);
		Date emptyDate = new Date();
		BigInteger emptyBigInteger = new BigInteger("0");
		BigDecimal emptyBigDecimal = new BigDecimal(0d);
		Timestamp emptyTimestamp = new Timestamp(new Date().getTime());
		Map<String, Object> emptyMap = new HashMap<String, Object>();
		LinkedHashMap<String, Object> emptyLinkedHashMap = new LinkedHashMap<String, Object>();
		List<Object> emptyListObject = new ArrayList<Object>();
		GenericDto emptyGenericDto = new GenericDto();
		T result = null;
		String typeClass = "";
		Class<T> clazz = (Class<T>) type;
		String typeClazz = clazz.getSimpleName();
		if (object != null)
			typeClass = object.getClass().getName();
		else {
			typeClass = clazz.getSimpleName();
		}
		if (object == null) {
			if (defaultResponse == Boolean.TRUE) {
				if (typeClass.equalsIgnoreCase("String")) {
					result = (T) emptyString;
				} else if (typeClass.equalsIgnoreCase("Integer")) {
					result = (T) emptyInteger;
				} else if (typeClass.equalsIgnoreCase("Double")) {
					result = (T) emptyDouble;
				} else if (typeClass.equalsIgnoreCase("Long")) {
					result = (T) emptyLong;
				} else if (typeClass.equalsIgnoreCase("Boolean")) {
					result = (T) emptyBoolean;
				} else if (typeClass.equalsIgnoreCase("Date")) {
					result = (T) emptyDate;
				} else if (typeClass.equalsIgnoreCase("BigInteger")) {
					result = (T) emptyBigInteger;
				} else if (typeClass.equalsIgnoreCase("BigDecimal")) {
					result = (T) emptyBigDecimal;
				} else if (typeClass.equalsIgnoreCase("Timestamp")) {
					result = (T) emptyTimestamp;
				} else if (typeClass.equalsIgnoreCase("HashMap")) {
					result = (T) emptyMap;
				} else if (typeClass.equalsIgnoreCase("LinkedHashMap")) {
					result = (T) emptyLinkedHashMap;
				} else if (typeClass.equalsIgnoreCase("List")) {
					result = (T) emptyListObject;
				} else if (typeClass.equalsIgnoreCase("GenericDto")) {
					result = (T) emptyGenericDto;
				}
			}
			return result;
		} else if (object instanceof String) {
			String field = (String) object;
			if (formatDate != null) {
				boolean valid = isDateFormatValid(field, formatDate);
				field = valid ? field : null;
			}
			result = (T) field;
		} else if (object instanceof Integer) {
			Integer field = (Integer) object;
			result = (T) field;
		} else if (object instanceof Double) {
			Double field = (Double) object;
			result = (T) field;
		} else if (object instanceof Long) {
			Long field = (Long) object;
			result = (T) field;
		} else if (object instanceof Boolean) {
			Boolean field = (Boolean) object;
			result = (T) field;
		} else if (object instanceof BigInteger) {
			BigInteger field = (BigInteger) object;
			result = (T) field;
		} else if (object instanceof BigDecimal) { // Tipo de dato NUMERIC en
													// Oracle
			if (typeClazz.equalsIgnoreCase("Long")) {
				Long field = new Long(((BigDecimal) object).longValue());
				result = (T) field;
			} else if (typeClazz.equalsIgnoreCase("Integer")) {
				Integer field = new Integer(((BigDecimal) object).intValue());
				result = (T) field;
			} else if (typeClazz.equalsIgnoreCase("Double")) {
				Double field = new Double(((BigDecimal) object).doubleValue());
				result = (T) field;
			}
		} else if (object instanceof Timestamp) { // Tipo de dato TIMIESTAMP en
													// Oracle
			Date field = new Date(((Timestamp) object).getTime());
			result = (T) field;
		} else if (object instanceof HashMap) {
			Map<String, Object> field = (Map<String, Object>) object;
			result = (T) field;
		} else if (object instanceof LinkedHashMap) {
			LinkedHashMap<String, Object> field = (LinkedHashMap<String, Object>) object;
			result = (T) field;
		} else if (object instanceof List) {
			List<Object> field = (List<Object>) object;
			result = (T) field;
		} else if (object instanceof GenericDto) {
			GenericDto field = (GenericDto) object;
			result = (T) field;
		}
		return result;
	}

	public static boolean isDateFormatValid(String dateInput, int formatDate) {
		// String date = "2014-02-02gy";
		try {
			int tam = formatDate > (AppUtil.formatDate.length - 1) ? 2 : formatDate;
			String defaultFormatDate = AppUtil.formatDate[tam];
			DateFormat df = new SimpleDateFormat(defaultFormatDate);
			Date dateObject = df.parse(dateInput);
			String dateString = df.format(dateObject);
			if (!dateString.equals(dateInput))
				throw new ParseException(dateInput, 0);
			return true;
		} catch (ParseException ex) { // not yyyy-mm-dd date.
			return false;
		}
	}

	public static Date toDate(String dateInString, int formatDate) {
		String defaultFormatDate = "dd/MM/yyyy HH:mm:ss";
		int tam = formatDate > (AppUtil.formatDate.length - 1) ? 2 : formatDate;
		defaultFormatDate = AppUtil.formatDate[tam];
		SimpleDateFormat format = new SimpleDateFormat(defaultFormatDate);
		try {
			return format.parse(dateInString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
}