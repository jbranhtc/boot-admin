package com.htc.ea.mcs.consumer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"consumerOperation","resource"})
public class ServiceRequest<T> {

	private String accessToken;
	private String country;
	private String clientId;
	private String operationRefId;
	private Date requestDate;
	private String consumerLocation;
	private String environmentId;
	private String endUser;
	private T data;
	private Map<String, String> parameters = new HashMap<>();
	protected ConsumerOperation consumerOperation;
	private String resource;
	private String category;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getOperationRefId() {
		return operationRefId;
	}
	public void setOperationRefId(String operationRefId) {
		this.operationRefId = operationRefId;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public String getConsumerLocation() {
		return consumerLocation;
	}
	public void setConsumerLocation(String consumerLocation) {
		this.consumerLocation = consumerLocation;
	}
	public String getEnvironmentId() {
		return environmentId;
	}
	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}
	public String getEndUser() {
		return endUser;
	}
	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Map<String, String> getParameters() {
		if (parameters==null) {
			parameters = new HashMap<>();
		}
		return parameters;
	}
	protected ConsumerOperation getConsumerOperation() {
		return consumerOperation;
	}
	protected void setConsumerOperation(ConsumerOperation consumerOperation) {
		this.consumerOperation = consumerOperation;
	}
	protected String getResource() {
		return resource;
	}
	protected void setResource(String resource) {
		this.resource = resource;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceRequest [accessToken=").append(accessToken).append(", country=").append(country)
				.append(", clientId=").append(clientId).append(", operationRefId=").append(operationRefId)
				.append(", requestDate=").append(requestDate).append(", consumerLocation=").append(consumerLocation)
				.append(", environmentId=").append(environmentId).append(", endUser=").append(endUser).append(", data=")
				.append(data).append(", parameters=").append(parameters).append(", consumerOperation=")
				.append(consumerOperation).append(", resource=").append(resource).append("]");
		return builder.toString();
	}
}