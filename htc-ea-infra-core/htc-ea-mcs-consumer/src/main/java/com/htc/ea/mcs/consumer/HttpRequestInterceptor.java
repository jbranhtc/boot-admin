package com.htc.ea.mcs.consumer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

import com.htc.ea.mcs.consumer.util.AppUtil;
import com.htc.ea.mcs.consumer.util.ClientHttpResponseContext;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.TransactionIdUtil;

public class HttpRequestInterceptor implements ClientHttpRequestInterceptor {
	
	private final Class<?> clazz = this.getClass();
	
	private AppUtil appUtil;
	private Map<String, String> parameters;

	public HttpRequestInterceptor(AppUtil appUtil, Map<String, String> parameters) {
		this.appUtil = appUtil;
		this.parameters = parameters;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		request.getHeaders().setAccept(Collections.singletonList(new MediaType("application","json")));
		HttpRequest wrapper = new HttpRequestWrapper(request);
		printHeaders(wrapper.getHeaders(), "headersRequest", true, null, null);
		ClientHttpResponse response = execution.execute(wrapper, body);
		response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
		printHeaders(response.getHeaders(), "headersResponse", false, response.getStatusCode().value(), response.getStatusText());
        return response;
	}
		
	private void printHeaders(HttpHeaders httpHeaders, String headersType, boolean outbound, Integer statusCode, String statusText) {
		String httpStatus = "";
		if (appUtil.logging(this.parameters.get(com.htc.ea.mcs.consumer.Constants.CATEGORY), EventLevel.DEBUG.name())) {
			Map<String, String> headersMap = httpHeaders.toSingleValueMap()!=null ? httpHeaders.toSingleValueMap() : new HashMap<>();			
			appUtil.settingUntilLastSequence(headersMap, outbound);
			if (statusCode!=null)
				httpStatus = "[HttpStatus " + statusCode + " - " + statusText + "] ==> ";
			String headersStr = httpStatus + Arrays.toString(headersMap.entrySet().toArray());
			appUtil.debug(null, clazz, Thread.currentThread().getStackTrace()[2].getMethodName(), headersType, headersStr, TransactionIdUtil.getMsisdn(), null, this.parameters.get(com.htc.ea.mcs.consumer.Constants.CATEGORY), headersMap.get(Constants.CONSUMER_LOCATION), headersMap, outbound, null);
		}
	}
	
	@SuppressWarnings("unused")
	private void getHeadersResponse(ClientHttpResponse response, Map<String, String> headersReqMap) throws IOException {
		boolean isError = isError(response.getStatusCode());
		if (isError) {
			if (response.getHeaders().toSingleValueMap().get(Constants.UNTIL_LAST_SEQUENCE)!=null){
				headersReqMap.put(Constants.UNTIL_LAST_SEQUENCE, response.getHeaders().toSingleValueMap().get(Constants.UNTIL_LAST_SEQUENCE));
			}
		}
	}	
	
	@SuppressWarnings("unused")
	private Map<String, String> getHeadersRequest(HttpHeaders httpHeaders, Set<String> headersSet) {
		Map<String, String> headersReqMap = new HashMap<>();
		if (httpHeaders.toSingleValueMap()!=null) {
			headersReqMap.put(Constants.OPERATION_REF_ID, httpHeaders.toSingleValueMap().get(Constants.OPERATION_REF_ID));
			headersReqMap.put(Constants.UNTIL_LAST_SEQUENCE, httpHeaders.toSingleValueMap().get(Constants.UNTIL_LAST_SEQUENCE));
			headersReqMap.put(Constants.X_USER_AGENT_KEY, httpHeaders.toSingleValueMap().get(Constants.X_USER_AGENT_KEY));
		}
		return headersReqMap;
	}
	
	private boolean isError(HttpStatus httpStatus) {
		boolean isError = RestUtil.isError(httpStatus);
		return isError;
	}
}