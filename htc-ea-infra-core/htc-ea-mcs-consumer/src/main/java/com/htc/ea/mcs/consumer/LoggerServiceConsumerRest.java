package com.htc.ea.mcs.consumer;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.htc.ea.mcs.consumer.util.AppUtil;
import com.htc.ea.mcs.consumer.util.ClientHttpResponseContext;
import com.htc.ea.util.log.EventLevel;
import com.htc.ea.util.util.TimeChronometer;
import com.htc.ea.util.util.TransactionIdUtil;

public class LoggerServiceConsumerRest implements ServiceConsumerRest {
	
	@Autowired
	private AppUtil appUtil;
	@Autowired
	private RestUtil restUtil;
	
	private Map<String, Object> getMetadataRequest(ServiceRequest<?> request) {
		String node = restUtil.getProtocol() + restUtil.getMetadataServer.get(Constants.HOST) + ":" + restUtil.getMetadataServer.get(Constants.PORT);
		String app = restUtil.getProperty("application.id");
		String resource = getResource(request);
		String httpMethod = getConsumerOperation(request.getConsumerOperation());
		String httpHeaders = Arrays.toString(request.getParameters().entrySet().toArray());
		Map<String, Object> attributesRest = new HashMap<>();
		attributesRest.put(Constants.NODE_SERVICE, node); // <dnsclient> pto-admin:5001
		attributesRest.put(Constants.CLIENT, app); // pto-admin
		attributesRest.put(Constants.RESOURCE, resource); // /technical-product-catalog
		attributesRest.put(Constants.HTTP_METHOD, httpMethod); // POST
		attributesRest.put(Constants.METADATA_REQ, app + "@" + resource + ", "  + httpMethod); // technical-product-catalog:5001
		attributesRest.put(Constants.HTTP_HEADERS, httpHeaders);		
		return attributesRest; 
	}
	
	private Map<String, Object> getMetadataResponse(ServiceRequest<?> request, ServiceResponse<?> response, Map<String, Object> attributesRest) {
		Long stateCode = response.getHttpCode();
		String httpHeaders = Arrays.toString(response.getHttpHeaders().entrySet().toArray());
		if (stateCode==null || httpHeaders==null) {
			try {
				Integer statusCode = ClientHttpResponseContext.getContext()!=null ? ClientHttpResponseContext.getContext().getRawStatusCode() : Integer.parseInt(Constants.DEFAULT_CODE_ERROR.toString());
				HttpHeaders headersResponse = ClientHttpResponseContext.getContext()!=null ? ClientHttpResponseContext.getContext().getHeaders() : null;
				if (statusCode!=null) stateCode = new Long(statusCode.toString());
				if (headersResponse!=null) httpHeaders =  Arrays.toString(headersResponse.toSingleValueMap().entrySet().toArray());				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String app = attributesRest.get(Constants.CLIENT).toString();
		String resource = attributesRest.get(Constants.RESOURCE).toString();
		String httpMethod = attributesRest.get(Constants.HTTP_METHOD).toString();
		String stateCodeStr = "HttpStatus: " + (stateCode==null ? null : stateCode.toString());
		attributesRest.put(Constants.METADATA_RES, app + "@" + resource + ", "  + httpMethod + " => " + stateCodeStr);
		attributesRest.put(Constants.HTTP_HEADERS, httpHeaders);	
		return attributesRest;
	}
	
	@Override
	public <T> ServiceResponse<T> execute(ServiceRequest<?> request, Class<T> responseType) throws ServiceException {
		Map<String, Object> attributesRest = getMetadataRequest(request);
		String sequence = TransactionIdUtil.nextSequenceId();
		appUtil.requestLoggerInfo(request, sequence, attributesRest);
		TimeChronometer tc = new TimeChronometer("ServiceConsumer - HTC El Salvador, CA");
		ServiceResponse<T> response = new ServiceResponse<T>();
		try {			
			response = executionRest(request, responseType);
		} catch (Exception ex) {
			sequence = TransactionIdUtil.nextSequenceId();			
			getMetadataResponse(request, response, attributesRest);
			appUtil.exceptionLoggerError(request, ex, sequence, attributesRest);
			builderServiceException(ex);			
		}
		getMetadataResponse(request, response, attributesRest);
		sequence = TransactionIdUtil.nextSequenceId();
		appUtil.responseLoggerInfo(request, response, sequence, attributesRest, tc.elapsedMilisUntilNow());
		if (response.serviceCode!=0 ) {
			throw new ServiceException(response);
		}
		return response;
	}
	
	@Override
	public <T> ServiceResponse<List<T>> executeList(ServiceRequest<?> request, Class<T[]> responseType) throws ServiceException {
		Map<String, Object> attributesRest = getMetadataRequest(request);
		String sequence = TransactionIdUtil.nextSequenceId();
		appUtil.requestLoggerInfo(request, sequence, attributesRest);
		long init = System.currentTimeMillis();		
		ServiceResponse<List<T>> response = new ServiceResponse<>();
		try {			
			response = executionRestList(request, responseType);
		} catch (Exception ex) {
			sequence = TransactionIdUtil.nextSequenceId();
			getMetadataResponse(request, response, attributesRest);
			appUtil.exceptionLoggerError(request, ex, sequence, attributesRest);
			builderServiceException(ex);
		}
		long duration = System.currentTimeMillis() - init;
		getMetadataResponse(request, response, attributesRest);
		sequence = TransactionIdUtil.nextSequenceId();
		appUtil.responseLoggerInfo(request, response, sequence, attributesRest, duration);
		if (response.serviceCode!=0 ) {
			throw new ServiceException(response);
		}
		return response;
	}
	
	private <T> ServiceResponse<T> executionRest(ServiceRequest<?> request, Class<T> responseType) throws Exception {
		ServiceResponse<T> response = new ServiceResponse<T>();
		ResponseEntity<T> responseEntity = null;
		if (request.getCategory()==null || (request.getCategory()!=null  && request.getCategory().isEmpty())) throw new IllegalArgumentException("Specify an event category");
		if (responseType==null) throw new IllegalArgumentException("Specify whether it is a collection of objects or an object");
		String consumerTrace = request.getOperationRefId() + ":" + TransactionIdUtil.getUntilLastSequence() + ":" + request.getParameters().get(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY) + ":" + request.getCategory();
		responseEntity = invoke(request, consumerTrace, responseType);		
		settingServiceResponse(request, response, responseEntity);
		return response;
	}	
	
	private <T> ServiceResponse<List<T>> executionRestList(ServiceRequest<?> request, Class<T[]> responseType) throws Exception {
		ServiceResponse<List<T>> response = new ServiceResponse<>();
		ResponseEntity<T[]> responseEntity = null;
		String consumerTrace = request.getOperationRefId() + ":" + TransactionIdUtil.getUntilLastSequence() + ":" + request.getParameters().get(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY) + ":" + request.getCategory();
		if (responseType==null) throw new IllegalArgumentException("Specify whether it is a collection of objects or an object");
    	responseEntity = invokeList(request, consumerTrace, responseType);			
		settingCollectionServiceResponse(request, response, responseEntity);	
		return response;
	}
	
	private <T> ResponseEntity<T> invoke(ServiceRequest<?> request, String consumerTrace, Class<T> classType) throws Exception {
		ResponseEntity<T> responseEntity = null;
		settingHeadersCustomized(request);
		switch (request.getConsumerOperation().ordinal()) {
		case 0:
			responseEntity = restUtil.sendGet(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 1:
			responseEntity = restUtil.sendHead(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 2:
			responseEntity = restUtil.sendPost(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());			
			break;
		case 3:
			responseEntity = restUtil.sendPut(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 4:
			responseEntity = restUtil.sendPatch(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 5:
			responseEntity = restUtil.sendDelete(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 6:		
			responseEntity = restUtil.sendOptions(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 7:			
			responseEntity = restUtil.sendTrace(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		default:
			break;
		}
		return responseEntity;
	}
	
	private <T> ResponseEntity<T[]> invokeList(ServiceRequest<?> request, String consumerTrace, Class<T[]> classType) throws Exception {
		ResponseEntity<T[]> responseEntity = null;
		settingHeadersCustomized(request);
		switch (request.getConsumerOperation().ordinal()) {
		case 0:
			responseEntity = restUtil.sendGetList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 1:		
			responseEntity = restUtil.sendHeadList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 2:
			responseEntity = restUtil.sendPostList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());			
			break;
		case 3:
			responseEntity = restUtil.sendPutList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 4:
			responseEntity = restUtil.sendPatchList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 5:
			responseEntity = restUtil.sendDeleteList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 6:
			responseEntity = restUtil.sendOptionsList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		case 7:
			responseEntity = restUtil.sendTraceList(request.getResource(), request.getData(), consumerTrace, classType, request.getParameters());
			break;
		default:
			break;
		}
		return responseEntity;
	}
	
	public void setAppUtil(AppUtil appUtil) {
		this.appUtil = appUtil;
	}
	
	private String getResource(ServiceRequest<?> request) {
		String uri = "";
		if (ConsumerOperation.getConsumerOperation(request.getConsumerOperation().ordinal())==ConsumerOperation.GET)
			uri = request.getResource().toString() + (request.getData()!=null ? request.getData().toString() : "");
		else
			uri = request.getResource().toString();
		return uri;
	}
	
	private <T> void verifyResultService(ServiceRequest<?> request, ServiceResponse<?> response, ResponseEntity<T> responseEntity) {
		Long serviceCode =  com.htc.ea.util.util.Constants.CODE_FAILED_KEY;
		String serviceDescription = com.htc.ea.util.util.Constants.CODE_FAILED_VALUE;
		Integer untilLastSequence = 0, untilLastSequenceFromTarget = 0;
	    response.setOperationRefId(request.getOperationRefId());		
		if (responseEntity!=null) {
			HttpStatus httpStatus = responseEntity.getStatusCode();
			HttpHeaders httpHeaders =  responseEntity.getHeaders();
			Map<String, String> headersMap = new HashMap<>();
			if (httpHeaders.toSingleValueMap()!=null) headersMap = httpHeaders.toSingleValueMap();
			boolean isError = RestUtil.isError(httpStatus);
			if (isError) {
				if (headersMap.get(com.htc.ea.util.util.Constants.SERVICE_CODE)!=null && 
					headersMap.get(com.htc.ea.util.util.Constants.SERVICE_DESCRIPTION)!=null) {
					boolean isNumber = restUtil.isIntegerNumber(headersMap.get(com.htc.ea.util.util.Constants.SERVICE_CODE).toString());					
					if (isNumber){
						serviceCode = new Long(headersMap.get(com.htc.ea.util.util.Constants.SERVICE_CODE).toString());
						serviceDescription = headersMap.get(com.htc.ea.util.util.Constants.SERVICE_DESCRIPTION).toString();					
					}
				}else{
					serviceCode = new Long(responseEntity.getStatusCodeValue());
					serviceDescription = com.htc.ea.util.util.Constants.CODE_FAILED_VALUE;		
				}							
			}else{
				serviceCode = com.htc.ea.util.util.Constants.CODE_SUCCESS_KEY;
				serviceDescription = com.htc.ea.util.util.Constants.CODE_SUCCESS_VALUE;
			}
			if (headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE)!=null) {
				boolean isNumber2 = restUtil.isIntegerNumber(headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE).toString());
				if (isNumber2) {
					if (appUtil.logging(request.getCategory(), EventLevel.DEBUG.name())) {
						untilLastSequence = TransactionIdUtil.getUntilLastSequence();
						untilLastSequenceFromTarget = new Integer(headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE).toString());
					}else{
						untilLastSequence = new Integer(headersMap.get(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE).toString());	
						untilLastSequenceFromTarget = untilLastSequence;
					}
				}
			}
		}
		if (untilLastSequence==0) {
			untilLastSequence = TransactionIdUtil.getUntilLastSequence();
		}else{
			TransactionIdUtil.setSequence(untilLastSequence);
		}
		response.setServiceCode(serviceCode);
		response.setServiceDescription(serviceDescription);		
		response.setUntilLastSequence(untilLastSequenceFromTarget);
	}
	
	private String getConsumerOperation(ConsumerOperation consumerOperation) {
		String operation = ConsumerOperation.getConsumerOperation(consumerOperation.ordinal()).name();		
		return operation;
	}
	
	private <T> void settingCollectionServiceResponse(ServiceRequest<?> request, ServiceResponse<List<T>> response, ResponseEntity<T[]> responseEntity) {
		if (responseEntity!=null) {
			response.getHttpHeaders().putAll(responseEntity.getHeaders()!=null ? responseEntity.getHeaders().toSingleValueMap() : new HashMap<String,String>());
			response.setHttpCode(new Long(responseEntity.getStatusCodeValue()));
			List<T> data = (responseEntity.getBody()!=null ? Arrays.asList(responseEntity.getBody()) : null);
			response.setData(data);
		}
		response.setOperationRefId(request.getOperationRefId());
		verifyResultService(request, response, responseEntity);		
	}
	
	private <T> void settingServiceResponse(ServiceRequest<?> request, ServiceResponse<T> response, ResponseEntity<T> responseEntity) {
		if (responseEntity!=null) {
			response.getHttpHeaders().putAll(responseEntity.getHeaders()!=null ? responseEntity.getHeaders().toSingleValueMap() : new HashMap<String,String>());
			response.setHttpCode(new Long(responseEntity.getStatusCodeValue()));
			response.setData(responseEntity.getBody());
		}
		response.setOperationRefId(request.getOperationRefId());
		verifyResultService(request, response, responseEntity);		
	}
	
	private void settingHeadersCustomized(ServiceRequest<?> request) {
		String accessToken = request.getAccessToken()!=null ? request.getAccessToken() : null;
		if (accessToken!=null) request.getParameters().put(Constants.ACCESS_TOKEN, accessToken);
		if (request.getParameters().get(com.htc.ea.util.util.Constants.CONSUMER_LOCATION)==null) {
			String consumerLocation = request.getConsumerLocation()!=null ? request.getConsumerLocation() : "UnknownHost";
			if (consumerLocation!=null) request.getParameters().put(com.htc.ea.util.util.Constants.CONSUMER_LOCATION, consumerLocation);
		}
	}
	
	private <T> void builderServiceException(Exception ex) throws ServiceException {
		boolean isError;
		long code = Constants.DEFAULT_CODE_ERROR;
		String desc = Constants.DEFAULT_DESCRIPTION_ERROR;
		try {
			Integer statusCode = ClientHttpResponseContext.getContext()!=null ? ClientHttpResponseContext.getContext().getRawStatusCode() : Integer.parseInt(Constants.DEFAULT_CODE_ERROR.toString());
			isError = statusCode==Integer.parseInt(Constants.DEFAULT_CODE_ERROR.toString()) ? false : RestUtil.isError(ClientHttpResponseContext.getContext().getStatusCode());
			if (isError) {
				String codeText = String.valueOf(ClientHttpResponseContext.getContext().getRawStatusCode());
				String statusText = ClientHttpResponseContext.getContext().getStatusText();
				code = codeText==null ? code : new Long(codeText);
				desc = statusText==null ? desc : statusText;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		throw new ServiceException(code, desc, ClientHttpResponseContext.getContext().getHeaders().toSingleValueMap(), ex);
	}
}