package com.htc.ea.mcs.consumer;

public interface Constants {

	//host:port del application
	public final static String HOST = "host";
	public final static String PORT = "port";
	//Categoria logging serviceConsumer
	public final static String CONSUMER_REST_CATEGORY = "consumer-rest";
	public final static String CATEGORY = "category";
	
	//Metadata for Request
	public final static String NODE_SERVICE = "node";
	public final static String CLIENT = "client";
	public final static String RESOURCE = "resource";
	public final static String HTTP_METHOD = "httpMethod";
	public final static String METADATA_REQ = "metadata-req";	
	//Metadata for Response
	public final static String METADATA_RES = "metadata-res";
	public final static String HTTP_HEADERS = "httpHeaders";	
	public final static String ACCESS_TOKEN = "access-token";
	//Code errors
	public final static Long DEFAULT_CODE_ERROR = 1L;
	public final static String DEFAULT_DESCRIPTION_ERROR = "Unknown Error";
}