package com.htc.ea.mcs.consumer.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.htc.ea.mcs.consumer.ConsumerRestHelper;
import com.htc.ea.mcs.consumer.DefaultConsumerRestHelper;
import com.htc.ea.mcs.consumer.LoggerServiceConsumerRest;
import com.htc.ea.mcs.consumer.ReferenceIdGenerator;
import com.htc.ea.mcs.consumer.ServiceConsumerRest;
import com.htc.ea.mcs.consumer.TransactionIdGenerator;

@Configuration
@ComponentScan(basePackages={"com.htc.ea.mcs.consumer"})
public class ServiceConsumerRestConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceConsumerRestConfig.class);

	@Autowired
	private Environment env;
	
	@Bean
	public ServiceConsumerRest serviceConsumerRest() {
		logger.debug("Creating @Bean serviceConsumerRest");
		LoggerServiceConsumerRest serviceConsumer = new LoggerServiceConsumerRest();			
		logger.debug("Succesful creation of @Bean serviceConsumerRest");
		return serviceConsumer;
	}

	@Bean
	public ConsumerRestHelper consumerRestHelper() {
		logger.debug("Creating @Bean consumerRestHelper");
		DefaultConsumerRestHelper helper = new DefaultConsumerRestHelper();		
		helper.setServiceConsumerRest(serviceConsumerRest());
		helper.setAccessToken(env.getProperty("consumer.accessToken"));
		helper.setApplicationId(env.getProperty("application.id"));
		helper.setCountry(env.getProperty("consumer.country"));
		helper.setEnvironmentId(env.getProperty("application.environment"));
		helper.setReferenceIdGenerator(referenceIdGenerator());
		logger.debug("Succesful creation of @Bean consumerRestHelper");
		return helper;
	}
	
	@Bean
	public ReferenceIdGenerator referenceIdGenerator() {
		return new TransactionIdGenerator();
	}
}