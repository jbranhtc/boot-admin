package com.htc.ea.mcs.consumer;

import com.htc.ea.util.util.TransactionIdUtil;

public class TransactionIdGenerator implements ReferenceIdGenerator {

	@Override
	public String generateRefId() {
		return TransactionIdUtil.getId();
	}

}