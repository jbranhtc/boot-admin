package com.htc.ea.mcs.consumer.util;

import org.springframework.http.client.ClientHttpResponse;

public class ClientHttpResponseContext {

	private static ThreadLocal<ClientHttpResponse> threadLocal = new ThreadLocal<ClientHttpResponse>();

	public static void setContext(ClientHttpResponse context) {
		threadLocal.set(context);
	}
	
	public static ClientHttpResponse getContext() {
		return threadLocal.get();
	}	
	
}