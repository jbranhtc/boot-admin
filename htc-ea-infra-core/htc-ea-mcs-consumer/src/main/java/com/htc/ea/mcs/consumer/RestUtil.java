package com.htc.ea.mcs.consumer;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.htc.ea.mcs.consumer.util.AppUtil;

@Component
public class RestUtil {

	@Autowired
	private AppUtil appUtil;
		
	public String getProtocol() {
		boolean isSsl = appUtil.getProperty("server.ssl.enabled")!=null ? new Boolean(System.getProperty("server.ssl.enabled")) : false;
		String protocol = isSsl ? "https://" : "http://";
		return protocol;
	}
	
	public Map<String, String> getMetadataServer;
	{
		Map<String, String> tempMap = new HashMap<>();
		String port = System.getProperty("server.port");
		String host = "UnknownHost";
		try {
			host = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			host = "UnknownHost";
		}
		tempMap = new HashMap<>();
		tempMap.put(Constants.HOST, host);
		tempMap.put(Constants.PORT, port);
		getMetadataServer = Collections.unmodifiableMap(tempMap);
	}
	
	public static boolean isError(HttpStatus status) {
		HttpStatus.Series series = status.series();
		return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series)
				|| HttpStatus.INTERNAL_SERVER_ERROR.equals(status));
	}

    public static HttpHeaders getHttpHeaders(String loggingTrace, Map<String,String> headersRequest){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(new MediaType("application","json")));
        String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		headers.set(com.htc.ea.util.util.Constants.OPERATION_REF_ID, loggingTraceArr[0]);
		headers.set(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE, loggingTraceArr[1]);
		headers.set(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, String.format(com.htc.ea.util.util.Constants.X_USER_AGENT_VALUE, loggingTraceArr[2]));
		headersRequest.remove(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY);
		if (headersRequest!=null && !headersRequest.isEmpty()) headers.setAll(headersRequest);
        return headers;
    }
	
	@SuppressWarnings("unused")
	private String toQueryParams(Object requestObject) {
		StringBuilder queryParams = new StringBuilder();
		Field[] fields = requestObject.getClass().getDeclaredFields();
		for (Field f : fields) {
			try {
				f.setAccessible(true);
				Object value = f.get(requestObject);
				if (value != null && !(value instanceof List)) {
					if (queryParams.length() == 0)
						queryParams.append("?");
					else
						queryParams.append("&");
					queryParams.append(f.getName()).append("=").append(value);
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		String params = queryParams.toString();
		params = params.replaceAll(" ", "%20");
		return params;
	}
	
	@SuppressWarnings("unused")
	private String getEncode(String request) {
		String parameters = "";
		try {
			parameters = URLEncoder.encode(request, "UTF-8");					
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return parameters;
	}
	
	private static String[] getLoggingTrace(String trace) {
		 String[] traceArr = trace.split(":");
		 return traceArr;
	}
	
	@SuppressWarnings("unchecked")
	private String getUriFull(String uri, Object requestObject) {
		String uriFull = "";
		UriComponents builder = null;
		Map<String, String> paramsUri = new HashMap<>();
		Map<String, String> templateUri = new HashMap<>();
		Map<String, String> queryUri = new HashMap<>();
		if (requestObject!=null && requestObject instanceof HashMap) {
			paramsUri = (Map<String, String>) requestObject;
			if (paramsUri.size()>0) {
				Object[] paramsUriObj = getParamsUri(paramsUri);
				templateUri.putAll((Map<? extends String, ? extends String>) paramsUriObj[0]);
				queryUri.putAll((Map<? extends String, ? extends String>) paramsUriObj[1]);
				if (templateUri.size()>0) {
//					for (Map.Entry<String, String> entry : templateUri.entrySet()) {
//						System.out.println("entry templateUri: " + entry.getKey() + "/" + entry.getValue());
//					}
					builder = UriComponentsBuilder.fromUriString(uri).build().expand(templateUri).encode();
				}else
					builder = UriComponentsBuilder.fromUriString(uri).build();
				int count = 0;
				StringBuilder queryUriBuilder = new StringBuilder();
				if (queryUri.size()>0) {
					for (Map.Entry<String, String> entry : queryUri.entrySet()) {
//						System.out.println("entry queryUri: " + entry.getKey() + "/" + entry.getValue());
						if (count==0) 
							queryUriBuilder.append("?");
						else
							queryUriBuilder.append("&");
						queryUriBuilder.append(entry.getKey()).append("=").append(entry.getValue());
						count++;
					}
					String queryUriStr = queryUriBuilder.toString().replaceAll(" ", "+");					
					uriFull = builder.toUriString() + queryUriStr;					
				}else
					uriFull = builder.toUriString();
//				System.out.println("uri builded : " + uriFull);				
			}else{
				if (uri.indexOf("{")>-1 || uri.indexOf("}")>-1) throw new IllegalArgumentException("Specify \"{values}\" whether it is a uri of the type TemplateUri");
				uriFull = uri;				
			}				
		}else{
			if (uri.indexOf("{")>-1 || uri.indexOf("}")>-1) throw new IllegalArgumentException("Specify \"{values}\" whether it is a uri of the type TemplateUri");
			uriFull = uri;
		}
		return uriFull;
	}
	
	private Object[] getParamsUri(Map<String, String> paramsUri) {
		Map<String, String> templateUri = new HashMap<>();
		Map<String, String> queryUri = new HashMap<>();
		for (Map.Entry<String, String> entry : paramsUri.entrySet()) {
			if (entry.getKey().startsWith("&")) {
				String key = entry.getKey().substring(1,entry.getKey().length());
				queryUri.put(key, entry.getValue());
			}else{
				templateUri.put(entry.getKey(), entry.getValue());
			}
		}
		Object[] paramsUriObj = new Object[]{templateUri,queryUri};
		return paramsUriObj;
	}
	
	public boolean isIntegerNumber(String data) {
		String regex = "^-*[0-9]+$"; //uno o mas digitos numericos enteros positivos o negativos
		Pattern queryLangPattern = Pattern.compile(regex);
		Matcher matcher = queryLangPattern.matcher(data);
		return matcher.matches();
	}

	public String getProperty(String key) {
		return appUtil.getProperty(key);
	}
	
	@SuppressWarnings("unused")
	private <T> ParameterizedTypeReference<List<T>> getParameterizedTypeReferenceList(Class<T> typeParameterClass) {
		ParameterizedTypeReference<List<T>> typeRef = new ParameterizedTypeReference<List<T>>(){};
		return typeRef;
	}
	
	@SuppressWarnings("unused")
	private <T> ParameterizedTypeReference<T> getParameterizedTypeReference(Class<T> typeParameterClass) {
		ParameterizedTypeReference<T> typeRef = new ParameterizedTypeReference<T>(){};
		return typeRef;
	}
	
	public <T> ResponseEntity<T> sendGet(String prUrl, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);		
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);	    
		URI uri = null;
		String parameters = prUrl.toString().replaceAll(" ", "+");
		uri = new URI(parameters);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, typeParameterClass);
		return responseEntity;
	}
		
	public <T> ResponseEntity<T> sendGet(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);	
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
		URI uri = null;		
		String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, typeParameterClass);
		return responseEntity;
	}

	public <T> ResponseEntity<T[]> sendGetList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
		URI uri = null;
		String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, typeParameterClass);			
		return responseEntity;
	}
	
	public <T> ResponseEntity<T[]> sendGetList2(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception { 
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
		URI uri = null;
		String parameters = "";
		if (requestObject!=null) requestObject.toString().replaceAll(" ", "+");	
		uri = new URI(prUrl + parameters);
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendPost(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
		URI uri = null;
		String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
//		uri = new URI(prUrl);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendPostList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendPut(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendPutList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.PUT, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendDelete(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendDeleteList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendPatch(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendPatchList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendHead(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.HEAD, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendHeadList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.HEAD, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendOptions(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.OPTIONS, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendOptionsList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.OPTIONS, requestEntity, typeParameterClass);
		return responseEntity;
	}
	
	public <T> ResponseEntity<T> sendTrace(String prUrl, Object requestObject, String loggingTrace, Class<T> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T> responseEntity = restTemplate.exchange(uri, HttpMethod.TRACE, requestEntity, typeParameterClass);
		return responseEntity;
	}	
	
	public <T> ResponseEntity<T[]> sendTraceList(String prUrl, Object requestObject, String loggingTrace, Class<T[]> typeParameterClass, Map<String,String> headersRequest) throws Exception {
		String[] loggingTraceArr = getLoggingTrace(loggingTrace);
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.CATEGORY, loggingTraceArr[3]);
		RestTemplate restTemplate = RestTemplateFactory.restTemplate(appUtil, params);
	    HttpHeaders headers= getHttpHeaders(loggingTrace, headersRequest);
	    URI uri = null;
	    String uriFull = getUriFull(prUrl, requestObject);		
		uri = new URI(uriFull);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestObject, headers);
		ResponseEntity<T[]> responseEntity = restTemplate.exchange(uri, HttpMethod.TRACE, requestEntity, typeParameterClass);
		return responseEntity;
	}
}