package com.htc.ea.mcs.consumer;

public enum ConsumerOperation {
	
	GET(0), HEAD(1), POST(2), PUT(3), PATCH(4), DELETE(5), OPTIONS(6), TRACE(7);
	
	private int value;
	
	private ConsumerOperation(int value){
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static ConsumerOperation getConsumerOperation(int value) {
		ConsumerOperation consumerOperation = null;
		switch (value) {
		case 0:
			consumerOperation = GET;
			break;
		case 1:
			consumerOperation = HEAD;
			break;
		case 2:
			consumerOperation = POST;
			break;
		case 3:
			consumerOperation = PUT;
			break;
		case 4:
			consumerOperation = PATCH;
			break;
		case 5:
			consumerOperation = DELETE;
			break;
		case 6:
			consumerOperation = OPTIONS;
			break;
		case 7:
			consumerOperation = TRACE;
			break;			
		}
		return consumerOperation;
	}	
}