package com.htc.ea.mcs.consumer;

import java.util.List;

public interface ServiceConsumerRest {

	public <T> ServiceResponse<T> execute(ServiceRequest<?> request, Class<T> responseType) throws ServiceException;
	
	public <T> ServiceResponse<List<T>> executeList(ServiceRequest<?> request, Class<T[]> responseType) throws ServiceException;
}