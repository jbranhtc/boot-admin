package com.htc.ea.mcs.consumer;

import java.util.HashMap;
import java.util.Map;

public class ServiceResponse<T> {

	protected String operationRefId;
	protected Long serviceCode;
	protected String serviceDescription;
	protected T data;
	protected Map<String,String> httpHeaders;
	protected Long httpCode;
	protected Integer untilLastSequence;
	
	public String getOperationRefId() {
		return operationRefId;
	}
	public void setOperationRefId(String operationRefId) {
		this.operationRefId = operationRefId;
	}
	public Long getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(Long serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Map<String,String> getHttpHeaders() {
		if (httpHeaders==null) {
			httpHeaders = new HashMap<>();
		}
		return httpHeaders;
	}
	public Long getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(Long httpCode) {
		this.httpCode = httpCode;
	}
	public Integer getUntilLastSequence() {
		return untilLastSequence;
	}
	public void setUntilLastSequence(Integer untilLastSequence) {
		this.untilLastSequence = untilLastSequence;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceResponse [operationRefId=").append(operationRefId).append(", serviceCode=")
				.append(serviceCode).append(", serviceDescription=").append(serviceDescription).append(", data=")
				.append(data).append(", httpHeaders=").append(httpHeaders).append(", httpCode=").append(httpCode)
				.append(", untilLastSequence=").append(untilLastSequence).append("]");
		return builder.toString();
	}
}