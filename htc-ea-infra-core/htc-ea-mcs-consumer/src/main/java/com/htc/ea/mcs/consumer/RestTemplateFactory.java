package com.htc.ea.mcs.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.htc.ea.mcs.consumer.util.AppUtil;

public class RestTemplateFactory {
	
	private static ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
		mapper.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);		
		mapper.setTimeZone(TimeZone.getDefault());
		return mapper;
	}	
	
    public static RestTemplate restTemplate(AppUtil appUtil, Map<String, String> parameters) {
		List<ClientHttpRequestInterceptor> interceptorList = new ArrayList<>();
		interceptorList.add(new HttpRequestInterceptor(appUtil, parameters));
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(interceptorList);
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter(objectMapper()));
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler(appUtil, parameters));
        return restTemplate;
    }
}