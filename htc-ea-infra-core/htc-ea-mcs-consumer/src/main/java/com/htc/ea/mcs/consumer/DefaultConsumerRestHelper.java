package com.htc.ea.mcs.consumer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

public class DefaultConsumerRestHelper implements ConsumerRestHelper {

	private static String HOST_ADDRESS;

	protected String accessToken;
	protected String applicationId;
	protected String country;
	protected String environmentId;
	protected String consumerLocation = HOST_ADDRESS;
	protected ReferenceIdGenerator referenceIdGenerator = new UUIDReferenceIdGenerator();
	protected ServiceConsumerRest serviceConsumerRest;

	static {
		try {
			HOST_ADDRESS = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			HOST_ADDRESS = "UnknownHost";
		}
	}

//	@Override
	private <T> ServiceRequest<T> createServiceRequest() {
		ServiceRequest<T> request = new ServiceRequest<T>();
		request.setAccessToken(accessToken);
		request.setClientId(applicationId);
		request.setOperationRefId(referenceIdGenerator.generateRefId());
		request.setCountry(country);
		request.setEnvironmentId(environmentId);		
		request.setRequestDate(new Date());
		request.setConsumerLocation(consumerLocation);
		return request;
	}

	@Override
	public <T> ServiceRequest<T> createServiceRequest(String resource, ConsumerOperation consumerOperation) {
		ServiceRequest<T> request = createServiceRequest();
		request.setConsumerOperation(consumerOperation);
		request.setResource(resource);
		return request;
	}

	@Override
	public <T> ServiceResponse<T> executeService(ServiceRequest<?> request, Class<T> responseType)
			throws ServiceException {
		return serviceConsumerRest.execute(request, responseType);	
	}
	
	@Override
	public <T> ServiceResponse<List<T>> executeCollectionService(ServiceRequest<?> request, Class<T[]> responseType)
			throws ServiceException {
		return serviceConsumerRest.executeList(request, responseType);	
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEnvironmentId() {
		return environmentId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public String getConsumerLocation() {
		return consumerLocation;
	}

	public void setConsumerLocation(String consumerLocation) {
		this.consumerLocation = consumerLocation;
	}

	public ReferenceIdGenerator getReferenceIdGenerator() {
		return referenceIdGenerator;
	}

	public void setReferenceIdGenerator(ReferenceIdGenerator referenceIdGenerator) {
		if (referenceIdGenerator == null) {
			throw new IllegalArgumentException("referenceIdGenerator cannot be null");
		}
		this.referenceIdGenerator = referenceIdGenerator;
	}

	public ServiceConsumerRest getServiceConsumerRest() {
		return serviceConsumerRest;
	}

	public void setServiceConsumerRest(ServiceConsumerRest serviceConsumerRest) {
		if (serviceConsumerRest == null) {
			throw new IllegalArgumentException("serviceConsumer cannot be null");
		}
		this.serviceConsumerRest = serviceConsumerRest;
	}
}