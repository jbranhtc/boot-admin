package com.htc.ea.mcs.consumer;

import java.util.List;

public interface ConsumerRestHelper {
	
	public <T> ServiceRequest<T> createServiceRequest(String resource, ConsumerOperation consumerOperation);
	
	public <T> ServiceResponse<T> executeService(ServiceRequest<?> request, Class<T> responseType) throws ServiceException;
	
	public <T> ServiceResponse<List<T>> executeCollectionService(ServiceRequest<?> request, Class<T[]> responseType) throws ServiceException;

}