package com.htc.ea.mcs.consumer;

import java.util.UUID;

public class UUIDReferenceIdGenerator implements ReferenceIdGenerator {

	@Override
	public String generateRefId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

}