package com.htc.ea.mcs.consumer;

public interface ReferenceIdGenerator {
	
	public String generateRefId();

}