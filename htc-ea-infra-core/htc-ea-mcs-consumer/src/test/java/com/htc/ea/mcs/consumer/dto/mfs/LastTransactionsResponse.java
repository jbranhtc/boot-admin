package com.htc.ea.mcs.consumer.dto.mfs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.htc.ea.mcs.consumer.util.AppUtil;

@JsonPropertyOrder({ "amount", "currency", "type", "transactionStatus", "requestDate", "transactionReference",
		"debitParty", "creditParty", "metadata" })
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LastTransactionsResponse extends ErrorResponse {

	private Double amount;
	private Integer currency;
	private String type;
	private Integer transactionStatus;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppUtil.TIMESTAMP_FORMAT)
	private Date requestDate;
	private Long transactionReference;
	private List<DebitParty> debitParty;
	private List<CreditParty> creditParty;
	private TransactionInfo transactionInfo;

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(Integer transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Long getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(Long transactionReference) {
		this.transactionReference = transactionReference;
	}

	public List<DebitParty> getDebitParty() {
		if (debitParty == null) {
			debitParty = new ArrayList<>();
		}
		return debitParty;
	}

	public void setDebitParty(List<DebitParty> debitParty) {
		this.debitParty = debitParty;
	}

	public List<CreditParty> getCreditParty() {
		if (creditParty == null) {
			creditParty = new ArrayList<>();
		}
		return creditParty;
	}

	public void setCreditParty(List<CreditParty> creditParty) {
		this.creditParty = creditParty;
	}

	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LastTransactionsResponse [amount=").append(amount).append(", currency=").append(currency)
				.append(", type=").append(type).append(", transactionStatus=").append(transactionStatus)
				.append(", requestDate=").append(requestDate).append(", transactionReference=")
				.append(transactionReference).append(", debitParty=").append(debitParty).append(", creditParty=")
				.append(creditParty).append(", transactionInfo=").append(transactionInfo).append("]");
		return builder.toString();
	}
}