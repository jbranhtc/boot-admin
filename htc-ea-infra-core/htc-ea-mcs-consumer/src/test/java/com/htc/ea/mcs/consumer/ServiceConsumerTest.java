package com.htc.ea.mcs.consumer;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.ea.mcs.consumer.config.ServiceConsumerRestConfig;
import com.htc.ea.mcs.consumer.dto.ApplicationDto;
import com.htc.ea.mcs.consumer.dto.CommandDto;
import com.htc.ea.mcs.consumer.dto.TechnicalProductCatalogDto;
import com.htc.ea.util.configuration.UtilAppConfig;
import com.htc.ea.util.util.TransactionIdUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {UtilAppConfig.class,ServiceConsumerRestConfig.class})
public class ServiceConsumerTest {
		
	@Autowired
	private ConsumerRestHelper consumerRestHelper;
	@Autowired
	private RestUtil restUtil;
	private final String gateway = "PTO ServiceConsumer Gateway";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArguments.setupConfig();
	}

	@Test
	public void testMCSServiceConsumerGetForBuilderQueryURI() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/71272525/4050/info/get";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("&wallet", "DIVERTIGO");
		data.put("&account", "1234564899");
		data.put("&msisdn", "71272525");
		data.put("&remark", "All params sent");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetForBuilderTemplateURI() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/{msisdn}/{offering}/info/get";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "71272525");
		data.put("offering", "4050");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetForBuilderURIFull() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/{msisdn}/{offering}/info/get";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "71272525");
		data.put("offering", "4050");
		data.put("&wallet", "DIVERTIGO");
		data.put("&account", "1234564899");
		data.put("&msisdn", "71272525");
		data.put("&remark", "All params sent");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetListForBuilderURIFull() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/{msisdn}/{offering}/info/getList";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "71272525");
		data.put("offering", "4050");
		data.put("&wallet", "DIVERTIGO");
//		data.put("&account", "1234564899");
//		data.put("&msisdn", "71272525");
//		data.put("&remark", "All params sent");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGet() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:5001/pto/development/v1/technical-product-catalog";
		String data = "?typeCatalog=1&nameCatalog=Command All PTO";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetList() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogGetList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse =  consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());
			if (responseData!=null) {
				System.out.println("responseData size => " + responseData.size());
				for (CommandDto command: responseData) {
					System.out.println(command);
				}
			}
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerPost() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogPost";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>(); 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
//		serviceRequest.setData("{\"test\":\"hola\"}");
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}	
	
	@Test
	public void testMCSServiceConsumerPostList() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogPostList2";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
		serviceRequest.setData(null);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse =  consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("\n responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());
			if (responseData!=null) {
				System.out.println("responseData size => " + responseData.size());
				for (CommandDto command: responseData) {
					System.out.println(command);
				}
			}
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testRestGet() {
		String resource = "http://localhost:5001/pto/development/v1/technical-product-catalog";
		String data = "?typeCatalog=0&nameCatalog=CommandAllPTO";
		ResponseEntity<TechnicalProductCatalogDto> response = null;
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		try {
			response = restUtil.sendGet(resource, data, originRefId+":"+"3"+":"+gateway, TechnicalProductCatalogDto.class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		TechnicalProductCatalogDto res = response.getBody();
		System.out.println("responseData => " + res);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testRestGetList() {
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		ResponseEntity<CommandDto[]> response = null;
		List<CommandDto> responseData = null;
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		try {
			response = restUtil.sendGetList(resource, data, originRefId+":"+"3"+":"+gateway, CommandDto[].class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		if (responseData!=null) responseData = Arrays.asList(response.getBody());
		System.out.println("responseData => " + responseData);
		if (responseData!=null) {
			System.out.println("responseData size => " + responseData.size());
			for (CommandDto command: responseData) {
				System.out.println(command);
			}
		}
	}
		
	@Test
	public <T> void testRestGetList2() throws URISyntaxException {
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		resource = "http://localhost:8090/QueryEventLogService/Application";
		data = "/bo";
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		List<ApplicationDto> responseData = null;
		ResponseEntity<ApplicationDto[]> response = null;
		try {
			response = restUtil.sendGetList2(resource + data, null, originRefId+":"+"3"+":"+gateway, ApplicationDto[].class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    if (response != null)
	    	responseData = Arrays.asList(response.getBody());
		System.out.println("responseData size() => " + responseData.size());
		for (ApplicationDto app: responseData) {
			System.out.println(app);
		}
	}	
}