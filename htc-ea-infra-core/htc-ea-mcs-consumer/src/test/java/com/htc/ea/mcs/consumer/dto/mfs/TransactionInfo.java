package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionInfo {

	@JsonProperty("TRANSACTION_ID")
	private Integer transactionId;
	@JsonProperty("BRAND_ID")
	private Integer brandId;
	@JsonProperty("SOURCE")
	private Integer source;
	@JsonProperty("DESTINY")
	private Integer destiny;
	@JsonProperty("TS_TRANSACTION")
	private String tsTransaction;
	@JsonProperty("TYPE")
	private Integer type;
	@JsonProperty("RESULT")
	private Integer result;
	@JsonProperty("INTERFACE")
	private String txInterface;
	@JsonProperty("CURRENCY_ID")
	private Integer currencyId;
	@JsonProperty("MOVEMENT_QUANTITY")
	private Integer movementQuantity;
	@JsonProperty("SERVICE_ID")
	private Integer serviceId;
	@JsonProperty("SOURCE_DEVICE_ID")
	private Integer sourceDeviceId;
	@JsonProperty("SOURCE_DEVICE_KEY")
	private String sourceDeviceKey;
	@JsonProperty("SOURCE_DEVICE_TYPE_ID")
	private String sourceDeviceTypeId;
	@JsonProperty("TARGET_DEVICE_ID")
	private String targetDeviceId;
	@JsonProperty("TARGET_DEVICE_KEY")
	private String targetDeviceKey;
	@JsonProperty("TARGET_DEVICE_TYPE_ID")
	private String targetDeviceTypeId;
	@JsonProperty("DOCUMENT_ID")
	private String documentId;
	@JsonProperty("ERROR_CODE")
	private String errorCode;
	@JsonProperty("AMOUNT")
	private String amount;
	@JsonProperty("SERVICE_DESCRIPTION")
	private String serviceDescription;
	@JsonProperty("SERVICE_TYPE")
	private String serviceType;
	@JsonProperty("RN")
	private Integer rn;
	@JsonProperty("INFO_TRX_UTFI")
	private String infoTrxUtfi;
	@JsonProperty("INFO_RESERVE_TRX_ID")
	private String infoReserveTrxId;
	@JsonProperty("RECHARGE_MSISDN")
	private String rechargeMsisdn;
	@JsonProperty("DETAILS")
	private String details;

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Integer getDestiny() {
		return destiny;
	}

	public void setDestiny(Integer destiny) {
		this.destiny = destiny;
	}

	public String getTsTransaction() {
		return tsTransaction;
	}

	public void setTsTransaction(String tsTransaction) {
		this.tsTransaction = tsTransaction;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getTxInterface() {
		return txInterface;
	}

	public void setTxInterface(String txInterface) {
		this.txInterface = txInterface;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getMovementQuantity() {
		return movementQuantity;
	}

	public void setMovementQuantity(Integer movementQuantity) {
		this.movementQuantity = movementQuantity;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getSourceDeviceId() {
		return sourceDeviceId;
	}

	public void setSourceDeviceId(Integer sourceDeviceId) {
		this.sourceDeviceId = sourceDeviceId;
	}

	public String getSourceDeviceKey() {
		return sourceDeviceKey;
	}

	public void setSourceDeviceKey(String sourceDeviceKey) {
		this.sourceDeviceKey = sourceDeviceKey;
	}

	public String getSourceDeviceTypeId() {
		return sourceDeviceTypeId;
	}

	public void setSourceDeviceTypeId(String sourceDeviceTypeId) {
		this.sourceDeviceTypeId = sourceDeviceTypeId;
	}

	public String getTargetDeviceId() {
		return targetDeviceId;
	}

	public void setTargetDeviceId(String targetDeviceId) {
		this.targetDeviceId = targetDeviceId;
	}

	public String getTargetDeviceKey() {
		return targetDeviceKey;
	}

	public void setTargetDeviceKey(String targetDeviceKey) {
		this.targetDeviceKey = targetDeviceKey;
	}

	public String getTargetDeviceTypeId() {
		return targetDeviceTypeId;
	}

	public void setTargetDeviceTypeId(String targetDeviceTypeId) {
		this.targetDeviceTypeId = targetDeviceTypeId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getRn() {
		return rn;
	}

	public void setRn(Integer rn) {
		this.rn = rn;
	}

	public String getInfoTrxUtfi() {
		return infoTrxUtfi;
	}

	public void setInfoTrxUtfi(String infoTrxUtfi) {
		this.infoTrxUtfi = infoTrxUtfi;
	}

	public String getInfoReserveTrxId() {
		return infoReserveTrxId;
	}

	public void setInfoReserveTrxId(String infoReserveTrxId) {
		this.infoReserveTrxId = infoReserveTrxId;
	}

	public String getRechargeMsisdn() {
		return rechargeMsisdn;
	}

	public void setRechargeMsisdn(String rechargeMsisdn) {
		this.rechargeMsisdn = rechargeMsisdn;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionInfo [transactionId=").append(transactionId).append(", brandId=").append(brandId)
				.append(", source=").append(source).append(", destiny=").append(destiny).append(", tsTransaction=")
				.append(tsTransaction).append(", type=").append(type).append(", result=").append(result)
				.append(", txInterface=").append(txInterface).append(", currencyId=").append(currencyId)
				.append(", movementQuantity=").append(movementQuantity).append(", serviceId=").append(serviceId)
				.append(", sourceDeviceId=").append(sourceDeviceId).append(", sourceDeviceKey=").append(sourceDeviceKey)
				.append(", sourceDeviceTypeId=").append(sourceDeviceTypeId).append(", targetDeviceId=")
				.append(targetDeviceId).append(", targetDeviceKey=").append(targetDeviceKey)
				.append(", targetDeviceTypeId=").append(targetDeviceTypeId).append(", documentId=").append(documentId)
				.append(", errorCode=").append(errorCode).append(", amount=").append(amount)
				.append(", serviceDescription=").append(serviceDescription).append(", serviceType=").append(serviceType)
				.append(", rn=").append(rn).append(", infoTrxUtfi=").append(infoTrxUtfi).append(", infoReserveTrxId=")
				.append(infoReserveTrxId).append(", rechargeMsisdn=").append(rechargeMsisdn).append(", details=")
				.append(details).append("]");
		return builder.toString();
	}

}