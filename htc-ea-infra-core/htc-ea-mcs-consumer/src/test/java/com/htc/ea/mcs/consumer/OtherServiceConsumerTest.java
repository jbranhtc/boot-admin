package com.htc.ea.mcs.consumer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.htc.ea.mcs.consumer.config.ServiceConsumerRestConfig;
import com.htc.ea.mcs.consumer.dto.ApplicationDto;
import com.htc.ea.mcs.consumer.dto.CommandDto;
import com.htc.ea.mcs.consumer.dto.TechnicalProductCatalogDto;
import com.htc.ea.mcs.consumer.dto.mfs.AccountInfoResponse;
import com.htc.ea.mcs.consumer.dto.mfs.CreditParty;
import com.htc.ea.mcs.consumer.dto.mfs.DebitParty;
import com.htc.ea.mcs.consumer.dto.mfs.LastTransactionsResponse;
import com.htc.ea.mcs.consumer.dto.mfs.TransactionInfo;
import com.htc.ea.mcs.consumer.util.AppUtil;
import com.htc.ea.util.configuration.UtilAppConfig;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.parsing.ParsingUtil;
import com.htc.ea.util.util.TransactionIdUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= {UtilAppConfig.class,ServiceConsumerRestConfig.class})
public class OtherServiceConsumerTest {
		
	@Autowired
	private ConsumerRestHelper consumerRestHelper;
	@Autowired
	private RestUtil restUtil;
	private final String gateway = "PTO ServiceConsumer Gateway";
	@Autowired
	private AppUtil appUtil;
	@Autowired
	private ParsingUtil parsingUtil;
	private static ObjectMapper objectMapper;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		OtherRunArguments.setupConfig();
		objectMapper = new ObjectMapper();
	}

	private void getAsFormattedJsonString(Object object) {
	    ObjectMapper mapper = new ObjectMapper();
	    try
	    {
	        String res = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
	        System.out.println("json => " + res);
	    }
	    catch (JsonProcessingException e)
	    {
	        e.printStackTrace();
	    }
	} 
	
	@SuppressWarnings("unused")
	private byte[] convertToByteArray(String data) {
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		ObjectOutputStream out = null;		
	    try {
	    	out = new ObjectOutputStream(byteOut);
			out.writeObject(data);
			return byteOut.toByteArray();
		} catch (IOException e) {
			System.out.println("Error parsing map2byte[] => ");
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unused")
	private void parsingManual(String response) throws JsonProcessingException, IOException {
//		byte[] bytes = convertToByteArray(response);
		JsonNode root = objectMapper.readTree(response);
//		root.get
		JsonNode arrayNode = objectMapper.createArrayNode().add("one").add("two");
		// acquire reader for the right type
		ObjectReader reader = objectMapper.readerFor(new TypeReference<List<Map<String, Object>>>() {
		});
		// use it
		List<GenericDto> list = reader.readValue(arrayNode);
		System.out.println("Lista: " + list.size());		
	}
	
	@SuppressWarnings("unchecked")
	private List<LastTransactionsResponse> fillTransactionInfoResponse(List<Object> response) {
		List<LastTransactionsResponse> lastTransactionsResponseList = new ArrayList<>();
		for (Object obj: response) {
			Map<String, Object> objMap = (Map<String, Object>) obj;
			LastTransactionsResponse lastTransactionsResponse = new LastTransactionsResponse();
			fillTransactionInfo(lastTransactionsResponse, objMap);
			fillDebitParty(lastTransactionsResponse, objMap);
			fillCreditParty(lastTransactionsResponse, objMap);
			Object metadataObj = objMap.get("metadata");
			fillMetadata(lastTransactionsResponse, metadataObj);
			lastTransactionsResponseList.add(lastTransactionsResponse);
		}
		return lastTransactionsResponseList;
	}
	
	@SuppressWarnings({ "static-access" })
	private void fillTransactionInfo(LastTransactionsResponse lastTransactionsResponse, Map<String, Object> data) {
		lastTransactionsResponse.setAmount(appUtil.castingData(data.get("amount"), Double.class, Boolean.FALSE, null));
		lastTransactionsResponse.setCurrency(appUtil.castingData(data.get("currency"), Integer.class, false, null));
		lastTransactionsResponse.setType(appUtil.castingData(data.get("type"), String.class, false, null));
		lastTransactionsResponse.setTransactionStatus(appUtil.castingData(data.get("transactionStatus"), Integer.class, false, null));
		lastTransactionsResponse.setRequestDate(appUtil.castingData(data.get("requestDate"), Date.class, false, 13));
		lastTransactionsResponse.setTransactionReference(appUtil.castingData(data.get("transactionReference"), Long.class, false, null));		
	}
	
	@SuppressWarnings({ "static-access", "unchecked" })
	private void fillDebitParty(LastTransactionsResponse lastTransactionsResponse, Object containerObj) {
		if (containerObj!=null) {
			Map<String, Object> containerMap = appUtil.castingData(containerObj, HashMap.class, Boolean.TRUE, null);
			List<Object> debitPartyObjList = (List<Object>) containerMap.get("debitParty");
			for (Object debitPartyObjMap: debitPartyObjList) {
				Map<String, Object> debitPartyMap = appUtil.castingData(debitPartyObjMap, HashMap.class, Boolean.TRUE, null);
				String debitPartyKey = appUtil.castingData(debitPartyMap.get("key"), String.class, Boolean.FALSE, null);
				Long debitPartyValue = appUtil.castingData(debitPartyMap.get("value"), Long.class, Boolean.FALSE, null);
				if (debitPartyKey!=null && debitPartyValue!=null) {
					DebitParty debitParty = new DebitParty();
					debitParty.setKey(debitPartyKey);
					debitParty.setValue(debitPartyValue);
					lastTransactionsResponse.getDebitParty().add(debitParty);
				}
			}	
		}
	}	
	
	@SuppressWarnings({ "static-access", "unchecked" })
	private void fillCreditParty(LastTransactionsResponse lastTransactionsResponse, Object containerObj) {
		if (containerObj!=null) {
			Map<String, Object> containerMap = appUtil.castingData(containerObj, HashMap.class, Boolean.TRUE, null);
			List<Object> creditPartyObjList = (List<Object>) containerMap.get("creditParty");
			for (Object creditPartyObjMap: creditPartyObjList) {
				Map<String, Object> creditPartyMap = appUtil.castingData(creditPartyObjMap, HashMap.class, Boolean.TRUE, null);
				String creditPartyKey = appUtil.castingData(creditPartyMap.get("key"), String.class, Boolean.FALSE, null);
				Long creditPartyValue = appUtil.castingData(creditPartyMap.get("value"), Long.class, Boolean.FALSE, null);
				if (creditPartyKey!=null && creditPartyValue!=null) {
					CreditParty creditParty = new CreditParty();
					creditParty.setKey(creditPartyKey);
					creditParty.setValue(creditPartyValue);
					lastTransactionsResponse.getCreditParty().add(creditParty);
				}
			}	
		}
	}
	
	@SuppressWarnings("unchecked")
	private void fillMetadata(LastTransactionsResponse lastTransactionsResponse, Object metadataObj) {
		if (metadataObj!=null) {
			List<Object> metadataObjList = (List<Object>) metadataObj;
			for (Object metadata: metadataObjList) {
				Map<String, Object> metadataMap = (Map<String, Object>) metadata;
				String transactionInfoKey = (String) metadataMap.get("key");
				if (transactionInfoKey!=null && transactionInfoKey.equals("transactionInfo")) {
					Map<String, Object> transactionInfoMap = (Map<String, Object>) metadataMap.get("value");						
					String details = (String) transactionInfoMap.get("DETAILS");
					TransactionInfo transactionInfo = new TransactionInfo();
					transactionInfo.setDetails(details);
					lastTransactionsResponse.setTransactionInfo(transactionInfo);
				}
			}	
		}
	}
	
	
	@Test
	public void testMFSForTransactions2() throws JsonProcessingException, IOException {
		TransactionIdUtil.begin();
		String host = "172.16.208.93:8007";
		String uri = "/development/retrieves/transactions/msisdn/{msisdn}/limit/latest";
//		host = "localhost:8090";
//		uri = "";
		String resource = "http://"+host+uri;
		ServiceResponse<List<LastTransactionsResponse>> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "73005590");
		data.put("limit", "10");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
//		serviceRequest.getParameters().put("Content-Type","application/json;charset=UTF-8");
//		serviceRequest.getParameters().put("test-rubenovo", "Ruben 5K");
		serviceRequest.getParameters().put("sessionid", "78EAA1B403A14D7984E1");
//		serviceRequest.getParameters().put("Accept-Encoding", "gzip,deflate");
		List<LastTransactionsResponse> responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeCollectionService(serviceRequest, LastTransactionsResponse[].class);
			responseData = serviceResponse.getData();
			getAsFormattedJsonString(responseData);
//			System.out.println("Parseo hechizo ==> \n" + parsingUtil.convertObjectToJsonPretty(fillTransactionInfoResponse(responseData)));
//			parsingManual(responseData);
//			System.out.println("Don 5k =========> " + responseData);
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			System.out.println("HttpHeaders from ServiceException: " +  Arrays.toString(e.getAttributes().entrySet().toArray()));
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
//		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMFSForTransactions() throws JsonProcessingException, IOException {
		TransactionIdUtil.begin();
		String host = "apigsma.mts.tigo.net.bo";
		String uri = "/ws_api-gsma/v0.15_beta/accounts/msisdn/73005590/transactions";
		host = "localhost:8090";
		String resource = "http://"+host+uri;
		ServiceResponse<List<Object>> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
//		data.put("&limit", "100");
//		data.put("&offset", "0");
//		data.put("&fromDateTime", "2017-10-13%2000:00:00");
//		data.put("&toDateTime", "2017-12-12%2000:00:00");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("Content-Type","application/json;charset=UTF-8");
//		serviceRequest.getParameters().put("test-rubenovo", "Ruben 5K");
//		serviceRequest.getParameters().put("Authorization", "Bearer 72cfc0da4a7c05deecc154a76d394101");
//		serviceRequest.getParameters().put("Accept-Encoding", "gzip,deflate");
		List<Object> responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeCollectionService(serviceRequest, Object[].class);
			responseData = serviceResponse.getData();
//			getAsFormattedJsonString(responseData);
			System.out.println("Parseo hechizo ==> \n" + parsingUtil.convertObjectToJsonPretty(fillTransactionInfoResponse(responseData)));
//			parsingManual(responseData);
//			System.out.println("Don 5k =========> " + responseData);
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			System.out.println("HttpHeaders from ServiceException: " +  Arrays.toString(e.getAttributes().entrySet().toArray()));
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
//		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testMCSServiceConsumerGetForBuilderQueryURIForMFS2() {
		TransactionIdUtil.begin();
		String plainCreds = "middleware:client_secret";
		byte[] plainCredsBytes = plainCreds.getBytes(Charset.forName("UTF-8"));
	    byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
	    String base64Creds = new String(base64CredsBytes);
		String resource = "https://apigsma.mts.tigo.net.bo/oauth2_provider/v1.0_beta/manager/authorize";
		ServiceResponse<String> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("&grant_type", "password");
		data.put("&scope", "write");
		data.put("&password", "4321");
		data.put("&username", "60094343");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "Victorio 5K");
		serviceRequest.getParameters().put("Authorization", "Basic " + "bWlkZGxld2FyZTpjbGllbnRfc2VjcmV0");
//		serviceRequest.getParameters().put("Accept-Encoding", "gzip,deflate");
		serviceRequest.getParameters().put("Content-Type", "application/x-www-form-urlencoded");
		Object responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, String.class);
			responseData = serviceResponse.getData();
			System.out.println("Don 5k =========> " + responseData);
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetForBuilderQueryURIForMFS() {
		TransactionIdUtil.begin();
		String resource = "https://apigsma.mts.tigo.net.bo/oauth2_provider/v1.0_beta/token/authorize";
		ServiceResponse<Object> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("&grant_type", "password");
		data.put("&scope", "write");
		data.put("&password", "1234");
		data.put("&username", "69147514");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "Victorio 5K");
		serviceRequest.getParameters().put("Authorization", "Bearer bWlkZGxld2FyZTpjbGllbnRfc2VjcmV0");
//		serviceRequest.getParameters().put("Accept-Encoding", "gzip,deflate");
//		serviceRequest.getParameters().put("Content-Type", "application/x-www-form-urlencoded");
		Object responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, Object.class);
			responseData = serviceResponse.getData();
			System.out.println("Don 5k =========> " + responseData);
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetForBuilderTemplateURI() {
		TransactionIdUtil.begin();
		String resource = "https://apigsma.mts.tigo.net.bo/ws_api-gsma/v0.15_beta/accounts/msisdn/{msisdn}/accountinfo";
		ServiceResponse<AccountInfoResponse> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "60094343");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		serviceRequest.getParameters().put("Authorization", "Bearer cd922d7660207b857500015cff7c85d8");
		serviceRequest.getParameters().put("Accept-Encoding", "gzip,deflate");
		AccountInfoResponse responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, AccountInfoResponse.class);		
			responseData = serviceResponse.getData();
			System.out.println("Don 5k =========> " + responseData);
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetForBuilderURIFull() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/{msisdn}/{offering}/info/get";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "71272525");
		data.put("offering", "4050");
		data.put("&wallet", "DIVERTIGO");
		data.put("&account", "1234564899");
		data.put("&msisdn", "71272525");
		data.put("&remark", "All params sent");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetListForBuilderURIFull() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/{msisdn}/{offering}/info/getList";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		Map<String, String> data = new HashMap<>();
		data.put("msisdn", "71272525");
		data.put("offering", "4050");
		data.put("&wallet", "DIVERTIGO");
//		data.put("&account", "1234564899");
//		data.put("&msisdn", "71272525");
//		data.put("&remark", "All params sent");
		ServiceRequest<Map<String, String>> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGet() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:5001/pto/development/v1/technical-product-catalog";
		String data = "?typeCatalog=1&nameCatalog=Command All PTO";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>();
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;		
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);		
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());	
//				responseData = (TechnicalProductCatalogDto) e.getResponse().getData();
//				serviceResponse = e.getResponse();
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerGetList() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogGetList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse =  consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());
			if (responseData!=null) {
				System.out.println("responseData size => " + responseData.size());
				for (CommandDto command: responseData) {
					System.out.println(command);
				}
			}
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testMCSServiceConsumerPost() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogPost";
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>(); 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
//		serviceRequest.setData("{\"test\":\"hola\"}");
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		TechnicalProductCatalogDto responseData = null;
		boolean isError = false;
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());				
		}
		TransactionIdUtil.end();
	}	
	
	@Test
	public void testMCSServiceConsumerPostList() {
		TransactionIdUtil.begin();
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogPostList2";
		ServiceResponse<List<CommandDto>> serviceResponse = null; 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.POST);
		serviceRequest.setData(null);
		serviceRequest.setCategory("consumer");
		serviceRequest.setEndUser("71272525");
		serviceRequest.setAccessToken("123456789");
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, gateway);
		serviceRequest.getParameters().put("test", "test");
		List<CommandDto> responseData = null;
		boolean isError = false;
		try {
			serviceResponse =  consumerRestHelper.executeCollectionService(serviceRequest, CommandDto[].class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse()!=null) {
				System.out.println("NotData found: " + e.getResponse().getData());
				System.out.println("operationRefId: " + e.getResponse().getOperationRefId());
				System.out.println("httpCode: " + e.getResponse().getHttpCode());
				System.out.println("serviceCode: " + e.getResponse().getServiceCode());
				System.out.println("serviceDescription: " + e.getResponse().getServiceDescription());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
				System.out.println("httpHeaders: " + e.getResponse().getHttpHeaders());				
			}
		}
		System.out.println("\n responseData => " + responseData);
		if (!isError) {
			System.out.println("Data found: " + serviceResponse.getData());
			System.out.println("operationRefId: " + serviceResponse.getOperationRefId());
			System.out.println("httpCode: " + serviceResponse.getHttpCode());
			System.out.println("serviceCode: " + serviceResponse.getServiceCode());
			System.out.println("serviceDescription: " + serviceResponse.getServiceDescription());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + ", untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());
			System.out.println("httpHeaders: " + serviceResponse.getHttpHeaders());
			if (responseData!=null) {
				System.out.println("responseData size => " + responseData.size());
				for (CommandDto command: responseData) {
					System.out.println(command);
				}
			}
		}
		TransactionIdUtil.end();
	}
	
	@Test
	public void testRestGet() {
		String resource = "http://localhost:5001/pto/development/v1/technical-product-catalog";
		String data = "?typeCatalog=0&nameCatalog=CommandAllPTO";
		ResponseEntity<TechnicalProductCatalogDto> response = null;
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		try {
			response = restUtil.sendGet(resource, data, originRefId+":"+"3"+":"+gateway, TechnicalProductCatalogDto.class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		TechnicalProductCatalogDto res = response.getBody();
		System.out.println("responseData => " + res);
	}
	
	@SuppressWarnings("unused")
	@Test
	public void testRestGetList() {
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		ResponseEntity<CommandDto[]> response = null;
		List<CommandDto> responseData = null;
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		try {
			response = restUtil.sendGetList(resource, data, originRefId+":"+"3"+":"+gateway, CommandDto[].class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		if (responseData!=null) responseData = Arrays.asList(response.getBody());
		System.out.println("responseData => " + responseData);
		if (responseData!=null) {
			System.out.println("responseData size => " + responseData.size());
			for (CommandDto command: responseData) {
				System.out.println(command);
			}
		}
	}
		
	@Test
	public <T> void testRestGetList2() throws URISyntaxException {
		String resource = "http://localhost:8090/pto/development/v1/technical-product-catalogList";
		String data = "?typeCatalog=0&nameCatalog=Command All PTO";
		resource = "http://localhost:8090/QueryEventLogService/Application";
		data = "/bo";
		String originRefId = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date()).toString();
		List<ApplicationDto> responseData = null;
		ResponseEntity<ApplicationDto[]> response = null;
		try {
			response = restUtil.sendGetList2(resource + data, null, originRefId+":"+"3"+":"+gateway, ApplicationDto[].class, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    if (response != null)
	    	responseData = Arrays.asList(response.getBody());
		System.out.println("responseData size() => " + responseData.size());
		for (ApplicationDto app: responseData) {
			System.out.println(app);
		}
	}	
}