package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "value" })
public class CreditParty {

	@JsonProperty("key")
	private String key;

	@JsonProperty("value")
	private Long value;

	@JsonProperty("key")
	public String getKey() {
		return key;
	}

	@JsonProperty("key")
	public void setKey(String key) {
		this.key = key;
	}

	@JsonProperty("value")
	public Long getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(Long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CreditParty [key=" + key + ", value=" + value + "]";
	}

}
