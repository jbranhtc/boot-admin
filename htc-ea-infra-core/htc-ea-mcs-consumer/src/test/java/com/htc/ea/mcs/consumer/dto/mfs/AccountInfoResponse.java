package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.htc.ea.mcs.consumer.dto.mfs.Metadata;
import com.htc.ea.mcs.consumer.dto.mfs.Name;

@JsonPropertyOrder({
"metadata",
"sub_status_id",
"sub_status",
"name",
"status"
})
public class AccountInfoResponse extends ErrorResponse {

	@JsonProperty("metadata")
	private Metadata metadata;

	@JsonProperty("sub_status_id")
	private Integer subStatusId;

	@JsonProperty("sub_status")
	private String subStatus;

	@JsonProperty("name")
	private Name name;

	@JsonProperty("status")
	private String status;

	@JsonProperty("metadata")
	public Metadata getMetadata() {
		return metadata;
	}

	@JsonProperty("metadata")
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	public Integer getSubStatusId() {
		return subStatusId;
	}

	public void setSubStatusId(Integer subStatusId) {
		this.subStatusId = subStatusId;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	@JsonProperty("name")
	public Name getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(Name name) {
		this.name = name;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AccountInfoResponse [metadata=" + metadata + ", subStatusId=" + subStatusId + ", subStatus=" + subStatus
				+ ", name=" + name + ", status=" + status + "]";
	}

}
