package com.htc.ea.mcs.consumer.dto;

public class StatementDto {

	private Integer sequence;
	private Boolean mandatory;
	private String order;
	
	public Boolean getMandatory() {
		return mandatory;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StatementDto [sequence=").append(sequence).append(", mandatory=").append(mandatory)
				.append(", order=").append(order).append("]");
		return builder.toString();
	}
}