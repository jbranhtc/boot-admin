package com.htc.ea.mcs.consumer;

public final class RunArguments {

	public static void setupConfig() {
		StringBuilder sb = new StringBuilder();
		sb.append("5001").append(",");
		sb.append("development").append(",");
		sb.append("C:\\logsBoot\\log").append(",");
		sb.append("D:\\devsoft\\Projects-TigoBO-TSB\\tigo-bo-sources-mcs\\apps\\htc-infra-mcs-properties\\logback\\tigo-ea-pto-admin.production-logback.xml").append(",");
		sb.append("D:\\devsoft\\Projects-TigoBO-TSB\\tigo-bo-sources-mcs\\apps\\htc-infra-mcs-properties\\production\\tigo-ea-pto-admin.development.yml");
		String[] arguments = sb.toString().split(",");
//		System.setProperty("server.port", arguments[0]);
		System.setProperty("app.env", arguments[1]);
		System.setProperty("app.log", arguments[2]);
		System.setProperty("logging.config", arguments[3]);
		System.setProperty("spring.config.location", arguments[4]);
	}	
}