package com.htc.ea.mcs.consumer.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "platform", "product", "action", "enabled", "commands"})
@JsonIgnoreProperties(value = { "subCommandDtoList", "subCommandTramaOriginMap", "subCommandTramaFormatMap" })
public class CommandDto {
	
	private String platform;	
	private String command;
	private String action;
	private Boolean enabled;
	private List<SubCommandDto> subCommandDtoList;
	private Map<String, List<String>> subCommandTramaOriginMap;
	private Map<String, List<String>> subCommandTramaFormatMap;	
	private Map<String, List<StatementDto>> commands;
	
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	@JsonProperty("product")
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public List<SubCommandDto> getSubCommandDtoList() {
		if (subCommandDtoList==null) {
			subCommandDtoList = new ArrayList<>();
		}
		return subCommandDtoList;
	}
	public Map<String, List<String>> getSubCommandTramaOriginMap() {
		if (subCommandTramaOriginMap==null) {
			subCommandTramaOriginMap = new HashMap<>();
		}
		return subCommandTramaOriginMap;
	}
	public Map<String, List<String>> getSubCommandTramaFormatMap() {
		if (subCommandTramaFormatMap==null) {
			subCommandTramaFormatMap = new HashMap<>();
		}
		return subCommandTramaFormatMap;
	}
	@JsonProperty("commands")
	public Map<String, List<StatementDto>> getCommands() {
		if (commands==null) {
			commands = new HashMap<>();
		}
		return commands;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommandDto [platform=").append(platform).append(", command=").append(command)
				.append(", action=").append(action).append(", enabled=").append(enabled).append(", subCommandDtoList=")
				.append(subCommandDtoList).append(", subCommandTramaOriginMap=").append(subCommandTramaOriginMap)
				.append(", subCommandTramaFormatMap=").append(subCommandTramaFormatMap).append(", commands=")
				.append(commands).append("]");
		return builder.toString();
	}
}