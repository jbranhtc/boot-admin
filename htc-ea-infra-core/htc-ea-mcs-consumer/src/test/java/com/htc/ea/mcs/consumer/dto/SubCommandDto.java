package com.htc.ea.mcs.consumer.dto;

public class SubCommandDto {

	private Integer id;
	private String module;
	private String subModule;
	private Integer sequence;
	private String key;
	private String value;
	private Boolean enabled;
	private Boolean mandatory;
	private String subCommandFull;
	
	public void setSubCommandFull() {
		StringBuilder sb = new StringBuilder();
		try {
			if (module!=null && !module.isEmpty()) sb.append(module.trim());
			if (subModule!=null && !subModule.isEmpty()) sb.append(" ").append(subModule.trim());
			if (key!=null && !key.isEmpty()) sb.append(" ").append(key.trim());
			if (value!=null && !value.isEmpty()) sb.append(" ").append(value.trim());			
		} catch (Exception e) {
			e.printStackTrace();
		}
		subCommandFull = sb.toString();		
	}
	
	public String getSubCommandFull() {
		return subCommandFull;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getSubModule() {
		return subModule;
	}
	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public Boolean getMandatory() {
		return mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SubCommandDto [id=").append(id).append(", module=").append(module).append(", subModule=")
				.append(subModule).append(", sequence=").append(sequence).append(", key=").append(key)
				.append(", value=").append(value).append(", enabled=").append(enabled).append(", mandatory=")
				.append(mandatory).append(", subCommandFull=").append(subCommandFull).append("]");
		return builder.toString();
	}
}