package com.htc.ea.mcs.consumer.dto.mfs;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metadata {

	@JsonProperty("client-id")
	private Integer clientId;

	@JsonProperty("client-role")
	private String clientRole;

	@JsonProperty("status-id")
	private Integer status_id;

	@JsonProperty("kyc")
	private Integer kyc;

	@JsonProperty("level-id")
	private Integer levelId;

	@JsonProperty("document-type")
	private String documentType;

	@JsonProperty("document-number")
	private String documentNumber;

	@JsonProperty("creation-date")
	private String creationDate;

	@JsonProperty("birth-date")
	private String birthDate;

	@JsonProperty("language")
	private String language;

	@JsonProperty("contact-device-id")
	private Integer contact_device_id;

	@JsonProperty("subscriber-id")
	private Integer subscriberId;

	@JsonProperty("brand-id")
	private Integer brandId;

	@JsonProperty("name")
	private String name;

	@JsonProperty("surname")
	private String surname;

	@JsonProperty("email")
	private String email;

	@JsonProperty("status-description")
	private String statusDescription;

	@JsonProperty("status-type")
	private String statusType;

	@JsonProperty("kyc-description")
	private String kycDescription;

	@JsonProperty("level-description")
	private String levelDescription;

	@JsonProperty("msisdn")
	private String[] msisdn;

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientRole() {
		return clientRole;
	}

	public void setClientRole(String clientRole) {
		this.clientRole = clientRole;
	}

	public Integer getStatus_id() {
		return status_id;
	}

	public void setStatus_id(Integer status_id) {
		this.status_id = status_id;
	}

	public Integer getKyc() {
		return kyc;
	}

	public void setKyc(Integer kyc) {
		this.kyc = kyc;
	}

	public Integer getLevelId() {
		return levelId;
	}

	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getContact_device_id() {
		return contact_device_id;
	}

	public void setContact_device_id(Integer contact_device_id) {
		this.contact_device_id = contact_device_id;
	}

	public Integer getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(Integer subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public String getKycDescription() {
		return kycDescription;
	}

	public void setKycDescription(String kycDescription) {
		this.kycDescription = kycDescription;
	}

	public String getLevelDescription() {
		return levelDescription;
	}

	public void setLevelDescription(String levelDescription) {
		this.levelDescription = levelDescription;
	}

	public String[] getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String[] msisdn) {
		this.msisdn = msisdn;
	}

	@Override
	public String toString() {
		return "Metadata [clientId=" + clientId + ", clientRole=" + clientRole + ", status_id=" + status_id + ", kyc="
				+ kyc + ", levelId=" + levelId + ", documentType=" + documentType + ", documentNumber=" + documentNumber
				+ ", creationDate=" + creationDate + ", birthDate=" + birthDate + ", language=" + language
				+ ", contact_device_id=" + contact_device_id + ", subscriberId=" + subscriberId + ", brandId=" + brandId
				+ ", name=" + name + ", surname=" + surname + ", email=" + email + ", statusDescription="
				+ statusDescription + ", statusType=" + statusType + ", kycDescription=" + kycDescription
				+ ", levelDescription=" + levelDescription + ", msisdn=" + Arrays.toString(msisdn) + "]";
	}
}