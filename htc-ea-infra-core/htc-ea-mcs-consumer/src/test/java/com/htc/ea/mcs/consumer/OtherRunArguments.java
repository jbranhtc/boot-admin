package com.htc.ea.mcs.consumer;

@SuppressWarnings("unused")
public final class OtherRunArguments {
	
	static {
		//for localhost testing only
	    javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
	    new javax.net.ssl.HostnameVerifier(){
	        public boolean verify(String hostname,
	                javax.net.ssl.SSLSession sslSession) {
	            if (hostname.equals("apigsma.mts.tigo.net.bo")) {
	                return true;
	            }
	            return false;
	        }
	    });	
	    String trustStore = "D:\\devsoft\\Projects-TigoBO-TSB\\tigo-bo-sources-mcs\\apps\\htc-infra-mcs-properties\\certificates\\htc-mfs2\\truststore.jks";
	    String trustStorePassword = "truststorepass";
	    String trustStoreType = "JKS";
//	    System.setProperty("javax.net.debug", "all");
//		System.setProperty("javax.net.ssl.trustStore", trustStore);
//        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
//        System.setProperty("javax.net.ssl.trustStoreType", trustStoreType);
   }
	
	public static void setupConfig() {
		StringBuilder sb = new StringBuilder();
		sb.append("5001").append(",");
		sb.append("development").append(",");
		sb.append("C:\\logsBoot\\log").append(",");
		sb.append("D:\\devsoft\\Projects-TigoBO-TSB\\tigo-bo-sources-mcs\\apps\\bo-tigo-provisioning\\tigo-bo-pto-config\\logback\\tigo-ea-pto-admin.production-logback.xml").append(",");
		sb.append("D:\\devsoft\\Projects-TigoBO-TSB\\tigo-bo-sources-mcs\\apps\\bo-tigo-provisioning\\tigo-bo-pto-config\\properties\\tigo-ea-pto-admin.development.yml");
		String[] arguments = sb.toString().split(",");
//		System.setProperty("server.port", arguments[0]);
		System.setProperty("app.env", arguments[1]);
		System.setProperty("app.log", arguments[2]);
		System.setProperty("logging.config", arguments[3]);
		System.setProperty("spring.config.location", arguments[4]);
	}	
}