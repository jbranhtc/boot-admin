package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;
import com.htc.ea.mcs.consumer.dto.mfs.ErrorParameters;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"errorCategory",
"errorCode",
"errorDescription",
"errorDateTime",
"errorParameters"
})
public class ErrorResponse {

	@JsonProperty("errorCategory")
	private String errorCategory;

	@JsonProperty("errorCode")
	private String errorCode;

	@JsonProperty("errorDescription")
	private String errorDescription;

	@JsonProperty("errorDateTime")
	private String errorDateTime;
	
	@JsonProperty("error_description")
	private String error_description;

	@JsonProperty("error")
	private String error2;

	@JsonProperty("errorParameters")
	private ErrorParameters errorParameters = null;

	public ErrorResponse(String errorCategory, String errorCode, String errorDescription, String errorDateTime,
			ErrorParameters errorParameters) {
		super();
		this.errorCategory = errorCategory;
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.errorDateTime = errorDateTime;
		this.errorParameters = errorParameters;
	}

	public ErrorResponse() {
	}

	@JsonProperty("errorCategory")
	public String getErrorCategory() {
		return errorCategory;
	}

	@JsonProperty("errorCategory")
	public void setErrorCategory(String errorCategory) {
		this.errorCategory = errorCategory;
	}

	@JsonProperty("errorCode")
	public String getErrorCode() {
		return errorCode;
	}

	@JsonProperty("errorCode")
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@JsonProperty("errorDescription")
	public String getErrorDescription() {
		return errorDescription;
	}

	@JsonProperty("errorDescription")
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@JsonProperty("errorDateTime")
	public String getErrorDateTime() {
		return errorDateTime;
	}

	@JsonProperty("errorDateTime")
	public void setErrorDateTime(String errorDateTime) {
		this.errorDateTime = errorDateTime;
	}

	@JsonProperty("errorParameters")
	public ErrorParameters getErrorParameters() {
		return errorParameters;
	}

	/*@JsonProperty("errorParameters")
	public void setErrorParameters(ErrorParameters errorParameters) {
		this.errorParameters = errorParameters;
	}*/
	
	public String getError_description() {
		return error_description;
	}

	public void setError_description(String error_description) {
		this.error_description = error_description;
	}

	public String getError2() {
		return error2;
	}

	public void setError2(String error2) {
		this.error2 = error2;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ErrorResponse [errorCategory=");
		builder.append(errorCategory);
		builder.append(", errorCode=");
		builder.append(errorCode);
		builder.append(", errorDescription=");
		builder.append(errorDescription);
		builder.append(", errorDateTime=");
		builder.append(errorDateTime);
		builder.append(", error_description=");
		builder.append(error_description);
		builder.append(", error2=");
		builder.append(error2);
		builder.append(", errorParameters=");
		builder.append(errorParameters);
		builder.append("]");
		return builder.toString();
	}

	@JsonSetter("errorParameters")
    public void setErrorParameters(JsonNode errorParameters) {
        if (errorParameters != null) {
           if (errorParameters.isObject()) {
        	   JsonNode aux = errorParameters.get("serverError");
        	   if(aux != null){
	               String serverError = errorParameters.get("serverError").asText();
	               this.errorParameters = new ErrorParameters(serverError);
        	   }
            }
        }
    }
}