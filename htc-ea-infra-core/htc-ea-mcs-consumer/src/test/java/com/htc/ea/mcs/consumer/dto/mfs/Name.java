package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
"name",
"last_name",
"first_name",
"nativeName",
"title"
})
public class Name {
	
	@JsonProperty("name")
	private String name;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("nativeName")
	private String nativeName;

	@JsonProperty("title")
	private String title;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("last_name")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("first_name")
	public String getFirstName() {
		return firstName;
	}

	@JsonProperty("first_name")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("nativeName")
	public String getNativeName() {
		return nativeName;
	}

	@JsonProperty("nativeName")
	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Name [name=" + name + ", lastName=" + lastName + ", firstName=" + firstName + ", nativeName="
				+ nativeName + ", title=" + title + "]";
	}

}
