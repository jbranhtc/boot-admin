package com.htc.ea.mcs.consumer.dto;

public class ApplicationDto  {
		
	private String id;
	private String name;
	private Boolean enabled;
	private String countryId;
	private String countryName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApplicationDto [id=").append(id).append(", name=").append(name).append(", enabled=")
				.append(enabled).append(", countryId=").append(countryId).append(", countryName=").append(countryName)
				.append("]");
		return builder.toString();
	}
}
