package com.htc.ea.mcs.consumer.dto.mfs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"serverError"
})
public class ErrorParameters {

	@JsonProperty("serverError")
	private String serverError;
	
	public ErrorParameters(String serverError) {
		super();
		this.serverError = serverError;
	}

	@JsonProperty("serverError")
	public String getServerError() {
		return serverError;
	}
	
	@JsonProperty("serverError")
	public void setServerError(String key) {
		this.serverError = key;
	}

	@Override
	public String toString() {
		return "ErrorParameters [serverError=" + serverError + "]";
	}

}