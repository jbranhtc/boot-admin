package com.htc.ea.util.cache;

import java.io.Serializable;
import java.util.Date;

public class CachedObject<T> implements Serializable {
	private static final long serialVersionUID = 3423810973455201647L;
	private String key;
	private Date created = new Date();
	private int usage = 0;
	private boolean expired;
	private T object;

	public CachedObject(String key, T object) {
		this.key = key;
		this.object = object;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public int getUsage() {
		return usage;
	}

	public void setUsage(int usage) {
		this.usage = usage;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public T getObject() {
		usage++;
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CachedObject [key=").append(key).append(", created=")
				.append(created).append(", usage=").append(usage)
				.append(", expired=").append(expired).append(", object=")
				.append(object).append("]");
		return builder.toString();
	}

}
