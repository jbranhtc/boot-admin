package com.htc.ea.util.classloading;

import java.util.Map;
import java.util.Set;

public interface ClassFactory {

	public void loadClassesMap(boolean refresh, Set<String> listLoadClass);
	public Map<String, Object> getClassMap();
	public Object getClassLoaded(String className);
	
}
