package com.htc.ea.util.redis.listener;

public interface MessagePublisher {

    void publish(final String message);
}
