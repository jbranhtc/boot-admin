package com.htc.ea.util.configentry;

import java.util.Set;

public interface ConfigurationService {
	
	public void refresh(boolean target);
	public String getValue(String key);
	public Set<String> getValues(String key);
	public String getValueTarget(String key);
	public Set<String> getValuesTarget(String key);
	public boolean isSingleValue(String key);
}