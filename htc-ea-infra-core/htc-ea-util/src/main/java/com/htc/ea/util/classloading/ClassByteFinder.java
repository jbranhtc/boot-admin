package com.htc.ea.util.classloading;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class ClassByteFinder {

	public String url;

	public ClassByteFinder(String currentRoot) {
		this.url = currentRoot;
	}

	public byte[] findClassBytes(String className) {
		String pathName = url + File.separatorChar
				+ className.replace('.', File.separatorChar) + ".class";
		try {
			URL myUrl = new URL(pathName);
			URLConnection connection = myUrl.openConnection();
			InputStream input = connection.getInputStream();
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int data = input.read();
			while (data != -1) {
				buffer.write(data);
				data = input.read();
			}
			input.close();
			byte[] classData = buffer.toByteArray();
			return classData;
		} 
		catch (MalformedURLException e) {
			throw new RuntimeException(e);
		} 
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}
