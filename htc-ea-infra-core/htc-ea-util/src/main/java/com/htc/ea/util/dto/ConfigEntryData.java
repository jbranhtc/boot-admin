package com.htc.ea.util.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ConfigEntryData implements Serializable{
	private static final long serialVersionUID = 893803098100713445L;
	
	private String key;
	private String value;
	private String type;
	private String dataType;
	private boolean singleValue;
	private boolean enabled;
	private Set<String> values = new HashSet<String>();
	
	public ConfigEntryData() {
	}
	
	public ConfigEntryData(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDataType() {
		return dataType;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public boolean isSingleValue() {
		return singleValue;
	}
	
	public void setSingleValue(boolean singleValue) {
		this.singleValue = singleValue;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Set<String> getValues() {
		return values;
	}
	
	public void setValues(Set<String> values) {
		this.values = values;
	}
	
	@Override
	public String toString() {
		return String
				.format("ConfigEntryData [key=%s, value=%s, type=%s, dataType=%s, singleValue=%s, enabled=%s, values=%s]",
						key, value, type, dataType, singleValue, enabled,
						values.toString());
	}
}