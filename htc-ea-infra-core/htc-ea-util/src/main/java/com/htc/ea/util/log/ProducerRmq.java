package com.htc.ea.util.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.htc.ea.util.configuration.RabbitMqConfig;
import com.htc.ea.util.parsing.RabbitTemplateWrapper;
import com.htc.ea.util.util.Constants;

public class ProducerRmq implements LoggingService {

	@Autowired
	private Environment env;
	private RabbitTemplateWrapper rabbitTemplateWrapper;

	public void setRabbitTemplateWrapper(RabbitTemplateWrapper rabbitTemplateWrapper) {
		this.rabbitTemplateWrapper = rabbitTemplateWrapper;
	}

	@Override
	public void log(EventData eventData) {
		rabbitTemplateWrapper.getRabbitTemplateMap().get(Constants.INFRA_LOG).convertAndSend(
				env.getProperty(Constants.LOG_AMQP_EXCHANGE), RabbitMqConfig.getLogRoutingKey(), eventData);
	}
}