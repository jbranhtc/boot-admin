package com.htc.ea.util.log;

/**
 * @Description
 * La utilidad es para permitir a las aplicaciones loggear y obtener la categorias.
 * 
 *
 */
public interface LoggingService {	
	public void log(EventData eventData);	
}