package com.htc.ea.util.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ConfigurationData implements Serializable {
	private static final long serialVersionUID = 6602743800248769445L;
	
	private Long id;
	private String applicationId;
	private String environmentId;
	private String version;
	private int eventLevel;
	private Set<String> eventCategories = new HashSet<String>();
	private Map<String, ConfigEntryData> configEntries = new HashMap<String, ConfigEntryData>();	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getEnvironmentId() {
		return environmentId;
	}
	
	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public int getEventLevel() {
		return eventLevel;
	}
	
	public void setEventLevel(int eventLevel) {
		this.eventLevel = eventLevel;
	}
	
	public Set<String> getEventCategories() {
		return eventCategories;
	}
	
	public void setEventCategories(Set<String> eventCategories) {
		this.eventCategories = eventCategories;
	}
	
	public Map<String, ConfigEntryData> getConfigEntries() {
		return configEntries;
	}
	
	public void setConfigEntries(Map<String, ConfigEntryData> configEntries) {
		this.configEntries = configEntries;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigurationData [id=").append(id)
				.append(", applicationId=").append(applicationId)
				.append(", environmentId=").append(environmentId)
				.append(", version=").append(version).append(", eventLevel=")
				.append(eventLevel).append(", eventCategories=")
				.append(eventCategories).append(", configEntries=")
				.append(configEntries).append("]");
		return builder.toString();
	}
}