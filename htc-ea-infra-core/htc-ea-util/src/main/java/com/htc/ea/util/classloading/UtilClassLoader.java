package com.htc.ea.util.classloading;

import java.util.ArrayList;
import java.util.List;

public class UtilClassLoader extends ClassLoader {

	private String url;

	private List<String> reloadingClasses = new ArrayList<String>();

	public UtilClassLoader(String url) {
		super(UtilClassLoader.class.getClassLoader());
		this.url = url;
	}

	@Override
	public Class<?> loadClass(String className) throws ClassNotFoundException {
		if (!reloadingClasses.contains(className)) {
			return super.loadClass(className);
		}
		return findClass(className);		
	}

	@Override
	public Class<?> findClass(String name) throws ClassNotFoundException {
		ClassByteFinder classByteFinder = new ClassByteFinder(url);
		byte[] classBytes = classByteFinder.findClassBytes(name);
		if (classBytes == null) {
			throw new ClassNotFoundException();
		} 
		else {
			return defineClass(name, classBytes, 0, classBytes.length);
		}
	}

}
