package com.htc.ea.util.util;

import java.net.URL;
import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.JndiDestinationResolver;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.jndi.JndiTemplate;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

public class FactoryBeanUtil {

	private static final Logger logger = LoggerFactory.getLogger(FactoryBeanUtil.class);

	@SuppressWarnings("unchecked")
	public static <T> T createJndiService(String jndi, String factory,
			String provider, Class<T> expectedType) throws Exception {
		JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
		factoryBean.setJndiName(jndi);
		factoryBean.setExpectedType(expectedType);
		if (factory != null && provider != null) {
			Properties jndiEnv = new Properties();
			jndiEnv.put(Context.INITIAL_CONTEXT_FACTORY, factory);
			jndiEnv.put(Context.PROVIDER_URL, provider);
			logger.debug("createJndiService() with JndiEnvironment {}", jndiEnv);
			factoryBean.setJndiEnvironment(jndiEnv);
		}
		try {
			factoryBean.afterPropertiesSet();
		} catch (Exception ex) {
			throw new Exception("Error creating jndiService impl with jndiName = " + jndi, ex);
		}
		return (T) factoryBean.getObject();
	}

	@SuppressWarnings("unchecked")
	public static <T> T createWsService(String wsdl, Class<T> serviceInterface)
			throws Exception {
		JaxWsPortProxyFactoryBean factoryBean = new JaxWsPortProxyFactoryBean();
		try {
			factoryBean.setWsdlDocumentUrl(new URL(wsdl));
			factoryBean.setServiceInterface(serviceInterface);			
			factoryBean.afterPropertiesSet();
		} catch (Exception ex) {
			throw new Exception("Error creating wsService impl with wsdl = "
					+ wsdl, ex);
		}
		return (T) factoryBean.getObject();
	}
	
	public static JndiTemplate getJndiTemplate(String factory, String provider) {
		JndiTemplate jndiTemplate = new JndiTemplate();
		if (factory != null && provider != null) {
			Properties jndiEnv = new Properties();
			jndiEnv.put(Context.INITIAL_CONTEXT_FACTORY, factory);
			jndiEnv.put(Context.PROVIDER_URL, provider);
			logger.debug("createJndiTemplate() with JndiEnvironment {}", jndiEnv);
			jndiTemplate.setEnvironment(jndiEnv);
		}
		return jndiTemplate;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T createJmsResource(String jndi, JndiTemplate jndiTemplate, Class<T> expectedType) throws Exception {
		JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
		factoryBean.setJndiTemplate(jndiTemplate);
		factoryBean.setJndiName(jndi);
		try {
			factoryBean.afterPropertiesSet();
		} catch (Exception ex) {
			throw new RuntimeException("Error creating jmsResource of the jndi = " + jndi, ex);
		}
		T jmsResource = (T) factoryBean.getObject();
		logger.debug("createJmsResource with Jndi {}", jndi);
		return jmsResource;
	}	
	
	private static JndiDestinationResolver jmsDestinationResolver(JndiTemplate jndiTemplate) {
		JndiDestinationResolver resolver = new JndiDestinationResolver();
		resolver.setJndiTemplate(jndiTemplate);
		resolver.setCache(true);
		return resolver;
	}
	
	public static JmsTemplate getJmsTemplate(ConnectionFactory connectionFactory, JndiTemplate jndiTemplate) {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(connectionFactory);
		template.setDestinationResolver(jmsDestinationResolver(jndiTemplate));
		return template;
	}
	
	public static Destination getDestination(String factory, String provider, String jmsConnectionFactory, String jmsQueue, JndiTemplate jndiTemplate){		
		Destination destination = null;
		try {			
			destination = createJmsResource(jmsQueue, jndiTemplate, Destination.class);
		} catch (Exception e) {
			throw new RuntimeException("Error creating jmsDestination of the jndi = " + jmsQueue, e);
		}
		return destination;
	}
}
