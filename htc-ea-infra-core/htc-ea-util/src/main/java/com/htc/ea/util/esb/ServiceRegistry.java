package com.htc.ea.util.esb;

import javax.jws.WebParam;
import javax.jws.WebService;


/**
 * @Description
 * Retorna los servicios para el orquestador.
 * 
 *
 */
@WebService(targetNamespace = ServiceRegistry.TARGET_NAMESPACE, name= ServiceRegistry.DEFAULT_NAME, serviceName = ServiceRegistry.DEFAULT_SERVICE_NAME, portName = ServiceRegistry.DEFAULT_PORT_NAME)
public interface ServiceRegistry {
	
	public static final String EJB_MAPPED_NAME = "ejb/serviceRegistry";
	public static final String ENDPOINT_INTERFACE = "com.tigo.ea.util.esb.ServiceRegistry";
	public static final String TARGET_NAMESPACE = "http://esb.util.ea.tigo.com/";
	public static final String DEFAULT_NAME = "ServiceRegistry";
	public static final String DEFAULT_PORT_NAME = "ServiceRegistryPort";
	public static final String DEFAULT_SERVICE_NAME = "ServiceRegistryWS";

	public ServiceData getService(@WebParam(name="serviceId") ServiceId serviceId);

}
