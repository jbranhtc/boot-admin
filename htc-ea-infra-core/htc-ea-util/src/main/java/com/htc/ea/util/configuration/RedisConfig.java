package com.htc.ea.util.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

import com.htc.ea.util.redis.dao.ConfigurationDataRepository;
import com.htc.ea.util.redis.dao.ConfigurationDataRepositoryImpl;
import com.htc.ea.util.util.Constants;

@Configuration
public class RedisConfig {

	@Autowired
	private Environment env;

	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		try {
			jedisConFactory.setHostName(env.getProperty(Constants.REDIS_CONNECTION_IP));
			jedisConFactory.setPort(Integer.valueOf(env.getProperty(Constants.REDIS_CONNECTION_PORT)));
			return jedisConFactory;
		} catch (Exception e) {
			throw new RuntimeException("Error creating @Bean jedisConnectionFactory");
		}
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new StringRedisSerializer());
		template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
		// template.setHashValueSerializer(new
		// GenericToStringSerializer<Object>(Object.class));
		template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		return template;
	}

	@Bean
	public ConfigurationDataRepository configurationDataRepository() {
		ConfigurationDataRepositoryImpl configurationDataRepository = new ConfigurationDataRepositoryImpl();
		return configurationDataRepository;
	}
}