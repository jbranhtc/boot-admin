package com.htc.ea.util.util;

public interface Constants {
	// Directory de properties
	public static final String DEFAULT_PROPERTIES_FILE = "app.properties";
	public static final String APP_ID_PROPERTY = "application.id";
	public static final String APP_ENVIRONMENT_PROPERTY = "app.env";
	public static final String APP_VERSION_PROPERTY = "application.version";
	public static final String APP_PATH = "app.dir";
	public static final String APP_ENVIRONMENT = "app.env";
	// JNDI DataSource
	public static final String JDBC_DRIVER = "jdbc.driver";
	public static final String JDBC_JNDI = "jdbc.jndi";
	public static final String JDBC_PASSWORD = "jdbc.password";
	public static final String JDBC_URL = "jdbc.url";
	public static final String JDBC_USERNAME = "jdbc.username";
	// Context WLS
	public static final String JMS_WLS_FACTORY = "logging.wls.factory";
	public static final String JMS_WLS_PROVIDER = "logging.wls.provider";
	public static final String LOGGING_IMPL = "logging.impl";
	public static final String LOGGING_ENABLED = "logging.enabled";
	public static final String LOGGING_EVENTLEVEL_ENABLED = "logging.eventlevel.enabled";
	// JNDI Jms
	public static final String JMS_CONNECTION_FACTORY = "logging.jms.cf";
	public static final String JMS_QUEUE = "logging.jms.queue";
	public static final String JMS_QUEUE_CONCURRENCY = "logging.jms.concurrency";
	// REDIS
	public static final String REDIS_CONNECTION_IP = "redis.ip";
	public static final String REDIS_CONNECTION_PORT = "redis.port";
	public static final String REDIS_CHANNEL_NAME = "redis.channel.name";
	// RABBIT_LOG
	public static final String AUDIT_LOG = "audit_log";
	public static final String INFRA_LOG = "infra_log";
	// IMPLEMENTATION LOG
	public static final String FILE_LOG = "file";
	public static final String QUEUE_RABBIT_LOG = "queuerabbit";
	public static final String QUEUE_WLS_LOG = "queuewls";
	// LOGGIN CONNECT RABBITMQ
	public static final String AMQP_ADDRESSES = "rabbitmq.addresses";
	public static final String LOG_AMQP_IP = "logging.amqp.ip";
	public static final String LOG_AMQP_PORT = "logging.amqp.port";
	public static final String AMQP_USER = "logging.amqp.user";
	public static final String AMQP_PASSWORD = "logging.amqp.password";
	// LOGGIN CONFIG RABBITMQ
	public static final String LOG_AMQP_QUEUE_NAME = "logging.queue.name";
	public static final String LOG_AMQP_EXCHANGE = "logging.queue.exchange";
	public static final String LOG_AMQP_BINDING_WITH = "logging.queue.routing.key";
	// LOGGIN CONNECT RABBITMQ
	public static final String AUDIT_QUEUE_ENABLED = "audit.queue.enabled";
	public static final String AUDIT_AMQP_IP = "audit.amqp.ip";
	public static final String AUDIT_AMQP_PORT = "audit.amqp.port";
	// LOGGIN CONFIG RABBITMQ
	public static final String AUDIT_AMQP_QUEUE_NAME = "audit.queue.name";
	public static final String AUDIT_AMQP_EXCHANGE = "audit.queue.exchange";
	public static final String AUDIT_AMQP_BINDING_WITH = "audit.queue.routing.key";
	// GENERAL CONFIGURATION
	public static final String GENERAL_CONFIGURATION = "target.platform";
	// ROUTER CONFIGURATION
	public static final String ROUTING_TYPE = "routing.type";
	public static final String ROUTING_LEGACY_SYSTEM = "routing.target.legacysystem";
	public static final String ROUTING_NEW_SYSTEM = "routing.target.newsystem";
	public static final String ROUTING_ACCEPTLIST = "routing.type";
	public static final String ROUTING_APP_ID = "routing";
	public static final String ROUTING_MESSAGE_REDIS = "RoutingData";
	// Headers for request/response para trazabilidad de log
	public static final String OPERATION_REF_ID = "operation-reference-id";
	public static final String UNTIL_LAST_SEQUENCE = "until-last-sequence";
	public static final String X_USER_AGENT_KEY = "x-user-agent";
	public static final String X_USER_AGENT_VALUE = "%s - HighTech Consulting El Salvador, CA";
	public static final String CONSUMER_LOCATION = "consumer-location";
	// Headers for response success|failed
	public final static String SERVICE_DESCRIPTION = "service-description";
	public final static String SERVICE_CODE = "service-code";
	public final static Long CODE_SUCCESS_KEY = 0L;
	public final static String CODE_SUCCESS_VALUE = "OK";
	public final static Long CODE_FAILED_KEY = -1L;
	public final static String CODE_FAILED_VALUE = "Unknown error";
	public final static String VALIDATE_HEADERS_MSG = "Headers validation result:";

	// Successful response
	public static final Long SUCCESS_CODE = 0L;
	public static final String SUCCESS_MESSAGE = "ok";

	// Exception status response
	public static final String EXCEPTION_CODE = "500";
	public static final String SERVICE_EXCEPTION_CODE = "409";
	public static final String SERVICE_EXCEPTION_DESC = "Unknown Error";
	
	//Class Loader
	public static final String CLASS_PATH = "classfiles.url";
	public static final String CLASS_FILES_PACKAGE = "classfiles.package";
	public static final String OWN_CLASSES = "own.classes.load";
	public static final String LOAD_CLASS_DEV = "classfiles.dev";
	
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
}