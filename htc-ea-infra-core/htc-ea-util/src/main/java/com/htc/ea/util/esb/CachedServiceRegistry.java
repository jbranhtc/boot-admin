package com.htc.ea.util.esb;


import com.htc.ea.util.cache.CacheManager;
import com.htc.ea.util.cache.CachedObject;

public class CachedServiceRegistry implements ServiceRegistry {
	private ServiceRegistry serviceRegistry;
	private CacheManager<ServiceData> serviceCacheManager;
	
	@Override
	public ServiceData getService(ServiceId serviceId) {
		ServiceData serviceData = null;
		String key = serviceId.getUniqueId();
		if (serviceCacheManager.containsKey(key)) {
			CachedObject<ServiceData> cachedObject = serviceCacheManager.get(key);
			serviceData = cachedObject.getObject();
		} else {
			serviceData = updateCache(serviceId);
		}
		return serviceData;
	}
	
	private ServiceData updateCache(ServiceId serviceId) {
		ServiceData serviceData = serviceRegistry.getService(serviceId);
		if (serviceData != null) {
			CachedObject<ServiceData> cachedObject = new CachedObject<ServiceData>(serviceId.getUniqueId(), serviceData);
			serviceCacheManager.put(cachedObject);
		} else {
			throw new RuntimeException("Service not found");
		}
		return serviceData;
	}
	
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public void setServiceCacheManager(CacheManager<ServiceData> serviceCacheManager) {
		this.serviceCacheManager = serviceCacheManager;
	}
}