package com.htc.ea.util.esb;

import java.io.Serializable;

public class ServiceData implements Serializable{
	private static final long serialVersionUID = -2890171280356992743L;
	
	private String serviceId;
	private String environmentId;
	private String countryId;
	private String name;
	private String groupName;
	private String endpointType;
	private String endpointValue;
	private boolean enabled;
	private boolean secured;
	private int timeout;
	private int priority;
	private boolean compensable;
	
	public String getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getEnvironmentId() {
		return environmentId;
	}
	
	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}
	
	public String getCountryId() {
		return countryId;
	}
	
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getEndpointType() {
		return endpointType;
	}
	
	public void setEndpointType(String endpointType) {
		this.endpointType = endpointType;
	}
	
	public String getEndpointValue() {
		return endpointValue;
	}
	
	public void setEndpointValue(String endpointValue) {
		this.endpointValue = endpointValue;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public boolean isSecured() {
		return secured;
	}
	
	public void setSecured(boolean secured) {
		this.secured = secured;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	public boolean isCompensable() {
		return compensable;
	}

	public void setCompensable(boolean compensable) {
		this.compensable = compensable;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceData [serviceId=").append(serviceId)
				.append(", environmentId=").append(environmentId)
				.append(", countryId=").append(countryId).append(", name=")
				.append(name).append(", groupName=").append(groupName)
				.append(", endpointType=").append(endpointType)
				.append(", endpointValue=").append(endpointValue)
				.append(", enabled=").append(enabled).append(", secured=")
				.append(secured).append(", timeout=").append(timeout)
				.append(", priority=").append(priority)
				.append(", compensable=").append(compensable).append("]");
		return builder.toString();
	}
	
}
