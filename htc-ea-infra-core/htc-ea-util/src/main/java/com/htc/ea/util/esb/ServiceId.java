package com.htc.ea.util.esb;

import java.io.Serializable;

public class ServiceId implements Serializable{
	private static final long serialVersionUID = -5790189098603473692L;
	
	private String serviceId;
	private String environmentId;
	private String countryId;
	
	public ServiceId() {	
	}	
	
	public ServiceId(String serviceId, String environmentId, String countryId) {
		super();
		this.serviceId = serviceId;
		this.environmentId = environmentId;
		this.countryId = countryId;
	}

	public String getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getEnvironmentId() {
		return environmentId;
	}
	
	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}
	
	public String getCountryId() {
		return countryId;
	}
	
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	public String getUniqueId() {
		return serviceId + "|" + environmentId + "|" + countryId;
	}
	
	@Override
	public String toString() {
		return String.format(
				"ServiceId [serviceId=%s, environmentId=%s, countryId=%s]",
				serviceId, environmentId, countryId);
	}
}