package com.htc.ea.util.configentry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.stereotype.Service;

import com.htc.ea.util.properties.ConfigurationDataSource;
import com.htc.ea.util.properties.ConfigurationDataSourceTarget;

@Service
public class ConfigurationServiceFacade {

	public static final String DEFAULT_PROPERTIES_FILE = "app.properties";
	public static final String APP_ID_PROPERTY = "application.id";
	public static final String APP_ENVIRONMENT_PROPERTY = "app.env";
	public static final String APP_VERSION_PROPERTY = "application.version";

	public ConfigurationServiceFacade() {
	}

	@Autowired
	PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;
	@Autowired
	Environment env;
	@Autowired
	private ConfigurationService configurationService;

	public void setConfigurationServiceFacade(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	public void loadConfigurationDataTarget() {
		configurationService.refresh(true);
		MutablePropertySources propertySources = propertySources();
		ConfigurationDataSourceTarget propertySource = new ConfigurationDataSourceTarget();
		propertySource.setConfigurationService(configurationService);
		propertySources.addFirst(propertySource);
	}

	public void loadConfigurationData() {
		configurationService.refresh(false);
		MutablePropertySources propertySources = propertySources();
		ConfigurationDataSource propertySource = new ConfigurationDataSource();
		propertySource.setConfigurationService(configurationService);
		propertySources.addFirst(propertySource);
	}

	private MutablePropertySources propertySources() {
		StandardEnvironment standardEnv = (StandardEnvironment) env;
		return standardEnv.getPropertySources();
	}
}
