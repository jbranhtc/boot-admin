package com.htc.ea.util.service;

public interface ReloadCacheHelper {
	public void refresh(String listName, String classLoadNames, boolean target);
}