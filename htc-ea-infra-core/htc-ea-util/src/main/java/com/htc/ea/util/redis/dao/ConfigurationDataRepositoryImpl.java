package com.htc.ea.util.redis.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.dto.GenericDto;

@Repository
public class ConfigurationDataRepositoryImpl implements ConfigurationDataRepository {

	private static String KEY = "";
	private static final String ID_CONF_DATA = "ConfigurationData";
	private static final String ID_CONV_DATA = "ConversionData";
	private static final String ID_ROUT_DATA = "RoutingData";
	private static final String ID_EVENT_CATEORY_DATA = "ConfigurationEventCategoryData";
	private static final String ID_LOAD_CLASSES = "LoadClasses";

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;	
	
	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public Object findAllConfigurationData(String applicationId, String environmentId, String version) {
		KEY = applicationId+"-"+environmentId+"-"+version;
		return redisTemplate.opsForHash().get(KEY, ID_CONF_DATA);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAllConversionData(String applicationId, String environmentId, String version) {
		KEY = applicationId+"-"+environmentId+"-"+version;
		return (List<Object>) redisTemplate.opsForHash().get(KEY, ID_CONV_DATA);
	}

	@Override
	public Object findAllConfigurationDataTarget(String target) {
		KEY = target;
		return redisTemplate.opsForHash().get(KEY, ID_CONF_DATA);
	}
	
	@Override
	public Object findRoutingData() {
		KEY = "routing";
		return redisTemplate.opsForHash().get(KEY, ID_ROUT_DATA);
	}

	@Override
	public String findAllOtherObject(String applicationId, String environmentId, String version,String listName) {
		KEY = applicationId+"-"+environmentId+"-"+version;
		GenericDto genericDto = (GenericDto) redisTemplate.opsForHash().get(KEY, listName);
		String json = genericDto.getStringProperty(listName);
		return json;
	}

	@Override
	public Object findAllOtherObject(String applicationId, String listName) {
		KEY = applicationId;
		GenericDto genericDto = (GenericDto) redisTemplate.opsForHash().get(KEY, listName);
		String json = genericDto.getStringProperty(listName);
		return json;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAllEventCategoryData(String applicationId, String environmentId, String version) {
		KEY = applicationId+"-"+environmentId+"-"+version;
		return (List<Object>) redisTemplate.opsForHash().get(KEY, ID_EVENT_CATEORY_DATA);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findLoadClass(String applicationId, String environmentId, String version) {
		KEY = applicationId+"-"+environmentId+"-"+version;
		return (List<Object>) redisTemplate.opsForHash().get(KEY, ID_LOAD_CLASSES);
	}
	
}