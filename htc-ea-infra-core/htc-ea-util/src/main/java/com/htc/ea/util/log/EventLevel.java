package com.htc.ea.util.log;

public enum EventLevel {
	TRACE, DEBUG, INFO, WARN, ERROR, FATAL;
}