package com.htc.ea.util.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sl4jLoggingService implements LoggingService {

	private static final Logger logger = LoggerFactory.getLogger(Sl4jLoggingService.class);

	@Override
	public void log(EventData eventData) {
		if (EventLevel.TRACE.toString().equals(eventData.getLevel())) {
			logger.trace("{}", eventData);
		} else if (EventLevel.DEBUG.toString().equals(eventData.getLevel())) {
			logger.debug("{}", builderDataLog(eventData));
		} else if (EventLevel.INFO.toString().equals(eventData.getLevel())) {
			logger.info("{}", builderDataLog(eventData));
		} else if (EventLevel.WARN.toString().equals(eventData.getLevel())) {
			logger.warn("{}", eventData);
		} else if (EventLevel.ERROR.toString().equals(eventData.getLevel())) {
			logger.error("{}", builderDataLog(eventData));
		} else { // fatal
			logger.error("{}", eventData);
		}
	}

	private String builderDataLog(EventData eventData) {
		String data = "";
		String source = getCompressedSource(eventData.getSource());
		String originRefId = eventData.getOriginReferenceId();
		Long duration = eventData.getDuration();
		String message = eventData.getMessage();
		String detail = "";
		String sequence = eventData.getSequence();
		if (eventData.getData() != null)
			detail = eventData.getData().toString();
		else
			detail = eventData.getDetail();
		String exception = eventData.getException();
		StringBuilder pattern = new StringBuilder();
		pattern.append("%s ");
		pattern.append("%s %s -");
		pattern.append(" %s");
		if (detail != null)
			pattern.append(" => %s");
		else
			pattern.append(" %s");
		if (duration != null)
			pattern.append(", duration: %s ms. ");
		pattern.append(" %s");
		if (originRefId == null)
			originRefId = "";
		if (detail == null)
			detail = "";
		if (exception == null)
			exception = "";
		if (duration != null)
			data = String.format(pattern.toString(), sequence, source, originRefId, message, detail, duration, exception);
		else
			data = String.format(pattern.toString(), sequence, source, originRefId, message, detail, exception);
		return data;
	}

	private String getCompressedSource(String source) {
		String[] sourceArray = source.split("\\.");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sourceArray.length; i++) {
			String element = sourceArray[i];
			if (i < (sourceArray.length - 1)) {
				sb.append(element.substring(0, 1));
				sb.append(".");
			} else
				sb.append(element);
		}
		return sb.toString();
	}
}