package com.htc.ea.util.configentry;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.ea.util.dto.ConfigEntryData;
import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;
import com.htc.ea.util.util.Constants;

@Service
public class ConfigurationServiceDefault implements ConfigurationService {
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationServiceDefault.class);

	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	@Autowired
	private Environment env;
	private ConfigurationData configurationData;
	private ConfigurationData configurationDataTarget;
	private String applicationId;
	private String environmentId;
	private String version;

	public void refresh(boolean target) {
		Object configurationDataObject = null;
		try {
			if (target) {
				logger.info("Configuration Data Target: " + env.getProperty(Constants.GENERAL_CONFIGURATION));
				configurationDataObject = configurationDataRepository
						.findAllConfigurationDataTarget(env.getProperty(Constants.GENERAL_CONFIGURATION));
				if (configurationDataObject == null) {
					configurationDataTarget = new ConfigurationData();
					logger.info("No Configuration data target");
				} else {
					configurationDataTarget = (ConfigurationData) configurationDataObject;
					logger.info("ConfigurationDataTarget ready.");
				}
			} else {
				configurationDataObject = configurationDataRepository.findAllConfigurationData(applicationId,
						environmentId, version);
				configurationData = (ConfigurationData) configurationDataObject;
				if (configurationData != null) {
					logger.info("ConfigurationData ready.");
				} else {
					throw new RuntimeException(
							"Error getting ConfigurationData. No connect @Bean jedisConnectionFactory.",
							new Exception("No Configuration Data for the applicationID"));
				}

			}

		} catch (Exception e) {
			// e.printStackTrace();
			throw new RuntimeException("Error getting ConfigurationData. No connect @Bean jedisConnectionFactory.", e);
		}
	}

	public String getApplicationId() {
		return applicationId;
	}

	public String getEnvironmentId() {
		return environmentId;
	}

	public String getVersion() {
		return version;
	}

	public String getValueTarget(String key) {
		String value = null;
		if (configurationDataTarget.getConfigEntries().containsKey(key)) {
			value = configurationDataTarget.getConfigEntries().get(key).getValue();
		}
		return value;
	}

	public Set<String> getValuesTarget(String key) {
		Set<String> values = null;
		if (configurationDataTarget.getConfigEntries().containsKey(key)) {
			ConfigEntryData configEntryData = configurationDataTarget.getConfigEntries().get(key);
			if (!configEntryData.isSingleValue()) {
				values = configEntryData.getValues();
			} else {
				values = new HashSet<String>();
				logger.warn("key {} is single value", key);
			}
		}
		return values;
	}

	public Collection<ConfigEntryData> getConfigEntriesTarget() {
		return configurationDataTarget.getConfigEntries().values();
	}

	public int getEventLevelTarget() {
		return configurationDataTarget.getEventLevel();
	}

	public Set<String> getEventCategoriesTarget() {
		return configurationDataTarget.getEventCategories();
	}

	public String getValue(String key) {
		String value = null;
		if (configurationData.getConfigEntries().containsKey(key)) {
			value = configurationData.getConfigEntries().get(key).getValue();
		}
		return value;
	}

	public Set<String> getValues(String key) {
		Set<String> values = null;
		if (configurationData.getConfigEntries().containsKey(key)) {
			ConfigEntryData configEntryData = configurationData.getConfigEntries().get(key);
			if (!configEntryData.isSingleValue()) {
				values = configEntryData.getValues();
			} else {
				values = new HashSet<String>();
				logger.warn("key {} is single value", key);
			}
		}
		return values;
	}

	public Collection<ConfigEntryData> getConfigEntries() {
		return configurationData.getConfigEntries().values();
	}

	public int getEventLevel() {
		return configurationData.getEventLevel();
	}

	public Set<String> getEventCategories() {
		return configurationData.getEventCategories();
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return String.format(
				"ConfigurationServiceFacade [configurationService=%s, configurationData=%s, applicationId=%s, environmentId=%s, version=%s]",
				null, configurationData, applicationId, environmentId, version);
	}

	@Override
	public boolean isSingleValue(String key) {
		if (configurationData.getConfigEntries().containsKey(key)) {
			ConfigEntryData configEntryData = configurationData.getConfigEntries().get(key);
			return configEntryData.isSingleValue();
		}
		return false;
	}
}