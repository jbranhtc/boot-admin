package com.htc.ea.util.parsing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

public class RabbitTemplateWrapper {

	private Map<String, RabbitTemplate> rabbitTemplateMap;

	public Map<String, RabbitTemplate> getRabbitTemplateMap() {
		if (rabbitTemplateMap == null) {
			rabbitTemplateMap = new HashMap<>();
		}
		return rabbitTemplateMap;
	}
}