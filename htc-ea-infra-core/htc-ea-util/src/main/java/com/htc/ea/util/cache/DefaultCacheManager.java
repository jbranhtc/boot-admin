package com.htc.ea.util.cache;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DefaultCacheManager<T> implements CacheManager<T> {
	private Map<String, CachedObject<T>> cache = new HashMap<String, CachedObject<T>>();
	private Date lastUpdate;

	@Override
	public int size() {
		return cache.size();
	}

	@Override
	public Set<String> keySet() {
		Set<String> keys = new HashSet<String>();
		for (String key : cache.keySet()) {
			keys.add(key);
		}
		return keys;
	}

	@Override
	public void clear() {
		cache.clear();
	}

	@Override
	public boolean containsKey(String key) {
		return cache.containsKey(key);
	}

	@Override
	public CachedObject<T> get(String key) {
		return cache.get(key);
	}

	@Override
	public void put(CachedObject<T> cachedObject) {		
		cache.put(cachedObject.getKey(), cachedObject);
		lastUpdate = new Date();
	}	

	@Override
	public void remove(String key) {
		cache.remove(key);
	}

	@Override
	public String getAsString(String key) {
		CachedObject<T> value = cache.get(key);
		if (value != null) {
			return value.toString();
		}
		return null;
	}
	
	@Override
	public Date lastUpdate() {
		return lastUpdate;
	}	

	public void setCache(Map<String, CachedObject<T>> cache) {
		this.cache = cache;
	}

}
