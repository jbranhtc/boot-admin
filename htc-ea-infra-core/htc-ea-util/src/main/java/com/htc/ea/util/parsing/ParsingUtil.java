package com.htc.ea.util.parsing;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.util.dto.GenericDto;

@Component
public class ParsingUtil {
	
	private ObjectMapper objectMapper;
//	private XmlMapper xmlMapper;

	public ParsingUtil() {
		if (objectMapper == null) {
			this.objectMapper = getObjectMapper();
			this.objectMapper.setTimeZone(TimeZone.getDefault());
		}
//		if (xmlMapper == null)
//			this.xmlMapper = getXmlMapper();
	}

	public final int INTERNAL_ERROR = 1;
	public final int UNKNOWN_ERROR = 201;
	public final int REQUEST_EMPTY_OR_NULL = 202;
	public final int QUERY_NOT_FOUND = 203;
	public final int RECORD_NOT_CREATED = 204;
	public final int ATTRIBUTES_UNMODIFIED = 205;
	public final int OPTION_INVALID = 206;
	public final int WEB_SERVICE_INVOKE_ERROR = 207;
	public final int OPERATION_NOT_REGISTRY = 208;
	public final int TYPE_NOT_FOUND = 209;
	public final int PROPERTIES_FOUND = 210;
	public final int REQUEST_INVALID_LENGTH_FIELD = 211;
	public final int RECORD_UNMODIFIED = 212;
	public final int PROPERTY_NOT_FOUND = 213;
	public final int CUSTOMER_NOT_FOUND = 216;
	public final int UNSUCCESSFUL_RECHARGE = 218;
	public final int OFFERING_NOT_FOUND = 214;
	public final int SUBSCRIBER_NOT_FOUND = 215;
	public final int PAYMENT_MODE_NOT_FOUND = 217;
	public final int SUBCRIBER_NOT_CONVERTED = 219;
	public final int FAVORITE_NOT_FOUND = 220;
	public final int FAVORITE_EXIST = 221;
	public final int CONVERSION_UNIT_NOT_FOUND = 222;
	public final int CONVERSION_COS_NOT_FOUND = 223;
	public final int INVALID_RETRIVE_CODE = 224;
	public final int INVALID_LIFE_CICLE = 225;
	public final int BALANCE_NOT_FOUND = 226;
	public final int OFFERING_INVALID = 227;
	public final int ACCOUNT_ALREADY_USED = 228;
	public final int INVALID_STATE = 229;
	public final int PARENT_NOT_FOUNF = 230;
	public final int OFFERING_NOT_FOUND_TSB = 231;
	public final int ACCOUNT_NOT_FOUND = 232;
	public final int FN_NOT_FOUND = 233;
	public final int INVALID_OFFERT_TYPE = 234;
	public final int CONTRACT_IS_DIFFERENT = 235;
	public final int CUSTOMER_GENERIC = 236;
	public final int CAERR01 = 237;
	public final int CAERR02 = 238;
	public final int CAERR03 = 239;
	public final int CAERR04 = 240;
	public final int OFFERT_NOT_ASSOCIATED = 241;

	public final String ISO8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"; // 12
	public final static String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss"; // 13
	public final String DATE_FORMAT = "yyyy-MM-dd"; // 8
	public Integer NUMBER_DECIMAL_CBS = 6;

	private static String[] formatDate = { "yyyy-MM-dd'T'HH:mm:ss", // 0
			"dd/MM/yyyy", // 1
			"dd-MM-yyyy", // 2
			"yyyyMMddHHmmss", // 3
			"yyyy-MM-dd'T'HH:mm:ss.SSS", // 4
			"yyyyMMddHHmmss", // 5
			"MM/dd/yyyy", // 6
			"yyyy-MM-dd'T'HH:mm:ss'Z'", // 7
			"yyyy-MM-dd", // 8
			"dd/MM/yyyy HH:mm:ss", // 9
			"MM/dd/yyyy HH:mm:ss", // 10
			"yyyy-MM-dd'T'HH:mm:ss", // 11
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", // 12
			"yyyy-MM-dd HH:mm:ss", // 13
			"yy/MM/dd", // 14
			"yyyyMMdd HH:mm:ss", // 15
			"yyyy-MM-dd'T'HH:mm:ss.SSSX", // 16
			"yyyyMMdd", // 17
			"yyyy-MM-dd'T'HH:mm:ss.SSS-HH:mm", // 18
			"yyyyMMdd_HHmmss",// 19
			"yyMMddHHmm" //20
	};

	public Map<Integer, String> errorMessages;
	{
		Map<Integer, String> tempMap = new HashMap<Integer, String>();
		tempMap.put(UNKNOWN_ERROR, "Unknown Error");
		tempMap.put(REQUEST_EMPTY_OR_NULL, "Request emtpy or null");
		tempMap.put(QUERY_NOT_FOUND, "Query not found");
		tempMap.put(RECORD_NOT_CREATED, "Insert not created");
		tempMap.put(ATTRIBUTES_UNMODIFIED, "Attributes not created or modified");
		tempMap.put(OPTION_INVALID, "Operational invalid option in request");
		tempMap.put(WEB_SERVICE_INVOKE_ERROR, "Target CBS WebService Error");
		tempMap.put(OPERATION_NOT_REGISTRY, "Operation failed in database");
		tempMap.put(TYPE_NOT_FOUND, "Type not found");
		tempMap.put(PROPERTIES_FOUND, "Properties not found");
		tempMap.put(REQUEST_INVALID_LENGTH_FIELD, "Invalid length field requested");
		tempMap.put(RECORD_UNMODIFIED, "Record unmodified");
		tempMap.put(PROPERTY_NOT_FOUND, "Property TSB not found");
		tempMap.put(CUSTOMER_NOT_FOUND, "Customer not found");
		tempMap.put(UNSUCCESSFUL_RECHARGE, "Unsuccessful recharge for the subscriber");
		tempMap.put(OFFERING_NOT_FOUND, "Offering not found");
		tempMap.put(SUBSCRIBER_NOT_FOUND, "Subscriber not found");
		tempMap.put(PAYMENT_MODE_NOT_FOUND, "Payment Mode not found");
		tempMap.put(SUBCRIBER_NOT_CONVERTED, "Subscriber can not be converted  to the COS");
		tempMap.put(FAVORITE_NOT_FOUND, "Favorite not found");
		tempMap.put(CONVERSION_UNIT_NOT_FOUND, "Conversion Unit not found");
		tempMap.put(FAVORITE_EXIST, "Favorite number already exists");
		tempMap.put(CONVERSION_COS_NOT_FOUND, "Convertion COS not found");
		tempMap.put(INVALID_RETRIVE_CODE, "");
		tempMap.put(INVALID_LIFE_CICLE, "Invalid life cycle");
		tempMap.put(BALANCE_NOT_FOUND, "Balance not found");
		tempMap.put(OFFERING_INVALID, "Offering invalid");
		tempMap.put(ACCOUNT_ALREADY_USED, "Account already used");
		tempMap.put(INVALID_STATE, "Invalid state");
		tempMap.put(PARENT_NOT_FOUNF, "Parent not found");
		tempMap.put(OFFERING_NOT_FOUND_TSB, "Offert not found in TSB");
		tempMap.put(ACCOUNT_NOT_FOUND, "Account not found");
		tempMap.put(FN_NOT_FOUND, "FN not found assoicied to the account");
		tempMap.put(INVALID_OFFERT_TYPE, "Invalid offert type");
		tempMap.put(CONTRACT_IS_DIFFERENT, "Contract owner is different");
		tempMap.put(CUSTOMER_GENERIC, "customerKey It is a generic code");
		tempMap.put(CAERR01,
				"Solicitud no v�lida. La operaci�n no permite combinaci�n de abonos y cargos en una misma solicitud.");
		tempMap.put(CAERR02,
				"Solicitud no v�lida. La operaci�n no permite combinaci�n de la billetera principal y otras billeteras en una solicitud de abonos.");
		tempMap.put(CAERR03,
				"Solicitud no v�lida. La operaci�n no permite m�s de una billetera en una misma solicitud.");
		tempMap.put(CAERR04,
				"Solicitud no v�lida. La operaci�n no permite abonos con fecha y hora de solicitud menores a la fecha y hora actual");
		tempMap.put(OFFERT_NOT_ASSOCIATED, "The offer of the subscriptor do not have associated FN");
		errorMessages = Collections.unmodifiableMap(tempMap);
	}

	private ThreadLocal<SimpleDateFormat> cbsFormatter = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};

	private ThreadLocal<SimpleDateFormat> appSequence = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyMMddHHmmssSSS");
		}
	};

	public Date toDate(String dateInString) {
		Date date = null;
		try {
			date = cbsFormatter.get().parse(dateInString);
		} catch (Exception e) {
		}
		return date;
	}

	public Date toDate(String dateInString, int formatDate) {
		String defaultFormatDate = "dd/MM/yyyy HH:mm:ss";
		int tam = formatDate > (ParsingUtil.formatDate.length - 1) ? 2 : formatDate;
		defaultFormatDate = ParsingUtil.formatDate[tam];
		SimpleDateFormat format = new SimpleDateFormat(defaultFormatDate);
		try {
			return format.parse(dateInString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Calendar getCalendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.setTimeZone(TimeZone.getDefault());
		return calendar;
	}

	public Calendar toCalendar(String dateInString) {
		Calendar calendar = null;
		Date date = toDate(dateInString);
		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}
		return calendar;
	}

	public Calendar toCalendar(String dateInString, int formatDate) {
		Calendar calendar = null;
		Date date = toDate(dateInString, formatDate);
		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}
		return calendar;
	}

	public Calendar toCalendar(XMLGregorianCalendar xmlgc) {
		Calendar calendar = null;
		try {
			Date date = xmlgc.toGregorianCalendar().getTime();
			calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.setTimeZone(TimeZone.getDefault());
			// calendar.setTimeInMillis(DatatypeConstants.FIELD_UNDEFINED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calendar;
	}

	public Calendar toCalendar(Date date) {
		Calendar calendar = null;
		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}
		return calendar;
	}

	public Date getSysdateInitial() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getDefault());
		calendar.setTime(new Date());
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0,
				0, 0);
		return calendar.getTime();
	}

	public Calendar toCalendarWithLocaleExplicit(XMLGregorianCalendar xmlgc) {
		if (xmlgc == null)
			return null;
		XMLGregorianCalendar gc = xmlgc;
		Calendar calendar = gc.toGregorianCalendar(TimeZone.getDefault(), Locale.getDefault(), xmlgc);
		return calendar;
	}

	public String toCbsDateFormat(Date date) {
		String strDate = null;
		try {
			strDate = cbsFormatter.get().format(date);
		} catch (Exception e) {
		}
		return strDate;
	}

	public XMLGregorianCalendar toXMLGregorianCalendar(java.util.Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		XMLGregorianCalendar xmlCalendar = null;
		try {
			xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
			return xmlCalendar;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	public XMLGregorianCalendar toXMLGregorianCalendar(String sdate, int formatDate) {
		if (sdate != null) {
			Date date = toDate(sdate, formatDate);
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			XMLGregorianCalendar xmlCalendar = null;
			try {
				xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
				return xmlCalendar;
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public String toString(XMLGregorianCalendar date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		try {
			return format.format(date.toGregorianCalendar().getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String toStringWithLocaleExplicit(XMLGregorianCalendar date, String formatDate) {
		SimpleDateFormat format = new SimpleDateFormat(formatDate);
		if (date == null || (formatDate == null || (formatDate != null && formatDate.isEmpty())))
			return null;
		try {
			Calendar calendar = toCalendarWithLocaleExplicit(date);
			return format.format(calendar.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getSequenceFileRead() {
		String strDate = null;
		try {
			strDate = appSequence.get().format(new Date());
		} catch (Exception e) {
		}
		String uuid = generateId();
		char[] uuidArray = uuid.toCharArray();
		String dig = "";
		int count = 0;
		for (int i = 0; i < uuidArray.length; i++) {
			if (count == 3)
				break;
			Character ch = uuidArray[i];
			boolean isDigit = Character.isDigit(ch);
			if (isDigit) {
				dig = dig + ch;
				count++;
			}
		}
		return strDate + dig;
	}

	/*****************
	 * Validaciones integer. Valida campos corresponden a un valor Integer
	 * [positivo|negativo] dado como un String
	 */
	public boolean isIntegerNumber(String data) {
		String regex = "^-*[0-9]+$"; // uno o m�s d�gitos num�ricos enteros
										// positivos o negativos
		Pattern queryLangPattern = Pattern.compile(regex);
		Matcher matcher = queryLangPattern.matcher(data);
		return matcher.matches();
	}

	/*****************
	 * Validaciones booleanos. Valida campos corresponden a un valor Booleano
	 * dado como un String
	 */
	public boolean isBoolean(String data) {
		String regex = "true|false";// true o false sin importar may�sculas o no
		Pattern queryLangPattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = queryLangPattern.matcher(data);
		return matcher.matches();
	}

	public String toSimpleDateFormat(java.util.Date date, int formatDate) {
		String defaultFormatDate = "dd/MM/yyyy";
		int tam = formatDate > (ParsingUtil.formatDate.length - 1) ? 2 : formatDate;
		defaultFormatDate = ParsingUtil.formatDate[tam];
		SimpleDateFormat format = new SimpleDateFormat(defaultFormatDate);
		try {
			return format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isDateFormatValid(String dateInput, int formatDate) {
		// String date = "2014-02-02gy";
		try {
			int tam = formatDate > (ParsingUtil.formatDate.length - 1) ? 2 : formatDate;
			String defaultFormatDate = ParsingUtil.formatDate[tam];
			DateFormat df = new SimpleDateFormat(defaultFormatDate);
			Date dateObject = df.parse(dateInput);
			String dateString = df.format(dateObject);
			if (!dateString.equals(dateInput))
				throw new ParseException(dateInput, 0);
			return true;
		} catch (ParseException ex) { // not yyyy-mm-dd date.
			return false;
		}
	}

	public Integer[] addElementToArrayInteger(Integer[] series, int newInt) {
		// create a new array with extra index
		Integer[] newSeries = new Integer[series.length + 1];
		// copy the integers from series to newSeries
		for (int i = 0; i < series.length; i++) {
			newSeries[i] = series[i];
		}
		// add the new integer to the last index
		newSeries[newSeries.length - 1] = newInt;
		return newSeries;
	}

	public boolean isNullObject(Object object) {
		boolean result = true;
		try {
			if (object != null)
				result = false;
		} catch (NullPointerException e) {
		}
		return result;
	}

	/************************************************************************************************************/
	/*****************
	 * CONVERSIONES XML Convierte Xml to List&lt;Object&gt;. => Ok m�todo
	 * funcional.
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> unmarshalList(Class<?> clazz, String xml) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(WrapperList.class, clazz);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		// StreamSource xml = new StreamSource(xmlLocation); //Si paso archivo
		// f�sico xml
		Source srcXml = new StreamSource(new StringReader(xml));
		WrapperList<T> objectDataList = (WrapperList<T>) unmarshaller.unmarshal(srcXml, WrapperList.class).getValue();
		return objectDataList.getItems();
	}

	/*****************
	 * CONVERSIONES XML Convierte List&lt;Object&gt; to Xml. => Ok m�todo
	 * funcional.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String marshalList(Class<?> clazz, List<?> list, String qName) throws JAXBException {
		QName QName = new QName(qName);
		WrapperList wrapperList = new WrapperList(list);
		JAXBContext jc = JAXBContext.newInstance(WrapperList.class, clazz);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		JAXBElement<WrapperList> jaxbElement = new JAXBElement<WrapperList>(QName, WrapperList.class, wrapperList);
		StringWriter sWriter = new StringWriter();
		marshaller.marshal(jaxbElement, sWriter);
		String xmlDataList = sWriter.toString();
		return xmlDataList;
	}

	/*****************
	 * CONVERSIONES XML Convierte Xml to Object. => Ok m�todo funcional.
	 */
	public <T> T unmarshal(Class<T> clazz, String xml) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(clazz);
		// StreamSource xml = new StreamSource(xmlLocation); //Si paso archivo
		// f�sico xml
		Source srcXml = new StreamSource(new StringReader(xml));
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		T objectData = unmarshaller.unmarshal(srcXml, clazz).getValue();
		return objectData;
	}

	/*****************
	 * CONVERSIONES XML Convierte Object to Xml. Sin uso clase Wrapper
	 * (Recomendado). => Ok m�todo funcional
	 */
	public String marshal(Object obj) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller marshaller = ctx.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(obj, sw);
		return sw.toString();
	}

	/*****************
	 * CONVERSIONES XML Convierte Object to Xml. Usando clase Wrapper
	 * (NoRecomendado) => Ok m�todo funcional
	 */
	@SuppressWarnings("rawtypes")
	public String marshalOther(Class<?> clazz, Object obj, String qName) throws JAXBException {
		QName QName = new QName(qName);
		Wrapper wrapper = new Wrapper(obj);
		JAXBContext jc = JAXBContext.newInstance(Wrapper.class, clazz);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		JAXBElement<Wrapper> jaxbElement = new JAXBElement<Wrapper>(QName, Wrapper.class, wrapper);
		StringWriter sWriter = new StringWriter();
		marshaller.marshal(jaxbElement, sWriter);
		String xmlData = sWriter.toString();
		return xmlData;
	}

	/************************************************************************************************************/
	/*****************
	 * CONVERSIONES JSON Convierte Json to List&lt;Object&gt;. => Ok m�todo
	 * funcional.
	 */
	public <T> List<T> convertJsonToList(Class<?> clazz, String json) throws IOException {
		JavaType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, clazz);
		List<T> objectDataList = null;
		try {
			objectDataList = objectMapper.readValue(json, javaType);
			return objectDataList;
		} catch (JsonParseException e) {
			throw new RuntimeException(e.getMessage());
		} catch (JsonMappingException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/*****************
	 * CONVERSIONES JSON Convierte List&lt;Object&gt; to Json. => Ok m�todo
	 * funcional
	 */
	public String convertListToJson(List<?> list) throws JsonProcessingException {
		String jsonData = null;
		try {
			jsonData = objectMapper.writeValueAsString(list);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/*****************
	 * CONVERSIONES JSON Convierte Json to Object. => Ok m�todo funcional
	 * 
	 * @return
	 */
	public <T> T convertJsonToObject(String json, Class<T> clazz) throws IOException {
		T objectData = null;
		try {
			objectData = objectMapper.readValue(json, clazz);
			return objectData;
		} catch (JsonParseException e) {
			throw new RuntimeException(e.getMessage());
		} catch (JsonMappingException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/*****************
	 * CONVERSIONES JSON Convierte Object to Json. => Ok m�todo funcional
	 */
	public String convertObjectToJson(Object obj) throws JsonProcessingException {
		String jsonData = null;
		try {
			jsonData = objectMapper.writeValueAsString(obj);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	/*****************
	 * CONVERSIONES JSON Convierte Object to Json Pretty. => Ok m�todo funcional
	 */
	public String convertObjectToJsonPretty(Object obj) throws JsonProcessingException {
		String jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		return jsonData;
	}

	/************************************************************************************************************/
	/*****************
	 * CONVERSIONES XML (Usando API XML [jackson-dataformat-xml v2.3.3]
	 * Convierte Xml to Object. => Ok m�todo funcional usando jackson-databind
	 */
//	public <T> T unmarshal2(Class<T> clazz, String data) throws IOException {
//		T objectData = xmlMapper.readValue(data, clazz);
//		return objectData;
//	}

	/*****************
	 * CONVERSIONES XML Convierte Object to Xml. => Ok m�todo funcional usando
	 * jackson-databind
	 */
//	public String marshal2(Object obj) throws IOException {
//		String xmlData = xmlMapper.writeValueAsString(obj);
//		return xmlData;
//	}

	public String generateId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/*****************
	 * Validaciones campos. Valida campos de acuerdo a un flag
	 * (defaultResponse), si est� encendido aunque el campo sea nulo retornar�
	 * una instancia vac�a para String, num�rica cero para Integer,Double,Long,
	 * false para Booleano y Mapa vac�o para HashMap; estableciendo previamente
	 * el tipo de clase esperado en type Tambi�n validar� el formato de fecha
	 * recibida en String de acuerdo a un formatDate pasado como Integer
	 * consultado en AppUtil.formatDate[] para todos los formatos soportados. Si
	 * el campo String no es una fecha indicar como formatDate = null para que
	 * omita la validaci�n respectiva.
	 */
	@SuppressWarnings({ "unchecked" })
	public <T> T validateField(Object object, Class<T> type, Boolean defaultResponse, Integer formatDate) {
		String emptyString = new String();
		Integer emptyInteger = new Integer(0);
		Double emptyDouble = new Double(0d);
		Long emptyLong = new Long(0L);
		Boolean emptyBoolean = new Boolean(false);
		Date emptyDate = new Date();
		BigInteger emptyBigInteger = new BigInteger("0");
		BigDecimal emptyBigDecimal = new BigDecimal(0d);
		Timestamp emptyTimestamp = new Timestamp(new Date().getTime());
		Map<String, Object> emptyMap = new HashMap<String, Object>();
		LinkedHashMap<String, Object> emptyLinkedHashMap = new LinkedHashMap<String, Object>();
		List<Object> emptyListObject = new ArrayList<Object>();
		GenericDto emptyGenericDto = new GenericDto();
		T result = null;
		String typeClass = "";
		Class<T> clazz = (Class<T>) type;
		String typeClazz = clazz.getSimpleName();
		if (object != null)
			typeClass = object.getClass().getName();
		else {
			typeClass = clazz.getSimpleName();
		}
		if (object == null) {
			if (defaultResponse == Boolean.TRUE) {
				if (typeClass.equalsIgnoreCase("String")) {
					result = (T) emptyString;
				} else if (typeClass.equalsIgnoreCase("Integer")) {
					result = (T) emptyInteger;
				} else if (typeClass.equalsIgnoreCase("Double")) {
					result = (T) emptyDouble;
				} else if (typeClass.equalsIgnoreCase("Long")) {
					result = (T) emptyLong;
				} else if (typeClass.equalsIgnoreCase("Boolean")) {
					result = (T) emptyBoolean;
				} else if (typeClass.equalsIgnoreCase("Date")) {
					result = (T) emptyDate;
				} else if (typeClass.equalsIgnoreCase("BigInteger")) {
					result = (T) emptyBigInteger;
				} else if (typeClass.equalsIgnoreCase("BigDecimal")) {
					result = (T) emptyBigDecimal;
				} else if (typeClass.equalsIgnoreCase("Timestamp")) {
					result = (T) emptyTimestamp;
				} else if (typeClass.equalsIgnoreCase("HashMap")) {
					result = (T) emptyMap;
				} else if (typeClass.equalsIgnoreCase("LinkedHashMap")) {
					result = (T) emptyLinkedHashMap;
				} else if (typeClass.equalsIgnoreCase("List")) {
					result = (T) emptyListObject;
				} else if (typeClass.equalsIgnoreCase("GenericDto")) {
					result = (T) emptyGenericDto;
				}
			}
			return result;
		} else if (object instanceof String) {
			String field = (String) object;
			if (formatDate != null) {
				boolean valid = isDateFormatValid(field, formatDate);
				field = valid ? field : null;
			}
			result = (T) field;
		} else if (object instanceof Integer) {
			Integer field = (Integer) object;
			result = (T) field;
		} else if (object instanceof Double) {
			Double field = (Double) object;
			result = (T) field;
		} else if (object instanceof Long) {
			Long field = (Long) object;
			result = (T) field;
		} else if (object instanceof Boolean) {
			Boolean field = (Boolean) object;
			result = (T) field;
		} else if (object instanceof BigInteger) {
			BigInteger field = (BigInteger) object;
			result = (T) field;
		} else if (object instanceof BigDecimal) { // Tipo de dato NUMERIC en
													// Oracle
			if (typeClazz.equalsIgnoreCase("Long")) {
				Long field = new Long(((BigDecimal) object).longValue());
				result = (T) field;
			} else if (typeClazz.equalsIgnoreCase("Integer")) {
				Integer field = new Integer(((BigDecimal) object).intValue());
				result = (T) field;
			} else if (typeClazz.equalsIgnoreCase("Double")) {
				Double field = new Double(((BigDecimal) object).doubleValue());
				result = (T) field;
			}
		} else if (object instanceof Timestamp) { // Tipo de dato TIMIESTAMP en
													// Oracle
			Date field = new Date(((Timestamp) object).getTime());
			result = (T) field;
		} else if (object instanceof HashMap) {
			Map<String, Object> field = (Map<String, Object>) object;
			result = (T) field;
		} else if (object instanceof LinkedHashMap) {
			LinkedHashMap<String, Object> field = (LinkedHashMap<String, Object>) object;
			result = (T) field;
		} else if (object instanceof List) {
			List<Object> field = (List<Object>) object;
			result = (T) field;
		} else if (object instanceof GenericDto) {
			GenericDto field = (GenericDto) object;
			result = (T) field;
		}
		return result;
	}

	/*****************
	 * Validaciones par�metros full. Valida par�meteros vac�os o nulos de campos
	 * mandatorios y formato de fechas (sean �stos mandatorios o no-mandatorios)
	 * Esta versi�n Full valida campos [String, Integer, Double, Long, Boolean,
	 * HashMap] Para el mapa con los campos mandatorios que se requieran y con
	 * mapas anidados entre s�
	 * 
	 * @param <T>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public <T> void validateParametersFull(Object[] fieldsValue, Object[] fieldsName, Object[] fieldsMandatory,
			Object[] fieldsFormatDate, Map<String, Object[]> arrayFieldsMap, Map<String, Object> result,
			ArrayList<String> emptyFields) {
		// Map<String, Object> result = new HashMap<String, Object>();
		for (int i = 0; i < fieldsName.length; i++) {
			String name = (String) fieldsName[i];
			Object value = (Object) fieldsValue[i];
			Boolean mandatory = (Boolean) fieldsMandatory[i];
			Integer formatDate = (Integer) fieldsFormatDate[i];
			if (value != null) {
				if (value instanceof String) {
					String field = (String) value;
					if (!((String) value).isEmpty()) {
						if (formatDate != null) { // Valida formato de fechas
							boolean valid = isDateFormatValid(field, formatDate);
							if (!valid) {
								int tam = formatDate > (ParsingUtil.formatDate.length - 1) ? 13 : formatDate;
								String formatDateValid = ParsingUtil.formatDate[tam];
								emptyFields.add(name + " (Invalid date format, expected... [" + formatDateValid + "])");
							}
						}
					} else {
						if (mandatory)
							emptyFields.add(name);
					}
				} else if (value instanceof Integer) {
					Integer field = (Integer) value;
				} else if (value instanceof Double) {
					Double field = (Double) value;
				} else if (value instanceof Long) {
					Long field = (Long) value;
				} else if (value instanceof Boolean) {
					Boolean field = (Boolean) value;
				} else if (value instanceof HashMap) { // Valida Map<String,
														// Object> y sus key
														// mandatorios
					Map<String, Object> field = (Map<String, Object>) value;
					if (((HashMap) value).isEmpty()) {
						if (mandatory)
							emptyFields.add(name);
					} else {
						if (mandatory) {
							if (arrayFieldsMap != null && !arrayFieldsMap.isEmpty()) {
								Object[] objects = arrayFieldsMap.get(name);
								if (objects != null) {
									Object[] fieldsNameMap = (Object[]) objects[0];
									Boolean[] fieldsMandatoryMap = (Boolean[]) objects[1];
									Integer[] fieldsFormatDateMap = (Integer[]) objects[2];
									Object[] fieldsValueAux = new Object[fieldsNameMap.length];
									for (int j = 0; j < fieldsNameMap.length; j++) {
										Object ob = fieldsNameMap[j];
										fieldsValueAux[j] = field.get(ob);
									}
									arrayFieldsMap.remove(name);
									validateParametersFull(fieldsValueAux, fieldsNameMap, fieldsMandatoryMap,
											fieldsFormatDateMap, arrayFieldsMap, result, emptyFields);
								}
							}
						}
					}
				} else if (value instanceof List) { // Valida List<Map<String,
													// Object>> con Map y sus
													// key mandatorios
					T fieldGeneric = null;
					String typeClass = value.getClass().getName();
					if (typeClass.equalsIgnoreCase("java.util.ArrayList")) {
						List<Object> listObject = (List<Object>) value;
						fieldGeneric = (T) listObject;
					}
					if (((List) value).isEmpty()) {
						if (mandatory)
							emptyFields.add(name);
					} else {
						if (mandatory) {
							if (arrayFieldsMap != null && !arrayFieldsMap.isEmpty()) {
								Object[] objects = arrayFieldsMap.get(name);
								if (objects != null) {
									Object[] fieldsNameMap = (Object[]) objects[0];
									Boolean[] fieldsMandatoryMap = (Boolean[]) objects[1];
									Integer[] fieldsFormatDateMap = (Integer[]) objects[2];
									Object[] fieldsValueAux = new Object[fieldsNameMap.length];
									List<Map<String, Object>> field = (List<Map<String, Object>>) fieldGeneric;
									for (int k = 0; k < field.size(); k++) {
										for (int j = 0; j < fieldsNameMap.length; j++) {
											Object ob = fieldsNameMap[j];
											Map<String, Object> fieldMap = field.get(k);
											fieldsValueAux[j] = fieldMap.get(ob);
										}
										arrayFieldsMap.remove(name);
										validateParametersFull(fieldsValueAux, fieldsNameMap, fieldsMandatoryMap,
												fieldsFormatDateMap, arrayFieldsMap, result, emptyFields);
									}
								}
							}
						}
					}
				}
			} else {
				if (mandatory)
					emptyFields.add(name);
			}
		}
		if (emptyFields.size() > 0) {
			result.put("code", REQUEST_EMPTY_OR_NULL);
			result.put("message",
					errorMessages.get(REQUEST_EMPTY_OR_NULL) + ", invalids fields: " + emptyFields.toString());
			return;
		} else {
			result.put("code", 0);
			result.put("message", "OK");
		}
	}

	public void validateLength(Object[] fieldsValue, String[] fieldsName, Object[] fieldsLength,
			Map<String, Object> result, ArrayList<String> invalidLengthField) {
		for (int i = 0; i < fieldsValue.length; i++) {
			String name = (String) fieldsName[i];
			Object value = fieldsValue[i];
			Integer lengthValid = (Integer) fieldsLength[i];
			if (value != null) {
				if (value instanceof String) {
					String field = (String) value;
					if (field.length() > lengthValid.intValue()) {
						invalidLengthField.add(name + " (Invalid length field, expected... [" + lengthValid + "])");
					}
				}
			}
		}
		if (invalidLengthField.size() > 0) {
			result.put("code", REQUEST_INVALID_LENGTH_FIELD);
			result.put("message", errorMessages.get(REQUEST_INVALID_LENGTH_FIELD) + ", invalids fields: "
					+ invalidLengthField.toString());
			return;
		} else {
			result.put("code", 0);
			result.put("message", "OK");
		}
	}

	public Integer getResponseCode(String responseCode) {
		if (responseCode != null && !responseCode.isEmpty()) {
			boolean isInteger = isIntegerNumber(responseCode);
			if (isInteger) {
				return Integer.parseInt(responseCode);
			}
		}
		return -1;
	}

	public String getMessage(int code) {
		return errorMessages.get(code);
	}

	public Double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException("Digits a round is negative");
		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public Long roundInteger(Double value) {
		if (value == null)
			throw new IllegalArgumentException("Double value to round up is null");
		Long roundIntegerProx = Math.round(value);
		return roundIntegerProx;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getMapObject(Object obj) {
		return obj != null ? (Map<String, Object>) obj : null;
	}

	public List<GenericDto> getListMapGenericDto(List<Object> listObj) {
		if (listObj != null) {
			List<GenericDto> dto = new ArrayList<>();
			for (Object obj : listObj) {
				Map<String, Object> map = getMapObject(obj);
				GenericDto generic = new GenericDto();
				for (Map.Entry<String, Object> entry : map.entrySet()) {
					generic.put(entry.getKey(), entry.getValue());
				}
				dto.add(generic);
			}
			return dto;
		} else {
			return null;
		}
	}

	public Integer isOperationAllowed(String[] operationType, String opType) {
		for (int i = 0; i < operationType.length; i++) {
			String optType = operationType[i];
			if (opType != null && opType.equals(optType))
				return i;
		}
		return null;
	}

	public boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os != null) {
			if (os.indexOf("windows") != -1 || os.indexOf("nt") != -1 || os.equals("windows 95")
					|| os.equals("windows 98")) {
				// System.out.println("Sistema Operativo es Windows => " + os);
				return true;
			} else if (os.indexOf("mac") != -1) {
				// System.out.println("Sistema Operativo es Mac => " + os);
				return false;
			} else if (os.indexOf("linux") != -1) {
				// System.out.println("Sistema Operativo es Linux => " + os);
				return false;
			} else {
				// System.out.println("Sistema Operativo es Unix => " + os);
				return false;
			}
		} else {
			// System.out.println("Sistema Operativo no fue posible
			// identificarlo => null ");
			return false;
		}
	}

	public Integer validateParametersOperation(String[] operationType, String operation) {
		Integer access = isOperationAllowed(operationType, operation);
		if (access != null && (access < (operationType.length)))
			return access;
		return -1;
	}

	public Double getAbsValue(Double value) {
		return Math.abs(value);
	}

	public Boolean validateExpirationDate(Date expirationDate) {
		Calendar todayCalendar = Calendar.getInstance();
		Date todayDate = todayCalendar.getTime();
		return expirationDate.before(todayDate);
	}

	public int getNumberOfDecimalPlaces(Double value) {
		BigDecimal bigDecimal = new BigDecimal(value.toString());
		return Math.max(0, bigDecimal.stripTrailingZeros().scale());
	}

	private Integer getRemainDecimalCbs(Double value) {
		int res = getNumberOfDecimalPlaces(value);
		res = res > NUMBER_DECIMAL_CBS ? NUMBER_DECIMAL_CBS : res;
		Integer exponent = NUMBER_DECIMAL_CBS - res;
		return exponent;
	}

	public Integer getExponentCbs(Double amount) {
		Integer res = null;
		Integer remain = getRemainDecimalCbs(amount);
		if (remain.intValue() == 0)
			res = NUMBER_DECIMAL_CBS;
		else
			res = remain;
		return res;
	}

	private ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setTimeZone(TimeZone.getDefault());
		return mapper;
	}

//	private XmlMapper getXmlMapper() {
//		XmlMapper mapper = new XmlMapper();
//		return mapper;
//	}

	public String getSource(Class<?> source) {
		return source.getName();
	}

	public String getDuration(long duration) {
		long days = TimeUnit.MILLISECONDS.toDays(duration);
		duration -= TimeUnit.DAYS.toMillis(days);
	    long hours = TimeUnit.MILLISECONDS.toHours(duration);
	    duration -= TimeUnit.HOURS.toMillis(hours);
	    long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
	    duration -= TimeUnit.MINUTES.toMillis(minutes);
	    long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);
	    StringBuilder sb = new StringBuilder(64);
	    sb.append("[");
        sb.append(days);
        sb.append(" Days, ");
        sb.append(hours);
        sb.append(" Hours, ");
        sb.append(minutes);
        sb.append(" Minutes, ");
        sb.append(seconds);
        sb.append(" Seconds]");	    
//		int seconds = (int) (duration / 1000) % 60 ;
//		int minutes = (int) ((duration / (1000*60)) % 60);
//		int hours   = (int) ((duration / (1000*60*60)) % 24);
//		String result = String.format("[%02d hour, %02d min, %02d sec]", hours, minutes, seconds);
		return sb.toString();
	}
	
	public String getValue(Object values){
		return values!=null ? "'"+values+"'" : null;
	}
//	
//	public EventLevelLog getEventLevelLog(String evl){
//		EventLevelLog eventLevelLog = null;
//		try {
//			eventLevelLog = convertJsonToObject(evl, EventLevelLog.class);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return eventLevelLog;
//	}
}