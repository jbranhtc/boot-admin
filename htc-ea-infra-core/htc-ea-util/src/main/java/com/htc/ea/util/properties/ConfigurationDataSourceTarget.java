package com.htc.ea.util.properties;

import org.springframework.core.env.PropertySource;

import com.htc.ea.util.configentry.ConfigurationServiceDefault;
import com.htc.ea.util.configentry.ConfigurationService;

public class ConfigurationDataSourceTarget extends PropertySource<String> {

	private ConfigurationService configurationService;

	public ConfigurationDataSourceTarget() {
		super("ConfigurationDataTarget");
	}

	@Override
	public String getProperty(String name) {
		String value = null;
		if (configurationService != null) {
			value = configurationService.getValueTarget(name);
		}
		return value;
	}

	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
