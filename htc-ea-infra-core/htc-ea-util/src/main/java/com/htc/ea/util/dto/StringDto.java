package com.htc.ea.util.dto;

import java.util.HashMap;

public class StringDto extends HashMap<String, String> {
	
	private static final long serialVersionUID = 1L;
		
	public boolean containsProperty(String name) {
		return containsKey(name);
	}
	
	public void setProperty(String name, String value) {
		put(name, value);
	}

	public String getProperty(String name) {
		return get(name);
	}	
	
}
