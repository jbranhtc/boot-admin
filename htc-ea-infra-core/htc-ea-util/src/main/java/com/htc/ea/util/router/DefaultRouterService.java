package com.htc.ea.util.router;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.dto.RoutingDto;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;
import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.TransactionIdUtil;

@Service
public class DefaultRouterService implements RouterService {

	private static Logger logger = LoggerFactory.getLogger(DefaultRouterService.class);
	public static final int ROUTING_TYPE_ENABLED = 2;
	public static final int ROUTING_TYPE_LEGACY_SYSTEM = 1;
	public static final int ROUTING_TYPE_NEW_SYSTEM = 0;
	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	@Autowired
	private Environment env;
	@Autowired
	private ConfigurationService configurationService;
	private List<RoutingDto> routingList;
	Set<String> subscribers = new HashSet<String>();;
	Set<String> types = new HashSet<String>();

	@Override
	public void loadRoutingData() {
		refresh();
	}

	@SuppressWarnings("unchecked")
	private void refresh() {
		Object routingObject = null;

		routingObject = configurationDataRepository.findRoutingData();

		routingList = (List<RoutingDto>) routingObject;
		if (routingList == null) {
			routingList = new ArrayList<RoutingDto>();
			logger.info("No RouterData with routingType: " + env.getProperty(Constants.ROUTING_TYPE));
		} else {
			fillSubscriber();
			fillType();
			logger.info("RouterData ready with routingType: " + env.getProperty(Constants.ROUTING_TYPE));
		}
	}
	
	@Override
	public boolean isIncluded(String subscriberID, String serviceName) {
		String target;
		String msisdn;
		Set<String> acceptRoutingServiceList = configurationService.getValues(Constants.ROUTING_ACCEPTLIST);
		if (acceptRoutingServiceList == null) {
			acceptRoutingServiceList = new HashSet<String>();
		}
		boolean res = false;
		if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_ENABLED) {
			if (acceptRoutingServiceList.contains(serviceName))
				res = subscribers.contains(subscriberID);
			else
				res = false;
		} else if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_LEGACY_SYSTEM) {
			res = false;
		} else if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_NEW_SYSTEM) {
			res = true;
		}
		target = res ? env.getProperty(Constants.ROUTING_NEW_SYSTEM) : env.getProperty(Constants.ROUTING_LEGACY_SYSTEM);
		TransactionIdUtil.setTarget(target);
		msisdn = subscriberID;
		TransactionIdUtil.setMsisdn(msisdn);
		return res;
	}

	@Override
	public boolean isIncluded(String type, String subscriberID, String serviceName) {
		String target;
		String msisdn;
		String key = type + "|" + subscriberID;
		Set<String> acceptRoutingServiceList = configurationService.getValues(Constants.ROUTING_ACCEPTLIST);
		boolean res = false;
		if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_ENABLED) {
			if (acceptRoutingServiceList.contains(serviceName))
				res = types.contains(key);
			else
				res = false;
		} else if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_LEGACY_SYSTEM) {
			res = false;
		} else if (Integer.valueOf(env.getProperty(Constants.ROUTING_TYPE)) == ROUTING_TYPE_NEW_SYSTEM) {
			res = true;
		}
		target = res ? env.getProperty(Constants.ROUTING_NEW_SYSTEM) : env.getProperty(Constants.ROUTING_LEGACY_SYSTEM);
		TransactionIdUtil.setTarget(target);
		msisdn = subscriberID;
		TransactionIdUtil.setMsisdn(msisdn);
		return res;
	}

	private Set<String> fillSubscriber() {
		subscribers.clear();
		for (RoutingDto routing : routingList) {
			if (routing.getType().trim().equals("subscriber")) {
				subscribers.add(routing.getValue().trim());
			}
		}
		return subscribers;
	}

	private Set<String> fillType() {
		types.clear();
		for (RoutingDto routing : routingList) {
			types.add(routing.getType().trim() + "|" + routing.getValue().trim());
		}
		return types;
	}
}