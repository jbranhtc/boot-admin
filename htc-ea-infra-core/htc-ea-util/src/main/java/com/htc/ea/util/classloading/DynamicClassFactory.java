package com.htc.ea.util.classloading;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.util.Constants;

@Service
public class DynamicClassFactory implements ClassFactory {

	private static final Logger logger = LoggerFactory.getLogger(DynamicClassFactory.class);

	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private Environment env;
	private LoadClassesService loadClassesService;
	private String url;
	private String basePackage;
	private Map<String, Object> classMap = new ConcurrentHashMap<String, Object>();
	private Set<String> ownClasses = new HashSet<String>();
	private Set<String> ownClassesRedis = new HashSet<String>();
	private String msg;
	private boolean refresh;

	public DynamicClassFactory() {
	}

	public DynamicClassFactory(String url, String basePackage) {
		this.url = url;
		this.basePackage = basePackage;
	}

	public void setLoadClassesService(LoadClassesService loadClassesService) {
		this.loadClassesService = loadClassesService;
	}

	@Override
	public void loadClassesMap(boolean refresh, Set<String> listLoadClass) {
		logger.info("Loading own classes...");
		boolean classLoaded = false;
		if (configurationService.isSingleValue(Constants.OWN_CLASSES))
			this.refresh = false;
		if (refresh && listLoadClass != null && !listLoadClass.contains("")) {
			ownClassesRedis.clear();
			ownClassesRedis.addAll(listLoadClass);
			fillOwnClasses();
			if (!validateListNameClass(refresh)) {
				throw new RuntimeException("Error creating @Bean DynamicClassFactory. LoadClasses data don´t match =\n"
						+ ownClasses + " != " + ownClassesRedis);
			}
			ownClasses.clear();
			ownClasses.addAll(listLoadClass);
			classLoaded = refreshClassMap();

		} else {
			fillOwnClasses();
			fillOwnClassesRedis();
			if (!validateListNameClass(refresh)) {
				throw new RuntimeException("Error creating @Bean DynamicClassFactory. LoadClasses data don´t match =\n"
						+ ownClasses + " != " + ownClassesRedis);
			}
			classLoaded = fillClassMap();
		}
		if (!classLoaded)
			msg = "No own classes found";
		logger.info(msg);
	}

	private boolean validateListNameClass(boolean refresh) {

		if (refresh) {
			for (String string : ownClassesRedis) {
				if (ownClasses.contains(string))
					return true;
			}
		} else {
			if ((!ownClassesRedis.containsAll(ownClasses) && !ownClasses.containsAll(ownClassesRedis))
					|| ownClassesRedis.size() != ownClasses.size()) {
				return false;
			}
		}

		return true;
	}

	@Override
	public Map<String, Object> getClassMap() {
		if (classMap == null) {
			classMap = new HashMap<>();
		}
		return classMap;
	}

	@Override
	public Object getClassLoaded(String className) {
		return classMap.get(className);
	}

	private Set<String> fillOwnClasses() {
		ownClasses.clear();
		if (!configurationService.isSingleValue(Constants.OWN_CLASSES)) {
			ownClasses = configurationService.getValues(Constants.OWN_CLASSES);
		} else {
			if (Boolean.valueOf(env.getProperty(Constants.OWN_CLASSES)))
				getAllClassName();
		}

		return ownClasses;
	}

	private Set<String> fillOwnClassesRedis() {
		ownClassesRedis.clear();
		if (!configurationService.isSingleValue(Constants.OWN_CLASSES)) {
			ownClassesRedis.addAll(loadClassesService.getListLoadClass(this.refresh));
		} else {
			if (Boolean.valueOf(env.getProperty(Constants.OWN_CLASSES)))
				getAllClassName();
		}
		return ownClassesRedis;
	}

	public boolean fillClassMap() {
		msg = "Own classes loaded: \n";
		int numClass = 0;
		for (String acceptClass : ownClasses) {
			Object clase = uploadClass(acceptClass);
			if (clase != null) {
				classMap.put(acceptClass, clase);
				msg += "\t" + acceptClass + "\n";
				numClass++;
			}
		}
		if (numClass > 0)
			return true;
		return false;
	}

	public boolean refreshClassMap() {
		msg = "Own classes loaded: \n";
		int numClass = 0;
		for (String acceptClass : ownClasses) {
			if (classMap.containsKey(acceptClass)) {
				Object clase = uploadClass(acceptClass);
				if (clase != null) {
					classMap.replace(acceptClass, clase);
					msg += "\t" + acceptClass + "\n";
					numClass++;
				}
			}
		}
		if (numClass > 0)
			return true;
		return false;
	}

	public Object uploadClass(String className) {
		UtilClassLoader classLoader = new UtilClassLoader(url);
		try {
			Class<?> loadedClass = classLoader.loadClass(basePackage + "." + className);
			return loadedClass.newInstance();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void getAllClassName() {
		String sDirectorio = url.substring(5, url.length()) + basePackage.replace('.', File.separatorChar);
		File f = new File(sDirectorio);
		if (f.exists()) {
			File[] files = f.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().contains(".class")) {
					ownClasses.add(files[i].getName().substring(0, files[i].getName().length() - 6));
					ownClassesRedis.add(files[i].getName().substring(0, files[i].getName().length() - 6));
				}
			}
		} else {
			logger.info("No class to load");
		}
	}
}
