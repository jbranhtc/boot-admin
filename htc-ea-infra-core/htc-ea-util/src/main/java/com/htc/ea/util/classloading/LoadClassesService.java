package com.htc.ea.util.classloading;

import java.util.Set;

public interface LoadClassesService {

	public Set<String> getListLoadClass(boolean refresh);
}
