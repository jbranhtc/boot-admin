package com.htc.ea.util.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
public class DataSourceConfig {

	@Autowired
	private Environment env;

	@Bean(destroyMethod = "")
	@Profile("appserver")
	public DataSource jndiDataSource() {
		String jndiName = env.getProperty("jdbc.jndi");
		JndiObjectFactoryBean factory = new JndiObjectFactoryBean();
		factory.setJndiName(jndiName);
		try {
			factory.afterPropertiesSet();
		} catch (Exception e) {
			throw new RuntimeException(
					"Error, Creating @Bean DataSource with jndiName = "
							+ jndiName);
		}
		return (DataSource) factory.getObject();
	}

	@Bean(destroyMethod = "")
	@Profile("standalone")
	public DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(env
				.getProperty("jdbc.driver"));
		driverManagerDataSource.setUrl(env.getProperty("jdbc.url"));
		driverManagerDataSource.setUsername(env.getProperty("jdbc.username"));
		driverManagerDataSource.setPassword(env.getProperty("jdbc.password"));
		return driverManagerDataSource;
	}
}