package com.htc.ea.util.redis.listener;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import com.htc.ea.util.convert.LookupConverter;
import com.htc.ea.util.router.RouterService;
import com.htc.ea.util.service.ReloadCacheHelper;
import com.htc.ea.util.util.Constants;

@Service
public class RedisMessageSubscriber implements MessageListener {

	private static Logger logger = LoggerFactory.getLogger(RedisMessageSubscriber.class);
	@Autowired
	public Environment env;
	@Autowired
	public ReloadCacheHelper reloadCacheHelper;
	@Autowired
	public LookupConverter lookupConverter;
	public static List<String> messageList = new ArrayList<String>();
	@Autowired
	private RouterService routerService;

	public void setEnv(Environment env) {
		this.env = env;
	}

	public void setReloadCacheHelper(ReloadCacheHelper reloadCacheHelper) {
		this.reloadCacheHelper = reloadCacheHelper;
	}

	public void setLookupConverter(LookupConverter lookupConverter) {
		this.lookupConverter = lookupConverter;
	}

	public RouterService getRouterService() {
		return routerService;
	}

	public void setRouterService(RouterService routerService) {
		this.routerService = routerService;
	}

	public void onMessage(final Message message, final byte[] pattern) {
		try {
			String msj = new String(message.getBody());
//			 System.out.println("Mensaje recibido: " + msj);
			String[] place = msj.split(":");
			String appId = "", listName = "", classLoadNames = "";
			if (place.length > 1) {
				appId = place[0];
				listName = place[1];
				if (place.length == 3)
					classLoadNames = place[2];
				defaultRefresh(appId, listName, classLoadNames);
			} else {
				logger.info("Enviroment do not found.");
			}

		} catch (Throwable th) {
			handleListenerException(th);
		}
	}

	public void defaultRefresh(String appId, String listName, String classLoadNames) {
		String _appId = env.getProperty(Constants.APP_ID_PROPERTY);
		String _appEnv = env.getProperty(Constants.APP_ENVIRONMENT_PROPERTY);
		String _appVer = env.getProperty(Constants.APP_VERSION_PROPERTY);
		if (appId.equalsIgnoreCase(_appId + "-" + _appEnv + "-" + _appVer)) {
			// String con = lookupConverter.convertFrom("esb",
			// "voucherStatusFlag", "0");
			// System.out.println("Conversion: " + con);
			// System.out.println("Antes logging.enabled: " +
			// env.getProperty("logging.enabled"));
			// System.out.println("Antes consumer.dataType: " +
			// env.getProperty("consumer.dataType"));
			reloadCacheHelper.refresh(listName, classLoadNames, false);
			// String con1 = lookupConverter.convertFrom("esb",
			// "voucherStatusFlag", "0");
			// System.out.println("Conversion: " + con1);
			// System.out.println("Después logging.enabled: " +
			// env.getProperty("logging.enabled"));
			// System.out.println("Después consumer.dataType: " +
			// env.getProperty("consumer.dataType"));
		} else if (appId.equalsIgnoreCase(env.getProperty(Constants.GENERAL_CONFIGURATION))) {
			reloadCacheHelper.refresh(listName, "", true);
		} else if (appId.equalsIgnoreCase(Constants.ROUTING_APP_ID)
				&& listName.equalsIgnoreCase(Constants.ROUTING_MESSAGE_REDIS)) {
			logger.info("Refreshing routing...");
			refreshRoutingData();
		}
	}

	public static List<String> getMessageList() {
		return messageList;
	}

	public static void setMessageList(List<String> messageList) {
		RedisMessageSubscriber.messageList = messageList;
	}

	protected void handleListenerException(Throwable ex) {
		logger.info("Listener execution failed" + ex);
		ex.printStackTrace();
	}

	private void refreshRoutingData() {
		routerService.loadRoutingData();
	}
}