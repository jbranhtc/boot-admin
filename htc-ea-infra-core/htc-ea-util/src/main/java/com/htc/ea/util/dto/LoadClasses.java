package com.htc.ea.util.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.htc.ea.util.util.Constants;

public class LoadClasses {

	private String className;
	private String uploadDate;
	private boolean update;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	@Override
	public String toString() {
		return "LoadClasses [className=" + className + ", uploadDate=" + uploadDate + ", update=" + update + "]";
	}

}
