package com.htc.ea.util.redis.dao;

public interface ConfigurationDataRepository {
	Object findAllConfigurationDataTarget(String target);
	Object findAllConfigurationData(String applicationId, String environmentId, String version);
	Object findAllConversionData(String applicationId, String environmentId, String version);
	Object findAllEventCategoryData(String applicationId, String environmentId, String version);
	Object findAllOtherObject(String applicationId, String environmentId, String version, String listName);
	Object findAllOtherObject(String applicationId, String listName);
	public Object findRoutingData();
	public Object findLoadClass(String applicationId, String environmentId, String version);
}