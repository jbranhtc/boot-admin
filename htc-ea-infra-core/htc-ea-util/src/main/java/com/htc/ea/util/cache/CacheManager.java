package com.htc.ea.util.cache;

import java.util.Date;
import java.util.Set;

public interface CacheManager<T> {
	
	public int size();
	
	public Set<String> keySet();
	
	public void clear();
	
	public boolean containsKey(String key);
	
	public CachedObject<T> get(String key);
	
	public void put(CachedObject<T> cachedObject);
	
	public void remove(String key);
	
	public String getAsString(String key);
	
	public Date lastUpdate();

}