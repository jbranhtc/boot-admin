package com.htc.ea.util.category;

public interface EventCategoryService {

	public void loadEventCategoryData();
	public boolean verifyEventCategory(String key, String level);
	
}