package com.htc.ea.util.parsing;

import javax.xml.bind.annotation.XmlAnyElement;

public class Wrapper<T> {

	private Object clazz;

	public Wrapper(Object clazz) {
		this.clazz = clazz;
	}

	@XmlAnyElement(lax = true)
	public Object getClazz() {
		return clazz;
	}

}
