package com.htc.ea.util.service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.classloading.ClassFactory;
import com.htc.ea.util.classloading.LoadClassesService;
import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.convert.LookupConverter;
import com.htc.ea.util.properties.ConfigurationDataSource;
import com.htc.ea.util.properties.ConfigurationDataSourceTarget;
import com.htc.ea.util.util.Constants;

@Service
public class DefaultReloadCacheHelper implements ReloadCacheHelper, EnvironmentAware {
	@Autowired
	private Environment env;
	private static Logger logger = LoggerFactory.getLogger(DefaultReloadCacheHelper.class);
	@Autowired
	private ConfigurationService configurationService;
	private LookupConverter lookupConverter;
	private Environment environment;
	private boolean target;
	private EventCategoryService eventCategoryService;
	private ClassFactory classFactory;
	Set<String> listLoadClass = new HashSet<String>();
	private LoadClassesService loadClassesService;

	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	public void setLookupConverter(LookupConverter lookupConverter) {
		this.lookupConverter = lookupConverter;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public void setEventCategoryService(EventCategoryService eventCategoryService) {
		this.eventCategoryService = eventCategoryService;
	}

	public void setClassFactory(ClassFactory classFactory) {
		this.classFactory = classFactory;
	}

	public void refresh(String listName, String classLoadNames, boolean target) {
		this.target = target;
		MutablePropertySources propertySources = null;
		propertySources = propertySources();
//		System.out.println("classfiles.dev: " + env.getProperty(Constants.LOAD_CLASS_DEV));
		if (env.getProperty(Constants.LOAD_CLASS_DEV) != null
				&& Boolean.valueOf(env.getProperty(Constants.LOAD_CLASS_DEV)))
			fillListLoadClassDev(classLoadNames);
		else
			fillListLoadClassProd();

		switch (listName) {
		case "ConfigurationData":
			if (target) {
				logger.info("Refreshing configuration data target...");
				propertySources.replace(listName + "Target", configurationDataSourceTarget());
			} else {
				logger.info("Refreshing configuration data...");
				propertySources.replace(listName, configurationDataSource());
			}
			break;
		case "ConversionData":
			logger.info("Refreshing conversion...");
			refreshConversionEntry();
			break;
		case "ConfigurationFile":
			try {
				logger.info("Refreshing file configuration...");
				refreshConfigurationFile();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			break;
		case "ConfigurationEventCategoryData":
			logger.info("Refreshing event category...");
			refreshEventCategory();
			break;
		case "LoadClasses":
			logger.info("Refreshing own classes load...");
			propertySources.replace("ConfigurationData", configurationDataSource());
			classFactory.loadClassesMap(true, listLoadClass);
			break;
		default:
			logger.info("Unknown refresh in top.");
			break;
		}
	}

	private MutablePropertySources propertySources() {
		StandardEnvironment standardEnv = (StandardEnvironment) environment;
		return standardEnv.getPropertySources();
	}

	private ConfigurationDataSourceTarget configurationDataSourceTarget() {
		configurationService.refresh(this.target);
		ConfigurationDataSourceTarget propertySource = new ConfigurationDataSourceTarget();
		propertySource.setConfigurationService(configurationService);
		return propertySource;
	}

	private ConfigurationDataSource configurationDataSource() {
		configurationService.refresh(this.target);
		ConfigurationDataSource propertySource = new ConfigurationDataSource();
		propertySource.setConfigurationService(configurationService);
		return propertySource;
	}

	private void refreshConversionEntry() {
		lookupConverter.loadConversionData();
	}

	private void refreshEventCategory() {
		eventCategoryService.loadEventCategoryData();
	}

	public void setLoadClassesService(LoadClassesService loadClassesService) {
		this.loadClassesService = loadClassesService;
	}

	@SuppressWarnings("rawtypes")
	private void refreshConfigurationFile() throws UnknownHostException {
		String uri = "http://" + InetAddress.getLocalHost().getHostAddress() + ":" + env.getProperty("server.port")
				+ "/refresh";
		RestTemplate restTemplate = new RestTemplate();
		List conf = restTemplate.postForObject(uri, null, ArrayList.class);
		logger.info("Configuration updated: " + conf);
	}

	private void fillListLoadClassDev(String classLoadNames) {
		listLoadClass.clear();
		String[] classNames = classLoadNames.split("-");
		if (classNames.length > 1) {
			for (String className : classNames) {
				listLoadClass.add(className);
			}
		} else {
			listLoadClass.add(classLoadNames);
		}
	}

	private void fillListLoadClassProd() {
		listLoadClass.clear();
		listLoadClass.addAll(loadClassesService.getListLoadClass(true));
	}
}