package com.htc.ea.util.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;

@Service
public class DefaultEventCategoryService implements EventCategoryService {

	private static Logger logger = LoggerFactory.getLogger(DefaultEventCategoryService.class);
	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	private String applicationId;
	private String environmentId;
	private String version;
	private Map<String, Map<String, Boolean>> keysMap = new HashMap<String, Map<String, Boolean>>();

	@Override
	public void loadEventCategoryData() {
		refresh();
	}

	public void refresh() {
		List<ConfigurationEventCategoryData> eventCategoryList = getEventCategory();
		keysMap.clear();
		for (ConfigurationEventCategoryData eventCategoryData : eventCategoryList) {
			Map<String, Boolean> values = new HashMap<String, Boolean>();

			for (ConfigurationEventCategoryData eventCategoryData2 : eventCategoryList) {
				if (eventCategoryData2.getEventcategoryid().equals(eventCategoryData.getEventcategoryid())) {
					values.put(eventCategoryData2.getLevel().toUpperCase(), eventCategoryData2.getEnable());
				}
			}
			keysMap.put(eventCategoryData.getEventcategoryid(), values);

		}
	}

	@Override
	public boolean verifyEventCategory(String key, String level) {
		Map<String, Boolean> values = new HashMap<String, Boolean>();
		if (keysMap.containsKey(key)) {
			values = keysMap.get(key);
			if (values.get(level) != null)
				return (values.get(level));
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<ConfigurationEventCategoryData> getEventCategory() {
		List<ConfigurationEventCategoryData> eventCategoryData = new ArrayList<ConfigurationEventCategoryData>();
		Object eventCategoryDataObject;
		try {
			eventCategoryDataObject = configurationDataRepository.findAllEventCategoryData(applicationId, environmentId,
					version);
			eventCategoryData = (List<ConfigurationEventCategoryData>) eventCategoryDataObject;
		} catch (Exception e) {
			throw new RuntimeException("Error getting ConversionData. No connect @Bean jedisConnectionFactory");
		}

		if (eventCategoryData == null) {
			eventCategoryData = new ArrayList<ConfigurationEventCategoryData>();
			logger.info("No ConfigurationEventCategoryData");
		} else {
			logger.info("ConfigurationEventCategoryData ready");
		}
		return eventCategoryData;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
