package com.htc.ea.util.configuration;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.ConnectionFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.jndi.JndiTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.htc.ea.util.category.DefaultEventCategoryService;
import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.classloading.ClassFactory;
import com.htc.ea.util.classloading.DefaultLoadClassesService;
import com.htc.ea.util.classloading.DynamicClassFactory;
import com.htc.ea.util.classloading.LoadClassesService;
import com.htc.ea.util.configentry.ConfigurationService;
import com.htc.ea.util.configentry.ConfigurationServiceDefault;
import com.htc.ea.util.configentry.ConfigurationServiceFacade;
import com.htc.ea.util.convert.DefaultLookupConverter;
import com.htc.ea.util.convert.LookupConverter;
import com.htc.ea.util.log.AsyncLoggingServiceFacade;
import com.htc.ea.util.log.LoggingProducer;
import com.htc.ea.util.log.LoggingService;
import com.htc.ea.util.log.LoggingServiceFacade;
import com.htc.ea.util.log.ProducerRmq;
import com.htc.ea.util.log.Sl4jLoggingService;
import com.htc.ea.util.parsing.RabbitTemplateWrapper;
import com.htc.ea.util.redis.listener.MessagePublisher;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;
import com.htc.ea.util.redis.listener.RedisMessageSubscriber;
import com.htc.ea.util.router.DefaultRouterService;
import com.htc.ea.util.router.RouterService;
import com.htc.ea.util.service.AuditLog;
import com.htc.ea.util.service.DefaultAuditLog;
import com.htc.ea.util.service.DefaultReloadCacheHelper;
import com.htc.ea.util.service.LoadCacheInit;
import com.htc.ea.util.service.ReloadCacheHelper;
import com.htc.ea.util.util.Constants;
import com.htc.ea.util.util.FactoryBeanUtil;

@Configuration
@EnableScheduling
@EnableAsync
@Import({ RedisConfig.class, RabbitMqConfig.class })
@ComponentScan({ "com.htc.ea.util.configentry", "com.htc.ea.util.convert", "com.htc.ea.util.log",
		"com.htc.ea.util.service", "com.htc.ea.util.redis.dao", "com.htc.ea.util.parsing",
		"com.htc.ea.util.redis.listener", "com.htc.ea.util.properties", "com.htc.ea.util.util" })
public class UtilAppConfig {

	private static final Logger logger = LoggerFactory.getLogger(UtilAppConfig.class);
	@Autowired
	private Environment env;
	@Autowired
	private JedisConnectionFactory jedisConnectionFactory;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private RabbitTemplateWrapper rabbitTemplateWrapper;
	@Autowired
	private AuditLog auditLog;
	@Autowired
	private ConfigurationServiceFacade configurationServiceFacade;
	private DefaultReloadCacheHelper reloadCacheHelper;
	private DefaultLookupConverter lookupConverter;
	private ConfigurationServiceDefault configurationService;
	public static final String DEFAULT_PROPERTIES_FILE = "app.properties";
	public static final String APP_ID_PROPERTY = "application.id";
	public static final String APP_ENVIRONMENT_PROPERTY = "app.env";
	public static final String APP_VERSION_PROPERTY = "application.version";
	private LoadCacheInit loadCacheInit;

	@Bean
	public LoadCacheInit loadCacheInit() {
		loadCacheInit = new LoadCacheInit();
		loadCacheInit.setAuditLog(auditLog);
		loadCacheInit.setEnv(env);
		loadCacheInit.setConfigurationService(configurationServiceFacade);
		loadCacheInit.setLoggingServiceFacade(loggingServiceFacade());
		loadCacheInit.setLookupConverter(lookupConverter());
		loadCacheInit.setRouterService(routerService());
		loadCacheInit.setEventCategoryService(eventCategoryService());
		loadCacheInit.setClassFactory(classFactory());
		loadCacheInit.load();
		return loadCacheInit;
	}

	@Bean
	public LoggingService loggingService() {
		logger.info("Getting configurationData...");
		configurationServiceFacade.loadConfigurationData();
		configurationServiceFacade.loadConfigurationDataTarget();
		LoggingService loggingService = null;
		LoggingProducer loggingProducer = null;
		ProducerRmq producerRmq = null;
		String loggingEnabled = this.env.getProperty(Constants.LOGGING_ENABLED);
		// Properties configProperties = configProperties();
		String loggingImpl = this.env.getProperty(Constants.LOGGING_IMPL) != null
				? this.env.getProperty(Constants.LOGGING_IMPL) : Constants.FILE_LOG;
		logger.info("Creating @Bean loggingService with implementation: {}, logging enabled {}", loggingImpl,
				loggingEnabled);
		try {
			if (Constants.FILE_LOG.equalsIgnoreCase(loggingImpl) || "".equalsIgnoreCase(loggingImpl)) {
				loggingService = new Sl4jLoggingService();
				return loggingService;
			} else if (Constants.QUEUE_WLS_LOG.equalsIgnoreCase(loggingImpl)) {
				String factory = this.env.getProperty(Constants.JMS_WLS_FACTORY);
				String provider = this.env.getProperty(Constants.JMS_WLS_PROVIDER);
				String jmsConnectionFactory = this.env.getProperty(Constants.JMS_CONNECTION_FACTORY);
				String jmsQueue = this.env.getProperty(Constants.JMS_QUEUE);
				JndiTemplate jndiTemplate = FactoryBeanUtil.getJndiTemplate(factory, provider);
				ConnectionFactory connectionFactory = FactoryBeanUtil.createJmsResource(jmsConnectionFactory,
						jndiTemplate, ConnectionFactory.class);
				loggingProducer = new LoggingProducer();
				loggingProducer.setDestination(FactoryBeanUtil.getDestination(factory, provider, jmsConnectionFactory,
						jmsQueue, jndiTemplate));
				loggingProducer.setJmsTemplate(FactoryBeanUtil.getJmsTemplate(connectionFactory, jndiTemplate));
				return loggingProducer;
			} else if (Constants.QUEUE_RABBIT_LOG.equalsIgnoreCase(loggingImpl)) {
				producerRmq = new ProducerRmq();
				producerRmq.setRabbitTemplateWrapper(rabbitTemplateWrapper);
				return producerRmq;
			}
			logger.debug("Successful creation of @Bean loggingService");
		} catch (Exception ex) {
			throw new RuntimeException("Error creating @Bean loggingService impl = " + loggingImpl, ex);
		}
		return loggingService = new Sl4jLoggingService();
	}

	@Bean
	public LoggingServiceFacade loggingServiceFacade() {
		AsyncLoggingServiceFacade facade = new AsyncLoggingServiceFacade();
		// Properties configProperties = configProperties();
		facade.setApplicationId(env.getProperty(Constants.APP_ID_PROPERTY));
		facade.setEnvironmentId(env.getProperty(Constants.APP_ENVIRONMENT_PROPERTY));
		facade.setVersion(env.getProperty(Constants.APP_VERSION_PROPERTY));
		facade.setLoggingService(loggingService());
		// configurationServiceFacade.loadConfigurationData();
		// configurationServiceFacade.loadConfigurationDataTarget();
		// loadCacheInit();
		return facade;
	}

	@Bean
	public ConfigurationService configurationService() {
		this.configurationService = new ConfigurationServiceDefault();
		this.configurationService.setApplicationId(this.env.getProperty("application.id"));
		this.configurationService.setEnvironmentId(this.env.getProperty("app.env"));
		this.configurationService.setVersion(this.env.getProperty("application.version"));
		return this.configurationService;
	}

	@Bean
	public LookupConverter lookupConverter() {
		this.lookupConverter = new DefaultLookupConverter();
		this.lookupConverter.setApplicationId(this.env.getProperty("application.id"));
		this.lookupConverter.setEnvironmentId(this.env.getProperty("app.env"));
		this.lookupConverter.setVersion(this.env.getProperty("application.version"));
		return this.lookupConverter;
	}

	@Bean
	public ReloadCacheHelper reloadCacheHelper() {
		this.reloadCacheHelper = new DefaultReloadCacheHelper();
		this.reloadCacheHelper.setConfigurationService(this.configurationService);
		this.reloadCacheHelper.setLookupConverter(this.lookupConverter);
		this.reloadCacheHelper.setEnvironment(this.env);
		this.reloadCacheHelper.setEventCategoryService(eventCategoryService());
		this.reloadCacheHelper.setClassFactory(classFactory());
		this.reloadCacheHelper.setLoadClassesService(loadClassesService());
		return this.reloadCacheHelper;
	}

	@Bean
	public MessagePublisher redisPublisher() {
		return new RedisMessagePublisher(this.redisTemplate, topic());
	}

	@Bean
	public MessageListenerAdapter messageListener() {
		RedisMessageSubscriber redisMessageSubscriber = new RedisMessageSubscriber();
		redisMessageSubscriber.setEnv(this.env);
		redisMessageSubscriber.setLookupConverter(lookupConverter());
		redisMessageSubscriber.setReloadCacheHelper(reloadCacheHelper());
		redisMessageSubscriber.setRouterService(routerService());
		return new MessageListenerAdapter(redisMessageSubscriber);
	}

	@Bean
	public RedisMessageListenerContainer redisContainer() {
		Map<MessageListener, Collection<? extends Topic>> listener = new HashMap<MessageListener, Collection<? extends Topic>>();
		listener.put(messageListener(), Collections.singleton(topic()));
		RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(this.jedisConnectionFactory);
		container.setMessageListeners(listener);
		return container;
	}

	@Bean
	public ChannelTopic topic() {
		logger.info("Channel name: " + this.env.getProperty(Constants.REDIS_CHANNEL_NAME));
		return new ChannelTopic(this.env.getProperty(Constants.REDIS_CHANNEL_NAME));
	}

	@Bean
	public AuditLog producerObject() {
		DefaultAuditLog producerObject = new DefaultAuditLog();
		producerObject.setRabbitTemplateWrapper(rabbitTemplateWrapper);
		return producerObject;
	}

	@Bean
	public RouterService routerService() {
		DefaultRouterService routerService = new DefaultRouterService();
		return routerService;
	}

	@Bean
	public EventCategoryService eventCategoryService() {
		DefaultEventCategoryService defaultEventCategoryService = new DefaultEventCategoryService();
		defaultEventCategoryService.setApplicationId(this.env.getProperty("application.id"));
		defaultEventCategoryService.setEnvironmentId(this.env.getProperty("app.env"));
		defaultEventCategoryService.setVersion(this.env.getProperty("application.version"));
		return defaultEventCategoryService;
	}

	@Bean
	public ClassFactory classFactory() {
		String url = env.getProperty(Constants.CLASS_PATH);
		String basePackage = env.getProperty(Constants.CLASS_FILES_PACKAGE);
		DynamicClassFactory classFactory = new DynamicClassFactory(url, basePackage);
		classFactory.setLoadClassesService(loadClassesService());
		return classFactory;
	}

	@Bean
	public LoadClassesService loadClassesService() {
		DefaultLoadClassesService loadClassesService = new DefaultLoadClassesService();
		loadClassesService.setApplicationId(this.env.getProperty("application.id"));
		loadClassesService.setEnvironmentId(this.env.getProperty("app.env"));
		loadClassesService.setVersion(this.env.getProperty("application.version"));
		return loadClassesService;
	}
}