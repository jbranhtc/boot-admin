package com.htc.ea.util.properties;

import org.springframework.core.env.PropertySource;

import com.htc.ea.util.configentry.ConfigurationServiceDefault;
import com.htc.ea.util.configentry.ConfigurationService;

public class ConfigurationDataSource extends PropertySource<String> {

	private ConfigurationService configurationService;

	public ConfigurationDataSource() {
		super("ConfigurationData");
	}

	@Override
	public String getProperty(String name) {
		String value = null;
		if (configurationService != null) {
			value = configurationService.getValue(name);
		}
		return value;
	}

	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
