package com.htc.ea.util.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.htc.ea.util.configuration.RabbitMqConfig;
import com.htc.ea.util.parsing.RabbitTemplateWrapper;
import com.htc.ea.util.util.Constants;

public class DefaultAuditLog implements AuditLog {

	@Autowired
	private Environment env;
	private RabbitTemplateWrapper rabbitTemplateWrapper;
	public static final String AUDIT_QUEUE_ENABLED = "audit.queue.enabled";

	public void setRabbitTemplateWrapper(RabbitTemplateWrapper rabbitTemplateWrapper) {
		this.rabbitTemplateWrapper = rabbitTemplateWrapper;
	}

	@Override
	public void sendAudit(Object objectData) {
		boolean activeAuditQueue = Boolean.parseBoolean(this.env.getProperty(AUDIT_QUEUE_ENABLED));
		if (activeAuditQueue) {
			rabbitTemplateWrapper.getRabbitTemplateMap().get(Constants.AUDIT_LOG).convertAndSend(
					env.getProperty(Constants.AUDIT_AMQP_EXCHANGE), RabbitMqConfig.getAuditRoutingKey(),
					objectData);
		}
	}
}