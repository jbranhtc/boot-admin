package com.htc.ea.util.router;

public interface RouterService {
	public void loadRoutingData();
	/**
	 * @param subscriberID ID del subscriptor
	 * @param serviceName Nombre del Servicio
	 * @return
	 */
	public boolean isIncluded(String subscriberID, String serviceName);
	/**
	 * @param type
	 * @param subscriberID
	 * @param serviceName
	 * @return
	 */
	public boolean isIncluded(String type, String subscriberID, String serviceName);
}