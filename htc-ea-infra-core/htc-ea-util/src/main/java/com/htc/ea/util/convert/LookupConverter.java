package com.htc.ea.util.convert;


public interface LookupConverter {
	
	public String convertTo(String appDest, String topicId, String value);
	
	public String convertFrom(String appSource, String topicId, String value);
	
	public void loadConversionData();
	
}
