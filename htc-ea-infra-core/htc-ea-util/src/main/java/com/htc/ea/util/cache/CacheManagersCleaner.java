package com.htc.ea.util.cache;

import java.util.ArrayList;
import java.util.List;

public class CacheManagersCleaner {
	
	private List<CacheManager<?>> managers = new ArrayList<CacheManager<?>>();
	
	public void clearAll() {
		for (CacheManager<?> manager : managers) {
			manager.clear();
		}
	}
	
	public int size() {
		return managers.size();
	}

	public void setManagers(List<CacheManager<?>> managers) {
		this.managers = managers;
	}	

}
