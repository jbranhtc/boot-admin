package com.htc.ea.util.configuration;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.htc.ea.util.parsing.RabbitTemplateWrapper;
import com.htc.ea.util.util.Constants;

@Configuration
public class RabbitMqConfig {

	@Value("#{environment['application.id']}")
	private String applicationId;
	@Autowired
	private Environment env;
	private static String logRoutingKey = "";
	private static String auditRoutingKey = "";

	public ConnectionFactory connectionFactoryLog() {
		CachingConnectionFactory connectionFactoryLog = new CachingConnectionFactory();
		connectionFactoryLog.setUsername(env.getProperty(Constants.AMQP_USER));
		connectionFactoryLog.setPassword(env.getProperty(Constants.AMQP_PASSWORD));
		connectionFactoryLog.setHost(env.getProperty(Constants.LOG_AMQP_IP));
		connectionFactoryLog.setPort(Integer.valueOf(env.getProperty(Constants.LOG_AMQP_PORT)));
		connectionFactoryLog.setChannelCacheSize(50);
		return connectionFactoryLog;
	}

	public ConnectionFactory connectionFactoryAudit() {
		CachingConnectionFactory connectionFactoryAudit = new CachingConnectionFactory();
		connectionFactoryAudit.setUsername(env.getProperty(Constants.AMQP_USER));
		connectionFactoryAudit.setPassword(env.getProperty(Constants.AMQP_PASSWORD));
		connectionFactoryAudit.setHost(env.getProperty(Constants.AUDIT_AMQP_IP));
		connectionFactoryAudit.setPort(Integer.valueOf(env.getProperty(Constants.AUDIT_AMQP_PORT)));
		connectionFactoryAudit.setChannelCacheSize(50);
		return connectionFactoryAudit;
	}

	@Bean
	public MessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public RabbitTemplateWrapper rabbitTemplateWrapper() {
		RabbitTemplateWrapper rabbitTemplateWrapper = new RabbitTemplateWrapper();
		String loggingImpl = this.env.getProperty(Constants.LOGGING_IMPL);
		boolean activeAuditQueue = Boolean.parseBoolean(this.env.getProperty(Constants.AUDIT_QUEUE_ENABLED));
		String[] logRoutingKeyArray = env.getProperty(Constants.AUDIT_AMQP_BINDING_WITH).split("\\.");
//		auditRoutingKey = logRoutingKeyArray[0] + "." + logRoutingKeyArray[1] + "." + applicationId;
		auditRoutingKey = env.getProperty(Constants.AUDIT_AMQP_BINDING_WITH);
//		System.out.println("AUDITROUTINGKEY = "+auditRoutingKey);
		String[] auditRoutingKeyArray = env.getProperty(Constants.LOG_AMQP_BINDING_WITH).split("\\.");
//		logRoutingKey = auditRoutingKeyArray[0] + "." + auditRoutingKeyArray[1] + "." + applicationId;
		logRoutingKey = env.getProperty(Constants.LOG_AMQP_BINDING_WITH);
//		System.out.println("LOGROUTINGKEY = "+logRoutingKey);
		if (Constants.QUEUE_RABBIT_LOG.equalsIgnoreCase(loggingImpl)) {
			RabbitTemplate templateLog = new RabbitTemplate(connectionFactoryLog());
			templateLog.setMessageConverter(jsonMessageConverter());
			rabbitTemplateWrapper.getRabbitTemplateMap().put(Constants.INFRA_LOG, templateLog);
		}

		if (activeAuditQueue) {
			RabbitTemplate templateAudit = new RabbitTemplate(connectionFactoryAudit());
			templateAudit.setMessageConverter(jsonMessageConverter());
			rabbitTemplateWrapper.getRabbitTemplateMap().put(Constants.AUDIT_LOG, templateAudit);
		}

		return rabbitTemplateWrapper;
	}

	public static String getLogRoutingKey() {
		return logRoutingKey;
	}

	public static String getAuditRoutingKey() {
		return auditRoutingKey;
	}
	
//	@Bean
//	public Queue qs() {
//		return new Queue("textQueue");
//	}

	//Listener for consumer queue
	//@Bean
	// public SimpleMessageListenerContainer listenerContainer() {
	// SimpleMessageListenerContainer listenerContainer = new
	// SimpleMessageListenerContainer();
	// listenerContainer.setConnectionFactory(connectionFactory());
	// listenerContainer.setQueues(queue());
	//// listenerContainer.setMessageConverter(jsonMessageConverter());
	//// listenerContainer.setMessageListener(new Consumer());
	// listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
	// return listenerContainer;
	// }

}