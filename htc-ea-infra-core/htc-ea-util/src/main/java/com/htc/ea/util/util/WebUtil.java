package com.htc.ea.util.util;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * Created by Stanley Rodriguez on 24/05/17
 *
 * Description: Setea cabeceras de response para los servicios rest de provider y envoltura de respuestas http
 */

@Component
public class WebUtil {

    public <T> ResponseEntity<T> createOkResponse(T body, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
        return createResponse(body, HttpStatus.OK, loggingTrace, serviceCode, serviceDescription, headersMap);
    }
    
    public <T> ResponseEntity<T> createFailResponse(T body, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
        return createResponse(body, HttpStatus.CONFLICT, loggingTrace, serviceCode, serviceDescription, headersMap);
    }
    
    public <T> ResponseEntity<T> createNotDataResponse(T body, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
        return createResponse(body, HttpStatus.NOT_FOUND, loggingTrace, serviceCode, serviceDescription, headersMap);
    }
    
    public <T> ResponseEntity<T> createBadReqResponse(T body, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
        return createResponse(body, HttpStatus.BAD_REQUEST, loggingTrace, serviceCode, serviceDescription, headersMap);
    }

    public <T> ResponseEntity<T> createCustomizedResponse(T body, Integer statusCode, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
    	HttpStatus httpStatus = HttpStatus.valueOf(statusCode.intValue());    	
    	return createResponse(body, httpStatus, loggingTrace, serviceCode, serviceDescription, headersMap);
    }

    private <T> ResponseEntity<T> createResponse(T body, HttpStatus httpStatus, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap) {
    	HttpHeaders httpHeaders = createHeadersResponse(httpStatus, loggingTrace, serviceCode, serviceDescription, headersMap);
    	if (body==null)
    		return new ResponseEntity<T>(httpHeaders, httpStatus);
    	else
    		return new ResponseEntity<T>(body, httpHeaders, httpStatus);
    }
    
    private String[] getServiceResult(HttpStatus httpStatus, Long serviceCode, String serviceDescription) {
    	String serviceDescriptionStr = com.htc.ea.util.util.Constants.CODE_SUCCESS_VALUE;
    	String serviceCodeStr = String.valueOf(com.htc.ea.util.util.Constants.CODE_SUCCESS_KEY);
    	if (serviceCode==null) {
    		boolean isError = isError(httpStatus);
    		if (isError) {
    			serviceCodeStr = String.valueOf(httpStatus.value());
    			if (serviceDescription==null)
    				serviceDescriptionStr = com.htc.ea.util.util.Constants.CODE_FAILED_VALUE;
    			else
    				serviceDescriptionStr = serviceDescription;
    		}
    	}else{
    		serviceCodeStr = serviceCode.toString();
    		serviceDescriptionStr = serviceDescription;
    	}
    	return new String[]{serviceCodeStr, serviceDescriptionStr};
    }
    
    private HttpHeaders createHeadersResponse(HttpStatus httpStatus, String loggingTrace, Long serviceCode, String serviceDescription, Map<String, String> headersMap){
    	String[] serviceResultArr = getServiceResult(httpStatus, serviceCode, serviceDescription);
        String[] loggingTraceArr = getLoggingTrace(loggingTrace);
        String xUserAgent = loggingTraceArr[2]; //String.format(com.htc.ea.util.util.Constants.X_USER_AGENT_VALUE, loggingTraceArr[2]);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(new MediaType("application","json")));
		headers.set(com.htc.ea.util.util.Constants.OPERATION_REF_ID, TransactionIdUtil.getId());
		headers.set(com.htc.ea.util.util.Constants.UNTIL_LAST_SEQUENCE, TransactionIdUtil.getUntilLastSequence().toString());
		headers.set(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, xUserAgent);
		headers.set(com.htc.ea.util.util.Constants.SERVICE_CODE, serviceResultArr[0]);
		headers.set(com.htc.ea.util.util.Constants.SERVICE_DESCRIPTION, serviceResultArr[1]);
		if (headersMap!=null) headers.setAll(headersMap);
		TransactionIdUtil.end();
        return headers;
    }
        
    private String[] getLoggingTrace(String loggingTrace) {
    	if (loggingTrace==null) loggingTrace = "";
    	String[] result = loggingTrace.split("\\:");
    	if (result.length!=3) {
    		TransactionIdUtil.setOperationRefId(TransactionIdUtil.getId());
    		TransactionIdUtil.setSequence(0);
    		result = new String[]{"","",String.format(Constants.X_USER_AGENT_VALUE, "Gateway")};
    	}
    	return result;    			
    }
    
    public static boolean isError(HttpStatus status) {
		HttpStatus.Series series = status.series();
		return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series)
				|| HttpStatus.INTERNAL_SERVER_ERROR.equals(status));
	}
	
    public boolean isIntegerNumber(String data) {
    	String regex = "^-*[0-9]+$"; //uno o mas digitos numericos enteros positivos o negativos
    	Pattern queryLangPattern = Pattern.compile(regex);
    	Matcher matcher = queryLangPattern.matcher(data);
    	return matcher.matches();
    }
	
	private String validateHeaders(Map<String, String> headersMap) throws IllegalArgumentException {
		String operationRefId = headersMap.get(Constants.OPERATION_REF_ID.toLowerCase());
		String untilLastSequence = headersMap.get(Constants.UNTIL_LAST_SEQUENCE.toLowerCase());
		String xUserAgent = headersMap.get(Constants.X_USER_AGENT_KEY.toLowerCase());
		StringBuilder result = new StringBuilder(Constants.VALIDATE_HEADERS_MSG);
		int count = 0;
		if (!(operationRefId!=null && !operationRefId.isEmpty())) {
			count++;
			String refId = "is empty";
			if (operationRefId==null) refId = null;
			result.append(" " + Constants.OPERATION_REF_ID + " = ").append(refId);
		}
		if (!(untilLastSequence!=null && !untilLastSequence.isEmpty())) {
//			count++;
//			String sequence = "is empty";
//			if (untilLastSequence==null) sequence = null;
//			result.append(" " + Constants.UNTIL_LAST_SEQUENCE + " = ").append(sequence);
			TransactionIdUtil.setSequence(0);
			untilLastSequence = String.valueOf(TransactionIdUtil.getUntilLastSequence());
			headersMap.put(Constants.UNTIL_LAST_SEQUENCE, untilLastSequence);
		}
		if (!(xUserAgent!=null && !xUserAgent.isEmpty())) {
//			count++;
//			String agent = "is empty";
//			if (xUserAgent==null) agent = null; 
//			result.append(" " + Constants.X_USER_AGENT_KEY + " = ").append(agent);
			headersMap.put(Constants.X_USER_AGENT_KEY, String.format(Constants.X_USER_AGENT_VALUE, "Gateway"));
		}
		boolean isNumber = isIntegerNumber(untilLastSequence);
		if (!isNumber) {
			count++;
			result.append(" " + Constants.UNTIL_LAST_SEQUENCE + " = ").append(" is not a number");
		}
		if (operationRefId!=null && !operationRefId.isEmpty()) {
			int length = operationRefId.length();
			if (length!=32) {
				count++;
				result.append(" " + Constants.OPERATION_REF_ID + " = ").append(length).append(" length is not equal to 32"); 
			}			
		}
		if (count>0) throw new IllegalArgumentException(result.toString());
		return getLoggingTrace(headersMap);		
	}
		
	public String getLoggingTrace(Map<String, String> headersMap) throws IllegalArgumentException{
		//operationRefId-untilLastSequence-gateway
		String[] loggingTraceArr = getHeadersRequest(headersMap);
		TransactionIdUtil.setOperationRefId(loggingTraceArr[0]);
		TransactionIdUtil.setSequence(Integer.parseInt(loggingTraceArr[1]));
		String loggingTrace = createLoggingTrace(TransactionIdUtil.getId(), String.valueOf(TransactionIdUtil.getUntilLastSequence()), loggingTraceArr[2]);
		return loggingTrace;
	}
	
	private String[] getHeadersRequest(Map<String, String> headersMap) throws IllegalArgumentException{
		String operationRefId = headersMap.get(Constants.OPERATION_REF_ID.toLowerCase());
		String untilLastSequence = headersMap.get(Constants.UNTIL_LAST_SEQUENCE.toLowerCase());
		String xUserAgent = headersMap.get(Constants.X_USER_AGENT_KEY.toLowerCase());
		String[] headersRequest = new String[]{operationRefId, untilLastSequence, xUserAgent}; 
		return headersRequest;
	}
	
	private String createLoggingTrace(String operationRefId, String untilLastSequence, String gateway) {
		return operationRefId + ":" + untilLastSequence + ":" + gateway;
	}
	
	public String validateHeaders(HttpHeaders httpHeaders) {
		Map<String, String> headersMap = null;
		String loggingTrace = "";
		try {
			if (httpHeaders==null) throw new IllegalArgumentException("Request headers not found");
			headersMap = httpHeaders.toSingleValueMap();
			loggingTrace = validateHeaders(headersMap);			
		} catch (Exception e) {
			loggingTrace = e.getMessage();	
		}
		return loggingTrace;
	}
	
	public String getHeaderByName(HttpHeaders httpHeaders, String name) {
		String result = null;
		if (httpHeaders==null) return result;
		return httpHeaders.toSingleValueMap().get(name);
	}
}