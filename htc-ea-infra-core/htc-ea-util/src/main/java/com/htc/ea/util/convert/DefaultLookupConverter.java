package com.htc.ea.util.convert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;
import com.htc.ea.util.service.DefaultReloadCacheHelper;

@Service
public class DefaultLookupConverter implements LookupConverter {

	private static Logger logger = LoggerFactory.getLogger(DefaultLookupConverter.class);
	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	private String applicationId;
	private String environmentId;
	private String version;
	private Map<String, ConversionData> keysMap = new HashMap<String, ConversionData>();
	private Map<String, ConversionData> valuesMap = new HashMap<String, ConversionData>();

	private String getMapKey(String applicationId, String topicId, String value) {
		return applicationId + "|" + topicId + "|" + value;
	}

	private String resolveValue(String appSource, String appDest, String topicId, String value) {
		ConversionData conversionSource = valuesMap.get(getMapKey(appSource, topicId, value));
		if (conversionSource != null) {
			ConversionData conversionDest = keysMap.get(getMapKey(appDest, topicId, conversionSource.getKey()));
			if (conversionDest != null) {
				return conversionDest.getValue();
			}
		}
		return null;
	}

	@Override
	public String convertTo(String appDest, String topicId, String value) {
		return resolveValue(applicationId, appDest, topicId, value);
	}

	@Override
	public String convertFrom(String appSource, String topicId, String value) {
		return resolveValue(appSource, applicationId, topicId, value);
	}

	public void loadConversionData() {
		refresh();
	}

	public void refresh() {
		List<ConversionData> conversionsList = getConversions(applicationId);
		keysMap.clear();
		valuesMap.clear();
		for (ConversionData conversionData : conversionsList) {
			keysMap.put(
					getMapKey(conversionData.getApplicationId(), conversionData.getTopicId(), conversionData.getKey()),
					conversionData);
			valuesMap.put(getMapKey(conversionData.getApplicationId(), conversionData.getTopicId(),
					conversionData.getValue()), conversionData);
		}
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@SuppressWarnings("unchecked")
	public List<ConversionData> getConversions(String applicationId) {
		List<ConversionData> conversionData = new ArrayList<ConversionData>();
		Object conversionDataObject;
		try {
			conversionDataObject = configurationDataRepository.findAllConversionData(applicationId, environmentId,
					version);
			conversionData = (List<ConversionData>) conversionDataObject;
		} catch (Exception e) {
			throw new RuntimeException("Error getting ConversionData. No connect @Bean jedisConnectionFactory");
		}

		if (conversionData == null) {
			conversionData = new ArrayList<ConversionData>();
			logger.info("No ConversionData");
		}
		else{
			logger.info("ConversionData ready");
		}
		return conversionData;
	}
}
