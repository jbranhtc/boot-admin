package com.htc.ea.util.log;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

import com.htc.ea.util.category.EventCategoryService;

public class AsyncLoggingServiceFacade implements LoggingServiceFacade {
	
	private static final Logger logger = LoggerFactory.getLogger(AsyncLoggingServiceFacade.class);
	
	@Autowired
	private Environment env;
	@Autowired
	private EventCategoryService eventCategoryService;
	@Value("#{environment['application.id']}")
	private String applicationId;
	@Value("#{environment['application.environment']}")
	private String environmentId;
	@Value("#{environment['application.version']}")
	private String version;
	private LoggingService loggingService;
	
	public AsyncLoggingServiceFacade() {
	}

	private void fillProperties(EventData eventData) {
		eventData.setApplicationId(applicationId);
		eventData.setEnvironmentId(environmentId);
		eventData.setVersion(version);
	}

	@Async
	@Override	
	public void log(EventData eventData) {
		if (logging(eventData)) {
			fillProperties(eventData);
			try {
//				System.out.println(eventData);
				loggingService.log(eventData);
			} catch (Exception e) {
				logger.error("log: Error in loggingService.log (eventData = " + eventData + ")", e);
			}
		}
	}

	@Async
	@Override
	public void log(String level, String source, String originRefId, String message, String detail, Long duration,
			Throwable throwable) {
		EventData eventData = new EventData();
		eventData.setLevel(level);
		eventData.setSource(source);
		eventData.setOriginReferenceId(originRefId);
		eventData.setMessage(message);
		eventData.setDetail(detail);
		eventData.setDuration(duration);
		if (throwable != null) {
			StringWriter writer = new StringWriter();
			throwable.printStackTrace(new PrintWriter(writer));
			eventData.setException(writer.toString());			
//			eventData.setException(throwable.getMessage());
		}
		
		log(eventData);
	}

	@Async
	@Override
	public void trace(String source, String message, Throwable throwable) {
		log(EventLevel.TRACE.toString(), source, null, message, null, null, throwable);
	}

	@Async
	@Override
	public void trace(String source, String message) {
		trace(source, message, null);
	}

	@Async
	@Override
	public void debug(String source, String originRefId, String message, String detail, Long duration, Throwable throwable) {
		log(EventLevel.DEBUG.toString(), source, originRefId, message, detail, duration, throwable);
	}

	@Async
	@Override
	public void debug(String source, String originRefId, String message, String detail, Long duration) {
		debug(source, originRefId, message, detail, duration, null);
	}

	@Async
	@Override
	public void info(String source, String originRefId, String message, String detail, Long duration, Throwable throwable) {
		log(EventLevel.INFO.toString(), source, originRefId, message, detail, duration, throwable);
	}

	@Async
	@Override
	public void info(String source, String originRefId, String message, String detail, Long duration) {
		info(source, originRefId, message, detail, duration,null);
	}

	@Async
	@Override
	public void warn(String source, String message, Throwable throwable) {
		log(EventLevel.WARN.toString(), source, null, message, null, null, throwable);
	}

	@Async
	@Override
	public void warn(String source, String message) {
		warn(source, message, null);
	}

	@Async
	@Override
	public void fatal(String source, String message, Throwable throwable) {
		log(EventLevel.FATAL.toString(), source, null, message, null, null, throwable);
	}

	@Async
	@Override
	public void fatal(String source, String message) {
		fatal(source, message, null);
	}

	@Async
	@Override
	public void error(String source, String originRefId, String message, String detail, Throwable throwable) {
		log(EventLevel.ERROR.toString(), source, originRefId, message, detail, null, throwable);		
	}

	@Async
	@Override
	public void error(String source, String originRefId, String message, String detail) {
		error(source, originRefId, message, detail, null);		
	}
	
	@Override
	public void setLoggingService(LoggingService loggingService) {
		this.loggingService = loggingService;
	}	
	
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean logging(EventData eventData) {
		boolean loggingEnabled = Boolean.parseBoolean(env.getProperty("logging.enabled"));
		boolean eventLevelEnabled = eventCategoryService.verifyEventCategory(eventData.getCategory(), eventData.getLevel());
		return loggingEnabled && eventLevelEnabled;
	}
}