package com.htc.ea.util.classloading;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.dto.LoadClasses;
import com.htc.ea.util.redis.dao.ConfigurationDataRepository;

@Service
public class DefaultLoadClassesService implements LoadClassesService {

	@Autowired
	private ConfigurationDataRepository configurationDataRepository;
	private String applicationId;
	private String environmentId;
	private String version;
	private Set<String> ownClasses = new HashSet<String>();
	private List<LoadClasses> loadClasses = new ArrayList<LoadClasses>();

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getListLoadClass(boolean refresh) {
		ownClasses.clear();
		loadClasses.clear();
		Object loadClassObject;
		loadClassObject = configurationDataRepository.findLoadClass(applicationId, environmentId, version);
		loadClasses = (List<LoadClasses>) loadClassObject;

		if (loadClasses == null)
			loadClasses = new ArrayList<LoadClasses>();
		for (LoadClasses loadClasse : loadClasses) {
			if (!refresh)
				loadClasse.setUpdate(true);
			if (loadClasse.isUpdate())
				ownClasses.add(loadClasse.getClassName());
		}
		return ownClasses;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getEnvironmentId() {
		return environmentId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
