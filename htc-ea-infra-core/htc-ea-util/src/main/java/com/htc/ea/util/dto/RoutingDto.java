package com.htc.ea.util.dto;

import java.io.Serializable;

public class RoutingDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String context;
	private boolean enabled;
	private String type;
	private String value;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Routing [id=" + id + ", context=" + context + ", enabled="
				+ enabled + ", type=" + type + ", value=" + value + "]";
	}
}