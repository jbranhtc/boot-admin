package com.htc.ea.util.util;

import java.util.UUID;

public class TransactionIdUtil {

	private static ThreadLocal<Integer> sequenceId = new ThreadLocal<Integer>();
	private static ThreadLocal<String> target = new ThreadLocal<String>();
	private static ThreadLocal<String> msisdn = new ThreadLocal<String>();	
	
	public static String getTarget() {
		return target.get();
	}

	public static void setTarget(String target) {
		TransactionIdUtil.target.set(target);
	}
	
	public static String getMsisdn() {
		return msisdn.get();
	}

	public static void setMsisdn(String msisdn) {
		TransactionIdUtil.msisdn.set(msisdn);
	}

	public static Integer getSequence() {
		return sequenceId.get();
	}

	public static void setSequence(Integer seq) {
		sequenceId.set(seq);
	}
	
	public static String nextSequenceId() {
		int sequence = getSequence();
		setSequence(sequence += 1);
		return String.valueOf(getSequence());
	}

	private static ThreadLocal<String> threadLocal = new ThreadLocal<String>() {
		@Override
		protected String initialValue() {
			return generateId();
		}
	};

	public static void begin() {
		setMsisdn("");
		setTarget("");
		setSequence(0);
		threadLocal.set(generateId());
	}

	public static String getId() {
		return threadLocal.get();
	}

	public static void end() {
		threadLocal.remove();
		sequenceId.remove();
		target.remove();
		msisdn.remove();
	}

	public static String generateId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static void setOperationRefId(String operationRefId) {
		TransactionIdUtil.threadLocal.set(operationRefId);
	}

	public static Integer getUntilLastSequence() {
		return getSequence();
	}
}