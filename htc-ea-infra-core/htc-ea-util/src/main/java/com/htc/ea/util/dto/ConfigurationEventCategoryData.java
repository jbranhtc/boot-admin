package com.htc.ea.util.dto;

import java.io.Serializable;

public class ConfigurationEventCategoryData implements Serializable {

	private static final long serialVersionUID = -6653962123005876657L;

	private Long configurationid;

	private boolean enable;

	private String eventcategoryid;

	private String level;

	public ConfigurationEventCategoryData() {
	}

	public Long getConfigurationid() {
		return this.configurationid;
	}

	public void setConfigurationid(Long configurationid) {
		this.configurationid = configurationid;
	}

	public boolean getEnable() {
		return this.enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getEventcategoryid() {
		return this.eventcategoryid;
	}

	public void setEventcategoryid(String eventcategoryid) {
		this.eventcategoryid = eventcategoryid;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigurationEventCategoryData [configurationid=");
		builder.append(configurationid);
		builder.append(", enable=");
		builder.append(enable);
		builder.append(", eventcategoryid=");
		builder.append(eventcategoryid);
		builder.append(", level=");
		builder.append(level);
		builder.append("]");
		return builder.toString();
	}
}