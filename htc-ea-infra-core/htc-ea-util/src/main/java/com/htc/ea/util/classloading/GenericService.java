package com.htc.ea.util.classloading;

import java.util.Map;

public interface GenericService<T, S> {

	public S execute(T input);

	public Map<String, Object> getParameters();
}