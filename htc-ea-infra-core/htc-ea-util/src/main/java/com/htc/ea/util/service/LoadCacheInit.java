package com.htc.ea.util.service;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.htc.ea.util.category.EventCategoryService;
import com.htc.ea.util.classloading.ClassFactory;
import com.htc.ea.util.configentry.ConfigurationServiceFacade;
import com.htc.ea.util.convert.LookupConverter;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.log.LoggingServiceFacade;
import com.htc.ea.util.router.RouterService;
import com.htc.ea.util.util.Constants;

public class LoadCacheInit {
	private static final Logger logger = LoggerFactory.getLogger(LoadCacheInit.class);
	private LookupConverter lookupConverter;
	private ConfigurationServiceFacade configurationServiceFacade;
	private LoggingServiceFacade loggingServiceFacade;
	private AuditLog auditLog;
	private Environment env;
	private RouterService routerService;
	private EventCategoryService eventCategoryService;
	private ClassFactory classFactory;

	public void setLookupConverter(LookupConverter lookupConverter) {
		this.lookupConverter = lookupConverter;
	}

	public void setConfigurationService(ConfigurationServiceFacade configurationServiceFacade) {
		this.configurationServiceFacade = configurationServiceFacade;
	}

	public void setLoggingServiceFacade(LoggingServiceFacade loggingServiceFacade) {
		this.loggingServiceFacade = loggingServiceFacade;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public void setRouterService(RouterService routerService) {
		this.routerService = routerService;
	}

	public void setEventCategoryService(EventCategoryService eventCategoryService) {
		this.eventCategoryService = eventCategoryService;
	}

	public void setClassFactory(ClassFactory classFactory) {
		this.classFactory = classFactory;
	}

	public void load() {
		boolean activeAuditQueue = Boolean.parseBoolean(this.env.getProperty(Constants.AUDIT_QUEUE_ENABLED));
		String loggingImpl = this.env.getProperty(Constants.LOGGING_IMPL);
		logger.info("Getting conversionData...");
		lookupConverter.loadConversionData();
		routerService.loadRoutingData();
		eventCategoryService.loadEventCategoryData();

		if (this.env.getProperty(Constants.OWN_CLASSES) != null)
			classFactory.loadClassesMap(false, null);

		// Ping RabbitMQ
		if (Constants.QUEUE_RABBIT_LOG.equalsIgnoreCase(loggingImpl)) {
			String initTrans = UUID.randomUUID().toString().replaceAll("-", "");
			try {
				loggingServiceFacade.info(this.getClass().getName(), initTrans, "Rabbit Connection=> ",
						"Connecting to Log queuerabbit", 0L);
			} catch (Exception e) {
				throw new RuntimeException("Error connecting @Bean connectionFactoryLog:", e);
			}
		}
		GenericDto genericDto = new GenericDto();
		if (activeAuditQueue) {
			try {
				auditLog.sendAudit(genericDto);
			} catch (Exception e) {
				throw new RuntimeException("Error connecting @Bean connectionFactoryAudit:", e);
			}
		}
	}
}
