package com.htc.ea.util.dto;

import java.io.Serializable;

public class ConversionData implements Serializable {
	private static final long serialVersionUID = -565022356704436307L;
	
	private long id;
	private String applicationId;
	private String topicId;
	private String key;
	private String value;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getTopicId() {
		return topicId;
	}
	
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConversionData [id=").append(id).append(", applicationId=").append(applicationId)
				.append(", topicId=").append(topicId).append(", key=").append(key).append(", value=").append(value)
				.append("]");
		return builder.toString();
	}
}
