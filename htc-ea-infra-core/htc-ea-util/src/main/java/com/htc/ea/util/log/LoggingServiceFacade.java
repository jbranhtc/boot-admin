package com.htc.ea.util.log;

public interface LoggingServiceFacade extends LoggingService {

	public void log(String level, String source, String originRefId, String message, String detail, Long duration,
			Throwable throwable);

	public void trace(String source, String message, Throwable throwable);

	public void trace(String source, String message);

	public void debug(String source, String originRefId, String message, String detail, Long duration, Throwable throwable);

	public void debug(String source, String originRefId, String message, String detail, Long duration);

	public void info(String source, String originRefId, String message, String detail, Long duration, Throwable throwable);

	public void info(String source, String originRefId, String message, String detail, Long duration);

	public void warn(String source, String message, Throwable throwable);

	public void warn(String source, String message);

	public void fatal(String source, String message, Throwable throwable);

	public void fatal(String source, String message);
	
	public void error(String source, String originRefId, String message, String detail, Throwable throwable);

	public void error(String source, String originRefId, String message, String detail);

	public void setLoggingService(LoggingService loggingService);
}