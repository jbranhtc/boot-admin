package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheConversionService;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class TransferConversionOracleToRedisTest extends VMArguments{
	
	@Autowired
	AdminCacheConversionService admin;
	
	AdminCachePojo refresh;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.refresh = new AdminCachePojo();
		this.refresh.setApplicationId("");
		this.refresh.setEnvironmentId("");
		this.refresh.setMapKey("");
		this.refresh.setOnlyRedis(true);
		this.refresh.setPlatform(0);
		this.refresh.setVersion("");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.transferConversionOracleToRedis(this.refresh);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}