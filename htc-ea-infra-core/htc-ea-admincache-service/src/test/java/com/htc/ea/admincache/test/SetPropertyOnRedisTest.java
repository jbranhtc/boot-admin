package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCacheInsert;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheService;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.util.dto.ConfigEntryData;

public class SetPropertyOnRedisTest extends VMArguments{
	
	@Autowired
	AdminCacheService admin;
	
	AdminCacheInsert insert;
	
	ConfigEntryData configEntry;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.insert = new AdminCacheInsert();
		this.configEntry = new ConfigEntryData();
		this.insert.setApplicationId("");
		this.insert.setDescription("");
		this.insert.setEnvironmentId("");
		this.insert.setMapKey("");
		this.insert.setOnlyRedis(true);
		this.insert.setPlatform(0);
		this.insert.setVersion("");
		
		this.configEntry.setDataType("");
		this.configEntry.setEnabled(true);
		this.configEntry.setKey("");
		this.configEntry.setSingleValue(true);
		this.configEntry.setType("");
		this.configEntry.setValue("");
		this.configEntry.setValues(new HashSet<String>());
		this.insert.setConfigEntry(configEntry);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.setPropertyOnRedis(this.insert);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}