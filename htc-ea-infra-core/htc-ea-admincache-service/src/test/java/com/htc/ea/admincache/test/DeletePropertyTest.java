package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCacheDelete;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheService;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class DeletePropertyTest extends VMArguments{
	
	@Autowired
	AdminCacheService admin;
	
	AdminCacheDelete del;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.del = new AdminCacheDelete();
		this.del.setApplicationId("");
		this.del.setEnvironmentId("");
		this.del.setKey("");
		this.del.setMapKey("");
		this.del.setOnlyRedis(true);
		this.del.setPlatform(0);
		this.del.setVersion("");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.deleteProperty(this.del);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e){
			e.printStackTrace();
		}
		assertTrue(true);
	}
}