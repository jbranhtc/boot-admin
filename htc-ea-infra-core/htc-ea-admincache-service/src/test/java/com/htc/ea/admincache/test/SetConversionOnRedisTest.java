package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheConversionService;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class SetConversionOnRedisTest extends VMArguments{
	
	@Autowired
	AdminCacheConversionService admin;
	
	AdminCacheConversionInsert insert;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.insert = new AdminCacheConversionInsert();
		this.insert.setApplicationId("");
		this.insert.setEnvironmentId("");
		this.insert.setKey("");
		this.insert.setMapKey("");
		this.insert.setOnlyRedis(true);
		this.insert.setPlatform(0);
		this.insert.setTopicId("");
		this.insert.setValue("");
		this.insert.setVersion("");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.setConversionOnRedis(this.insert);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}