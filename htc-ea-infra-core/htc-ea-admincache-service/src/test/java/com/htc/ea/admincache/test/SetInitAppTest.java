package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheService;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class SetInitAppTest extends VMArguments{
	
	@Autowired
	AdminCacheService admin;
	
	AdminCachePojo pojo;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.pojo = new AdminCachePojo();
		this.pojo.setApplicationId("");
		this.pojo.setEnvironmentId("");
		this.pojo.setMapKey("");
		this.pojo.setOnlyRedis(true);
		this.pojo.setPlatform(0);
		this.pojo.setVersion("");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.setInitApp(this.pojo);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}