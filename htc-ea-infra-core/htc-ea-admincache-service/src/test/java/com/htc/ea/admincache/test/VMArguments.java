package com.htc.ea.admincache.test;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.htc.ea.admincache.config.AdminCacheConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=AdminCacheConfig.class)
public class VMArguments {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("app.env", "production");
		System.setProperty("app.log", "C:\\propertiesBoot\\log");
		System.setProperty("logging.config", "C:\\Users\\Victor Arucha\\Desktop\\2017\\bitbucket\\tigo-bo-mfs-config\\logback\\htc-ea-admincache-service.production-logback.xml");
		System.setProperty("spring.config.location", "C:\\Users\\Victor Arucha\\Desktop\\2017\\bitbucket\\tigo-bo-mfs-config\\production\\htc-ea-admincache-service.production.yml");
		System.setProperty("spring.boot.admin.url", "http://localhost:8006/production/admin-boot");
		System.setProperty("application.container.version", "1.0");
	}
}