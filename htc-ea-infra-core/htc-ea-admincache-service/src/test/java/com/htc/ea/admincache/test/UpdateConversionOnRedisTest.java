package com.htc.ea.admincache.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheConversionService;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class UpdateConversionOnRedisTest extends VMArguments{
	
	@Autowired
	AdminCacheConversionService admin;
	
	AdminCacheConversionInsert update;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.update = new AdminCacheConversionInsert();
		this.update.setApplicationId("");
		this.update.setEnvironmentId("");
		this.update.setKey("");
		this.update.setMapKey("");
		this.update.setOnlyRedis(true);
		this.update.setPlatform(0);
		this.update.setTopicId("");
		this.update.setValue("");
		this.update.setVersion("");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		AdminCacheResponse response = admin.updateConversionOnRedis(this.update);
		try {
			System.out.println(AdminCacheUtil.convertObjectToJson(response));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
}