package com.tigo.ea.pto.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TechnicalProductCatalogDto {
	
	private List<CommandDto> commandDtoList;
		
	@JsonProperty("product-list")
	public List<CommandDto> getCommandDtoList() {
		if (commandDtoList==null) {
			commandDtoList = new ArrayList<>();
		}
		return commandDtoList;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TechnicalCommandsCatalogDto [commandDtoList=").append(commandDtoList).append("]");
		return builder.toString();
	}
}