package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: EventCategory</h1>
 * 
 * @description This entity models the category types of events that occur in a
 *              log. its primary key is a String type id.
 * 
 *              EventCategory has <b>@OneToMany</b> relationship with EventLog
 *              EventCategory has <b>@ManyToMany</b> relationship with
 *              Configuration associative table:Configuration_EventCategory
 * 
 **/

@Entity
public class EventCategory extends StringIdBaseEntity {

	private static final long serialVersionUID = 4038517951700601747L;

	public EventCategory() {
	}

	public EventCategory(String id) {
		super(id);
	}

	public EventCategory(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventCategory [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
