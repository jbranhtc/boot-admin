package com.htc.ea.admincache.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoOracle;
import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.ConfigEntryDAO;
import com.htc.ea.admincache.dao.GetConfigurationDAO;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;

@Service
public class AdminCacheConfigEventCatService {
	
	@Autowired
	private AdminCacheDaoOracle adminCacheDaoOracle;
	@Autowired
	private AdminCacheDaoRedis adminCacheDaoRedis;
	@Autowired
	private AdminCacheUtilRedis adminCacheUtilRedis;
	@Autowired
    private RedisMessagePublisher servPublisher;
	
	private AdminCacheResponse response = null;
	@Autowired
	private ConfigEntryDAO configEntryDAO;
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;
	
	/**
	 * Copy config entry between oracle database and database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	@SuppressWarnings("deprecation")
	public AdminCacheResponse transferConfigEventCatOracleToRedis(AdminCachePojo refresh){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!refresh.validate()){
				// validamos si al app exista en la toolbox
				if(getConfigurationDAO.existsConfiguration(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion())){
					// obtenemos el objeto del web service de la toolbox
					List<ConfigurationEventCategoryData> as = adminCacheDaoOracle.getConfigurationEventCategoryOracle(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion());
					if(as != null){
						// guardamos el objeto en Redis
						adminCacheDaoRedis.hmsetE(refresh.getKeyRedis(), as, refresh.getMapKeyR());
						// validamos si el objeto insertado exista en Redis
						if(adminCacheUtilRedis.exists(refresh.getKeyRedis()) && adminCacheUtilRedis.existsKeyMap(refresh.getKeyRedis(), refresh.getMapKeyR())){
							// enviamos el mensaje para que la aplicacion se actualice
							servPublisher.publish(refresh.getKeyRedis() + ":" + refresh.getMapKeyR());
							resp.setCode(0);
							response.setStatus(HttpStatus.OK);
							resp.setDescription("Successful on Redis");
						}else{
							resp.setCode(1);
							response.setStatus(HttpStatus.METHOD_FAILURE);
							resp.setDescription("Fail: Not created on Redis");
						}
					}else{
						resp.setCode(2);
						response.setStatus(HttpStatus.NOT_FOUND);
						resp.setDescription("Fail: Error get event category from ToolBox");
					}
				}else{
					resp.setCode(3);
					resp.setDescription("Fail: App not exists on Toolbox");
					response.setStatus(HttpStatus.NOT_FOUND);
				}
			}else{
				resp.setCode(4);
				response.setStatus(HttpStatus.BAD_REQUEST);
				resp.setDescription("Fail: Params not must be null or empty");
			}
		}catch(Exception e){
			e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * Get properties on database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public List<ConfigurationEventCategoryData> getAllConfigEventCatFromRedis(AdminCachePojo config){
		List<ConfigurationEventCategoryData> aux = null;
		try{
			if(!config.validate()){
				aux = adminCacheDaoRedis.hgetE(config.getKeyRedis(), config.getMapKeyR());
				if(aux != null){
					return aux;
				}else{
					return new ArrayList<ConfigurationEventCategoryData>();
				}				
			}else{
				return new ArrayList<ConfigurationEventCategoryData>();
			}
		}catch(Exception e){
			e.printStackTrace();
			return new ArrayList<ConfigurationEventCategoryData>();
		}
	}
}