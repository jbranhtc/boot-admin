package com.htc.ea.admincache.interfaces;

public interface AdminCacheInterfaceUtilRedis<T> {
	
	boolean exists(String key);
	
	void delete(String key);
	
	public void delete(String key, String id);
	
	public boolean hexists(String key, String id);
	
	public void selectDB(int dbIndex);
	
	boolean existsKeyMap(String key, String hashKey);
}