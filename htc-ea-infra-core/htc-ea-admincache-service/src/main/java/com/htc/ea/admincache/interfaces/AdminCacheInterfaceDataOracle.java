package com.htc.ea.admincache.interfaces;

import java.util.List;

import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.dto.LoadClasses;
import com.tigo.ea.inswitch.dto.TransactionsLevelActorDto;

public interface AdminCacheInterfaceDataOracle {

	public ConfigurationData getConfigurationOracle(String applicationId, String environmentId, String version);
	public List<ConversionData> getConversionOracle(String applicationId);
	public List<ConfigurationEventCategoryData> getConfigurationEventCategoryOracle(String applicationId, String environmentId, String version);
	public List<LoadClasses> getLoadClassesOracle(String applicationId, String environmentId, String version);
	public List<TransactionsLevelActorDto> getConfigurationTransactions();
}