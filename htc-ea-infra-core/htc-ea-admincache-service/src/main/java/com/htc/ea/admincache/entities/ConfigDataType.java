package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: ConfigEntry</h1>
 * 
 * @description This modeling entity types configuration data. its primary key
 *              is a Long type id.
 * 
 *              ConfigDataType has <b>@OneToMany</b> relationship with
 *              ConfigEntry
 * 
 **/
@Entity
public class ConfigDataType extends StringIdBaseEntity {

	private static final long serialVersionUID = 2723950531580223222L;

	private boolean validable = false;

	@Column(nullable = true, length = 200)
	private String validateExpression;

	public ConfigDataType() {
		super();
	}

	public ConfigDataType(String id) {
		super(id);
	}

	public ConfigDataType(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	public boolean isValidable() {
		return validable;
	}

	public void setValidable(boolean validable) {
		this.validable = validable;
	}

	public String getValidateExpression() {
		return validateExpression;
	}

	public void setValidateExpression(String validateExpression) {
		this.validateExpression = validateExpression;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigDataType [validable=").append(validable)
				.append(", validateExpression=").append(validateExpression)
				.append(", id=").append(id).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
