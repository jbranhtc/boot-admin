package com.htc.ea.admincache.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/**
 * <h1>Project: Model</h1> <h1>entity: EventLog</h1>
 * 
 * @description This entity models the logs occurred for all applications. its
 *              primary key is a Long type id.
 * 
 *              EventLog has <b>@ManyToOne</b> relationship with Application
 *              EventLog has <b>@ManyToOne</b> relationship with EventCategory
 *              EventLog has <b>@ManyToOne</b> relationship with Environment
 * 
 **/
@Entity
public class EventLog implements Serializable {

	private static final long serialVersionUID = -4388195929459927782L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "EventLogGenerator")
	@SequenceGenerator(name = "EventLogGenerator", sequenceName = "EventLogSeq")
	private Long id;

	@JoinColumn(name = "applicationId")
	@ManyToOne
	private Application application;

	@JoinColumn(name = "environmentId")
	@ManyToOne
	private Environment environment;

	@JoinColumn(name = "eventCategoryId")
	@ManyToOne
	private EventCategory eventCategory;

	@Lob
	private String detail;

	@Lob
	@Column(name = "eventException")
	private String exception;

	@Column(length = 15)
	private String version;

	@Column(name = "eventLevel")
	private Integer level;

	@Column(updatable = false)
	private Date createdDate;

	@Column(updatable = false)
	private Date processDate;

	@Column(length = 50)
	private String serverLocation;

	@Column(length = 50)
	private String endUserLocation;

	@Column(length = 50)
	private String endUser;

	@Column(name = "eventSource", length = 100)
	private String source;

	@Column(name = "eventName", length = 100)
	private String name;

	@Column(length = 255)
	private String message;

	@Column(length = 100)
	private String referenceId;
	
	@Column(length = 100)
	private String originReferenceId;
	
	@Column(length = 100)
	private String targetReferenceId;

	@Column(length = 50)
	private String responseCode;

	private Boolean automatic;

	private Boolean succesful;

	private Boolean compensation;
	
	private Long duration;
	
	@JoinColumn(name = "eventLogId")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EventLogAttribute> eventLogAttributes = new ArrayList<EventLogAttribute>();

	public EventLog() {
	}

	public List<EventLogAttribute> getEventLogAttributes() {
		return eventLogAttributes;
	}

	public void setEventLogAttributes(List<EventLogAttribute> eventLogAttributes) {
		this.eventLogAttributes = eventLogAttributes;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getVersion() {
		return version;
	}

	public Boolean getAutomatic() {
		return automatic;
	}

	public void setAutomatic(Boolean automatic) {
		this.automatic = automatic;
	}

	public Boolean getSuccesful() {
		return succesful;
	}

	public void setSuccesful(Boolean succesful) {
		this.succesful = succesful;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public String getServerLocation() {
		return serverLocation;
	}

	public void setServerLocation(String serverLocation) {
		this.serverLocation = serverLocation;
	}

	public String getEndUserLocation() {
		return endUserLocation;
	}

	public void setEndUserLocation(String endUserLocation) {
		this.endUserLocation = endUserLocation;
	}

	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
	
	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getOriginReferenceId() {
		return originReferenceId;
	}

	public void setOriginReferenceId(String originReferenceId) {
		this.originReferenceId = originReferenceId;
	}

	public String getTargetReferenceId() {
		return targetReferenceId;
	}

	public void setTargetReferenceId(String targetReferenceId) {
		this.targetReferenceId = targetReferenceId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public EventCategory getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(EventCategory eventCategory) {
		this.eventCategory = eventCategory;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getCompensation() {
		return compensation;
	}

	public void setCompensation(Boolean compensation) {
		this.compensation = compensation;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventLog [id=").append(id).append(", application=")
				.append(application).append(", environment=")
				.append(environment).append(", eventCategory=")
				.append(eventCategory).append(", detail=").append(detail)
				.append(", exception=").append(exception).append(", version=")
				.append(version).append(", level=").append(level)
				.append(", createdDate=").append(createdDate)
				.append(", processDate=").append(processDate)
				.append(", serverLocation=").append(serverLocation)
				.append(", endUserLocation=").append(endUserLocation)
				.append(", endUser=").append(endUser).append(", source=")
				.append(source).append(", name=").append(name)
				.append(", message=").append(message)
				.append(", originReferenceId=").append(originReferenceId)
				.append(", targetReferenceId=").append(targetReferenceId)
				.append(", responseCode=").append(responseCode)
				.append(", automatic=").append(automatic)
				.append(", succesful=").append(succesful)
				.append(", compensation=").append(compensation)
				.append(", duration=").append(duration).append(", eventLogAttributes=")
				.append(eventLogAttributes).append("]");
		return builder.toString();
	}
	
}
