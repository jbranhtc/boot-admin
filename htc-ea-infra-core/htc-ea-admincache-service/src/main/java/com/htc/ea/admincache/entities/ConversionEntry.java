package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * <h1>Project: Model</h1> <h1>entity: ConversionEntry</h1>
 * 
 * @description This entity models ConversionEntry its primary key is a Long
 *              type id.
 * 
 *              ConversionEntry has <b>@ManyToOne</b> relationship with
 *              Application ConversionEntry has <b>@ManyToOne</b> relationship
 *              with ConversionTopic
 * 
 * 
 **/

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "applicationId",
		"conversionTopicId", "key" }))
public class ConversionEntry extends GenericBaseEntity<Long> {

	private static final long serialVersionUID = 3470313891363890888L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ConversionEntryGenerator")
	@SequenceGenerator(name = "ConversionEntryGenerator", sequenceName = "ConversionEntrySeq")
	protected Long id;

	@JoinColumn(name = "applicationId")
	@ManyToOne
	private Application application;

	@JoinColumn(name = "conversionTopicId")
	@ManyToOne
	private ConversionTopic conversionTopic;

	@Column(nullable = false, length = 50)
	private String key;

	@Column(nullable = false, length = 50)
	private String value;

	public ConversionEntry() {
	}

	public ConversionEntry(Long id) {
		super();
		this.id = id;
	}

	public ConversionEntry(Boolean enabled) {
		super(enabled);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public ConversionTopic getConversionTopic() {
		return conversionTopic;
	}

	public void setConversionTopic(ConversionTopic conversionTopic) {
		this.conversionTopic = conversionTopic;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConversionEntry [id=").append(id)
				.append(", application=").append(application)
				.append(", conversionTopic=").append(conversionTopic)
				.append(", key=").append(key).append(", value=").append(value)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}	
	
}
