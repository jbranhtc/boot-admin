package com.htc.ea.admincache.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.Application;
import com.htc.ea.admincache.entities.ConversionEntry;
import com.htc.ea.admincache.entities.ConversionTopic;
import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.util.dto.ConversionData;

@Repository
public class ConversionEntryDAO{
	
	private static Logger logger = LoggerFactory.getLogger(ConversionEntryDAO.class);
	
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;	
	@Autowired
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<ConversionEntry> findConversionsIdApp(String applicationId) {
		List<ConversionEntry> aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionEntry convert WHERE convert.application.id=:applicationId")
		.setParameter("applicationId", applicationId);
		if(!query.getResultList().isEmpty()){
			aux = (List<ConversionEntry>) query.getResultList();
		}
		return aux;
	}
	
	public ConversionEntry findConversionId(AdminCacheConversionInsert insert) {
		ConversionEntry aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionEntry convert WHERE convert.application.id=:applicationId and convert.key=:key and convert.value=:value and convert.conversionTopic.id=:conversionTopicId")
		.setParameter("applicationId", insert.getApplicationId())
		.setParameter("key", insert.getKey())
		.setParameter("value", insert.getValue())
		.setParameter("conversionTopicId", insert.getTopicId());
		if(!query.getResultList().isEmpty()){
			aux = (ConversionEntry) query.getSingleResult();
		}
		return aux;
	}
	
	public ConversionEntry findConversionIdKey(AdminCacheConversionInsert insert) {
		ConversionEntry aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionEntry convert WHERE convert.application.id=:applicationId and convert.key=:key and convert.conversionTopic.id=:conversionTopicId")
		.setParameter("applicationId", insert.getApplicationId())
		.setParameter("key", insert.getKey())
		.setParameter("conversionTopicId", insert.getTopicId());
		if(!query.getResultList().isEmpty()){
			aux = (ConversionEntry) query.getSingleResult();
		}
		return aux;
	}
	
	public ConversionEntry findConversionIdKeyESB(AdminCacheConversionInsert insert) {
		ConversionEntry aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionEntry convert WHERE convert.application.id=:applicationId and convert.key=:key and convert.conversionTopic.id=:conversionTopicId")
		.setParameter("applicationId", "esb")
		.setParameter("key", insert.getKey())
		.setParameter("conversionTopicId", insert.getTopicId());
		if(!query.getResultList().isEmpty()){
			aux = (ConversionEntry) query.getSingleResult();
		}
		return aux;
	}
	
	@SuppressWarnings("unchecked")
	public List<ConversionEntry> findByConversionTopicId(String applicationId, String conversionTopicId) {
		List<ConversionEntry> aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionEntry convert WHERE convert.application.id NOT IN(:applicationId) AND convert.conversionTopic.id=:conversionTopicId")
		.setParameter("applicationId", applicationId)
		.setParameter("conversionTopicId", conversionTopicId);
		if(!query.getResultList().isEmpty()){
			aux = (List<ConversionEntry>) query.getResultList();
		}
		return aux;
	}
	
	public boolean existConversionEntry(AdminCacheConversionInsert insert){
		boolean enable = false;
		ConversionEntry entry = this.findConversionIdKey(insert);
		if(entry != null){
			enable = entry.getEnabled() != null ? entry.getEnabled() : enable;
		}
		return enable;
	}
	
	public boolean existConversionTopic(String id){
		boolean exist = false;
		ConversionTopic entry = this.findConversionTopicId(id);
		if(entry != null){
			exist = true;
		}
		return exist;
	}
	
	public boolean enabledConversionEntry(AdminCacheConversionInsert update) {
		boolean enable = false;
		ConversionEntry entry = findConversionIdKey(update);
		if(entry != null){
			enable = entry.getEnabled() != null ? entry.getEnabled() : enable;
		}else{
			System.out.println("no viene nada");
		}
		return enable;
	}
	
	public boolean enabledConversionTopic(String id) {
		boolean enable = false;
		ConversionTopic entry = this.findConversionTopicId(id);
		if(entry != null){
			enable = entry.getEnabled() != null ? entry.getEnabled() : enable;
		}
		return enable;
	}
	
	public List<ConversionData> getConversions(String applicationId) {
		List<ConversionData> conversions = null;

		List<ConversionEntry> conversionEntries = this.findConversionsIdApp(applicationId);
		if (conversionEntries == null) {
			throw new RuntimeException(
					"Conversion entries not found for applicationId: "
							+ applicationId);
		}

		conversions = new ArrayList<ConversionData>();

		String validaApplicationId = "", conversionTopicId = "";
		for (ConversionEntry conversionEntry : conversionEntries) {
			ConversionData conversionData = new ConversionData();
			conversionData.setId(conversionEntry.getId());
			conversionData.setApplicationId(conversionEntry.getApplication()
					.getId());
			conversionData.setTopicId(conversionEntry.getConversionTopic()
					.getId());
			conversionData.setKey(conversionEntry.getKey());
			conversionData.setValue(conversionEntry.getValue());
			conversions.add(conversionData);

			if (!(validaApplicationId.equals(conversionEntry.getApplication()
					.getId()) && conversionTopicId.equals(conversionEntry
					.getConversionTopic().getId()))) {

				validaApplicationId = conversionData.getApplicationId();
				conversionTopicId = conversionData.getTopicId();

				for (ConversionEntry subConversionEntry : this.findByConversionTopicId(
								conversionData.getApplicationId(),
								conversionData.getTopicId())) {
					ConversionData subConversionData = new ConversionData();
					subConversionData.setId(subConversionEntry.getId());
					subConversionData.setApplicationId(subConversionEntry
							.getApplication().getId());
					subConversionData.setTopicId(subConversionEntry
							.getConversionTopic().getId());
					subConversionData.setKey(subConversionEntry.getKey());
					subConversionData.setValue(subConversionEntry.getValue());
					conversions.add(subConversionData);
				}
			}
		}
		return conversions;
	}
	
	public ConversionTopic findConversionTopicId(String id) {
		ConversionTopic aux = null;
		Query query = entityManager.createQuery("SELECT convert FROM ConversionTopic convert WHERE convert.id=:id")
		.setParameter("id", id);
		if(!query.getResultList().isEmpty()){
			aux = (ConversionTopic) query.getSingleResult();
		}
		return aux;
	}
	
	public boolean createConversionEntry(AdminCacheConversionInsert insert){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		ConversionEntry entryValue = new ConversionEntry();
		Application application = getConfigurationDAO.findApp(insert.getApplicationId());
		ConversionTopic conversionTopic = this.findConversionTopicId(insert.getTopicId());
		try{
			entryValue.setApplication(application);
			//entryValue.setCreatedDate(new Date());
			entryValue.setConversionTopic(conversionTopic);
			entryValue.setEnabled(true);
			entryValue.setKey(insert.getKey());
			entryValue.setValue(insert.getValue());
			entityManager.persist(entryValue);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	public boolean updateConversionEntry(AdminCacheConversionInsert update){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			ConversionEntry entry = this.findConversionIdKey(update);
			ConversionEntry temp = entityManager.find(ConversionEntry.class, entry.getId());
			temp.setValue(update.getValue());
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
	        }
			throw e;
		}
	}	
	
	public boolean deleteConversionEntry(AdminCacheConversionInsert delete){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			ConversionEntry entry = this.findConversionIdKey(delete);
			ConversionEntry temp = entityManager.find(ConversionEntry.class, entry.getId());
			entityManager.remove(temp);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
	        }
			throw e;
		}
	}
	
	public void clear(){
		entityManager.clear();
	}
}