package com.htc.ea.admincache.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the CONFIGURATION_EVENTCATEGORY database table.
 * 
 */
@Entity
@Table(name="CONFIGURATION_EVENTCATEGORY")
@NamedQuery(name="ConfigurationEventcategory.findAll", query="SELECT c FROM ConfigurationEventcategory c")
public class ConfigurationEventcategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long configurationid;

	@Column(name="\"ENABLED\"")
	private boolean enabled;

	@Id
	private String eventcategoryid;

	@Id
	@Column(name="\"LEVEL\"")
	private String level;

	public ConfigurationEventcategory() {
	}

	public Long getConfigurationid() {
		return this.configurationid;
	}

	public void setConfigurationid(Long configurationid) {
		this.configurationid = configurationid;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnable(boolean enabled) {
		this.enabled = enabled;
	}

	public String getEventcategoryid() {
		return this.eventcategoryid;
	}

	public void setEventcategoryid(String eventcategoryid) {
		this.eventcategoryid = eventcategoryid;
	}

	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

}