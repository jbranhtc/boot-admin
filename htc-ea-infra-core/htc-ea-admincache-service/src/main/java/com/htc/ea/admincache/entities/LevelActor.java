package com.htc.ea.admincache.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the LEVEL_ACTOR database table.
 * 
 */
@Entity
@Table(name="LEVEL_ACTOR")
@NamedQuery(name="LevelActor.findAll", query="SELECT l FROM LevelActor l")
public class LevelActor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name="ACTOR_NAME")
	private String actorName;

	private Timestamp createddate;

	private BigDecimal enabled;

	@Column(name="LEVEL_NUMBER")
	private String levelNumber;

	public LevelActor() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActorName() {
		return this.actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}

	public Timestamp getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public BigDecimal getEnabled() {
		return this.enabled;
	}

	public void setEnabled(BigDecimal enabled) {
		this.enabled = enabled;
	}

	public String getLevelNumber() {
		return this.levelNumber;
	}

	public void setLevelNumber(String levelNumber) {
		this.levelNumber = levelNumber;
	}
}