package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.TechnicalProductCatalogService;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.technical-product:false}")
@RequestMapping(value = "/technical-product-catalog")
@Api(value="TechnicalProductCatalogDto", tags = "Operaciones para catalogos de productos en Redis")
public class TechnicalProductCatalogController {
	
	@Autowired
	private TechnicalProductCatalogService technicalProductCatalogService;
	
	@ApiOperation(value = "Refrescar catalogos de productos en Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful on Redis"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh() {
		AdminCacheResponse response = technicalProductCatalogService.refresh();
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
}