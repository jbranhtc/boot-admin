package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: EndpointType</h1>
 * 
 * @description This entity models the catalog countries. its primary key is a
 *              String type id.
 * 
 *              EndpointType has <b>@OneToMany</b> relationship with Application
 * 
 **/
@Entity
public class EndpointType extends StringIdBaseEntity {

	private static final long serialVersionUID = 7090234716161679624L;

	public EndpointType() {
	}

	public EndpointType(String id) {
		super(id);
	}

	public EndpointType(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EndpointType [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
