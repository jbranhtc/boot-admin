package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.TransactionsMFSService;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.plataform-general-entry:false}")
@RequestMapping(value = "/platforms-general")
@Api(value="Plataform IG", tags="Operaciones Generales para Plataformas - Toolbox y Redis")
public class PlatformGeneralController {
	
	@Autowired
	Environment env;
	@Autowired
	private TransactionsMFSService transactionsMFSService;
	
	@ApiOperation(value = "Refrescar plataforma de Level Actor de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh-level-actor", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refreshVoucher(@RequestBody AdminCachePojo refresh) {
		refresh.setPlatform(1);
		refresh.setEnvironmentId(env.getProperty("app.env"));
		refresh.setMapKey("LevelActorData");
		AdminCacheResponse response = null;
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Refrescar plataforma de Transactions de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh-transactions", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refreshTemplate(@RequestBody AdminCachePojo refresh) {
		refresh.setPlatform(1);
		refresh.setEnvironmentId(env.getProperty("app.env"));
		refresh.setMapKey("TransactionsData");
		AdminCacheResponse response = transactionsMFSService.transferConfigEntriesOracleToRedis(refresh);
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}	
}