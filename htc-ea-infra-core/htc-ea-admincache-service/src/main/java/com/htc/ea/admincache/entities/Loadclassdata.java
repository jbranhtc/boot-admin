package com.htc.ea.admincache.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the LOADCLASSDATA database table.
 * 
 */
@Entity
@Table(name="Loadclassdata")
@NamedQuery(name="Loadclassdata.findAll", query="SELECT l FROM Loadclassdata l")
public class Loadclassdata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 200)
	private String classname;

	@JoinColumn(name = "configurationId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JsonBackReference
	private Configuration configuration;

	@Column(name="\"UPDATE\"")
	private boolean update = false;

	private Date uploaddate ;

	public Loadclassdata() {
	}

	public String getClassname() {
		return this.classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public boolean getUpdate() {
		return this.update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public Date getUploaddate() {
		return this.uploaddate;
	}

	public void setUploaddate(Date uploaddate) {
		this.uploaddate = uploaddate;
	}
}