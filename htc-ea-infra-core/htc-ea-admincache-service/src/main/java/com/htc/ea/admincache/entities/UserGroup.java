package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * <h1>Project: Model</h1> <h1>entity: UserGroup</h1>
 * 
 * @description This entity models the user groups. its primary key is a Long
 *              type id.
 * 
 *              UserGroup has <b>@ManyToMany</b> relationship with User
 * 
 **/

@Entity
@PrimaryKeyJoinColumn(name = "id")
@DiscriminatorValue("G")
public class UserGroup extends Authority {

	private static final long serialVersionUID = 2159344105537659818L;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "userGroup_user", joinColumns = { @JoinColumn(name = "groupId") }, inverseJoinColumns = { @JoinColumn(name = "userId") })
	private List<User> users = new ArrayList<User>();

	@JoinColumn(name = "applicationId")
	@ManyToOne
	private Application application;

	public UserGroup() {
	}

	public UserGroup(Long id) {
		super(id);
	}

	public UserGroup(String name, String description, Boolean enabled) {
		super(name, description, enabled);
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getAuthorityId() {
		return "GROUP_" + id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserGroup [users=").append(users)
				.append(", application=").append(application).append(", id=")
				.append(id).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
