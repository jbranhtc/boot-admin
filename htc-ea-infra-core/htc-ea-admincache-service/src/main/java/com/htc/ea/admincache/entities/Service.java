package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * <h1>Project: Model</h1> <h1>entity: Service</h1>
 * 
 * @description This entity models the services available. its primary key is a
 *              Long type id.
 * 
 *              Service has <b>@ManyToOne</b> relationship with EndpointType
 *              Service has <b>@ManyToOne</b> relationship with ServiceGroup
 * 
 **/
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "serviceId",
		"environmentId", "countryId" }))
public class Service extends NameableEntity<Long> {

	private static final long serialVersionUID = -533900577420613255L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ServiceGenerator")
	@SequenceGenerator(name = "ServiceGenerator", sequenceName = "ServiceSeq")
	private Long id;

	@Column(nullable = false, length = 50)
	private String serviceId;

	@JoinColumn(name = "environmentId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Environment environment;

	@JoinColumn(name = "countryId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Country country;

	@JoinColumn(name = "serviceGroupId")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private ServiceGroup serviceGroup;

	@JoinColumn(name = "endpointTypeId")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private EndpointType endpointType;

	@Column(nullable = true, length = 200)
	private String endpointValue;

	@Column(nullable = false)
	private Integer timeout = 0;

	@Column(nullable = false)
	private Integer priority = 0;

	private Boolean secured = false;

	private Boolean compensable = false;

	public Service() {
	}

	public Service(Long id) {
		super();
		this.id = id;
	}

	public Service(String name, String description, Boolean enabled) {
		super();
		// this.id = id;
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public ServiceGroup getServiceGroup() {
		return serviceGroup;
	}

	public void setServiceGroup(ServiceGroup serviceGroup) {
		this.serviceGroup = serviceGroup;
	}

	public EndpointType getEndpointType() {
		return endpointType;
	}

	public void setEndpointType(EndpointType endpointType) {
		this.endpointType = endpointType;
	}

	public String getEndpointValue() {
		return endpointValue;
	}

	public void setEndpointValue(String endpointValue) {
		this.endpointValue = endpointValue;
	}

	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Boolean getSecured() {
		return secured;
	}

	public void setSecured(Boolean secured) {
		this.secured = secured;
	}

	public Boolean getCompensable() {
		return compensable;
	}

	public void setCompensable(Boolean compensable) {
		this.compensable = compensable;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Service [id=").append(id).append(", serviceId=")
				.append(serviceId).append(", environment=").append(environment)
				.append(", country=").append(country).append(", serviceGroup=")
				.append(serviceGroup).append(", endpointType=")
				.append(endpointType).append(", endpointValue=")
				.append(endpointValue).append(", timeout=").append(timeout)
				.append(", priority=").append(priority).append(", secured=")
				.append(secured).append(", compensable=").append(compensable)
				.append(", name=").append(name).append(", description=")
				.append(description).append(", enabled=").append(enabled)
				.append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}

}
