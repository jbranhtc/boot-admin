package com.htc.ea.admincache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.ea.admincache.entities.Configuration;
import com.htc.ea.admincache.entities.Loadclassdata;
import com.htc.ea.admincache.entities.Transaction;
import com.htc.ea.admincache.interfaces.AdminCacheInterfaceDataOracle;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.dto.LoadClasses;
import com.tigo.ea.inswitch.dto.TransactionsLevelActorDto;

@Component
public class AdminCacheDaoOracle implements AdminCacheInterfaceDataOracle{
	
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;
	@Autowired
	private ConversionEntryDAO conversionEntryDAO;
	@Autowired
	private ConfigurationEventCategoryDAO configurationEventCategoryDAO;
	@Autowired
	private LoadClassDAO loadClassDAO;
	@Autowired
	private TransactionDAO transactionDAO;

	@Override
	public ConfigurationData getConfigurationOracle(String applicationId, String environmentId, String version) {
		ConfigurationData data = null;
		getConfigurationDAO.clear();
		data = getConfigurationDAO.getConfigurationData(applicationId, environmentId, version);
		return data;
	}

	@Override
	public List<ConversionData> getConversionOracle(String applicationId){		
		List<ConversionData> conversions = null;
		conversionEntryDAO.clear();
		conversions = conversionEntryDAO.getConversions(applicationId);
		return conversions;
	}

	@Override
	public List<ConfigurationEventCategoryData> getConfigurationEventCategoryOracle(String applicationId, String environmentId, String version) {
		List<ConfigurationEventCategoryData> event = null;
		conversionEntryDAO.clear();
		Configuration config = getConfigurationDAO.findByAppAndEnvAndVersion(applicationId, environmentId, version);
		event = configurationEventCategoryDAO.getConfigurationEventCategory(config.getId());
		return event;
	}

	@Override
	public List<LoadClasses> getLoadClassesOracle(String applicationId, String environmentId, String version) {
		List<Loadclassdata> db = loadClassDAO.findListLoadclassdata(applicationId, environmentId, version);
		if(db != null && !db.isEmpty())
			return AdminCacheUtil.convertLoadclassdataToLoadClasses(db);
		return null;
	}
	
	@Override
	public List<TransactionsLevelActorDto> getConfigurationTransactions() {
		getConfigurationDAO.clear();
		List<TransactionsLevelActorDto> lstReturn = new ArrayList<TransactionsLevelActorDto>();
		List<Transaction> lst = transactionDAO.getTransaction();
		TransactionsLevelActorDto aux;
		for(Transaction temp : lst){
			aux = new TransactionsLevelActorDto();
			aux.setApi_mts(temp.getApiMts());
			aux.setApi_utiba(temp.getApiUtiba());
			aux.setCreateddate(temp.getCreateddate());
			aux.setCredit_account(Integer.valueOf(temp.getCredit().getLevelNumber()));
			aux.setDebit_account(Integer.valueOf(temp.getDebit().getLevelNumber()));
			aux.setEnabled(temp.getEnabled());
			aux.setId((int)temp.getId());
			aux.setService_mts(temp.getServiceMts());
			lstReturn.add(aux);
		}
		return lstReturn;
	}
}