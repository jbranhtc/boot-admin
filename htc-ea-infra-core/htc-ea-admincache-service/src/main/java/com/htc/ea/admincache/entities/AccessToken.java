package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 * <h1>Project: Model</h1> <h1>entity: AccessToken</h1>
 * 
 * @description This entity models the acccessToken. its primary key is a Long
 *              type id.
 * 
 *              AccessToken has <b>@ManyToOne</b> relationship with Application
 *              AccessToken has <b>@ManyToOne</b> relationship with User
 * 
 **/

@Entity
public class AccessToken extends NameableEntity<Long> {

	private static final long serialVersionUID = -5679943725374466901L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "AccessTokenGenerator")
	@SequenceGenerator(name = "AccessTokenGenerator", sequenceName = "AccessTokenSeq")
	private Long id;

	@JoinColumn(name = "applicationId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Application application;

	@JoinColumn(name = "userId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private User user;

	@Column(nullable = false, length = 100)
	private String token;

	public AccessToken() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccessToken [id=").append(id).append(", application=")
				.append(application).append(", user=").append(user)
				.append(", token=").append(token).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}	

}
