package com.htc.ea.admincache.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoOracle;
import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.ConversionEntryDAO;
import com.htc.ea.admincache.dao.GetConfigurationDAO;
import com.htc.ea.admincache.entities.ConversionEntry;
import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;

@Service
public class AdminCacheConversionService {
	
	@Autowired
	private AdminCacheDaoOracle adminCacheDaoOracle;
	@Autowired
	private AdminCacheDaoRedis adminCacheDaoRedis;
	@Autowired
	private AdminCacheUtilRedis adminCacheUtilRedis;
	@Autowired
    private RedisMessagePublisher servPublisher;
	
	private AdminCacheResponse response = null;
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;
	@Autowired
	private ConversionEntryDAO conversionEntryDAO;
	
	/**
	 * Copy conversion between oracle database and database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public AdminCacheResponse transferConversionOracleToRedis(AdminCachePojo refresh){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!refresh.validate()){
				// validamos si al app exista en la toolbox
				if(getConfigurationDAO.existsConfiguration(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion())){
					// obtenemos el objeto del web service de la toolbox
					List<ConversionData> as = adminCacheDaoOracle.getConversionOracle(refresh.getApplicationId());
					if(as != null){
						// guardamos el objeto en Redis
						adminCacheDaoRedis.hmsetC(refresh.getKeyRedis(), as, refresh.getMapKeyR());
						// validamos si el objeto insertado exista en Redis
						if(adminCacheUtilRedis.exists(refresh.getKeyRedis())){
							// enviamos el mensaje para que la aplicacion se actualice
							servPublisher.publish(refresh.getKeyRedis() + ":" + refresh.getMapKeyR());
							resp.setCode(0);
							response.setStatus(HttpStatus.OK);
							resp.setDescription("Successful on Redis");
						}else{
							resp.setCode(14);
							resp.setDescription("Fail: Not created on Redis");
						}
					}else{
						resp.setCode(2);
						resp.setDescription("Fail: Error get conversions from ToolBox");
					}
				}else{
					resp.setCode(3);
					resp.setDescription("Fail: App not exists on Toolbox");
					response.setStatus(HttpStatus.NOT_FOUND);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * Insert new conversion on database Redis
	 * @param String - Name of application Id
	 * @param String - Key
	 * @param String - Value
	 * @return 
	 */
	public AdminCacheResponse setConversionOnRedis(AdminCacheConversionInsert insert){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		try{
			// validamos los parametros de entrada
			if(!insert.validate()){
				
				// Process to Toolbox
				if(!insert.isOnlyRedis()){
					resp = new ResponseCache();
					// validamos si al app exista en la toolbox
					if(getConfigurationDAO.existsConfiguration(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion())){
						// validamos si no existe la conversionEntry
						boolean exist = conversionEntryDAO.existConversionEntry(insert);
						if(!exist){
							// validamos si existe conversionTopicId
							boolean existTopic = conversionEntryDAO.existConversionTopic(insert.getTopicId());
							if(existTopic){
								// guardamos la conversionEntry
								boolean save = conversionEntryDAO.createConversionEntry(insert);
								if(save){
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Toolbox");
								}else{
									toolbox = false;
									resp.setCode(6);
									resp.setDescription("Fail: Conversion not added on Toolbox");
								}
							}else{
								toolbox = false;
								resp.setCode(7);
								resp.setDescription("Fail: ConversionTopic not exists on Toolbox");
								response.setStatus(HttpStatus.NOT_FOUND);
							}							
						}else{
							toolbox = false;
							resp.setCode(7);
							resp.setDescription("Fail: Conversion exists on Toolbox");
							response.setStatus(HttpStatus.CONFLICT);
						}						
					}else{
						toolbox = false;
						resp.setCode(3);
						resp.setDescription("Fail: App not exists on Toolbox");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
				// Process to Redis
				if(toolbox){
					resp = new ResponseCache();
					if(adminCacheUtilRedis.exists(insert.getKeyRedis())){
						// validamos si existe conversionTopicId
						boolean existTopic = conversionEntryDAO.existConversionTopic(insert.getTopicId());
						if(existTopic){
							// obtenemos el objeto en Redis
							List<ConversionData> data = this.getAllConversionFromRedis(insert);
							if(data != null && !data.isEmpty()){
								// validamos si la nueva conversionEntry no exista en Redis
								if(!AdminCacheUtil.existConversionRedis(data, insert)){
									// insertamos la nueva conversionEntry en el objeto
									ConversionData nueva = new ConversionData();
									nueva.setApplicationId(insert.getApplicationId());
									nueva.setKey(insert.getKey());
									nueva.setValue(insert.getValue());
									nueva.setTopicId(insert.getTopicId());
									data.add(nueva);
									// par esb
									ConversionEntry cea = conversionEntryDAO.findConversionIdKeyESB(insert);
									ConversionData esb = new ConversionData();
									esb.setApplicationId(cea.getApplication().getId());
									esb.setKey(insert.getKey());
									esb.setValue(cea.getValue());
									esb.setTopicId(insert.getTopicId());
									data.add(esb);									
									// actualizamos el objeto entero con la nueva conversionEntry en redis
									adminCacheDaoRedis.hmsetC(insert.getKeyRedis(), data, insert.getMapKeyR());
									data.clear();
									data = this.getAllConversionFromRedis(insert);
									if(AdminCacheUtil.existConversionRedis(data, insert)){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(insert.getKeyRedis() + ":" + insert.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Conversion not added on Redis");
									}
								}else{
									resp.setCode(7);
									resp.setDescription("Fail: Conversion exists on Redis");
									response.setStatus(HttpStatus.CONFLICT);
								}
							}else{
								resp.setCode(8);
								resp.setDescription("Fail: ConversionData is null on Redis");
							}
						}else{
							resp.setCode(7);
							resp.setDescription("Fail: ConversionTopic not exists on Redis");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}else{
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Redis");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Update conversion on database Redis
	 * @param String - Name of application Id
	 * @param String - Key
	 * @param String - Value
	 * @return 
	 */
	public AdminCacheResponse updateConversionOnRedis(AdminCacheConversionInsert update){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		try{
			if(!update.validate()){
				
				// Process to Toolbox
				if(!update.isOnlyRedis()){
					resp = new ResponseCache();
					// validamos si al app exista en la toolbox
					if(getConfigurationDAO.existsConfiguration(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion())){
						// validamos si existe la conversionEntry
						boolean exist = conversionEntryDAO.existConversionEntry(update);
						if(exist){
							// validamos si la conversionEntry esta habilitada
							boolean enable = conversionEntryDAO.enabledConversionEntry(update); 
							if(enable){
								// validamos si la conversionTopic esta habilitada
								boolean enableT = conversionEntryDAO.enabledConversionTopic(update.getTopicId());
								if(enableT){
									// actualizamos la conversionEntry
									boolean updated = conversionEntryDAO.updateConversionEntry(update);
									if(updated){
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Toolbox");
									}else{
										toolbox = false;
										resp.setCode(6);
										resp.setDescription("Fail: Conversion not updated on Toolbox");
									}
								}else{
									resp.setCode(12);
									resp.setDescription("Fail: Conversion Topic disable on Toolbox");
								}
							}else{
								toolbox = false;
								resp.setCode(13);
								resp.setDescription("Fail: Conversion disable on Toolbox");
							}
						}else{
							toolbox = false;
							resp.setCode(7);
							resp.setDescription("Fail: Conversion not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}else{
						toolbox = false;
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Toolbox");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
				// Process to Redis
				if(toolbox){
					resp = new ResponseCache();
					if(adminCacheUtilRedis.exists(update.getKeyRedis())){
						List<ConversionData> data = this.getAllConversionFromRedis(update);
						if(data != null && !data.isEmpty()){
							// validamos si la conversionTopic exista en el objeto en Redis
							boolean existTopic = conversionEntryDAO.existConversionTopic(update.getTopicId());
							if(existTopic){
								// validamos si la conversionEntry exista en el objeto en Redis
								if(AdminCacheUtil.existConversionRedis(data, update)){
									ConversionData temp = AdminCacheUtil.getConversionRedis(data, update);
									data.remove(temp);
									temp.setValue(update.getValue());
									data.add(temp);
									adminCacheDaoRedis.hmsetC(update.getKeyRedis(), data, update.getMapKeyR());
									// validamos si actualizamos el objeto
									data = this.getAllConversionFromRedis(update);
									ConversionData actualizado = AdminCacheUtil.getConversionRedis(data, update);
									if(actualizado.getValue().equals(update.getValue())){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(update.getKeyRedis() + ":" + update.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Conversion not updated on Redis");
									}
								}else{
									resp.setCode(7);
									resp.setDescription("Fail: Conversion not exists on Redis");
									response.setStatus(HttpStatus.NOT_FOUND);
								}
							}else{
								resp.setCode(7);
								resp.setDescription("Fail: Conversion Topic not exists on Redis");
								response.setStatus(HttpStatus.NOT_FOUND);
							}
						}else{
							resp.setCode(8);
							resp.setDescription("Fail: ConfigurationData is null on Redis");
						}
					}else{
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Redis");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Get conversions on database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public List<ConversionData> getAllConversionFromRedis(AdminCachePojo config){
		List<ConversionData> aux = null;
		try{
			if(!config.validate()){
				aux = adminCacheDaoRedis.hgetC(config.getKeyRedis(), config.getMapKeyR());
				if(aux != null){
					return aux;
				}else{
					return new ArrayList<ConversionData>();
				}
			}else{
				return new ArrayList<ConversionData>();
			}
		}catch(Exception e){
			//e.printStackTrace();
			return new ArrayList<ConversionData>();
		}
	}
	
	/**
	 * Delete conversion in object on database Redis
	 * @param String - Name of application Id
	 * @param String - key
	 * @return 
	 */
	public AdminCacheResponse deleteConversion(AdminCacheConversionInsert del){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		try{
			if(!del.validate()){
				
				// Process to Toolbox
				if(!del.isOnlyRedis()){
					resp = new ResponseCache();
					// validamos si al app exista en la toolbox
					if(getConfigurationDAO.existsConfiguration(del.getApplicationId(), del.getEnvironmentIdp(), del.getVersion())){
						// validamos si existe la conversionEntry
						boolean exist = conversionEntryDAO.existConversionEntry(del);
						if(exist){
							// borramos la conversionEntry
							boolean delete = conversionEntryDAO.deleteConversionEntry(del);
							if(delete){
								resp.setCode(0);
								response.setStatus(HttpStatus.OK);
								resp.setDescription("Successful on Toolbox");
							}else{
								toolbox = false;
								resp.setCode(6);
								resp.setDescription("Fail: Conversion not deleted on Toolbox");
							}						
						}else{
							toolbox = false;
							resp.setCode(7);
							resp.setDescription("Fail: Conversion not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}else{
						toolbox = false;
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Toolbox");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
				// Process to Redis
				if(toolbox){
					resp = new ResponseCache();
					if(adminCacheUtilRedis.exists(del.getKeyRedis())){
						List<ConversionData> data = this.getAllConversionFromRedis(del);
						if(data != null){
							if(AdminCacheUtil.existConversionRedis(data, del)){
								// eliminamos la conversionEntry
								for (Iterator<ConversionData> iter = data.listIterator(); iter.hasNext(); ) {
									ConversionData a = iter.next();
								    if (a.getKey().equals(del.getKey()) && a.getTopicId().equals(del.getTopicId())) {
								        iter.remove();
								        break;
								    }
								}
								adminCacheDaoRedis.hmsetC(del.getKeyRedis(), data, del.getMapKeyR());
								if(!AdminCacheUtil.existConversionRedis(data, del)){
									// enviamos el mensaje para que la aplicacion se actualice
									servPublisher.publish(del.getKeyRedis() + ":" + del.getMapKeyR());
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Redis");
								}else{
									resp.setCode(6);
									resp.setDescription("Fail: Conversion not deleted on Redis");
								}
							}else{
								resp.setCode(7);
								resp.setDescription("Fail: Conversion not exists on Redis");
								response.setStatus(HttpStatus.NOT_FOUND);
							}
						}else{
							resp.setCode(8);
							resp.setDescription("Fail: ConversionData is null on Redis");
						}
					}else{
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Redis");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
}