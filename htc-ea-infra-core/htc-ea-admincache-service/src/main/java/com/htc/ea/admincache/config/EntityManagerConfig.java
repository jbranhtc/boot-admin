package com.htc.ea.admincache.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.htc.ea.admincache.entities", entityManagerFactoryRef = "factoryManager", transactionManagerRef = "entityManager" )
public class EntityManagerConfig {

	@Autowired
	Environment env;
	
	@Bean(name = "dataSource")
	@RefreshScope
	public DataSource dataSource() {
		DriverManagerDataSource dataSourceBuilder = new DriverManagerDataSource();
	    dataSourceBuilder.setDriverClassName(env.getProperty("datasource.db-config.driverClassName"));
	    dataSourceBuilder.setUrl(env.getProperty("datasource.db-config.url"));
	    dataSourceBuilder.setUsername(env.getProperty("datasource.db-config.username"));
	    dataSourceBuilder.setPassword(env.getProperty("datasource.db-config.password"));
	    return dataSourceBuilder;  
	}

	@Bean(name = "factoryManager")
	@RefreshScope
	public EntityManagerFactory entityManagerFactory() {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		// vendorAdapter.setShowSql(true);
		
		String dbSelect = env.getProperty("datasource.db-select") != null ? env.getProperty("datasource.db-select") : "";
		switch (dbSelect.toLowerCase()) {
	        case "oracle":  vendorAdapter.setDatabase(Database.ORACLE);
	                 break;
	        case "postgresql":  vendorAdapter.setDatabase(Database.POSTGRESQL);
	                 break;
	        case "sqlserver":  vendorAdapter.setDatabase(Database.SQL_SERVER);
	                 break;
	        case "mysql":  vendorAdapter.setDatabase(Database.MYSQL);
	                 break;
	        default: vendorAdapter.setDatabase(Database.ORACLE);
	                 break;
	    }
		
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan("com.htc.ea.admincache.entities");
		factory.setDataSource(dataSource());
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	@Bean(name = "entityManager")
	@RefreshScope
	public EntityManager entityManager() {
		return entityManagerFactory().createEntityManager();
	}
}