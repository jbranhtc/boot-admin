package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCacheDelete;
import com.htc.ea.admincache.model.AdminCacheInsert;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.AdminCacheUpdate;
import com.htc.ea.admincache.service.AdminCacheService;
import com.htc.ea.util.dto.ConfigurationData;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.config-entry:false}")
@RequestMapping(value = "/configurations")
@Api(value="ConfigurationData", tags="Operaciones para ConfigEntries - Toolbox y Redis")
public class AdminCacheController{

	@Autowired
	AdminCacheService adminCacheService;
	
	@Autowired
	Environment env;
	
	@ApiOperation(value = "Crear aplicacion en Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful on Redis"),
            @ApiResponse(code = 404, message = "App exists on Redis"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/redis/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setInitApp(@RequestBody AdminCachePojo init) {
		init.setMapKey("ConfigurationData");
		init.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheService.setInitApp(init);
		response.setApp(init.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Refrescar propiedades de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Refresh toolbox on redis", response = AdminCacheResponse.class),
            @ApiResponse(code = 404, message = "Error get config entry from ToolBox/App not exists on Toolbox"),
            @ApiResponse(code = 400, message = "Params not must be null or empty"),
            @ApiResponse(code = 420, message = "Not created on Redis/Exception"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh(@RequestBody AdminCachePojo refresh) {
		refresh.setMapKey("ConfigurationData");
		refresh.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheService.transferConfigEntriesOracleToRedis(refresh);
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Insertar propiedad en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setProperty(@RequestBody AdminCacheInsert insert) {
		insert.setMapKey("ConfigurationData");
		insert.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheService.setPropertyOnRedis(insert);
		response.setApp(insert.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Actualizar propiedad en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateProperty(@RequestBody AdminCacheUpdate update) {
		update.setMapKey("ConfigurationData");
		update.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheService.updatePropertyOnRedis(update);
		response.setApp(update.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}

	@ApiOperation(value = "Obtener propiedades de una aplicacion en Redis", response = ConfigurationData.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@GetMapping(value = "/{applicationId}/{version}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getConfiguration(@PathVariable("applicationId") final String applicationId, @PathVariable("version") final Double version) {
		AdminCachePojo config = new AdminCachePojo();
		config.setApplicationId(applicationId);
		config.setVersion(version.toString());
		config.setMapKey("ConfigurationData");
		config.setEnvironmentId(env.getProperty("app.env"));
		ConfigurationData response = adminCacheService.getAllPropertiesFromRedis(config, null);
		return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Borrar propiedad en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteProperty(@RequestBody AdminCacheDelete del) {
		del.setMapKey("ConfigurationData");
		del.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheService.deleteProperty(del);
		response.setApp(del.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
}