package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * <h1>Project: Model</h1> <h1>entity: Role</h1>
 * 
 * @description This entity models the roles associated to an application. its
 *              primary key is a String type id.
 * 
 *              Role extends Credential
 * 
 **/
@Entity
@PrimaryKeyJoinColumn(name = "id")
@DiscriminatorValue("R")
public class Role extends Authority {

	private static final long serialVersionUID = -335370102990056359L;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "role_user", joinColumns = { @JoinColumn(name = "roleId") }, inverseJoinColumns = { @JoinColumn(name = "userId") })
	private List<User> users = new ArrayList<User>();

	@JoinColumn(name = "applicationId")
	@ManyToOne
	private Application application;

	public Role() {
	}
	
	public Role(Long id) {
		super(id);
	}

	public Role(String name, String description, Boolean enabled) {
		super(name, description, enabled);
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public String getAuthorityId() {
		return "ROLE_" + id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Role [users=").append(users).append(", application=")
				.append(application).append(", id=").append(id)
				.append(", name=").append(name).append(", description=")
				.append(description).append(", enabled=").append(enabled)
				.append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}	

}
