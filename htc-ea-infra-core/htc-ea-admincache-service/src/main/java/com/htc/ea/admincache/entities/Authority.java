package com.htc.ea.admincache.entities;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;

/**
 * <h1>Project: Model</h1> <h1>entity: Authority</h1>
 * 
 * @description This entity models an abstract class for users, roles and groups
 *              its primary key is a Long type id.
 * 
 *              Authority: abstract class Authority has <b>@OneToMany</b>
 *              relationship with Group Authority has <b>@OneToMany</b>
 *              relationship with Role Authority has <b>@OneToMany</b>
 *              relationship with User
 * 
 **/

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "authorityType", discriminatorType = DiscriminatorType.STRING)
public abstract class Authority extends NameableEntity<Long> {

	private static final long serialVersionUID = 2582829202294444961L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "AuthorityGenerator")
	@SequenceGenerator(name = "AuthorityGenerator", sequenceName = "AuthoritySeq")
	protected Long id;

	public Authority() {
		super();
	}

	public Authority(Long id) {
		super();
		this.id = id;
	}
	
	public Authority(String name, String description, Boolean enabled) {
		super(name, description, enabled);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public abstract String getAuthorityId();

}
