package com.htc.ea.admincache.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

public class AdminCacheResponse {
	
	private String app;
	private List<ResponseCache> response;
	@SuppressWarnings("deprecation")
	private HttpStatus status = HttpStatus.METHOD_FAILURE;
	
	public AdminCacheResponse(){
		response = new ArrayList<ResponseCache>();
	}

	public List<ResponseCache> getResponse() {
		return response;
	}

	public void setResponse(List<ResponseCache> response) {
		this.response = response;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}
	
	public HttpStatus getStatus() {
        return status;
    }
	
    public void setStatus(final HttpStatus status) {
        this.status = status;
    }
}