package com.htc.ea.admincache.model;

import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.util.dto.ConfigEntryData;

public class AdminCacheInsert extends AdminCachePojo{
	
	private ConfigEntryData configEntry;
	private String description;
	private Object genericObject;
	
	public ConfigEntryData getConfigEntry() {
		return configEntry;
	}

	public void setConfigEntry(ConfigEntryData configEntry) {
		this.configEntry = configEntry;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean validate(){
		return AdminCacheUtil.stringValidation(this.getApplicationId(), this.getEnvironmentIdp(), this.getVersion(),
				this.getDescription(), this.getConfigEntry().getDataType(), this.getConfigEntry().getKey(), this.getConfigEntry().getType());
	}

	public Object getObj() {
		return genericObject;
	}

	public void setObj(Object genericObject) {
		this.genericObject = genericObject;
	}
}
