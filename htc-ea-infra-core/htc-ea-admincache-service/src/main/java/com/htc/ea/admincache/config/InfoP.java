/*package com.htc.ea.admincache.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

@Component
public final class InfoP implements InfoContributor{

	@Autowired
	private Environment env;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void contribute(
			org.springframework.boot.actuate.info.Info.Builder builder) {
		Map<String, Object> map = new HashMap<String, Object>();
        for(Iterator it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext(); ) {
            PropertySource propertySource = (PropertySource) it.next();
            if (propertySource instanceof MapPropertySource) {
                map.putAll(((MapPropertySource) propertySource).getSource());
            }
        }
        if(map != null && map.size() > 0){
        	for (Map.Entry<String, Object> entry : map.entrySet()) {
        	    builder.withDetail(entry.getKey(), Collections.singletonMap(entry.getKey(), entry.getValue()));
        	}
			
        }
		
	}
}*/