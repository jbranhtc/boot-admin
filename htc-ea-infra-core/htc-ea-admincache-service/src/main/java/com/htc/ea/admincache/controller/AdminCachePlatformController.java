package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCacheDelete;
import com.htc.ea.admincache.model.AdminCacheInsert;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.AdminCacheUpdate;
import com.htc.ea.admincache.service.AdminCacheService;
import com.htc.ea.util.dto.ConfigurationData;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.plataform-entry:false}")
@RequestMapping(value = "/platforms")
@Api(value="Plataform", tags="Operaciones para Plataformas - Toolbox y Redis")
public class AdminCachePlatformController {
	
	@Autowired
	AdminCacheService adminCacheService;
	
	@Autowired
	Environment env;

	@ApiOperation(value = "Crear plataforma en Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/redis/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setInitApp(@RequestBody AdminCachePojo init) {
		init.setPlatform(1);
		init.setEnvironmentId(" ");
		init.setVersion(" ");
		init.setMapKey("ConfigurationData");
		AdminCacheResponse response = adminCacheService.setInitApp(init);
		response.setApp(init.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Refrescar plataforma de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh(@RequestBody AdminCachePojo refresh) {
		refresh.setPlatform(1);
		refresh.setEnvironmentId(env.getProperty("app.env"));
		refresh.setMapKey("ConfigurationData");
		AdminCacheResponse response = adminCacheService.transferConfigEntriesOracleToRedis(refresh);
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Insertar propiedad de plataforma en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setProperty(@RequestBody AdminCacheInsert insert) {
		insert.setPlatform(1);
		insert.setEnvironmentId(env.getProperty("app.env"));
		insert.setMapKey("ConfigurationData");
		AdminCacheResponse response = adminCacheService.setPropertyOnRedis(insert);
		response.setApp(insert.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Actualizar propiedad de plataforma en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateProperty(@RequestBody AdminCacheUpdate update) {
		update.setPlatform(1);
		update.setEnvironmentId(env.getProperty("app.env"));
		update.setMapKey("ConfigurationData");
		AdminCacheResponse response = adminCacheService.updatePropertyOnRedis(update);
		response.setApp(update.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}

	@ApiOperation(value = "Obtener propiedades de plataforma en Redis", response = ConfigurationData.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@GetMapping(value = "/{applicationId}/{version}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getConfiguration(@PathVariable("applicationId") final String applicationId, @PathVariable("version") final Double version) {
		AdminCachePojo config = new AdminCachePojo();
		config.setApplicationId(applicationId);
		config.setVersion(version.toString());
		config.setPlatform(1);
		config.setEnvironmentId(env.getProperty("app.env"));
		config.setMapKey("ConfigurationData");
		ConfigurationData response = adminCacheService.getAllPropertiesFromRedis(config, null);
		return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Borrar propiedad de plataforma en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteProperty(@RequestBody AdminCacheDelete del) {
		del.setPlatform(1);
		del.setEnvironmentId(env.getProperty("app.env"));
		del.setMapKey("ConfigurationData");
		AdminCacheResponse response = adminCacheService.deleteProperty(del);
		response.setApp(del.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
}