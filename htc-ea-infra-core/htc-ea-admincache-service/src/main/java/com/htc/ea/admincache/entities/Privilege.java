package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: Privilege</h1>
 * 
 * @description This entity models the permissions. its primary key is a String
 *              type id.
 * 
 *              Privilege has <b>@OneToMany</b> relationship with Permission
 * 
 **/
@Entity
public class Privilege extends StringIdBaseEntity {

	private static final long serialVersionUID = 3636917124853410394L;

	public Privilege() {
	}

	public Privilege(String id) {
		super(id);
	}

	public Privilege(String id, String name, String description, Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Privilege [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
