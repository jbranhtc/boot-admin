package com.htc.ea.admincache.model;

import java.util.HashSet;
import java.util.Set;

import com.htc.ea.admincache.util.AdminCacheUtil;

public class AdminCacheUpdate extends AdminCachePojo{
	
	private String key;
	private String value;
	private Set<String> configEntryValues = new HashSet<String>();
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public Set<String> getConfigEntryValues() {
		return configEntryValues;
	}
	
	public void setConfigEntryValues(Set<String> configEntryValues) {
		this.configEntryValues = configEntryValues;
	}
	
	public boolean validate(){
		return AdminCacheUtil.stringValidation(this.getKey(), this.getApplicationId(), this.getEnvironmentIdp(), this.getVersion());
	}
}