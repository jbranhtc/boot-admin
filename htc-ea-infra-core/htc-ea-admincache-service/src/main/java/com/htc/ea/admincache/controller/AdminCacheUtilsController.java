package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.AdminCacheDaoRedisAux;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.util.dto.ConfigurationData;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.utils:false}")
@RequestMapping(value = "/utils")
@Api(value="Operations", tags="Operaciones Utiles - Toolbox y Redis")
public class AdminCacheUtilsController{

	@Autowired
	AdminCacheDaoRedisAux adminCacheDaoRedisAux;
	
	@Autowired
	private AdminCacheDaoRedis adminCacheDaoRedis;
	
	@Autowired
	Environment env;
	
	@ApiOperation(value = "Crear objeto/s en Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful on Redis"),
            @ApiResponse(code = 404, message = "App exists on Redis"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@ApiImplicitParams({
		@ApiImplicitParam(name="dbIndex", value="", required=true, dataType = "string", paramType = "query", allowableValues = "(0,15)")
	})
	@PostMapping(value = "/redis/object/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setInitObject(@RequestParam(required = true) String key, @RequestParam(required = true) List<String> nameObject, @RequestParam(required = true) String obj, @RequestParam(required = true) int dbIndex) {
		AdminCacheResponse response = new AdminCacheResponse();
		List<ResponseCache> lst = new ArrayList<ResponseCache>();
		ResponseCache msg = new ResponseCache();
		try{
			adminCacheDaoRedisAux.changeDB(dbIndex);
			for(String aux : nameObject){
				adminCacheDaoRedisAux.hmset(key, obj, aux);
			}			
			adminCacheDaoRedisAux.changeDB(0);
			msg.setCode(0);
			msg.setDescription("Successful on Redis");
			response.setStatus(HttpStatus.OK);
		}catch(Exception e){
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
			msg.setCode(-1);
			msg.setDescription(e.getMessage());
		}
		lst.add(msg);
		response.setResponse(lst);
		response.setApp(key);
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Crear aplicacion en Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful on Redis"),
            @ApiResponse(code = 404, message = "App exists on Redis"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@ApiImplicitParams({
	})
	@PostMapping(value = "/redis/aplication/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setInitApp(@RequestParam(name = "applicationId", required = true) String applicationId, @RequestParam(name = "version", required = true) Double version) {
		AdminCacheResponse response = new AdminCacheResponse();
		List<ResponseCache> lst = new ArrayList<ResponseCache>();
		ResponseCache msg = new ResponseCache();
		try{
			ConfigurationData as = new ConfigurationData();
			adminCacheDaoRedis.hmset(applicationId + "-" + env.getProperty("app.env") + "-" + version.toString(), as, "ConfigurationData");
			msg.setCode(0);
			msg.setDescription("Successful on Redis");
			response.setStatus(HttpStatus.OK);
		}catch(Exception e){
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
			msg.setCode(-1);
			msg.setDescription(e.getMessage());
		}
		lst.add(msg);
		response.setResponse(lst);
		response.setApp(applicationId);
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
}