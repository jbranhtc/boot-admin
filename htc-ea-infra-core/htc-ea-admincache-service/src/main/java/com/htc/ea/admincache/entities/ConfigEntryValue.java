package com.htc.ea.admincache.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * <h1>Project: Model</h1> <h1>entity: ConfigEntryValue</h1>
 * 
 * @description This entity models the input configuration values. its primary
 *              key is a Long type id.
 * 
 *              ConfigEntryValue has <b>@ManyToOne</b> relationship with
 *              ConfigEntry
 * 
 **/
@Entity
public class ConfigEntryValue implements Serializable {

	private static final long serialVersionUID = -6327070464804864238L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ConfigEntryValueGenerator")
	@SequenceGenerator(name = "ConfigEntryValueGenerator", sequenceName = "ConfigEntryValueSeq")
	private Long id;

	@Column(nullable = true, length = 200)
	private String value;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "configEntryId")
	@JsonBackReference
	private ConfigEntry configEntry;

	private Boolean enabled;

	public ConfigEntryValue() {
		super();
	}
	
	public ConfigEntryValue(Long id) {
		this.id = id;
	}

	public ConfigEntryValue(Long id, String value, ConfigEntry configEntry,
			Boolean enabled) {
		this.id = id;
		this.value = value;
		this.configEntry = configEntry;
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ConfigEntry getConfigEntry() {
		return configEntry;
	}

	public void setConfigEntry(ConfigEntry configEntry) {
		this.configEntry = configEntry;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigEntryValue [id=").append(id).append(", value=")
				.append(value).append(", configEntry=").append(configEntry)
				.append(", enabled=").append(enabled).append("]");
		return builder.toString();
	}
	
}
