package com.htc.ea.admincache.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the TRANSACTIONS database table.
 * 
 */
@Entity
@Table(name="TRANSACTIONS")
@NamedQuery(name="Transaction.findAll", query="SELECT t FROM Transaction t")
public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name="API_MTS")
	private String apiMts;

	@Column(name="API_UTIBA")
	private String apiUtiba;

	private Date createddate;

	private Integer enabled;

	@Column(name="SERVICE_MTS")
	private String serviceMts;

	//bi-directional many-to-one association to LevelActor
	@ManyToOne
	@JoinColumn(name="DEBIT_ACCOUNT")
	private LevelActor debit;

	//bi-directional many-to-one association to LevelActor
	@ManyToOne
	@JoinColumn(name="\"CREDIT ACCOUNT\"")
	private LevelActor credit;

	public Transaction() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApiMts() {
		return this.apiMts;
	}

	public void setApiMts(String apiMts) {
		this.apiMts = apiMts;
	}

	public String getApiUtiba() {
		return this.apiUtiba;
	}

	public void setApiUtiba(String apiUtiba) {
		this.apiUtiba = apiUtiba;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Integer getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public String getServiceMts() {
		return this.serviceMts;
	}

	public void setServiceMts(String serviceMts) {
		this.serviceMts = serviceMts;
	}

	public LevelActor getDebit() {
		return this.debit;
	}

	public void setDebit(LevelActor debit) {
		this.debit = debit;
	}

	public LevelActor getCredit() {
		return this.credit;
	}

	public void setCredit(LevelActor credit) {
		this.credit = credit;
	}

}