package com.htc.ea.admincache.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoOracle;
import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.GetConfigurationDAO;
import com.htc.ea.admincache.dao.LoadClassDAO;
import com.htc.ea.admincache.entities.Loadclassdata;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.util.dto.LoadClasses;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;

@Service
public class LoadClassLogicalService {
	
	@Autowired
	private AdminCacheDaoOracle adminCacheDaoOracle;
	@Autowired
	private AdminCacheDaoRedis adminCacheDaoRedis;
	@Autowired
	private AdminCacheUtilRedis adminCacheUtilRedis;
	@Autowired
    private RedisMessagePublisher servPublisher;
	
	private AdminCacheResponse response = null;
	@Autowired
	private LoadClassDAO loadClassDAO;
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;
	
	private String keyConfigEntry = "init.config";
	@Autowired
	private AdminCacheService adminCacheService;
	
	/**
	 * Begin object of app on Redis
	 * @param init
	 * @return
	 */
	public AdminCacheResponse setInitApp(AdminCachePojo init){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!init.validate()){
				if(!adminCacheUtilRedis.hexists(init.getKeyRedis(), init.getMapKeyR())){
					List<LoadClasses> lst = new ArrayList<LoadClasses>();					
					adminCacheDaoRedis.hmsetL(init.getKeyRedis(), lst, init.getMapKeyR());
					resp.setCode(0);
					response.setStatus(HttpStatus.OK);
					resp.setDescription("Successful on Redis");
				}else{
					resp.setCode(1);
					resp.setDescription("Fail: Object exists on Redis");
					response.setStatus(HttpStatus.CONFLICT);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.setStatus(HttpStatus.BAD_REQUEST);
			}
		}catch(Exception e){
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * Copy load class between oracle database and database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	@SuppressWarnings("deprecation")
	public AdminCacheResponse transferConfigEntriesOracleToRedis(AdminCachePojo refresh){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!refresh.validate()){
				// validamos si al app exista en la toolbox
				if(getConfigurationDAO.existsConfiguration(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion())){
					// obtenemos el objeto del web service de la toolbox
					List<LoadClasses> as = adminCacheDaoOracle.getLoadClassesOracle(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion());
					if(as != null){
						// guardamos el objeto en Redis
						adminCacheDaoRedis.hmsetL(refresh.getKeyRedis(), as, refresh.getMapKeyR());
						// validamos si el objeto insertado exista en Redis
						if(adminCacheUtilRedis.exists(refresh.getKeyRedis())){
							// enviamos el mensaje para que la aplicacion se actualice
							servPublisher.publish(refresh.getKeyRedis() + ":" + refresh.getMapKeyR());
							resp.setCode(0);
							response.setStatus(HttpStatus.OK);
							resp.setDescription("Successful on Redis");
						}else{
							resp.setCode(1);
							response.setStatus(HttpStatus.METHOD_FAILURE);
							resp.setDescription("Fail: Not created on Redis");
						}
					}else{
						resp.setCode(2);
						response.setStatus(HttpStatus.NOT_FOUND);
						resp.setDescription("Fail: Error get List Class from ToolBox");
					}
				}else{
					resp.setCode(3);
					resp.setDescription("Fail: App not exists on Toolbox");
					response.setStatus(HttpStatus.NOT_FOUND);
				}
			}else{
				resp.setCode(4);
				response.setStatus(HttpStatus.BAD_REQUEST);
				resp.setDescription("Fail: Params not must be null or empty");
			}
		}catch(Exception e){
			e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * 
	 * @param nameClass
	 * @param insert
	 * @return
	 */
	public AdminCacheResponse insertLoadClassOnRedis(String nameClass, AdminCachePojo insert){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		AdminCachePojo aux = new AdminCachePojo();
		aux.setApplicationId(insert.getApplicationId());
		aux.setEnvironmentId(insert.getEnvironmentIdp());
		aux.setVersion(insert.getVersion());
		aux.setMapKey("ConfigurationData");
		try{
			// validamos los parametros de entrada
			if(!AdminCacheUtil.stringValidation(nameClass) && !insert.validate()){
				if(AdminCacheUtil.existConfigEntry(adminCacheService.getAllPropertiesFromRedis(aux, null), keyConfigEntry)){
					// Process to Toolbox
					if(!insert.isOnlyRedis()){
						resp = new ResponseCache();
						// validamos si al app exista en la toolbox
						if(getConfigurationDAO.existsConfiguration(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion())){
							// validamos si no existe la class
							boolean exist = loadClassDAO.existLoadclassdata(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion(), nameClass);
							if(!exist){
								// guardamos la class
								boolean save = loadClassDAO.createLoadclassdata(nameClass, insert);
								if(save){
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Toolbox");
								}else{
									toolbox = false;
									resp.setCode(6);
									resp.setDescription("Fail: Class not added on Toolbox");
								}
							}else{
								toolbox = false;
								resp.setCode(7);
								resp.setDescription("Fail: Class exists on Toolbox");
								response.setStatus(HttpStatus.CONFLICT);
							}
						}else{
							toolbox = false;
							resp.setCode(3);
							resp.setDescription("Fail: App not exists on Toolbox");
							response.setStatus(HttpStatus.CONFLICT);
						}
						resp.setKey(nameClass);
						response.getResponse().add(resp);
					}
					// Process to Redis
					if(toolbox){
						resp = new ResponseCache();
						if(adminCacheUtilRedis.exists(insert.getKeyRedis())){
							// obtenemos el objeto en Redis
							List<LoadClasses> data = this.getAllPropertiesFromRedis(insert, null);
							if(data != null){
								// validamos si la nueva classLoad no exista en Redis
								if(!AdminCacheUtil.existClassLoad(data, nameClass)){
									Loadclassdata load = loadClassDAO.findLoadclassdata(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion(), nameClass);
									// insertamos la nueva class load en el objeto
									LoadClasses nueva = new LoadClasses();
									nueva.setClassName(nameClass);
									nueva.setUpdate(false);
									nueva.setUploadDate(load != null ? AdminCacheUtil.getFormatDate(load.getUploaddate(), 6) : AdminCacheUtil.getFormatDate(new Date(), 6));
									data.add(nueva);
									// actualizamos el objeto entero con la nueva classLoad en redis
									adminCacheDaoRedis.hmsetL(insert.getKeyRedis(), data, insert.getMapKeyR());
									if(AdminCacheUtil.existClassLoad(this.getAllPropertiesFromRedis(insert, null), nameClass)){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(insert.getKeyRedis() + ":" + insert.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Class not added on Redis");
									}
								}else{
									resp.setCode(7);
									resp.setDescription("Fail: Class exists on Redis");
									response.setStatus(HttpStatus.CONFLICT);
								}
							}else{
								resp.setCode(8);
								resp.setDescription("Fail: List Classs is null on Redis");
							}
						}else{
							resp.setCode(3);
							resp.setDescription("Fail: Aplication Id not exists on Redis");
							response.setStatus(HttpStatus.CONFLICT);
						}
						resp.setKey(nameClass);
						response.getResponse().add(resp);
					}
				}else{
					resp.setCode(8);
					resp.setDescription("Fail: Config Entry <" + keyConfigEntry + "> dont exist in " + insert.getApplicationId());
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Update load class on database Redis
	 * @param String - Name of application Id
	 * @param String - Key
	 * @param String - Value
	 * @return 
	 */
	public AdminCacheResponse updateLoadClassOnRedis(String nameClass, AdminCachePojo update){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		AdminCachePojo aux = new AdminCachePojo();
		aux.setApplicationId(update.getApplicationId());
		aux.setEnvironmentId(update.getEnvironmentIdp());
		aux.setVersion(update.getVersion());
		aux.setMapKey("ConfigurationData");
		try{
			if(!AdminCacheUtil.stringValidation(nameClass) && !update.validate()){
				if(AdminCacheUtil.existConfigEntry(adminCacheService.getAllPropertiesFromRedis(aux, null), keyConfigEntry)){
					// Process to Toolbox
					if(!update.isOnlyRedis()){
						resp = new ResponseCache();
						// validamos si al app exista en la toolbox
						if(getConfigurationDAO.existsConfiguration(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion())){
							// validamos si existe la class
							boolean exist = loadClassDAO.existLoadclassdata(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), nameClass);
							if(exist){
								// actualizamos la class
								boolean updated = loadClassDAO.updateConfigEntry(nameClass, update);
								if(updated){
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Toolbox");
								}else{
									toolbox = false;
									resp.setCode(6);
									resp.setDescription("Fail: Class not updated on Toolbox");
								}
							}else{
								toolbox = false;
								resp.setCode(7);
								resp.setDescription("Fail: Class not exists on Toolbox");
								response.setStatus(HttpStatus.NOT_FOUND);
							}
						}else{
							toolbox = false;
							resp.setCode(3);
							resp.setDescription("Fail: Aplication Id not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
						resp.setKey(nameClass);
						response.getResponse().add(resp);
					}
					// Process to Redis
					if(toolbox){
						resp = new ResponseCache();
						if(adminCacheUtilRedis.exists(update.getKeyRedis())){
							List<LoadClasses> data = this.getAllPropertiesFromRedis(update, null);
							if(data != null){
								if(AdminCacheUtil.existClassLoad(data, nameClass)){
									Loadclassdata load = loadClassDAO.findLoadclassdata(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), nameClass);
									LoadClasses temp = AdminCacheUtil.getClassLoad(data, nameClass);
									data.remove(temp);
									temp.setUpdate(true);
									temp.setUploadDate(load != null ? AdminCacheUtil.getFormatDate(load.getUploaddate(), 6) : AdminCacheUtil.getFormatDate(new Date(), 6));
									data.add(temp);
									adminCacheDaoRedis.hmsetL(update.getKeyRedis(), data, update.getMapKeyR());
									if(AdminCacheUtil.existClassLoad(this.getAllPropertiesFromRedis(update, null), nameClass)){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(update.getKeyRedis() + ":" + update.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Class not updated on Redis");
									}
								}else{
									resp.setCode(7);
									resp.setDescription("Fail: Class not exists on Redis");
									response.setStatus(HttpStatus.NOT_FOUND);
								}
							}else{
								resp.setCode(8);
								resp.setDescription("Fail: List Classs is null on Redis");
							}
						}else{
							resp.setCode(3);
							resp.setDescription("Fail: Aplication Id not exists on Redis");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}
					resp.setKey(nameClass);
					response.getResponse().add(resp);
				}else{
					resp.setCode(8);
					resp.setDescription("Fail: Config Entry <" + keyConfigEntry + "> dont exist in " + update.getApplicationId());
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Get load class on database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public List<LoadClasses> getAllPropertiesFromRedis(AdminCachePojo config, String typeProperty){
		List<LoadClasses> aux = null;
		AdminCachePojo temp = new AdminCachePojo();
		temp.setApplicationId(config.getApplicationId());
		temp.setEnvironmentId(config.getEnvironmentIdp());
		temp.setVersion(config.getVersion());
		temp.setMapKey("ConfigurationData");
		try{
			if(!config.validate()){
				if(AdminCacheUtil.existConfigEntry(adminCacheService.getAllPropertiesFromRedis(temp, null), keyConfigEntry)){
					aux = adminCacheDaoRedis.hgetL(config.getKeyRedis(), config.getMapKeyR());
					if(aux != null){
						return aux;
					}else{
						return new ArrayList<LoadClasses>();
					}	
				}else{
					return null;
				}
			}else{
				return new ArrayList<LoadClasses>();
			}
		}catch(Exception e){
			e.printStackTrace();
			return new ArrayList<LoadClasses>();
		}
	}
}