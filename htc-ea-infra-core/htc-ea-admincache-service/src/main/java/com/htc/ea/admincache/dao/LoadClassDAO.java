package com.htc.ea.admincache.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.Configuration;
import com.htc.ea.admincache.entities.Loadclassdata;
import com.htc.ea.admincache.model.AdminCachePojo;

@Repository
public class LoadClassDAO{
	
	private static Logger logger = LoggerFactory.getLogger(LoadClassDAO.class);
	
	@Autowired
	private EntityManager entityManager;

	public Loadclassdata findLoadclassdata(String applicationId, String environmentId, String version, String nameClass) {
		Query query = entityManager.createQuery("SELECT entry FROM Loadclassdata entry INNER JOIN entry.configuration config WHERE config.application.id=:applicationId AND config.environment.id=:environmentId AND config.version=:version AND entry.classname=:nameClass", Loadclassdata.class)
		.setParameter("applicationId", applicationId)
		.setParameter("environmentId", environmentId)
		.setParameter("version", version)
		.setParameter("nameClass", nameClass);
		if(!query.getResultList().isEmpty()){
			return (Loadclassdata) query.getSingleResult();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Loadclassdata> findListLoadclassdata(String applicationId, String environmentId, String version) {
		Query query = entityManager.createQuery("SELECT entry FROM Loadclassdata entry INNER JOIN entry.configuration config WHERE config.application.id=:applicationId AND config.environment.id=:environmentId AND config.version=:version", Loadclassdata.class)
		.setParameter("applicationId", applicationId)
		.setParameter("environmentId", environmentId)
		.setParameter("version", version);
		if(!query.getResultList().isEmpty()){
			return query.getResultList();
		}
		return null;
	}
	
	public boolean existLoadclassdata(String applicationId, String environmentId, String version, String nameClass){
		Loadclassdata entry = findLoadclassdata(applicationId, environmentId, version, nameClass);
		if(entry != null){
			return true;
		}
		return false;
	}
	
	public Configuration findConfiguration(String applicationId, String environmentId, String version){
		Configuration temp = null;
		Query query = entityManager.createQuery("SELECT entry FROM Configuration entry WHERE entry.application.id=:applicationId AND entry.environment.id=:environmentId AND entry.version=:version", Configuration.class)
				.setParameter("applicationId", applicationId)
				.setParameter("environmentId", environmentId)
				.setParameter("version", version);
		if(!query.getResultList().isEmpty()){
			temp = (Configuration) query.getSingleResult();
		}
		return temp;
	}
	
	@SuppressWarnings("unused")
	public boolean createLoadclassdata(String nameClass, AdminCachePojo insert) throws Exception{
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		Configuration con = this.findConfiguration(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion());
		try{
			if(con != null){
				Loadclassdata aux = new Loadclassdata();
				aux.setConfiguration(con);
				aux.setClassname(nameClass);
				aux.setUpdate(false);
				aux.setUploaddate(new Date());
				entityManager.persist(aux);
				entityManager.getTransaction().commit();
				return true;
			}else{
				if(con == null) throw new Exception("applicationId: " + insert.getApplicationId() + ", enviroment: " + insert.getEnvironmentIdp() + ", version: " + insert.getVersion() + ", not found");
				return false;
			}
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	public boolean updateConfigEntry(String nameClass, AdminCachePojo update){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			Loadclassdata entry = findLoadclassdata(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), nameClass);
			Loadclassdata temp = entityManager.find(Loadclassdata.class, entry.getClassname());
			temp.setUploaddate(new Date());
			temp.setUpdate(true);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
	        }
			throw e;
		}
	}

	public void clear(){
		entityManager.clear();
	}
}