package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheConfigEventCatService;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.event-category:false}")
@RequestMapping(value = "/events")
@Api(value="ConfigurationEventCategoryData", tags="Operaciones para ConfigurationEventCategory - Toolbox y Redis")
public class AdminCacheConfigEventCatController{

	@Autowired
	AdminCacheConfigEventCatService adminCacheConfigEventCatService;
	
	@Autowired
	Environment env;

	@ApiOperation(value = "Refrescar Configuraciones de Categorias de Eventos de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Refresh toolbox on redis", response = AdminCacheResponse.class),
            @ApiResponse(code = 404, message = "Error get config entry from ToolBox/App not exists on Toolbox"),
            @ApiResponse(code = 400, message = "Params not must be null or empty"),
            @ApiResponse(code = 420, message = "Not created on Redis/Exception"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh(@RequestBody AdminCachePojo refresh) {
		refresh.setMapKey("ConfigurationEventCategoryData");
		refresh.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheConfigEventCatService.transferConfigEventCatOracleToRedis(refresh);
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}

	@ApiOperation(value = "Obtener Configuraciones de Categorias de Eventos de una aplicacion en Redis", response = ConfigurationEventCategoryData.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getConfiguration(@ModelAttribute("config") AdminCachePojo config) {
		config.setMapKey("ConfigurationEventCategoryData");
		config.setEnvironmentId(env.getProperty("app.env"));
		List<ConfigurationEventCategoryData> response = adminCacheConfigEventCatService.getAllConfigEventCatFromRedis(config);
		return new ResponseEntity<Object>(response, new HttpHeaders(), HttpStatus.OK);
	}
}