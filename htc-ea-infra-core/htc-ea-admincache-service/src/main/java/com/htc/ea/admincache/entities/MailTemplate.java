package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

/**
 * <h1>Project: Model</h1> <h1>entity: MailTemplate</h1>
 * 
 * @description This entity models the applications sent by post. its primary
 *              key is a Long type id.
 * 
 * 
 **/
@Entity
public class MailTemplate extends StringIdBaseEntity {

	private static final long serialVersionUID = 6821978569497035935L;

	@Column(name = "toField", nullable = true, length = 255)
	private String to;

	@Column(name = "fromField", nullable = true, length = 255)
	private String from;

	@Column(name = "cc", nullable = true, length = 255)
	private String cc;

	@Column(name = "bcc", nullable = true, length = 255)
	private String bcc;

	@Column(nullable = false, length = 255)
	private String subject;

	@Column(nullable = false)
	private Boolean html;

	@Lob
	@Column(nullable = false)
	private String text;

	public MailTemplate() {
		super();
	}

	public MailTemplate(String id) {
		super(id);
	}

	public MailTemplate(String id, String name, String description, Boolean enabled) {
		super(id, name, description, enabled);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Boolean getHtml() {
		return html;
	}

	public void setHtml(Boolean html) {
		this.html = html;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MailTemplate [id=").append(id).append(", to=")
				.append(to).append(", from=").append(from).append(", cc=")
				.append(cc).append(", bcc=").append(bcc).append(", subject=")
				.append(subject).append(", html=").append(html)
				.append(", text=").append(text).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}	

}
