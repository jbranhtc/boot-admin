package com.htc.ea.admincache.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * <h1>Project: Model</h1> <h1>entity: GenericBaseEntity<T></h1>
 * 
 * @description This abstract class serves as a model for entities with key
 *              type Long. its primary key is a Long type id.
 * 
 *              GenericBaseEntity<T>: abstract class
 * 
 **/
@MappedSuperclass
public abstract class GenericBaseEntity<T> implements Serializable {

	private static final long serialVersionUID = -3159355592167378050L;

	protected Boolean enabled;

	@Column(nullable = false, updatable = false)
	protected Date createdDate;

	@Column(insertable = false)
	protected Date modifiedDate;

	public GenericBaseEntity() {
	}

	public GenericBaseEntity(Boolean enabled) {
		super();
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}

	@PreUpdate
	public void preUpdate() {
		modifiedDate = new Date();
	}
}
