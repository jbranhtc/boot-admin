package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * <h1>Project: Model</h1> <h1>entity: ConfigEntry</h1>
 * 
 * @description This entity models the different configuration entry. its
 *              primary key is a Long type id.
 * 
 *              ConfigEntry has <b>@ManyToOne</b> relationship with
 *              ConfigDataType
 * 
 **/
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "configurationId",
		"key" }))
public class ConfigEntry extends NameableEntity<Long> {

	private static final long serialVersionUID = -9099424505054460211L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ConfigEntryGenerator")
	@SequenceGenerator(name = "ConfigEntryGenerator", sequenceName = "ConfigEntrySeq")
	private Long id;

	@JoinColumn(name = "configurationId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JsonBackReference
	private Configuration configuration;

	@Column(nullable = false, length = 50)
	private String key;

	@JoinColumn(name = "configTypeId", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private ConfigType configType;

	@JoinColumn(name = "configDataTypeId", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private ConfigDataType configDataType;

	@Column(nullable = true, length = 200)
	private String value;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "configEntry")
	@JsonManagedReference
	private List<ConfigEntryValue> configEntryValues = new ArrayList<ConfigEntryValue>();

	private boolean singleValue = true;

	public ConfigEntry() {
		super();
	}

	public ConfigEntry(Long id) {
		super();
		this.id = id;
	}

	public ConfigEntry(String name, String description,
			Configuration configuration, String key, String value) {
		super(name, description, true);
		this.configuration = configuration;
		this.key = key;
		this.value = value;
	}

	public List<ConfigEntryValue> getConfigEntryValues() {
		return configEntryValues;
	}

	public void setConfigEntryValues(List<ConfigEntryValue> configEntryValues) {
		this.configEntryValues = configEntryValues;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public ConfigType getConfigType() {
		return configType;
	}

	public void setConfigType(ConfigType configType) {
		this.configType = configType;
	}

	public ConfigDataType getConfigDataType() {
		return configDataType;
	}

	public void setConfigDataType(ConfigDataType configDataType) {
		this.configDataType = configDataType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSingleValue() {
		return singleValue;
	}

	public void setSingleValue(boolean singleValue) {
		this.singleValue = singleValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigEntry [id=").append(id)
				.append(", configuration=").append(configuration)
				.append(", key=").append(key).append(", configType=")
				.append(configType).append(", configDataType=")
				.append(configDataType).append(", value=").append(value)
				.append(", configEntryValues=").append(configEntryValues)
				.append(", singleValue=").append(singleValue).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
