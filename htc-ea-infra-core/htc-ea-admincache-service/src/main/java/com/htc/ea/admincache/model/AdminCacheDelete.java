package com.htc.ea.admincache.model;

import com.htc.ea.admincache.util.AdminCacheUtil;

public class AdminCacheDelete extends AdminCachePojo{
	
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public boolean validate(){
		return AdminCacheUtil.stringValidation(this.getKey(), this.getApplicationId(), this.getEnvironmentIdp(), this.getVersion());
	}
}
