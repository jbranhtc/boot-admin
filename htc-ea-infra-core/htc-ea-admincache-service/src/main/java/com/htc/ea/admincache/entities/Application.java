package com.htc.ea.admincache.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * <h1>Project: Model</h1> <h1>entity: Application</h1>
 * 
 * @description This entity models the catalog of enterprise applications its
 *              primary key is a String type id.
 * 
 *              Application has <b>@ManyToOne</b> relationship with Country
 *              Application has <b>@OneToMany</b> relationship with
 *              ConversionEntry Application has <b>@OneToMany</b> relationship
 *              with Configuration Application has <b>@OneToMany</b>
 *              relationship with EventLog Application has <b>@OneToMany</b>
 *              relationship with Role
 * 
 **/

@Entity
public class Application extends StringIdBaseEntity {

	private static final long serialVersionUID = -1118213084472602201L;

	@JoinColumn(name = "countryId")
	@ManyToOne
	private Country country;

	public Application() {
	}

	public Application(String id) {
		super(id);
	}

	public Application(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Application [country=").append(country).append(", id=")
				.append(id).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
