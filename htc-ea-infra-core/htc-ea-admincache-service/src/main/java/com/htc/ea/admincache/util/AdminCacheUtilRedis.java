package com.htc.ea.admincache.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.htc.ea.admincache.interfaces.AdminCacheInterfaceUtilRedis;

@Component
public class AdminCacheUtilRedis implements AdminCacheInterfaceUtilRedis<Object> {

	@Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
	
	@Override
	public boolean exists(String key) {
		return redisTemplate.hasKey(key);
	}

	@Override
	public void delete(String key) {
		redisTemplate.delete(key);
	}
	
	@Override
	public boolean hexists(String key, String id) {
		if(redisTemplate.getConnectionFactory() != null){
			return redisTemplate.opsForHash().hasKey(key, id);
		}else{
			return false;
		}
	}
	
	@Override
	public void delete(String key, String id) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().delete(key, id);
		}
	}
	
	@Override
	public void selectDB(int dbIndex) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.getConnectionFactory().getConnection().select(dbIndex);
		}
	}

	@Override
	public boolean existsKeyMap(String key, String hashKey) {
		return redisTemplate.opsForHash().hasKey(key, hashKey);
	}
}