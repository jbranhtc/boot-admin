package com.htc.ea.admincache.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.RoutingDao;
import com.htc.ea.admincache.entities.Routing;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.admincache.util.ApiError;
import com.htc.ea.util.dto.RoutingDto;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;

@Service
public class RoutingService {
	
	@Autowired
	private RoutingDao dao;
	@Autowired
	private AdminCacheUtilRedis util;
	@Autowired
	private AdminCacheDaoRedis daoRedis;
	@Autowired
    private RedisMessagePublisher redisPublisher;
	private String redisKey = "routing";
	private String mesgChannel = "routing:RoutingData";

	public ApiError isIncluded(String type, String value, boolean onlyRedis){
		ApiError response = null;
		Routing entry = new Routing();
		entry.setType(type);
		entry.setValue(value);
		try{
			boolean val = AdminCacheUtil.stringValidation(type, value);
			if(!val){
				if(!onlyRedis){
					if(dao.existsRouting(entry)){
						response = new ApiError(HttpStatus.OK,"true");
					}else{
						response = new ApiError(HttpStatus.NOT_FOUND,"false");
					}
				}else{
					response = new ApiError(HttpStatus.CONFLICT,"false");
					if(util.exists(redisKey)){
						List<RoutingDto> data = this.getAllRoutingFromRedis(redisKey);
						if(data != null && !data.isEmpty()){
							if(AdminCacheUtil.existRoutingRedis(data, entry)){
								response = new ApiError(HttpStatus.OK,"true");
							}else{
								//System.out.println("no existe en redis");// no existe en redis
							}
						}else{
							//System.out.println("la data no existe");// la data no existe
						}
					}else{
						//System.out.println("no existe el key en redis");// no existe el key en redis
					}
				}
			}else{
				response = new ApiError(HttpStatus.BAD_REQUEST,"Missing Values", entry.toString());
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED,"Exception Routing", e.getMessage());
		}
		return response;
	}
	
	public ApiError isIncludedByContext(String type, String value, String context, boolean onlyRedis){
		ApiError response = null;
		Routing entry = new Routing();
		entry.setType(type);
		entry.setValue(value);
		entry.setContext(context);
		try{
			boolean val = AdminCacheUtil.stringValidation(type, value, context);
			if(!val){
				if(!onlyRedis){
					if(dao.existsRouting(entry)){
						response = new ApiError(HttpStatus.OK,"true");
					}else{
						response = new ApiError(HttpStatus.NOT_FOUND,"false");
					}
				}else{
					response = new ApiError(HttpStatus.NOT_FOUND,"false");
					if(util.exists(redisKey)){
						List<RoutingDto> data = this.getAllRoutingFromRedis(redisKey);
						if(data != null && !data.isEmpty()){
							if(AdminCacheUtil.existRoutingRedis(data, entry)){
								response = new ApiError(HttpStatus.OK,"true");
							}else{
								//System.out.println("no existe en redis");// no existe en redis
							}
						}else{
							//System.out.println("la data no existe");// la data no existe
						}
					}else{
						//System.out.println("no existe el key en redis");// no existe el key en redis
					}
				}
			}else{
				response = new ApiError(HttpStatus.BAD_REQUEST,"Missing Values", entry.toString());
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED,"Exception Routing", e.getMessage());
		}
		return response;
	}
	
	public ApiError add(String type, String value, String context, boolean onlyRedis) {
		ApiError response = null;
		Routing entry = new Routing();
		boolean toolbox = true;
		entry.setType(type);
		entry.setValue(value);
		entry.setContext(context);
		entry.setEnabled(true);
		try{
			boolean val = (context != null && !context.isEmpty()) ? AdminCacheUtil.stringValidation(type, value, context) : AdminCacheUtil.stringValidation(type, value);
			if(!val){
				if(!onlyRedis){
					if(!dao.existsRouting(entry)){
						dao.createRouting(entry);
						response = new ApiError(HttpStatus.OK,"Add Routing Toolbox");
					}else{
						response = new ApiError(HttpStatus.CREATED,"Exists Routing Toolbox");
						toolbox = false;
					}
				}
				if(toolbox){
					if(util.exists(redisKey)){
						List<RoutingDto> data = this.getAllRoutingFromRedis(redisKey);
						if(data != null && !data.isEmpty()){
							if(!AdminCacheUtil.existRoutingRedis(data, entry)){
								RoutingDto d = new RoutingDto();
								d.setContext(entry.getContext());
								d.setEnabled(entry.getEnabled());
								d.setType(entry.getType());
								d.setValue(entry.getValue());
								d.setId(entry.getId());
								data.add(d);
								daoRedis.hmsetR(redisKey, data, "RoutingData");
								data.clear();
								data = this.getAllRoutingFromRedis(redisKey);
								if(AdminCacheUtil.existRoutingRedis(data, entry)){
									response = new ApiError(HttpStatus.OK,"Add Routing Redis");
									redisPublisher.publish(mesgChannel);
								}
							}else{
								response = new ApiError(HttpStatus.CREATED, "Exists Routing data on Redis");
							}
						}else{
							response = new ApiError(HttpStatus.NOT_FOUND, "Routing is null on Redis");
						}
					}else{
						response = new ApiError(HttpStatus.NOT_FOUND,"Dont Exists Router key on Redis");
					}
				}
			}else{
				response = new ApiError(HttpStatus.BAD_REQUEST,"Missing Values", entry.toString());
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED,"Exception Routing", e.getMessage());
		}
		return response;
	}
	
	public ApiError delete(String type, String value, String context, boolean onlyRedis) {
		ApiError response = null;
		Routing entry = new Routing();
		boolean toolbox = true;
		entry.setType(type);
		entry.setValue(value);
		entry.setContext(context);
		try{
			boolean val = (context != null && !context.isEmpty()) ? AdminCacheUtil.stringValidation(type, value, context) : AdminCacheUtil.stringValidation(type, value);
			if(!val){
				if(!onlyRedis){
					if(dao.existsRouting(entry)){
						dao.deleteRouting(entry);
						response = new ApiError(HttpStatus.OK, "Delete Routing Toolbox");
					}else{
						response = new ApiError(HttpStatus.NOT_FOUND, "Dont Exists Routing Toolbox");
						toolbox = false;
					}
				}
				if(toolbox){
					if(util.exists(redisKey)){
						List<RoutingDto> data = this.getAllRoutingFromRedis(redisKey);
						if(data != null && !data.isEmpty()){
							if(AdminCacheUtil.existRoutingRedis(data, entry)){
								for (Iterator<RoutingDto> iter = data.listIterator(); iter.hasNext(); ) {
									RoutingDto a = iter.next();
									if(entry.getContext() != null && !entry.getContext().isEmpty()){
										if(entry.getType().equals(a.getType()) && entry.getValue().equals(a.getValue()) && entry.getContext().equals(a.getContext())){
									        iter.remove();
									        break;
									    }
									}else{
										if(entry.getType().equals(a.getType()) && entry.getValue().equals(a.getValue())){
											iter.remove();
									        break;
										}
									}								    
								}
								daoRedis.hmsetR(redisKey, data, "RoutingData");
								data.clear();
								data = this.getAllRoutingFromRedis(redisKey);
								if(!AdminCacheUtil.existRoutingRedis(data, entry)){
									response = new ApiError(HttpStatus.OK,"Delete Routing Redis");
									redisPublisher.publish(mesgChannel);
								}
							}else{
								response = new ApiError(HttpStatus.NOT_FOUND, "Dont Exists Routing data on Redis");
							}
						}else{
							response = new ApiError(HttpStatus.NOT_FOUND, "Routing is null on Redis");
						}
					}else{
						response = new ApiError(HttpStatus.NOT_FOUND, "Dont Exists Router key on Redis");
					}
				}
			}else{
				response = new ApiError(HttpStatus.BAD_REQUEST, "Missing Values", entry.toString());
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED, "Exception Routing", e.getMessage());
		}
		return response;
	}
	
	/**
	 * Get routing on database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public List<RoutingDto> getAllRoutingFromRedis(String key){
		List<RoutingDto> aux = null;
		try{
			aux = daoRedis.hgetR(key, "RoutingData");
			if(aux != null){
				return aux;
			}else{
				return new ArrayList<RoutingDto>();
			}			
		}catch(Exception e){
			return new ArrayList<RoutingDto>();
		}
	}
	
	public ApiError init(){
		ApiError response = null;
		try{
			if(!util.exists(redisKey)){
				RoutingDto aux = new RoutingDto();
				List<RoutingDto> data = new ArrayList<RoutingDto>();
				data.add(aux);
				daoRedis.hmsetR(redisKey, data, "RoutingData");
				response = new ApiError(HttpStatus.OK,"Routing Key created on Redis");
			}else{
				response = new ApiError(HttpStatus.CREATED, "Exists Router key on Redis");
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED, "Exception Routing", e.getMessage());
		}
		return response;
	}
	
	public ApiError refresh(){
		ApiError response = null;
		try{
			if(util.exists(redisKey)){
				List<Routing> data = dao.searchRouting();
				List<RoutingDto> dto = AdminCacheUtil.convertRouterToRoutingDto(data);
				if(data != null){
					daoRedis.hmsetR(redisKey, dto, "RoutingData");
					response = new ApiError(HttpStatus.OK,"Routing transfered on Redis");
					redisPublisher.publish(mesgChannel);
				}else{
					response = new ApiError(HttpStatus.NOT_FOUND, "Routing is empty on toolbox");
				}
			}else{
				response = new ApiError(HttpStatus.NOT_FOUND, "Dont Exists Router key on Redis");
			}
		}catch(Exception e){
			response = new ApiError(HttpStatus.EXPECTATION_FAILED, "Exception Routing", e.getMessage());
		}
		return response;
	}
}