package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1>
 * <h1>entity: ResourceType</h1>
 *  
 * @description
 * This entity models the types of resources available to an application.
 * its primary key is a String type id.
 * 
 * ResourceType has <b>@OneToMany</b> relationship with Resource
 *  
 **/
@Entity
public class ResourceType  extends StringIdBaseEntity {

	private static final long serialVersionUID = 1499242474304370250L;

	public ResourceType() {
	}	
	
	public ResourceType(String id) {
		super(id);
	}

	public ResourceType(String id, String name, String description,Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResourceType [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}	
	
}
