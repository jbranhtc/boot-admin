package com.htc.ea.admincache.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 * <h1>Project: Model</h1> <h1>entity: Permission</h1>
 * 
 * @description This entity models the permissions. its primary key is a String
 *              type id.
 * 
 *              Permission has <b>@ManyToOne</b> relationship with Privilege
 *              Permission has <b>@ManyToOne</b> relationship with Credential
 *              Permission has <b>@ManyToOne</b> relationship with Resource
 * 
 **/
@Entity
public class Permission extends GenericBaseEntity<Long> {

	private static final long serialVersionUID = -1202706416445708087L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PermissionGenerator")
	@SequenceGenerator(name = "PermissionGenerator", sequenceName = "PermissionSeq")
	private Long id;

	@JoinColumn(name = "authorityId")
	@ManyToOne
	private Authority authority;

	@JoinColumn(name = "privilegeId")
	@ManyToOne
	private Privilege privilege;

	@JoinColumn(name = "resourceId")
	@ManyToOne
	private Resource resource;

	public Permission() {
	}

	public Permission(Long id) {
		super();
		this.id = id;
	}

	public Permission(Boolean enabled) {
		super(enabled);
	}

	public Resource getResource() {
		return resource;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Privilege getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Permission [id=").append(id).append(", authority=")
				.append(authority).append(", privilege=").append(privilege)
				.append(", resource=").append(resource).append(", enabled=")
				.append(enabled).append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}	
	
}
