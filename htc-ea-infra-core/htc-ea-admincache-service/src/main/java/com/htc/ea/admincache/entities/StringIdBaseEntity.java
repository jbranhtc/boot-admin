package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * <h1>Project: Model</h1> <h1>entity: StringIdBaseEntity</h1>
 * 
 * @description This class serves as a model for entities with String key
 *              type.
 * 
 *              StringIdBaseEntity extends GenericBaseEntity<String>
 * 
 **/

@MappedSuperclass
public class StringIdBaseEntity extends NameableEntity<String> {

	private static final long serialVersionUID = -7359111743597229032L;

	@Id
	@Column(nullable = false, updatable = false, length = 50)
	protected String id;

	public StringIdBaseEntity() {
		super();
	}

	public StringIdBaseEntity(String id) {
		super();
		this.id = id;
	}

	public StringIdBaseEntity(String id, String name, String description,
			Boolean enabled) {
		super(name, description, enabled);
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
