package com.htc.ea.admincache.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.mcs.consumer.ConsumerOperation;
import com.htc.ea.mcs.consumer.ConsumerRestHelper;
import com.htc.ea.mcs.consumer.ServiceException;
import com.htc.ea.mcs.consumer.ServiceRequest;
import com.htc.ea.mcs.consumer.ServiceResponse;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;
import com.htc.ea.util.util.TransactionIdUtil;
import com.tigo.ea.pto.dto.TechnicalProductCatalogDto;

@Service
public class TechnicalProductCatalogService {
	
	@Autowired
	private Environment env;
	@Autowired
	private AdminCacheDaoRedis daoRedis;
	@Autowired
	private AdminCacheUtilRedis util;
	@Autowired
	private ConsumerRestHelper consumerRestHelper;
	@Autowired
    private RedisMessagePublisher redisPublisher;
	private String key = "pto-product-catalog", nameObject = "TechnicalProductCatalogDto";
	
	public AdminCacheResponse refresh(){
		AdminCacheResponse response = new AdminCacheResponse();
		ResponseCache msg = new ResponseCache();
  		try{
		    TechnicalProductCatalogDto pto = testMCSServiceConsumerGet();
		    response.setApp("");

			    if(pto == null){
			    	msg.setCode(1);
			 	    msg.setDescription("Object null");
			    	response.setStatus(HttpStatus.EXPECTATION_FAILED);
			    }else{
			    	if(pto.getCommandDtoList() == null){
			    		msg.setCode(2);
				 	    msg.setDescription("List null");
			    		response.setStatus(HttpStatus.NOT_FOUND);
			    	}else{
			    		if(pto.getCommandDtoList().isEmpty()){
			    			msg.setCode(3);
			    	 	    msg.setDescription("List empty");
			    			response.setStatus(HttpStatus.NO_CONTENT);
			    		}else{
			    			List<TechnicalProductCatalogDto> lst = new ArrayList<TechnicalProductCatalogDto>();
			    			lst.add(pto);
			    			String json = AdminCacheUtil.convertObjectToJson(lst);
			    			if(json != null){
			    				GenericDto gen = new GenericDto();
			    				gen.put("TechnicalProductCatalogDto", json);
				    			daoRedis.hmsetG(key, gen, nameObject);
				    			if(util.exists(key)){
				    				response.setStatus(HttpStatus.OK);
				    			    msg.setCode(0);
				    			    msg.setDescription("Successful on Redis");
				    			    redisPublisher.publish(key + ":" + nameObject);
				    			}else{
				    				msg.setCode(2);
							 	    msg.setDescription("Dont created on Redis");
						    		response.setStatus(HttpStatus.NOT_FOUND);
				    			}
			    			}
			    		}
			    	}
			    }
  		}catch(Exception e){
  			msg.setCode(4);
	 	    msg.setDescription(e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
			e.printStackTrace();
	    }
	    response.getResponse().add(msg);
		return response;
	}
	
	public TechnicalProductCatalogDto testMCSServiceConsumerGet() {
		TransactionIdUtil.begin();
		//TimeChronometer time = new TimeChronometer("testMCSServiceConsumerGet");
		String resource = env.getProperty("admincache.uri.technical.product");
		String data = null;
		ServiceResponse<TechnicalProductCatalogDto> serviceResponse = new ServiceResponse<>(); 
		ServiceRequest<String> serviceRequest = consumerRestHelper.createServiceRequest(resource, ConsumerOperation.GET);
		serviceRequest.setData(data);
		serviceRequest.setEndUser(key);
		serviceRequest.getParameters().put(com.htc.ea.util.util.Constants.X_USER_AGENT_KEY, "PTO Admin Cache");
		TechnicalProductCatalogDto responseData = null;
		boolean isError = false;
		serviceRequest.setCategory("consumer");
		try {
			serviceResponse = consumerRestHelper.executeService(serviceRequest, TechnicalProductCatalogDto.class);
			responseData = serviceResponse.getData();
		} catch (ServiceException e) {
			isError = true;
			System.out.println("Code from ServiceException: " + e.getCode());
			System.out.println("Description from ServiceException: " + e.getDescription());
			if (e.getResponse() != null) {
				System.out.println("ServiceException: " + e.getResponse().toString());
				System.out.println("untilLastSequence from provider: " + e.getResponse().getUntilLastSequence() + " untilLasSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());		
			}
		}
		System.out.println("responseData: " + responseData.toString());
		if (!isError) {
			System.out.println("ServiceResponse: " + serviceResponse.toString());
			System.out.println("untilLastSequence from provider: " + serviceResponse.getUntilLastSequence() + " untilLastSequence from TransactionIdUtil: " + TransactionIdUtil.getUntilLastSequence());	
		}
		TransactionIdUtil.end();
		return responseData;
	}
}