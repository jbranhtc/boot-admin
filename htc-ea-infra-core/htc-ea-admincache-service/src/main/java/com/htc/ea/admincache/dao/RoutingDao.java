package com.htc.ea.admincache.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.Routing;

@Repository
public class RoutingDao {
	
	private static Logger logger = LoggerFactory.getLogger(RoutingDao.class);

	@Autowired
	private EntityManager entityManager;

	public boolean createRouting(Routing entry){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			entry.setEnabled(true);
			entityManager.persist(entry);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	public boolean existsRouting(Routing entry){
		try{
			Routing temp = null;
			Query query = null;
			if(entry.getContext() != null && !entry.getContext().isEmpty()){
				query = entityManager.createQuery("SELECT entry FROM Routing entry WHERE entry.value=:value and entry.type=:type and entry.context=:context", Routing.class)
						.setParameter("value", entry.getValue())
						.setParameter("type", entry.getType())
						.setParameter("context", entry.getContext());
			}else{
				query = entityManager.createQuery("SELECT entry FROM Routing entry WHERE entry.value=:value and entry.type=:type", Routing.class)
						.setParameter("value", entry.getValue())
						.setParameter("type", entry.getType());
			}
			if(!query.getResultList().isEmpty()){
				int size = query.getResultList().size();
				if(size == 1){
					temp = (Routing) query.getSingleResult();
					if(temp.getValue().equals(entry.getValue()) && temp.getType().equals(entry.getType()) && temp.getContext().equals(entry.getContext()))
						return true;
					else
						return false;
				}else{
					if(size > 1){
						return true;
					}else
						return false;
				}
			}else
				return false;
			
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	public Routing searchRouting(Routing entry){
		Routing temp = null;
		try{			
			Query query = entityManager.createQuery("SELECT entry FROM Routing entry WHERE entry.value=:value and entry.type=:type and entry.context=:context", Routing.class)
					.setParameter("value", entry.getValue())
					.setParameter("type", entry.getType())
					.setParameter("context", entry.getContext());
			if(!query.getResultList().isEmpty()){
				int size = query.getResultList().size();
				if(size == 1){
					temp = (Routing) query.getSingleResult();
				}
			}
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
		return temp;
	}
	
	public boolean deleteRouting(Routing entry){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			Routing entryF = this.searchRouting(entry);
			Routing temp = entityManager.find(Routing.class, entryF.getId());
			entityManager.remove(temp);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Routing> searchRouting(){
		List<Routing> temp = null;
		try{			
			Query query = entityManager.createQuery("SELECT entry FROM Routing entry WHERE entry.enabled=1", Routing.class);
			if(!query.getResultList().isEmpty()){
				int size = query.getResultList().size();
				if(size >= 1){
					temp = (List<Routing>) query.getResultList();
				}
			}
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
		return temp;
	}
	
	public void clear(){
		entityManager.clear();
	}
}