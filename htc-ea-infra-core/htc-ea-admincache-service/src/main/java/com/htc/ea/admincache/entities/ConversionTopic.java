package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: ConversionTopic</h1>
 * 
 * @description This entity models ConversionTopic its primary key is a String
 *              type id.
 * 
 *              ConversionTopic has <b>@OneToMany</b> relationship with
 *              ConversionEntry
 * 
 * 
 **/

@Entity
public class ConversionTopic extends StringIdBaseEntity {

	private static final long serialVersionUID = -5517208320555785559L;

	public ConversionTopic() {
	}

	public ConversionTopic(String id) {
		super(id);
	}

	public ConversionTopic(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConversionTopic [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
