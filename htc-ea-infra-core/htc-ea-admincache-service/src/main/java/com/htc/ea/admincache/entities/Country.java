package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: Country</h1>
 * 
 * @description This entity models the catalog countries. its primary key is a
 *              String type id.
 * 
 *              Country has <b>@OneToMany</b> relationship with Application
 * 
 **/

@Entity
public class Country extends StringIdBaseEntity {

	private static final long serialVersionUID = -623549066559745258L;

	public Country() {
	}

	public Country(String id) {
		super(id);
	}

	public Country(String id, String name, String description, Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Country [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
