package com.htc.ea.admincache.model;

import com.htc.ea.admincache.util.AdminCacheUtil;

public class AdminCacheConversionInsert extends AdminCachePojo{
	
	private String topicId;
	private String key;
	private String value;

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean validate(){
		return AdminCacheUtil.stringValidation(this.getApplicationId(), this.getEnvironmentIdp(), this.getVersion(),
				this.getKey(), this.getValue(), this.getTopicId());
	}
}