package com.htc.ea.admincache.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.htc.ea.admincache.util.AdminCacheUtil;

public class AdminCachePojo {

	private String applicationId;
	@JsonIgnore
	private String environmentId;
	private String version;
	private boolean onlyRedis = true;
	@JsonIgnore
	private int platform = 0;
	@JsonIgnore
	private String mapKey = "";
	
	public AdminCachePojo(){
		
	}
	
	public AdminCachePojo(int platform){
		this.platform = platform;
	}
	
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}	

	public String getEnvironmentIdp() {
		return environmentId != null ? environmentId : "";
	}
	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public boolean isOnlyRedis() {
		return onlyRedis;
	}
	public void setOnlyRedis(boolean onlyRedis) {
		this.onlyRedis = onlyRedis;
	}
	
	public String getKeyRedis(){
		if(this.platform ==  0){
			return this.applicationId + "-" + this.environmentId + "-" + this.version;
		}else{
			return this.applicationId;
		}
	}
	
	public boolean validate(){
		return AdminCacheUtil.stringValidation(this.getApplicationId(), this.getEnvironmentIdp(), this.getVersion());
	}

	public void setPlatform(int platform) {
		this.platform = platform;
	}

	public String getMapKeyR() {
		return mapKey;
	}

	public void setMapKey(String mapKey) {
		this.mapKey = mapKey;
	}
}