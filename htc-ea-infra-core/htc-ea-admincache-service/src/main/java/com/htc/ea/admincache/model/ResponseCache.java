package com.htc.ea.admincache.model;

public class ResponseCache {
	
	private Integer code;
	private String description;
	private String key = "none";
	
	public ResponseCache(){
		this.code = -1;
		this.description = "Undefined error";
	}
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}