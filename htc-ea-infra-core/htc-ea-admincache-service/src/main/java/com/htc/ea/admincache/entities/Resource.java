package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * <h1>Project: Model</h1> <h1>entity: Resource</h1>
 * 
 * @description This entity models the resources available to an application.
 *              its primary key is a Long type id.
 * 
 *              Resource has <b>@ManyToOne</b> relationship with ResourceType
 * 
 **/
@Entity
@Table(name = "ResourceEntity")
public class Resource extends NameableEntity<Long> {

	private static final long serialVersionUID = -836177377860310767L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ResourceGenerator")
	@SequenceGenerator(name = "ResourceGenerator", sequenceName = "ResourceSeq")
	private Long id;

	@JoinColumn(name = "applicationId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Application application;

	@JoinColumn(name = "resourceTypeId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private ResourceType resourceType;

	@Column(nullable = false, length = 100)
	private String value;

	@JoinColumn(name = "parentId")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Resource parent;

	@JoinColumn(name = "resourceId")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Permission> Permissions = new ArrayList<Permission>();

	public Resource() {
	}

	public Resource(Long id) {
		super();
		this.id = id;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	public Long getId() {
		return id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Resource getParent() {
		return parent;
	}

	public void setParent(Resource parent) {
		this.parent = parent;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Resource(String name, String description, Boolean enabled) {
		super();
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}

	public List<Permission> getPermissions() {
		return Permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		Permissions = permissions;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Resource [id=").append(id).append(", application=")
				.append(application).append(", resourceType=")
				.append(resourceType).append(", value=").append(value)
				.append(", parent=").append(parent).append(", Permissions=")
				.append(Permissions).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
