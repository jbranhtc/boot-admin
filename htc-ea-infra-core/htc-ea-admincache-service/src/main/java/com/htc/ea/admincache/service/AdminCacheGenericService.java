package com.htc.ea.admincache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.htc.ea.admincache.dao.AdminCacheDaoOracle;
import com.htc.ea.admincache.dao.AdminCacheDaoRedis;
import com.htc.ea.admincache.dao.ConfigEntryDAO;
import com.htc.ea.admincache.dao.GetConfigurationDAO;
import com.htc.ea.admincache.model.AdminCacheDelete;
import com.htc.ea.admincache.model.AdminCacheInsert;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.model.AdminCacheUpdate;
import com.htc.ea.admincache.model.ResponseCache;
import com.htc.ea.admincache.util.AdminCacheUtil;
import com.htc.ea.admincache.util.AdminCacheUtilRedis;
import com.htc.ea.util.dto.ConfigEntryData;
import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.redis.listener.RedisMessagePublisher;

@Service
public class AdminCacheGenericService {
	
	@Autowired
	private AdminCacheDaoOracle adminCacheDaoOracle;
	@Autowired
	private AdminCacheDaoRedis adminCacheDaoRedis;
	@Autowired
	private AdminCacheUtilRedis adminCacheUtilRedis;
	@Autowired
    private RedisMessagePublisher servPublisher;
	
	private AdminCacheResponse response = null;
	@Autowired
	private ConfigEntryDAO configEntryDAO;
	@Autowired
	private GetConfigurationDAO getConfigurationDAO;
	
	/**
	 * Begin object of app on Redis
	 * @param init
	 * @return
	 */
	public AdminCacheResponse setInitApp(AdminCachePojo init){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!adminCacheUtilRedis.exists(init.getKeyRedis())){				
				GenericDto aux = new GenericDto();
				aux.setProperty(init.getMapKeyR(), "");
				adminCacheDaoRedis.hmsetG(init.getKeyRedis(), aux, init.getMapKeyR());
				resp.setCode(0);
				response.setStatus(HttpStatus.OK);
				resp.setDescription("Successful on Redis");
			}else{
				resp.setCode(1);
				resp.setDescription("Fail: App exists on Redis");
				response.setStatus(HttpStatus.CONFLICT);
			}
		}catch(Exception e){
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * Copy config entry between oracle database and database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public AdminCacheResponse transferConfigEntriesOracleToRedis(AdminCachePojo refresh){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		try{
			if(!refresh.validate()){
				// validamos si al app exista en la toolbox
				if(getConfigurationDAO.existsConfiguration(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion())){
					// obtenemos el objeto del web service de la toolbox
					ConfigurationData as = adminCacheDaoOracle.getConfigurationOracle(refresh.getApplicationId(), refresh.getEnvironmentIdp(), refresh.getVersion());
					if(as != null){
						// guardamos el objeto en Redis
						adminCacheDaoRedis.hmset(refresh.getKeyRedis(), as, refresh.getMapKeyR());
						// validamos si el objeto insertado exista en Redis
						if(adminCacheUtilRedis.exists(refresh.getKeyRedis())){
							// enviamos el mensaje para que la aplicacion se actualice
							servPublisher.publish(refresh.getKeyRedis() + ":" + refresh.getMapKeyR());
							resp.setCode(0);
							response.setStatus(HttpStatus.OK);
							resp.setDescription("Successful on Redis");
						}else{
							resp.setCode(1);
							resp.setDescription("Fail: Not created on Redis");
						}
					}else{
						resp.setCode(2);
						resp.setDescription("Fail: Error get config entry from ToolBox");
					}
				}else{
					resp.setCode(3);
					resp.setDescription("Fail: App not exists on Toolbox");
					response.setStatus(HttpStatus.NOT_FOUND);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		response.getResponse().add(resp);
		return response;
	}
	
	/**
	 * Insert new property on database Redis
	 * @param String - Name of application Id
	 * @param String - Key
	 * @param String - Value
	 * @return 
	 */
	public AdminCacheResponse setPropertyOnRedis(AdminCacheInsert insert){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();;
		boolean toolbox = true;
		try{
			// validamos los parametros de entrada
			if(!insert.validate()){
				
					// Process to Toolbox
					if(!insert.isOnlyRedis()){
						resp = new ResponseCache();
						// validamos si al app exista en la toolbox
						if(getConfigurationDAO.existsConfiguration(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion())){
							// validamos si no existe la configEntry
							boolean exist = configEntryDAO.existConfigEntry(insert.getApplicationId(), insert.getEnvironmentIdp(), insert.getVersion(), insert.getConfigEntry().getKey());
							if(!exist){
								// guardamos la configEntry
								boolean save = configEntryDAO.createConfigEntry(insert);
								if(save){
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Toolbox");
								}else{
									toolbox = false;
									resp.setCode(6);
									resp.setDescription("Fail: Property not added on Toolbox");
								}
							}else{
								toolbox = false;
								resp.setCode(7);
								resp.setDescription("Fail: Property exists on Toolbox");
								response.setStatus(HttpStatus.CONFLICT);
							}						
						}else{
							toolbox = false;
							resp.setCode(3);
							resp.setDescription("Fail: App not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
						response.getResponse().add(resp);
					}
					// Process to Redis
					if(toolbox){
						resp = new ResponseCache();
						if(adminCacheUtilRedis.exists(insert.getKeyRedis())){
							// obtenemos el objeto en Redis
							ConfigurationData data = this.getAllPropertiesFromRedis(insert);
							if(data != null){
								// validamos si la nueva configEntry no exista en Redis
								if(!data.getConfigEntries().containsKey(insert.getConfigEntry().getKey())){
									// insertamos la nueva configEntry en el objeto
									data.getConfigEntries().put(insert.getConfigEntry().getKey(), insert.getConfigEntry());
									// actualizamos el objeto entero con la nueva configEntry en redis
									adminCacheDaoRedis.hmset(insert.getKeyRedis(), data, insert.getMapKeyR());
									if(this.getAllPropertiesFromRedis(insert).getConfigEntries().containsKey(insert.getConfigEntry().getKey())){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(insert.getKeyRedis() + ":" + insert.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Property not added on Redis");
									}
								}else{
									resp.setCode(7);
									resp.setDescription("Fail: Property exists on Redis");
									response.setStatus(HttpStatus.CONFLICT);
								}
							}else{
								resp.setCode(8);
								resp.setDescription("Fail: ConfigurationData is null on Redis");
							}
						}else{
							resp.setCode(3);
							resp.setDescription("Fail: Aplication Id not exists on Redis");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
						response.getResponse().add(resp);
					}
				
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Update property on database Redis
	 * @param String - Name of application Id
	 * @param String - Key
	 * @param String - Value
	 * @return 
	 */
	public AdminCacheResponse updatePropertyOnRedis(AdminCacheUpdate update){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		try{
			if(!update.validate()){
				
				// Process to Toolbox
				if(!update.isOnlyRedis()){
					resp = new ResponseCache();
					// validamos si al app exista en la toolbox
					if(getConfigurationDAO.existsConfiguration(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion())){
						// validamos si existe la configEntry
						boolean exist = configEntryDAO.existConfigEntry(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), update.getKey());
						if(exist){
							// validamos si la configEntry esta habilitada
							boolean enable = configEntryDAO.enabledConfigEntry(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), update.getKey()); 
							if(enable){
								// validamos si es single value o no
								boolean single = configEntryDAO.isSingleValueConfig(update);
								if((single && update.getValue() != null && !update.getValue().isEmpty()) || (single == false && update.getConfigEntryValues() != null && AdminCacheUtil.isEmptySet(update.getConfigEntryValues()))){
									// actualizamos la configEntry
									boolean updated = configEntryDAO.updateConfigEntry(update);
									if(updated){
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Toolbox");
									}else{
										toolbox = false;
										resp.setCode(6);
										resp.setDescription("Fail: Property not updated on Toolbox");
									}
								}else{
									toolbox = false;
									if(single){
										resp.setCode(10);
										resp.setDescription("Fail: Missing value for Config Entry for Toolbox");
									}else{
										resp.setCode(11);
										resp.setDescription("Fail: Missing values for Config Entry Values for Toolbox");
									}
								}
							}else{
								toolbox = false;
								resp.setCode(9);
								resp.setDescription("Fail: Property disable on Toolbox");
							}
						}else{
							toolbox = false;
							resp.setCode(7);
							resp.setDescription("Fail: Property not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}else{
						toolbox = false;
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Toolbox");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
				// Process to Redis
				if(toolbox){
					resp = new ResponseCache();
					if(adminCacheUtilRedis.exists(update.getKeyRedis())){
						ConfigurationData data = this.getAllPropertiesFromRedis(update);
						if(data != null){
							if(data.getConfigEntries().containsKey(update.getKey())){
								ConfigEntryData temp = data.getConfigEntries().get(update.getKey());
								if((temp.isSingleValue() && update.getValue() != null && !update.getValue().isEmpty()) || (temp.isSingleValue() == false && update.getConfigEntryValues() != null && AdminCacheUtil.isEmptySet(update.getConfigEntryValues()))){
									if(temp.isSingleValue()){
										temp.setValue(update.getValue());
									}else{
										for(String aux : update.getConfigEntryValues()){
											temp.getValues().add(aux);
										}
										if(update.getValue() != null && !update.getValue().isEmpty()) temp.setValue(update.getValue());
									}
									data.getConfigEntries().put(temp.getKey(), temp);
									adminCacheDaoRedis.hmset(update.getKeyRedis(), data, update.getMapKeyR());
									if(this.getAllPropertiesFromRedis(update).getConfigEntries().get(update.getKey()).getValue().contains(update.getValue()) ||
											AdminCacheUtil.findElementSet(this.getAllPropertiesFromRedis(update).getConfigEntries().get(update.getKey()).getValues(), update.getConfigEntryValues())){
										// enviamos el mensaje para que la aplicacion se actualice
										servPublisher.publish(update.getKeyRedis() + ":" + update.getMapKeyR());
										resp.setCode(0);
										response.setStatus(HttpStatus.OK);
										resp.setDescription("Successful on Redis");
									}else{
										resp.setCode(6);
										resp.setDescription("Fail: Property not updated on Redis");
									}
								}else{
									if(temp.isSingleValue()){
										resp.setCode(10);
										resp.setDescription("Fail: Missing value for Config Entry for Redis");
									}else{
										resp.setCode(11);
										resp.setDescription("Fail: Missing values for Config Entry Values for Redis");
									}
								}
							}else{
								resp.setCode(7);
								resp.setDescription("Fail: Property not exists on Redis");
								response.setStatus(HttpStatus.NOT_FOUND);
							}
						}else{
							resp.setCode(8);
							resp.setDescription("Fail: ConfigurationData is null on Redis");
						}
					}else{
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Redis");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	/**
	 * Get properties on database Redis
	 * @param String - Name of application Id
	 * @return 
	 */
	public ConfigurationData getAllPropertiesFromRedis(AdminCachePojo config){
		ConfigurationData aux = null;
		try{
			if(!config.validate()){
				aux = adminCacheDaoRedis.hget(config.getKeyRedis(), config.getMapKeyR());
				if(aux != null){
					return aux;
				}else{
					return new ConfigurationData();
				}				
			}else{
				return new ConfigurationData();
			}
		}catch(Exception e){
			e.printStackTrace();
			return new ConfigurationData();
		}
	}
	
	/**
	 * Delete property in object on database Redis
	 * @param String - Name of application Id
	 * @param String - key
	 * @return 
	 */
	public AdminCacheResponse deleteProperty(AdminCacheDelete del){
		response = new AdminCacheResponse();
		ResponseCache resp = new ResponseCache();
		boolean toolbox = true;
		try{
			if(!del.validate()){
				
				// Process to Toolbox
				if(!del.isOnlyRedis()){
					resp = new ResponseCache();
					// validamos si al app exista en la toolbox
					if(getConfigurationDAO.existsConfiguration(del.getApplicationId(), del.getEnvironmentIdp(), del.getVersion())){
						// validamos si existe la configEntry
						boolean exist = configEntryDAO.existConfigEntry(del.getApplicationId(), del.getEnvironmentIdp(), del.getVersion(), del.getKey());
						if(exist){
							// borramos la configEntry
							boolean delete = configEntryDAO.deleteConfigEntry(del);
							if(delete){
								resp.setCode(0);
								response.setStatus(HttpStatus.OK);
								resp.setDescription("Successful on Toolbox");
							}else{
								toolbox = false;
								resp.setCode(6);
								resp.setDescription("Fail: Property not deleted on Toolbox");
							}						
						}else{
							toolbox = false;
							resp.setCode(7);
							resp.setDescription("Fail: Property not exists on Toolbox");
							response.setStatus(HttpStatus.NOT_FOUND);
						}
					}else{
						toolbox = false;
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Toolbox");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
				// Process to Redis
				if(toolbox){
					resp = new ResponseCache();
					if(adminCacheUtilRedis.exists(del.getKeyRedis())){
						ConfigurationData data = this.getAllPropertiesFromRedis(del);
						if(data != null){
							if(data.getConfigEntries().containsKey(del.getKey())){
								data.getConfigEntries().remove(del.getKey());
								adminCacheDaoRedis.hmset(del.getKeyRedis(), data, del.getMapKeyR());
								if(this.getAllPropertiesFromRedis(del).getConfigEntries().get(del.getKey()) == null){
									// enviamos el mensaje para que la aplicacion se actualice
									servPublisher.publish(del.getKeyRedis() + ":" + del.getMapKeyR());
									resp.setCode(0);
									response.setStatus(HttpStatus.OK);
									resp.setDescription("Successful on Redis");
								}else{
									resp.setCode(6);
									resp.setDescription("Fail: Property not deleted on Redis");
								}
							}else{
								resp.setCode(7);
								resp.setDescription("Fail: Property not exists on Redis");
								response.setStatus(HttpStatus.NOT_FOUND);
							}
						}else{
							resp.setCode(8);
							resp.setDescription("Fail: ConfigurationData is null on Redis");
						}
					}else{
						resp.setCode(3);
						resp.setDescription("Fail: Aplication Id not exists on Redis");
						response.setStatus(HttpStatus.NOT_FOUND);
					}
					response.getResponse().add(resp);
				}
			}else{
				resp.setCode(4);
				resp.setDescription("Fail: Params not must be null or empty");
				response.getResponse().add(resp);
			}
		}catch(Exception e){
			//e.printStackTrace();
			resp.setCode(5);
			resp.setDescription("Fail: Exception - " + e.getMessage());
			response.getResponse().add(resp);
			response.setStatus(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
}