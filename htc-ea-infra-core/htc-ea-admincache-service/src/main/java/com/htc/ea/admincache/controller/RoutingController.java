package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.service.RoutingService;
import com.htc.ea.admincache.util.ApiError;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.routing-entry:false}")
@RequestMapping(value = "/routings")
@Api(value="/routings", tags="Operaciones para Router - Toolbox y Redis")
public class RoutingController {
	
	@Autowired
	RoutingService routingService;
	
	@Autowired
	Environment env;
	
	@ApiOperation(value = "Verificar numero en router", response = ApiError.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "true"),
            @ApiResponse(code = 400, message = "Missing Values"),
            @ApiResponse(code = 404, message = "false"),
            @ApiResponse(code = 417, message = "Exception Routing")
    })
	@GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> isIncluded(@ApiParam(value = "Tipo de routing", name = "type", required = true) @RequestParam(value="type") String type, 
											 @ApiParam(value = "Valor del routing", name = "value", required = true) @RequestParam(value="value") String value, 
											 @ApiParam(value = "Contexto del routing", name = "context", required = false) @RequestParam(value="context", required = false) String context, 
											 @ApiParam(value = "Consultar en Redis o Toolbox", name = "onlyRedis", required = true, defaultValue = "true") @RequestParam(value="onlyRedis", required = true, defaultValue = "true") boolean onlyRedis) {
		ApiError response = null;
		if(context != null && !context.isEmpty()){
			response = routingService.isIncludedByContext(type, value, context, onlyRedis);
		}else{
			response = routingService.isIncluded(type, value, onlyRedis);
		}
		return new ResponseEntity<Object>(response, new HttpHeaders(), response.getStatus());
	}

	@ApiOperation(value = "Agregar numero en router", response = ApiError.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Add Routing Toolbox"),
            @ApiResponse(code = 400, message = "Missing Values"),
            @ApiResponse(code = 201, message = "Exists Routing Toolbox/Exists Routing data on Redis"),
            @ApiResponse(code = 404, message = "Routing is null on Redis/Dont Exists Router key on Redis"),
            @ApiResponse(code = 417, message = "Exception Routing")
    })
	@PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> add(@ApiParam(value = "Tipo de routing", name = "type", required = true) @RequestParam(value="type") String type, 
									  @ApiParam(value = "Valor del routing", name = "value", required = true) @RequestParam(value="value") String value, 
									  @ApiParam(value = "Contexto del routing", name = "context", required = false, defaultValue = "") @RequestParam(value="context", required = false, defaultValue = "") String context, 
									  @ApiParam(value = "Consultar en Redis o Toolbox", name = "onlyRedis", required = true, defaultValue = "true") @RequestParam(value="onlyRedis", required = true, defaultValue = "true") boolean onlyRedis) {
		ApiError response = routingService.add(type, value, context, onlyRedis);
		return new ResponseEntity<Object>(response, new HttpHeaders(), response.getStatus());
	}
	
	@ApiOperation(value = "Borrar numero en router", response = ApiError.class)
    @ApiResponses(value = {
    		@ApiResponse(code = 200, message = "Delete Routing Toolbox"),
            @ApiResponse(code = 400, message = "Missing Values"),
            @ApiResponse(code = 404, message = "Dont Exists Routing Toolbox/Dont Exists Routing data on Redis/Routing is null on Redis/Dont Exists Router key on Redis"),
            @ApiResponse(code = 417, message = "Exception Routing")
    })
	@DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> delete(@ApiParam(value = "Tipo de routing", name = "type", required = true) @RequestParam(value="type") String type, 
			 							 @ApiParam(value = "Valor del routing", name = "value", required = true) @RequestParam(value="value") String value, 
			 							 @ApiParam(value = "Contexto del routing", name = "context", required = false, defaultValue = "") @RequestParam(value="context", required = false, defaultValue = "") String context, 
			 							 @ApiParam(value = "Consultar en Redis o Toolbox", name = "onlyRedis", required = true, defaultValue = "true") @RequestParam(value="onlyRedis", required = true, defaultValue = "true") boolean onlyRedis) {
		ApiError response = routingService.delete(type, value, context, onlyRedis);
		return new ResponseEntity<Object>(response, new HttpHeaders(), response.getStatus());
	}
	
	@ApiOperation(value = "Crear objeto inicial en Redis", response = ApiError.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Routing Key created on Redis"),
            @ApiResponse(code = 201, message = "Exists Router key on Redis"),
            @ApiResponse(code = 417, message = "Exception Routing")
    })
	@PostMapping(value = "/redis/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> init() {
		ApiError response = null;
		response = routingService.init();
		return new ResponseEntity<Object>(response, new HttpHeaders(), response.getStatus());
	}
	
	@ApiOperation(value = "Refrescar routing desde la toolbox a redis", response = ApiError.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Routing transfered on Redis"),
            @ApiResponse(code = 404, message = "Routing is empty on toolbox/Dont Exists Router key on Redis"),
            @ApiResponse(code = 417, message = "Exception Routing")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh() {
		ApiError response = null;
		response = routingService.refresh();
		return new ResponseEntity<Object>(response, new HttpHeaders(), response.getStatus());
	}
}