package com.htc.ea.admincache.interfaces;


public interface AdminCacheInterfaceDaoRedis<T,V,A,R,E> {
	
	void hmset(String key, T obj, String nameObject);
 
    T hget(String key, String nameObject);
    
    void hmsetC(String key, V obj, String nameObject);
    
    V hgetC(String key, String nameObject);    
    
    void hmsetG(String key, A obj, String nameObject);
    
    A hgetG(String key, String nameObject);
    
    void hmsetR(String key, R obj, String nameObject);
    
    R hgetR(String key, String nameObject);
    
    void hmsetE(String key, E obj, String nameObject);
    
    E hgetE(String key, String nameObject);
}