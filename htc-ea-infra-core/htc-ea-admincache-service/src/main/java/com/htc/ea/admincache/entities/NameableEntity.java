package com.htc.ea.admincache.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * <h1>Project: Model</h1> <h1>entity: GenericBaseEntity<T></h1>
 * 
 * @description This abstract class serves as a model for entities with key
 *              type Long. its primary key is a Long type id.
 * 
 *              GenericBaseEntity<T>: abstract class
 * 
 **/
@MappedSuperclass
public abstract class NameableEntity<T> extends GenericBaseEntity<T> implements Serializable {

	private static final long serialVersionUID = -3159355592167378050L;

	@Column(nullable = false, length = 100)
	protected String name;

	@Column(nullable = false, length = 250)
	protected String description;

	public NameableEntity() {
	}

	public NameableEntity(String name, String description, Boolean enabled) {
		super();
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
