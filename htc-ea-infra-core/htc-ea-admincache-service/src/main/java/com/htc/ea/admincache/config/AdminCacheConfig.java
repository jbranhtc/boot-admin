package com.htc.ea.admincache.config;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.MvcEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.google.common.base.Predicates;
import com.htc.ea.mcs.consumer.config.ServiceConsumerRestConfig;
import com.htc.ea.util.configuration.UtilAppConfig;


@Configuration
@ComponentScan({"com.htc.ea.admincache"})
@Import({UtilAppConfig.class, ServiceConsumerRestConfig.class})
@EnableSwagger2
public class AdminCacheConfig {

	@Autowired
	Environment env;
	
	@Autowired
	@Bean
	public Docket swaggerSpringMvcPlugin(final EndpointHandlerMapping actuatorEndpointHandlerMapping) {
	    ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2)
	            .useDefaultResponseMessages(false)
	            .apiInfo(apiInfo())
	            .select()
	            .apis(RequestHandlerSelectors.basePackage("com.htc.ea.admincache.controller"));
	    // Ignore the spring-boot-actuator endpoints:
	    Set<MvcEndpoint> endpoints = actuatorEndpointHandlerMapping.getEndpoints();
	    endpoints.forEach(endpoint -> {
	        String path = endpoint.getPath();
	        builder.paths(Predicates.not(PathSelectors.regex(path + ".*")));
	    });
	    return builder.build();
	}
	
	private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
        		.title("ADMIN CACHE API Swagger Documentation")
                .description("Administra configuraciones en Redis y Toolbox")
                .termsOfServiceUrl("http://tigo.bo/terms.html")
                .contact(new Contact("High Tech Consulting El Salvador", "http://tigo.bo.com", ""))
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .version("1.0")
                .build();
    }
}