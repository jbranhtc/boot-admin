package com.htc.ea.admincache.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.ConfigurationEventcategory;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;

@Repository
public class ConfigurationEventCategoryDAO{
	
	@Autowired
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<ConfigurationEventcategory> findConfigurationEventCategory(Long id) {
		List<ConfigurationEventcategory> aux = null;
		Query query = entityManager.createQuery("SELECT event FROM ConfigurationEventcategory event WHERE event.configurationid=:configurationId")
		.setParameter("configurationId", id);
		if(!query.getResultList().isEmpty()){
			aux = (List<ConfigurationEventcategory>) query.getResultList();
		}
		return aux;
	}
	
	public List<ConfigurationEventCategoryData> getConfigurationEventCategory(Long id) {
		List<ConfigurationEventCategoryData> event = null;
		ConfigurationEventCategoryData temp = null;
		List<ConfigurationEventcategory> aux = this.findConfigurationEventCategory(id);
		if (aux == null) {
			return null;
		}
		event = new ArrayList<ConfigurationEventCategoryData>();
		for(ConfigurationEventcategory conf : aux){
			temp = new ConfigurationEventCategoryData();
			temp.setConfigurationid(conf.getConfigurationid());
			temp.setEnable(conf.getEnabled());
			temp.setEventcategoryid(conf.getEventcategoryid());
			temp.setLevel(conf.getLevel());		
			event.add(temp);
		}
		return event;
	}
	
	public void clear(){
		entityManager.clear();
	}
}