package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * <h1>Project: Model</h1> <h1>entity: Configuration</h1>
 * 
 * @description This entity models the configuration of an application. its
 *              primary key is a Long type id.
 * 
 *              Configuration has <b>@OneToMany</b> relationship with
 *              ConfigEntry Configuration has relationship with herself,
 *              attribute: parent; Configuration has <b>@ManyToOne</b>
 *              relationship with Application Configuration has
 *              <b>@ManyToOne</b> relationship with Environment Configuration
 *              has <b>@ManyToMany</b> relationship with EventCategory
 *              associative table:Configuration_EventCategory
 * 
 **/

@Entity
@Table(name = "Configuration", uniqueConstraints = @UniqueConstraint(columnNames = { "applicationId",
		"environmentId", "version" }))
public class Configuration extends NameableEntity<Long> {

	private static final long serialVersionUID = -9161189870600224806L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ConfigurationGenerator")
	@SequenceGenerator(name = "ConfigurationGenerator", sequenceName = "ConfigurationSeq")
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "configuration_eventCategory", joinColumns = { @JoinColumn(name = "configurationId") }, inverseJoinColumns = { @JoinColumn(name = "eventCategoryId") }, uniqueConstraints = { @UniqueConstraint(columnNames = {
			"configurationId", "eventCategoryId" }) })
	private List<EventCategory> eventCategories = new ArrayList<EventCategory>();

	@JoinColumn(name = "applicationId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Application application;

	@JoinColumn(name = "environmentId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private Environment environment;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "configuration")
	@MapKeyColumn(name = "key")
	@JsonManagedReference
	private Map<String, ConfigEntry> configEntries = new HashMap<String, ConfigEntry>();

	@JoinColumn(name = "parentId")
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	private Configuration parent;

	@Column(nullable = false, length = 15)
	private String version;

	@Column(nullable = false)
	private Integer eventLevel;

	public Configuration() {
		super();
	}

	public Configuration(Long id) {
		super();
		this.id = id;
	}

	public Configuration(Long id, Application application,
			Environment environment, String version, Date createdDate,
			boolean enabled) {
		super();
		this.id = id;
		this.application = application;
		this.environment = environment;
		this.version = version;
		this.createdDate = createdDate;
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Configuration getParent() {
		return parent;
	}

	public Map<String, ConfigEntry> getConfigEntries() {
		return configEntries;
	}

	public void setConfigEntries(Map<String, ConfigEntry> configEntries) {
		this.configEntries = configEntries;
	}

	public void setParent(Configuration parent) {
		this.parent = parent;
	}

	public List<EventCategory> getEventCategories() {
		return eventCategories;
	}

	public void setEventCategories(List<EventCategory> eventCategories) {
		this.eventCategories = eventCategories;
	}

	public Integer getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(Integer eventLevel) {
		this.eventLevel = eventLevel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Configuration [id=").append(id)
				.append(", eventCategories=").append(eventCategories)
				.append(", application=").append(application)
				.append(", environment=").append(environment)
				.append(", configEntries=").append(configEntries)
				.append(", parent=").append(parent).append(", version=")
				.append(version).append(", eventLevel=").append(eventLevel)
				.append(", name=").append(name).append(", description=")
				.append(description).append(", enabled=").append(enabled)
				.append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
