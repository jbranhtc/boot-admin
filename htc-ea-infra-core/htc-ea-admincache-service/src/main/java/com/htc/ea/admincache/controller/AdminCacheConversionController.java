package com.htc.ea.admincache.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.admincache.model.AdminCachePojo;
import com.htc.ea.admincache.model.AdminCacheResponse;
import com.htc.ea.admincache.service.AdminCacheConversionService;
import com.htc.ea.util.dto.ConversionData;

@RestController
@ConditionalOnExpression(value = "${admincache-modules.conversion-entry:false}")
@RequestMapping(value = "/conversions")
@Api(value="Conversion", tags="Operaciones para Conversiones - Toolbox y Redis")
public class AdminCacheConversionController {

	@Autowired
	AdminCacheConversionService adminCacheConversionService;
	
	@Autowired
	Environment env;
	
	@ApiOperation(value = "Refrescar conversiones de Toolbox a Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Conversiones actualizadas exitosamente"),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> refresh(@RequestBody AdminCachePojo refresh) {
		refresh.setMapKey("ConversionData");
		refresh.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheConversionService.transferConversionOracleToRedis(refresh);
		response.setApp(refresh.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}

	@ApiOperation(value = "Insertar conversion en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PostMapping(value = "/insert", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> setConversion(@RequestBody AdminCacheConversionInsert insert) {
		insert.setMapKey("ConversionData");
		insert.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheConversionService.setConversionOnRedis(insert);
		response.setApp(insert.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
	
	@ApiOperation(value = "Actualizar conversion en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateConversion(@RequestBody AdminCacheConversionInsert update) {
		update.setMapKey("ConversionData");
		update.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheConversionService.updateConversionOnRedis(update);
		response.setApp(update.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}

	@ApiOperation(value = "Obtener conversiones de una aplicacion en Redis", response = ConversionData.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@GetMapping(value = "/{applicationId}/{version}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getConversions(@PathVariable("applicationId") final String applicationId, @PathVariable("version") final Double version) {
		AdminCachePojo config = new AdminCachePojo();
		config.setApplicationId(applicationId);
		config.setVersion(version.toString());
		config.setMapKey("ConversionData");
		config.setEnvironmentId(env.getProperty("app.env"));
		List<ConversionData> response = adminCacheConversionService.getAllConversionFromRedis(config);
		return new ResponseEntity<Object>(response, new HttpHeaders(),HttpStatus.OK);
	}
	
	@ApiOperation(value = "Borrar conversion en Toolbox y/o Redis", response = AdminCacheResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 417, message = "Exception")
    })
	@DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteConversion(@RequestBody AdminCacheConversionInsert del) {
		del.setMapKey("ConversionData");
		del.setEnvironmentId(env.getProperty("app.env"));
		AdminCacheResponse response = adminCacheConversionService.deleteConversion(del);
		response.setApp(del.getApplicationId());
		return new ResponseEntity<Object>(response, new HttpHeaders(),response.getStatus());
	}
}