package com.htc.ea.admincache.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.dto.ConfigurationEventCategoryData;
import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.dto.GenericDto;
import com.htc.ea.util.dto.LoadClasses;
import com.htc.ea.util.dto.RoutingDto;
import com.tigo.ea.inswitch.dto.TransactionsLevelActorDto;

@Component
public class AdminCacheDaoRedis{

	@Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String,Object> redisTemplate;

	public void hmset(String key, ConfigurationData obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}

	public ConfigurationData hget(String key, String nameObject) {
		ConfigurationData temp = new ConfigurationData();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (ConfigurationData) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}

	public void hmsetC(String key, List<ConversionData> obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConversionData> hgetC(String key, String nameObject) {
		List<ConversionData> temp = new ArrayList<ConversionData>();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (List<ConversionData>) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}

	public void hmsetG(String key, GenericDto obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}

	public GenericDto hgetG(String key, String nameObject) {
		GenericDto temp = new GenericDto();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (GenericDto) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}

	public void hmsetR(String key, List<RoutingDto> obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}

	@SuppressWarnings("unchecked")
	public List<RoutingDto> hgetR(String key, String nameObject) {
		List<RoutingDto> temp = new ArrayList<RoutingDto>();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (List<RoutingDto>) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}
	
	public void hmsetE(String key, List<ConfigurationEventCategoryData> obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}

	@SuppressWarnings("unchecked")
	public List<ConfigurationEventCategoryData> hgetE(String key, String nameObject) {
		List<ConfigurationEventCategoryData> temp = new ArrayList<ConfigurationEventCategoryData>();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (List<ConfigurationEventCategoryData>) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}
	
	public void hmsetL(String key, List<LoadClasses> obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<LoadClasses> hgetL(String key, String nameObject) {
		List<LoadClasses> temp = new ArrayList<LoadClasses>();
		if(redisTemplate.getConnectionFactory() != null){
			temp = (List<LoadClasses>) redisTemplate.opsForHash().get(key, nameObject);
		}
		return temp;
	}
	
	public void setTransactionredis(String key, List<TransactionsLevelActorDto> obj, String nameObject) {
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}
}