package com.htc.ea.admincache.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * <h1>Project: Model</h1> <h1>entity: Environment</h1>
 * 
 * @description This entity models the different environments of application
 *              starts. its primary key is a String type id.
 * 
 *              Environment has <b>@ManyToOne</b> relationship with
 *              EnvironmentType
 * 
 **/
@Entity
public class Environment extends StringIdBaseEntity {

	private static final long serialVersionUID = 7848794869036464916L;

	@JoinColumn(name = "environmentTypeId")
	@ManyToOne
	private EnvironmentType environmentType;

	public Environment() {
	}

	public Environment(String id) {
		super(id);
	}

	public Environment(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	public EnvironmentType getEnvironmentType() {
		return environmentType;
	}

	public void setEnvironmentType(EnvironmentType environmentType) {
		this.environmentType = environmentType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Environment [environmentType=").append(environmentType)
				.append(", id=").append(id).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}