package com.htc.ea.admincache.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ROUTING database table.
 * 
 */
@Entity
@NamedQuery(name="Routing.findAll", query="SELECT r FROM Routing r")
public class Routing implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROUTING_ID_GENERATOR", sequenceName="ROUTINGSEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROUTING_ID_GENERATOR")
	private long id;

	private String context;

	private boolean enabled;

	@Column(name="\"TYPE\"")
	private String type;

	@Column(name="\"VALUE\"")
	private String value;

	public Routing() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContext() {
		return this.context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Routing [id=" + id + ", context=" + context + ", enabled="
				+ enabled + ", type=" + type + ", value=" + value + "]";
	}
}