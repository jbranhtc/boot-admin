package com.htc.ea.admincache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Victor Arucha
 *
 */
@SpringBootApplication
public class AdminCache {
	
	private static Logger logger = LoggerFactory.getLogger(AdminCache.class);
	
	public static void main(String[] args) {
        SpringApplication.run(AdminCache.class, args);
        logger.info("htc-ea-admincache-service Started");        
    }
}