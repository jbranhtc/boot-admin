package com.htc.ea.admincache.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * <h1>Project: Model</h1> <h1>entity: User</h1>
 * 
 * @description This entity models the application users. its primary key is a
 *              Long type id.
 * 
 *              User has <b>@ManyToOne</b> relationship with UserType User has
 *              <b>@ManyToMany</b> relationship with UserGroup User has
 *              <b>@ManyToMany</b> relationship with Role
 * 
 **/

@Entity
@Table(name = "UserEntity")
@PrimaryKeyJoinColumn(name = "id")
@DiscriminatorValue("U")
public class User extends Authority {

	private static final long serialVersionUID = 8998065527757919037L;

	@JoinColumn(name = "userTypeId")
	@ManyToOne
	private UserType userType;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "userGroup_user", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = { @JoinColumn(name = "groupId") })
	private List<UserGroup> groups = new ArrayList<UserGroup>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "role_user", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = { @JoinColumn(name = "roleId") })
	private List<Role> roles = new ArrayList<Role>();

	public User() {

	}

	public User(Long id) {
		super();
	}

	public User(String name, String description, Boolean enabled) {
		super();
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public List<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<UserGroup> groups) {
		this.groups = groups;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String getAuthorityId() {
		return "USER_" + id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [userType=").append(userType).append(", groups=")
				.append(groups).append(", roles=").append(roles)
				.append(", id=").append(id).append(", name=").append(name)
				.append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}

}
