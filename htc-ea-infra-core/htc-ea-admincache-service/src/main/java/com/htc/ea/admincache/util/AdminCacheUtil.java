package com.htc.ea.admincache.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.admincache.entities.Loadclassdata;
import com.htc.ea.admincache.entities.Routing;
import com.htc.ea.admincache.model.AdminCacheConversionInsert;
import com.htc.ea.util.dto.ConfigEntryData;
import com.htc.ea.util.dto.ConfigurationData;
import com.htc.ea.util.dto.ConversionData;
import com.htc.ea.util.dto.LoadClasses;
import com.htc.ea.util.dto.RoutingDto;

public class AdminCacheUtil {
	
	private static String[] formatDate = { "yyyy-MM-dd'T'HH:mm:ss", // 0
		"dd/MM/yyyy", // 1
		"dd-MM-yyyy", // 2
		"yyyyMMddHHmmss", // 3
		"yyyy-MM-dd'T'HH:mm:ss.SSS", // 4
		"yyyyMMddHHmmss", // 5
		"yyyy-MM-dd HH:mm:ss"//6
	};

	public static String getFormatDate(java.util.Date date, int formatDate){
		String defaultFormatDate = "dd/MM/yyyy";
		int tam = formatDate > (AdminCacheUtil.formatDate.length - 1) ? 2 : formatDate;
		defaultFormatDate = AdminCacheUtil.formatDate[tam];
		SimpleDateFormat format = new SimpleDateFormat(defaultFormatDate);
		try {
			return format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean stringValidation(String... values){
		  Boolean validation = false;
		  if(values  != null){
			int errors = 0;
			  for (String aux : values) {
				if(aux == null || aux.isEmpty())
					errors++;
			  }			  
			  if (errors > 0) 
				  validation = true;			  
			}else{
				validation = true;
			}		
			return validation;
	}
	
	public static boolean noEntries(String key){
		return key.contains("environmentId") || key.contains("eventLevel") || key.contains("version");
	}
	
	public static boolean isEmptySet(Set<String> s){
		boolean temp = false;
		Iterator<String> iterator = s.iterator();
		while(iterator.hasNext()){
		  String element = (String) iterator.next();
		  if(element != null && !element.isEmpty()) temp = true;
		}
		return temp;
	}
	
	public static boolean findElementSet(Set<String> conjunto, Set<String> muestra){
		boolean temp = false;
		Iterator<String> iterator = muestra.iterator();
		Iterator<String> iterator2 = conjunto.iterator();
		int i = 0;
		while(iterator.hasNext()){
		  String element = (String) iterator.next();
		  if(element != null && !element.isEmpty()) temp = true;
		  while(iterator2.hasNext()){
			  String conj = (String) iterator2.next();
			  if(conj.equals(element)) i++;
		  }
		  iterator2= conjunto.iterator();
		}
		if(i == muestra.size()) temp = true;
		return temp;
	}
	
	public static boolean existConversionRedis(List<ConversionData> data, AdminCacheConversionInsert insert){
		boolean temp = false;
		for(ConversionData aux : data){
			if(aux.getApplicationId() != null && aux.getKey() != null && aux.getTopicId() != null){
				if(aux.getApplicationId().equals(insert.getApplicationId()) && aux.getKey().equals(insert.getKey()) && aux.getTopicId().equals(insert.getTopicId())){
					temp = true;
					break;
				}
			}
		}
		return temp;
	}
	
	public static ConversionData getConversionRedis(List<ConversionData> data, AdminCacheConversionInsert insert){
		ConversionData temp = null;
		for(ConversionData aux : data){
			if(aux.getApplicationId() != null && aux.getKey() != null && aux.getTopicId() != null){
				if(aux.getKey().equals(insert.getKey()) && aux.getTopicId().equals(insert.getTopicId()) && aux.getApplicationId().equals(insert.getApplicationId())){
					temp = aux;
					break;
				}
			}
		}
		return temp;
	}
	
	public static String convertListToJson(List<?> list) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper(); 
		String jsonData = null;
		try {
			jsonData = objectMapper.writeValueAsString(list);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e.getMessage());
		}
	 }
	
	public static String convertObjectToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper(); 
		String jsonData = null;
		try {
			jsonData = objectMapper.writeValueAsString(obj);
			return jsonData;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e.getMessage());
		}
	 }
	
	public static boolean existRoutingRedis(List<RoutingDto> data, Routing entry){
		boolean temp = false;
		for(RoutingDto aux : data){
			if(entry.getContext() != null && !entry.getContext().isEmpty()){
				if(entry.getType() != null && entry.getValue() != null){
					if(entry.getType().equals(aux.getType()) && entry.getValue().equals(aux.getValue()) && entry.getContext().equals(aux.getContext())){
						temp = true;
						break;
					}
				}
			}else{
				if(entry.getType() != null && entry.getValue() != null){
					if(entry.getType().equals(aux.getType()) && entry.getValue().equals(aux.getValue())){
						temp = true;
						break;
					}
				}
			}
		}
		return temp;
	}
	
	public static List<RoutingDto> convertRouterToRoutingDto(List<Routing> data){
		List<RoutingDto> aux = new ArrayList<RoutingDto>();
		RoutingDto d = null;
		for(Routing r : data){
			d = new RoutingDto();
			d.setContext(r.getContext());
			d.setEnabled(r.getEnabled());
			d.setType(r.getType());
			d.setValue(r.getValue());
			d.setId(r.getId());
			aux.add(d);
		}
		return aux;
	}
	
	public static boolean existClassLoad(List<LoadClasses> data, String nameClass){
		for(LoadClasses aux : data){
			if(aux.getClassName().equals(nameClass)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean existConfigEntry(ConfigurationData data, String key){
		for(Map.Entry<String, ConfigEntryData> entry : data.getConfigEntries().entrySet()){
			if(entry.getValue().getKey().equals(key)){
				return true;
			}
		}
		return false;
	}
	
	public static LoadClasses getClassLoad(List<LoadClasses> data, String nameClass){
		for(LoadClasses aux : data){
			if(aux.getClassName().equals(nameClass)){
				return aux;
			}
		}
		return null;
	}
	
	public static List<LoadClasses> convertLoadclassdataToLoadClasses(List<Loadclassdata> data){
		List<LoadClasses> aux = new ArrayList<LoadClasses>();
		LoadClasses d = null;
		for(Loadclassdata r : data){
			d = new LoadClasses();
			d.setClassName(r.getClassname());
			d.setUpdate(r.getUpdate());
			d.setUploadDate(AdminCacheUtil.getFormatDate(r.getUploaddate(), 6));
			aux.add(d);
		}
		return aux;
	}
}