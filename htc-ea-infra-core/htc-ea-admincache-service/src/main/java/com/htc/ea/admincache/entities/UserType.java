package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: UserType</h1>
 * 
 * @description This entity models the user groups. its primary key is a String
 *              type id.
 * 
 *              UserType has <b>@OneToMany</b> relationship with User
 * 
 **/

@Entity
public class UserType extends StringIdBaseEntity {

	private static final long serialVersionUID = 5756424027864479376L;

	public UserType() {
	}

	public UserType(String id) {
		super(id);
	}

	public UserType(String id, String name, String description, Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserType [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
