package com.htc.ea.admincache.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 * <h1>Project: Model</h1> <h1>entity: ServiceGroup</h1>
 * 
 * @description This entity models the groups of services available. its primary
 *              key is a Long type id.
 * 
 *              ServiceGroup has <b>@OneToMany</b> relationship with Service
 *              ServiceGroup has <b>@ManyToOne</b> relationship with Country
 * 
 **/
@Entity
public class ServiceGroup extends NameableEntity<Long> {

	private static final long serialVersionUID = 6121817853310068191L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ServiceGroupGenerator")
	@SequenceGenerator(name = "ServiceGroupGenerator", sequenceName = "ServiceGroupSeq")
	private Long id;

	@JoinColumn(name = "countryId")
	@ManyToOne
	private Country country;

	@JoinColumn(name = "environmentId")
	@ManyToOne
	private Environment environment;

	@JoinColumn(name = "endpointTypeId")
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private EndpointType endpointType;

	@Column(nullable = true, length = 200)
	private String endpointValue;

	public ServiceGroup() {
	}

	public ServiceGroup(Long id) {
		super();
		this.id = id;
	}

	public ServiceGroup(String name, String description, Boolean enabled) {
		super(name, description, enabled);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public EndpointType getEndpointType() {
		return endpointType;
	}

	public void setEndpointType(EndpointType endpointType) {
		this.endpointType = endpointType;
	}

	public String getEndpointValue() {
		return endpointValue;
	}

	public void setEndpointValue(String endpointValue) {
		this.endpointValue = endpointValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ServiceGroup [id=").append(id).append(", country=")
				.append(country).append(", environment=").append(environment)
				.append(", endpointType=").append(endpointType)
				.append(", endpointValue=").append(endpointValue)
				.append(", name=").append(name).append(", description=")
				.append(description).append(", enabled=").append(enabled)
				.append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
