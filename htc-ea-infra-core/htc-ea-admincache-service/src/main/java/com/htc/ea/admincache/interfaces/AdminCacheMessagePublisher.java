package com.htc.ea.admincache.interfaces;

public interface AdminCacheMessagePublisher {

    void publish(final String message);
}