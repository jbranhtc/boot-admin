package com.htc.ea.admincache.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.Application;
import com.htc.ea.admincache.entities.ConfigEntry;
import com.htc.ea.admincache.entities.ConfigEntryValue;
import com.htc.ea.admincache.entities.Configuration;
import com.htc.ea.admincache.entities.EventCategory;
import com.htc.ea.util.dto.ConfigEntryData;
import com.htc.ea.util.dto.ConfigurationData;

@Repository
public class GetConfigurationDAO{
	
	@Autowired
	private EntityManager entityManager;
	
	private static final Pattern VARIABLE_PATTERN = Pattern
			.compile("\\$\\{(.*?)\\}");
	private static final int MAX_LEVEL = 5;

	public Configuration findByAppAndEnvAndVersion(String applicationId, String environmentId, String version) {
		this.clear();
		Configuration aux = null;
		Query query = entityManager.createQuery("SELECT config FROM Configuration config WHERE config.application.id=:applicationId AND config.environment.id=:environmentId AND config.version=:version", Configuration.class)
		.setParameter("applicationId", applicationId)
		.setParameter("environmentId", environmentId)
		.setParameter("version", version);
		if(!query.getResultList().isEmpty()){
			aux = (Configuration) query.getSingleResult();
		}
		return aux;
	}
	
	public boolean existsConfiguration(String applicationId, String environmentId, String version){
		Configuration temp = this.findByAppAndEnvAndVersion(applicationId, environmentId, version);
		if(temp != null){
			if(temp.getEnabled()){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public ConfigurationData getConfigurationData(String applicationId, String environmentId, String version) {
		Configuration configuration = findByAppAndEnvAndVersion(applicationId, environmentId, version);
		ConfigurationData configurationData = new ConfigurationData();
		configurationData.setApplicationId(configuration.getApplication()
				.getId());
		configurationData.setEnvironmentId(configuration.getEnvironment()
				.getId());
		configurationData.setVersion(configuration.getVersion());
		configurationData.setEventLevel(configuration.getEventLevel());
		for (EventCategory categoy : configuration.getEventCategories()) {
			configurationData.getEventCategories().add(categoy.getId());
		}

		Map<String, ConfigEntry> configEntries = new HashMap<String, ConfigEntry>();
		if (configuration.getParent() != null) {
			configEntries.putAll(configuration.getParent().getConfigEntries());
		}
		configEntries.putAll(configuration.getConfigEntries());

		for (ConfigEntry configEntry : configEntries.values()) {
			if(configEntry.getEnabled()){
				ConfigEntryData configEntryData = new ConfigEntryData(
						configEntry.getKey(), resolveVar(0, configEntry.getValue(),
								configEntries));
				configEntryData.setEnabled(configEntry.getEnabled());
				configEntryData
						.setDataType(configEntry.getConfigDataType().getId());
				configEntryData.setSingleValue(configEntry.isSingleValue());
				if (!configEntry.isSingleValue()) {
					Set<String> values = new HashSet<String>();
					for (ConfigEntryValue configEntryValue : configEntry
							.getConfigEntryValues()) {
						values.add(configEntryValue.getValue());
					}
					configEntryData.setValues(values);
				}
				configEntryData.setType(configEntry.getConfigType().getId());
				configurationData.getConfigEntries().put(configEntry.getKey(),
					configEntryData);
			}
		}
		return configurationData;
	}
	
	private String resolveVar(int level, String value,
			Map<String, ConfigEntry> entries) {
		if (level > MAX_LEVEL) {
			return value;
		}
		if (value != null) {
			Matcher matcher = VARIABLE_PATTERN.matcher(value);
			while (matcher.find()) {
				String key = matcher.group(1);
				ConfigEntry entry = entries.get(key);
				if (entry != null) {
					value = value.replace("${" + key + "}",
							resolveVar(level + 1, entry.getValue(), entries));
				}
			}
		}
		return value;
	}
	
	public Application findApp(String applicationId) {
		Application aux = null;
		Query query = entityManager.createQuery("SELECT config FROM Application config WHERE config.id=:applicationId", Application.class)
		.setParameter("applicationId", applicationId);
		if(!query.getResultList().isEmpty()){
			aux = (Application) query.getSingleResult();
		}
		return aux;
	}
	
	public void clear(){
		entityManager.clear();
	}
}