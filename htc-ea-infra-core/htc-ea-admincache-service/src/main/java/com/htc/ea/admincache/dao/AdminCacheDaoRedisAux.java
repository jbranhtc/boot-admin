package com.htc.ea.admincache.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class AdminCacheDaoRedisAux {

	@Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate<String,Object> redisTemplate;
	@Autowired
	JedisConnectionFactory jedisConnectionFactory;
	
	public void changeDB(int dbIndex){
		jedisConnectionFactory.setDatabase(dbIndex);
	}
	
	public void hmset(String key, Object obj, String nameObject) {
		redisTemplate.setConnectionFactory(jedisConnectionFactory);
		if(redisTemplate.getConnectionFactory() != null){
			redisTemplate.opsForHash().put(key, nameObject, obj);
		}
	}
}