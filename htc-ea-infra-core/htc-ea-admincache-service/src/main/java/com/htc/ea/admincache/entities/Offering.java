package com.htc.ea.admincache.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the OFFERING database table.
 * 
 */
@Entity
@NamedQuery(name="Offering.findAll", query="SELECT o FROM Offering o")
public class Offering implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String offeringid;

	private BigDecimal amount;

	private Timestamp created;

	private BigDecimal enabled;

	private BigDecimal mandatory;

	private String name;

	private String offeringcode;

	private BigDecimal paymentmode;

	@Column(name="\"TYPE\"")
	private String type;

	private Timestamp updated;

	//bi-directional many-to-one association to Offering
	@ManyToOne
	@JoinColumn(name="OFFERINGPARENT")
	private Offering offering;

	//bi-directional many-to-one association to Offering
	@OneToMany(mappedBy="offering")
	private List<Offering> offerings;

	public Offering() {
	}

	public String getOfferingid() {
		return this.offeringid;
	}

	public void setOfferingid(String offeringid) {
		this.offeringid = offeringid;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getCreated() {
		return this.created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public BigDecimal getEnabled() {
		return this.enabled;
	}

	public void setEnabled(BigDecimal enabled) {
		this.enabled = enabled;
	}

	public BigDecimal getMandatory() {
		return this.mandatory;
	}

	public void setMandatory(BigDecimal mandatory) {
		this.mandatory = mandatory;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOfferingcode() {
		return this.offeringcode;
	}

	public void setOfferingcode(String offeringcode) {
		this.offeringcode = offeringcode;
	}

	public BigDecimal getPaymentmode() {
		return this.paymentmode;
	}

	public void setPaymentmode(BigDecimal paymentmode) {
		this.paymentmode = paymentmode;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdated() {
		return this.updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Offering getOffering() {
		return this.offering;
	}

	public void setOffering(Offering offering) {
		this.offering = offering;
	}

	public List<Offering> getOfferings() {
		return this.offerings;
	}

	public void setOfferings(List<Offering> offerings) {
		this.offerings = offerings;
	}

	public Offering addOffering(Offering offering) {
		getOfferings().add(offering);
		offering.setOffering(this);

		return offering;
	}

	public Offering removeOffering(Offering offering) {
		getOfferings().remove(offering);
		offering.setOffering(null);

		return offering;
	}

}