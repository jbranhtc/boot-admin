package com.htc.ea.admincache.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.htc.ea.admincache.entities.ConfigDataType;
import com.htc.ea.admincache.entities.ConfigEntry;
import com.htc.ea.admincache.entities.ConfigEntryValue;
import com.htc.ea.admincache.entities.ConfigType;
import com.htc.ea.admincache.entities.Configuration;
import com.htc.ea.admincache.model.AdminCacheDelete;
import com.htc.ea.admincache.model.AdminCacheInsert;
import com.htc.ea.admincache.model.AdminCacheUpdate;

@Repository
public class ConfigEntryDAO{
	
	private static Logger logger = LoggerFactory.getLogger(ConfigEntryDAO.class);
	
	@Autowired
	private EntityManager entityManager;

	public ConfigEntry findByAppAndEnvAndVersionAndKey(String applicationId, String environmentId, String version, String key) {
		ConfigEntry aux = null;
		Query query = entityManager.createQuery("SELECT entry FROM ConfigEntry entry INNER JOIN entry.configuration config WHERE config.application.id=:applicationId AND config.environment.id=:environmentId AND config.version=:version AND entry.key=:key", ConfigEntry.class)
		.setParameter("applicationId", applicationId)
		.setParameter("environmentId", environmentId)
		.setParameter("version", version)
		.setParameter("key", key);
		if(!query.getResultList().isEmpty()){
			aux = (ConfigEntry) query.getSingleResult();
		}
		return aux;
	}

	public boolean enabledConfigEntry(String applicationId, String environmentId, String version, String key) {
		boolean enable = false;
		ConfigEntry entry = findByAppAndEnvAndVersionAndKey(applicationId, environmentId, version, key);
		if(entry != null){
			enable = entry.getEnabled() != null ? entry.getEnabled() : enable;
		}
		return enable;
	}	
	
	public boolean existConfigEntry(String applicationId, String environmentId, String version, String key){
		boolean exist = false;
		ConfigEntry entry = findByAppAndEnvAndVersionAndKey(applicationId, environmentId, version, key);
		if(entry != null){
			exist = true;
		}
		return exist;
	}
	
	public ConfigDataType findConfigDataType(String id){
		ConfigDataType temp = null;
		Query query = entityManager.createQuery("SELECT entry FROM ConfigDataType entry WHERE entry.id=:id", ConfigDataType.class)
				.setParameter("id", id);
		if(!query.getResultList().isEmpty()){
			temp = (ConfigDataType) query.getSingleResult();
		}
		return temp;
	}
	
	public ConfigType findConfigType(String id){
		ConfigType temp = null;
		Query query = entityManager.createQuery("SELECT entry FROM ConfigType entry WHERE entry.id=:id", ConfigType.class)
				.setParameter("id", id);
		if(!query.getResultList().isEmpty()){
			temp = (ConfigType) query.getSingleResult();
		}
		return temp;
	}
	
	public boolean isSingleValueConfig(AdminCacheUpdate update){
		boolean single = false;
		ConfigEntry temp = this.findByAppAndEnvAndVersionAndKey(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), update.getKey());
		if(temp != null){
			single = temp.isSingleValue();
		}
		return single;
	}
	
	public Configuration findConfiguration(String applicationId, String environmentId, String version){
		Configuration temp = null;
		Query query = entityManager.createQuery("SELECT entry FROM Configuration entry WHERE entry.application.id=:applicationId AND entry.environment.id=:environmentId AND entry.version=:version", Configuration.class)
				.setParameter("applicationId", applicationId)
				.setParameter("environmentId", environmentId)
				.setParameter("version", version);
		if(!query.getResultList().isEmpty()){
			temp = (Configuration) query.getSingleResult();
		}
		return temp;
	}
	
	public boolean createConfigEntry(AdminCacheInsert entry) throws Exception{
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		ConfigDataType datype = this.findConfigDataType(entry.getConfigEntry().getDataType());
		ConfigType conty = this.findConfigType(entry.getConfigEntry().getType());
		Configuration con = this.findConfiguration(entry.getApplicationId(), entry.getEnvironmentIdp(), entry.getVersion());
		List<ConfigEntryValue> lstvalues = new ArrayList<ConfigEntryValue>(entry.getConfigEntry().getValues().size());
		ConfigEntryValue entryValue = null;
		try{
			if(datype != null && conty != null && con != null){
				ConfigEntry aux = new ConfigEntry();
				aux.setConfigDataType(datype);
				//aux.setId(this.idMaxPlusOne());
				aux.setConfigType(conty);
				aux.setConfiguration(con);
				aux.setCreatedDate(new Date());
				aux.setDescription(entry.getDescription());
				aux.setEnabled(true);
				aux.setKey(entry.getConfigEntry().getKey());
				aux.setModifiedDate(null);
				aux.setName(entry.getConfigEntry().getKey());
				aux.setSingleValue(entry.getConfigEntry().isSingleValue());
				aux.setValue(entry.getConfigEntry().getValue());
				if(!entry.getConfigEntry().isSingleValue()){
					for(String temp : entry.getConfigEntry().getValues()){
						entryValue = new ConfigEntryValue();
						entryValue.setEnabled(true);
						entryValue.setValue(temp);
						entryValue.setConfigEntry(aux);
						lstvalues.add(entryValue);
					}
				}
				aux.setConfigEntryValues(lstvalues);
				entityManager.persist(aux);
				entityManager.getTransaction().commit();
				return true;
			}else{
				if(datype == null) throw new Exception("dataType: " + entry.getConfigEntry().getDataType() + ", not found");
				if(conty == null) throw new Exception("type: " + entry.getConfigEntry().getType() + ", not found");
				if(con == null) throw new Exception("applicationId: " + entry.getApplicationId() + ", enviroment: " + entry.getEnvironmentIdp() + ", version: " + entry.getVersion() + ", not found");
				return false;
			}
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
            }
			throw e;
		}
	}
	
	public boolean updateConfigEntry(AdminCacheUpdate update){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			ConfigEntry entry = this.findByAppAndEnvAndVersionAndKey(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), update.getKey());
			ConfigEntry temp = entityManager.find(ConfigEntry.class, entry.getId());
			if(!temp.isSingleValue()){
				Iterator<String> iterator = update.getConfigEntryValues().iterator();
				String values = "";
				ConfigEntryValue entryValue = null;
				boolean existVal = false;
				while(iterator.hasNext()){
				  values = (String) iterator.next();				 
				  for(ConfigEntryValue val : temp.getConfigEntryValues()){
					  if(val != null && val.getValue() != null && val.getValue().equals(values)){
						  existVal = true;
					  }
				  }
				  if(!existVal){
					  entryValue = new ConfigEntryValue();
					  entryValue.setEnabled(true);
					  entryValue.setValue(values);
					  entryValue.setConfigEntry(temp);
					  temp.getConfigEntryValues().add(entryValue);
				  }
				  existVal = false;
				}
				if(update.getValue() != null && !update.getValue().isEmpty()) temp.setValue(update.getValue());
			}else{
				temp.setValue(update.getValue());
			}
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
	        }
			throw e;
		}
	}
	
	public boolean deleteConfigEntry(AdminCacheDelete update){
		if (!entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().begin();
		}
		try{
			ConfigEntry entry = this.findByAppAndEnvAndVersionAndKey(update.getApplicationId(), update.getEnvironmentIdp(), update.getVersion(), update.getKey());
			ConfigEntry temp = entityManager.find(ConfigEntry.class, entry.getId());
			entityManager.remove(temp);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logger.error(this.getClass().getName(), e.fillInStackTrace());
			if(entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
	        }
			throw e;
		}
	}
	
	public void clear(){
		entityManager.clear();
	}
	
	public Long idMaxPlusOne() {
		Long aux = 1L;
		Query query = entityManager.createQuery("SELECT MAX(entry.id) + 1 FROM ConfigEntry entry", Long.class);
		if(!query.getResultList().isEmpty()){
			aux = (Long) query.getSingleResult();
		}
		return aux;
	}
}