package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: ConfigType</h1>
 * 
 * @description This entity models the types of configuration. its primary key
 *              is a Long type id.
 * 
 *              ConfigType has <b>@OneToMany</b> relationship with ConfigEntry
 * 
 **/
@Entity
public class ConfigType extends StringIdBaseEntity {

	private static final long serialVersionUID = -98708481659846764L;

	public ConfigType() {
		super();
	}

	public ConfigType(String id) {
		super(id);
	}

	public ConfigType(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigType [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}
