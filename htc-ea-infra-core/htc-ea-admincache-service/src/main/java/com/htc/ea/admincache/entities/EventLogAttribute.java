package com.htc.ea.admincache.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "eventLogId",
		"name" }))
public class EventLogAttribute implements Serializable {

	private static final long serialVersionUID = -3965283718889959254L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "EventLogAttributeGenerator")
	@SequenceGenerator(name = "EventLogAttributeGenerator", sequenceName = "EventLogAttributeSeq")
	private Long id;

	@Column(nullable = false, length = 50)
	private String name;

	@Column(nullable = false, length = 255)
	private String value;

	public EventLogAttribute() {
	}

	public EventLogAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
