package com.htc.ea.admincache.entities;

import javax.persistence.Entity;

/**
 * <h1>Project: Model</h1> <h1>entity: EnvironmentType</h1>
 * 
 * @description This entity models the types of environments in an application
 *              starts. its primary key is a String type id.
 * 
 *              EnvironmentType has <b>@ManyToOne</b> relationship with
 *              Environment
 * 
 **/
@Entity
public class EnvironmentType extends StringIdBaseEntity {

	private static final long serialVersionUID = -1249777660582368867L;

	public EnvironmentType() {
	}

	public EnvironmentType(String id) {
		super(id);
	}

	public EnvironmentType(String id, String name, String description,
			Boolean enabled) {
		super(id, name, description, enabled);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EnvironmentType [id=").append(id).append(", name=")
				.append(name).append(", description=").append(description)
				.append(", enabled=").append(enabled).append(", createdDate=")
				.append(createdDate).append(", modifiedDate=")
				.append(modifiedDate).append("]");
		return builder.toString();
	}
	
}