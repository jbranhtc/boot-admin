package com.htc.ea.admincache.entities;


/**
 * @Description:
 * Entidad que modela detalle de movimientos de servicios utilizados
 * 
 */


public class ServiceDetail extends GenericBaseEntity<Long> {
	private static final long serialVersionUID = 1L;
	
	/*@Id
	@GeneratedValue(strategy=GenerationType.AUTO,  generator="ServiceDetailGenerator")
	@SequenceGenerator(name="ServiceDetailGenerator", sequenceName="ServiceDetailSeq")
	private Long id;
	
	private String value;
	
	@JoinColumn(name = "serviceId")
	@ManyToOne
	private Service service;

	@JoinColumn(name = "serviceGroupId")
	@ManyToOne
	private ServiceGroup serviceGroup;	
	
	public ServiceGroup getServiceGroup() {
		return serviceGroup;
	}

	public void setServiceGroup(ServiceGroup serviceGroup) {
		this.serviceGroup = serviceGroup;
	}

	@JoinColumn(name = "countryId")
	@ManyToOne
	private Country country;

	@JoinColumn(name = "environmentId")
	@ManyToOne
	private Environment environment;	
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Environment getEnvironment() {
		return environment;
	}

	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	
	public ServiceDetail() {

	}	
	
	public ServiceDetail(Long id) {
		super();
		this.id = id;
	}	
	
	public ServiceDetail(String name, String description,Boolean enabled) {
		super();
		this.name = name;
		this.description = description;
		this.enabled = enabled;
	}
	
    @Override
    public String toString() {
        return String.format(
                "ServiceDetail[id='%d', name'%s',description='%s',enabled='%b',createdDate='%d',modifiedDate='%d']",
                id, name,description,enabled,createdDate,modifiedDate);
    }*/
}
