package com.htc.ea.util.log;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.htc.ea.util.log.EventData.Builder;

public class EventData implements Serializable {
	
	private static final long serialVersionUID = -5444215858246596546L;
	
	private static String HOST_ADDRESS;
	public static final String DEFAULT_LEVEL = EventLevel.INFO.toString();
	public static final String DEFAULT_CATEGORY = "general";
	public static final String DEFAULT_END_USER = "none";
	public static final String DEFAULT_LOCATION = "unknownHost";
	public static final String DEFAULT_DATA_TYPE = "json";
	private String applicationId;
	private String environmentId;
	private String version;
	private String level = DEFAULT_LEVEL;
	private String category = DEFAULT_CATEGORY;
	private final Date creationDate = new Date();
	private String serverLocation = HOST_ADDRESS;
	private String endUserLocation = HOST_ADDRESS;
	private String endUser = DEFAULT_END_USER;
	private String source;
	private String name = "none";
	private String message;
	private String detail;
	private String exception;
	private String referenceId;
	private String originReferenceId;
	private String targetReferenceId;
	private String responseCode;
	private Long duration;
	private Boolean automatic = false;
	private Boolean successful = null;
	private Boolean compensation = false;
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private Object data;
	private String dataType = DEFAULT_DATA_TYPE;
	private String sequence;
	
	static {
		try {
			HOST_ADDRESS = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			HOST_ADDRESS = DEFAULT_LOCATION;
		}
	}

	public EventData() {
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getEnvironmentId() {
		return environmentId;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}


	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getServerLocation() {
		return serverLocation;
	}

	public void setServerLocation(String serverLocation) {
		this.serverLocation = serverLocation;
	}

	public String getEndUserLocation() {
		return endUserLocation;
	}

	public void setEndUserLocation(String endUserLocation) {
		this.endUserLocation = endUserLocation;
	}

	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String user) {
		this.endUser = user;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getException() {
		return exception;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
	
	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getOriginReferenceId() {
		return originReferenceId;
	}

	public void setOriginReferenceId(String originReferenceId) {
		this.originReferenceId = originReferenceId;
	}

	public String getTargetReferenceId() {
		return targetReferenceId;
	}

	public void setTargetReferenceId(String targetReferenceId) {
		this.targetReferenceId = targetReferenceId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Boolean getAutomatic() {
		return automatic;
	}

	public void setAutomatic(Boolean automatic) {
		this.automatic = automatic;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}
	
	public Boolean getCompensation() {
		return compensation;
	}

	public void setCompensation(Boolean compensation) {
		this.compensation = compensation;
	}
	
	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	@Override
	public String toString() {
		StringBuilder builder2 = new StringBuilder();
		builder2.append("EventData [applicationId=").append(applicationId).append(", environmentId=")
				.append(environmentId).append(", version=").append(version).append(", level=").append(level)
				.append(", category=").append(category).append(", creationDate=").append(creationDate)
				.append(", serverLocation=").append(serverLocation).append(", endUserLocation=").append(endUserLocation)
				.append(", endUser=").append(endUser).append(", source=").append(source).append(", name=").append(name)
				.append(", message=").append(message).append(", detail=").append(detail).append(", exception=")
				.append(exception).append(", referenceId=").append(referenceId).append(", originReferenceId=")
				.append(originReferenceId).append(", targetReferenceId=").append(targetReferenceId)
				.append(", responseCode=").append(responseCode).append(", duration=").append(duration)
				.append(", automatic=").append(automatic).append(", successful=").append(successful)
				.append(", compensation=").append(compensation).append(", attributes=").append(attributes)
				.append(", data=").append(data).append(", dataType=").append(dataType).append(", sequence=").append(sequence)
				.append("]");
		return builder2.toString();
	}

	public static Builder builder() {
		return new Builder();
	}	

	public static class Builder {
		private EventData instance = new EventData();

		public Builder() {
		}

		public Builder applicationId(String applicationId) {
			instance.applicationId = applicationId;
			return this;
		}

		public Builder environmentId(String environmentId) {
			instance.environmentId = environmentId;
			return this;
		}

		public Builder version(String version) {
			instance.version = version;
			return this;
		}

		public Builder level(String level) {
			instance.level = level;
			return this;
		}

//		public Builder level(EventLevel level) {
//			instance.level = level.getValue();
//			return this;
//		}

		public Builder category(String category) {
			instance.category = category;
			return this;
		}

		public Builder serverLocation(String serverLocation) {
			instance.serverLocation = serverLocation;
			return this;
		}

		public Builder endUserLocation(String endUserLocation) {
			instance.endUserLocation = endUserLocation;
			return this;
		}

		public Builder endUser(String endUser) {
			instance.endUser = endUser;
			return this;
		}

		public Builder source(String source) {
			instance.source = source;
			return this;
		}

		public Builder source(Class<?> source) {
			instance.source = source.getName();
			return this;
		}

		public Builder name(String name) {
			instance.name = name;
			return this;
		}

		public Builder message(String message) {
			instance.message = message;
			return this;
		}
		
		public Builder detail(String detail) {
			instance.detail = detail;
			return this;
		}

		public Builder exception(String exception) {
			instance.exception = exception;
			return this;
		}

		public Builder exception(Throwable exception) {
			StringWriter writer = new StringWriter();
			exception.printStackTrace(new PrintWriter(writer));
			instance.exception = writer.toString();
			return this;
		}
		
		public Builder referenceId(String referenceId) {
			instance.referenceId = referenceId;
			return this;
		}

		public Builder originReferenceId(String originReferenceId) {
			instance.originReferenceId = originReferenceId;
			return this;
		}

		public Builder targetReferenceId(String targetReferenceId) {
			instance.targetReferenceId = targetReferenceId;
			return this;
		}

		public Builder responseCode(String responseCode) {
			instance.responseCode = responseCode;
			return this;
		}
		
		public Builder duration(Long duration) {
			instance.duration = duration;
			return this;
		}

		public Builder automatic(Boolean automatic) {
			instance.automatic = automatic;
			return this;
		}

		public Builder successful(Boolean successful) {
			instance.successful = successful;
			return this;
		}
		
		public Builder compensation(Boolean compensation) {
			instance.compensation = compensation;
			return this;
		}
		
		
		public Builder data(Object data) {
			instance.data = data;
			return this;
		}
		
		public Builder dataType(String dataType) {
			instance.dataType = dataType;
			return this;
		}
		
		public Builder sequence(String sequence) {
			instance.sequence = sequence;
			return this;
		}
		
		public Builder addAttributes(Map<String, Object> attributes){
			instance.attributes = attributes;
			return this;
		}
		
		public EventData build() {
			return instance;
		}
	}	
}