package com.htc.ea.logger.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.logger.model.EventLogs;
import com.htc.ea.logger.model.EventLogKey;
import com.htc.ea.logger.repository.EventLogRepository;
import com.htc.ea.logger.util.parsing.ParsingUtil;
import com.htc.ea.util.log.EventData;

@Component
public class CassandraLoggingService implements LoggingService {

	private EventLogRepository eventLogRepository;
	private ObjectMapper objectMapper;
	private ParsingUtil parsingUtil;
	
	@Autowired
	public CassandraLoggingService(EventLogRepository eventLogRepository, ObjectMapper objectMapper,
			ParsingUtil parsingUtil) {
		this.eventLogRepository = eventLogRepository;
		this.objectMapper = objectMapper;
		this.parsingUtil = parsingUtil;
	}

	@Override
	public void log(EventData eventData) {
		if(eventData.getEndUser() == null)
			eventData.setEndUser("none");
		try {
			EventLogs eventLog = new EventLogs();
			EventLogKey eventLogKey = new EventLogKey();

//			eventLogKey.setId(UUIDs.timeBased());
			eventLog.setApplication(eventData.getApplicationId());

			eventLog.setAutomatic(eventData.getAutomatic());
			eventLog.setCompensation(eventData.getCompensation());
			eventLogKey.setCreatedDate(eventData.getCreationDate());
			eventLog.setDetail(eventData.getDetail());
			eventLog.setDuration(eventData.getDuration());
			eventLogKey.setEndUser(eventData.getEndUser());
			eventLog.setEndUserLocation(eventData.getEndUserLocation());

			eventLog.setEnvironment(eventData.getEnvironmentId());
			eventLog.setEventCategory(eventData.getCategory());

			eventLog.setException(eventData.getException());
			eventLog.setLevel(eventData.getLevel());
			eventLog.setMessage(eventData.getMessage());
			eventLog.setEventName(eventData.getName());
			eventLogKey.setOriginReferenceId(eventData.getOriginReferenceId());
			eventLog.setProcessDate(new Date());
			eventLog.setResponseCode(eventData.getResponseCode());
			eventLog.setServerLocation(eventData.getServerLocation());
			eventLog.setEventSource(eventData.getSource());
			eventLog.setSuccesful(eventData.getSuccessful());
			eventLog.setVersion(eventData.getVersion());

			eventLog.setSequence(eventData.getSequence());

			if (eventData.getAttributes() != null && eventData.getAttributes().size() > 0) {
				eventLog.setMetadata(objectMapper.writeValueAsString(eventData.getAttributes()));
			}

			eventLog.setId(eventLogKey);
			String detail = "";

			if (eventData.getData() != null) {
				try {
					if (eventData.getDataType().equals("json")) {
						detail = parsingUtil.convertObjectToJsonPretty(eventData.getData());
					} else{
						detail = parsingUtil.marshal2String(eventData.getData());
						
						detail = parsingUtil.changeXmlParentNode(detail, eventData.getDetail());
						
					}
				} catch (Exception e) {
					detail = e.getMessage();
				}

				eventLog.setDetail(detail);
			}

			eventLogRepository.save(eventLog);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

}
