package com.htc.ea.logger.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.cassandra.core.PrimaryKeyType;
import org.springframework.data.cassandra.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class EventLogKey implements Serializable {
	
	private static final long serialVersionUID = 1L;

//	@PrimaryKeyColumn(name="id",ordinal = 0,type = PrimaryKeyType.PARTITIONED)
//	private UUID id;

	@PrimaryKeyColumn(name="createdDate",ordinal = 1, type = PrimaryKeyType.CLUSTERED)
	private Date createdDate;
	
	@PrimaryKeyColumn(name="endUser",ordinal = 2,type = PrimaryKeyType.CLUSTERED)
	private String endUser;
	
	@PrimaryKeyColumn(name="originReferenceId",ordinal = 3, type = PrimaryKeyType.PARTITIONED)
	private String originReferenceId;
	
	

//	public UUID getId() {
//		return id;
//	}

//	public void setId(UUID id) {
//		this.id = id;
//	}

	public String getEndUser() {
		return endUser;
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getOriginReferenceId() {
		return originReferenceId;
	}

	public void setOriginReferenceId(String originReferenceId) {
		this.originReferenceId = originReferenceId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}