package com.htc.ea.logger.repository;

import org.springframework.data.repository.CrudRepository;

import com.htc.ea.logger.model.EventLogs;

public interface EventLogRepository extends CrudRepository<EventLogs, String> {

}
