package com.htc.ea.logger.service;

import com.htc.ea.util.log.EventData;

public abstract interface LoggingService {
	public abstract void log(EventData paramEventData);
}
