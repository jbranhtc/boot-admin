package com.htc.ea.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.util.log.EventData;

@SpringBootApplication
@EnableRabbit
@EnableScheduling
public class HtcEaCassandraLoggerJmsApplication implements RabbitListenerConfigurer {
	private static final Logger logger = LoggerFactory.getLogger(HtcEaCassandraLoggerJmsApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(HtcEaCassandraLoggerJmsApplication.class, args);
		logger.info("Consuming logging messages...");
	}
	
	@Bean
	public ObjectMapper objectMapper(){
		return new ObjectMapper();
	}

	@Bean
	public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		Jackson2JsonMessageConverter jackson = new Jackson2JsonMessageConverter();
		
		DefaultClassMapper mapper = new DefaultClassMapper();
		mapper.setDefaultType(EventData.class);
		
		jackson.setClassMapper(mapper);
		
		return jackson;
	}

	@Bean
	public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
		MappingJackson2MessageConverter mapping = new MappingJackson2MessageConverter();
		return mapping; 
	}

	@Bean
	public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
		DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
		factory.setMessageConverter(consumerJackson2MessageConverter());
		return factory;
	}

	@Override
	public void configureRabbitListeners(final RabbitListenerEndpointRegistrar registrar) {
		registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
	}
}
