package com.htc.ea.logger.receiver;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.ea.logger.service.CassandraLoggingService;
import com.htc.ea.util.log.EventData;

@Service
public class CassandraReciver {

	@Autowired
	private CassandraLoggingService loggingService;

	@RabbitListener(queues = "${rabbit.queue}")
	public void receiveMessage(final EventData eventData) {
		try {
			loggingService.log(eventData);
		} catch (Exception e) {
			System.out.println("Error processing rabbitMQ message;");
			e.printStackTrace();
		}
	}
}
