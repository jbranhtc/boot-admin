package com.htc.ea.logger.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

@Table
public class EventLogs implements Serializable {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	private EventLogKey id;
	
	private String application;	
	private String environment;	
	private String eventCategory;	
	private String detail;	
	private String exception;
	private String version;
	private String level;
	private Date processDate;
	private String serverLocation;
	private String endUserLocation;
	private String eventSource;
	private String eventName;
	private String message;
	private String responseCode;
	private Boolean automatic;
	private Boolean succesful;
	private Boolean compensation;
	private Long duration;
	private String metadata;
	private String sequence;
	
	public EventLogKey getId() {
		return id;
	}
	public void setId(EventLogKey id) {
		this.id = id;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	public String getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public String getServerLocation() {
		return serverLocation;
	}
	public void setServerLocation(String serverLocation) {
		this.serverLocation = serverLocation;
	}
	public String getEndUserLocation() {
		return endUserLocation;
	}
	public void setEndUserLocation(String endUserLocation) {
		this.endUserLocation = endUserLocation;
	}
	public String getEventSource() {
		return eventSource;
	}
	public void setEventSource(String eventSource) {
		this.eventSource = eventSource;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public Boolean getAutomatic() {
		return automatic;
	}
	public void setAutomatic(Boolean automatic) {
		this.automatic = automatic;
	}
	public Boolean getSuccesful() {
		return succesful;
	}
	public void setSuccesful(Boolean succesful) {
		this.succesful = succesful;
	}
	public Boolean getCompensation() {
		return compensation;
	}
	public void setCompensation(Boolean compensation) {
		this.compensation = compensation;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
//		builder.append("EventLog [id=");
//		builder.append(id);
		builder.append(", application=");
		builder.append(application);
		builder.append(", environment=");
		builder.append(environment);
		builder.append(", eventCategory=");
		builder.append(eventCategory);
		builder.append(", detail=");
		builder.append(detail);
		builder.append(", exception=");
		builder.append(exception);
		builder.append(", version=");
		builder.append(version);
		builder.append(", level=");
		builder.append(level);
		builder.append(", createdDate=");
		builder.append(", processDate=");
		builder.append(processDate);
		builder.append(", serverLocation=");
		builder.append(serverLocation);
		builder.append(", endUserLocation=");
		builder.append(endUserLocation);
		builder.append(", endUser=");
		builder.append(", eventSource=");
		builder.append(eventSource);
		builder.append(", eventName=");
		builder.append(eventName);
		builder.append(", message=");
		builder.append(message);
		builder.append(", referenceId=");
		builder.append(", originReferenceId=");
		builder.append(", targetReferenceId=");
		builder.append(", responseCode=");
		builder.append(responseCode);
		builder.append(", automatic=");
		builder.append(automatic);
		builder.append(", succesful=");
		builder.append(succesful);
		builder.append(", compensation=");
		builder.append(compensation);
		builder.append(", duration=");
		builder.append(duration);
		builder.append("]");
		return builder.toString();
	}
}
